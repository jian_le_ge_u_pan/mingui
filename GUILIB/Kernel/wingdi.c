/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:			wingdi.c
*  PROGRAMMER:			ming.c
*  Date of Creation:	2006/08/8
* 
*  DESCRIPTION: 								
*     GDI				
*  NOTE:						 		
*    本单元的设计优劣直接决定了整个GUI的显示速度，因此本单元的设计宗旨为：
*        追求执行速度为主，尽可能适当的以牺牲代码长度来换取执行速度
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
* 
*  MODIFICATION HISTORY
*     LastModify  2006/10/30
******************************************************************************/
//---------------------------------------------------------------------------
#include "mingui.h"
//---------------------------------------------------------------------------
#define  JPEG_SUPPORT
//---------------------------------------------------------------------------
#if BITS_PER_PIXEL <= 8
  typedef	unsigned char   PIXEL;		 /* 像素存储单位BYTE */
#elif BITS_PER_PIXEL == 16
  typedef	unsigned short	PIXEL;	     /* 像素存储单位WORD */
#else
  typedef	unsigned long	PIXEL;		 /* 像素存储单位DWORD*/
#endif
/* PIXEL data type is only available for GDI*/
//---------------------------------------------------------------------------
typedef struct 
{   long	 width;		/* image width in pixels*/
	long	 height;		/* image height in pixels*/
	long	 bpp;		/* bits per pixel (1、2、 4 、8、15、16、24、32)*/
	long 	 pitch;		/* bytes per line*/
	long	 palsize; 	/* palette size*/
    PIXEL   *transPal;
    BYTE    *bits;
    BOOL     bitsFromHeap;
}TImageHead;
//---------------------------------------------------------------------------
#define BI_RGB          0
#define BI_RLE8         1
#define BI_RLE4         2
#define BI_BITFIELDS    3
//---------------------------------------------------------------------------
typedef struct BITMAPFILEHEADER
{  WORD   bfType;  /*'BM',BMP图象的标志*/
   DWORD  bfSize;    /*文件大小*/
   WORD   bfReserved1;
   WORD   bfReserved2;
   DWORD  bfOffBits; /*文件头大小: 文件头结构字节数＋图象头结构字节数＋调色板字节数*/
} BMPFILEHEADER;

/* windows style*/
typedef struct
{	/* BITMAPINFOHEADER*/
	DWORD	BiSize;       /*图象头结构字节数*/
	long	BiWidth;      /*图象宽度（象素）*/
	long	BiHeight;     /*图象高度（象素）*/
	WORD	BiPlanes;
	WORD	BiBitCount;
	DWORD	BiCompression; /*压缩方式*/
	DWORD	BiSizeImage;
	long	BiXpelsPerMeter;
	long	BiYpelsPerMeter;
	DWORD	BiClrUsed;    /*使用的颜色数*/
	DWORD	BiClrImportant;
}BMPINFOHEADER;
/* os/2 style*/
typedef struct 
{	/* BITMAPCOREHEADER*/
	DWORD	bcSize;
	short	bcWidth;
	short	bcHeight;
	WORD	bcPlanes;
	WORD	bcBitCount;
} BMPCOREHEADER;
//---------------------------------------------------------------------------

/*-----------------------------------------------------------------------------
类型
    TLcdImageHead
描述
   由共享软件Image2Lcd生成的图形数组的图像头结构
定义
  scan: 扫描模式
    Bit7: 0:自左至右扫描，1:自右至左扫描。
    Bit6: 0:自顶至底扫描，1:自底至顶扫描。
    Bit5: 0:字节内象素数据从高位到低位排列，1:字节内象素数据从低位到高位排列。
    Bit4: 0:WORD类型高低位字节顺序与PC相同，1:WORD类型高低位字节顺序与PC相反。
    Bit3~2: 保留。
    Bit1~0: [00]水平扫描，[01]垂直扫描，[10]数据水平,字节垂直，[11]数据垂直,字节水平。
  bpp: (bit per pixel)
    1:单色，2:四灰，4:十六灰，8:256色，12:4096色，16:16位彩色，24:24位彩色，32:32位彩色。
  width:  图像的宽度
  height: 图像的高度
---------------------------------------------------------------------------*/
typedef struct
{  unsigned char  scan;
   unsigned char  bpp;
   unsigned char  width[2];
   unsigned char  height[2];
}TLcdImageHead;
/*--------------------------------------------------------------------------
类型
    TTrueColorInfo
描述
   真彩色信息
   仅在“4096色/16位真彩色/18位真彩色/24位真彩色/32位真彩色”下才有。
   真彩色图像数据中紧跟在TImageHeadInfo结构后面
定义
   is565:
     在4096色模式下为0表示使用[16bits(WORD)]格式，此时图像数据中每个WORD表示一个象素；为1表示使用[12bits(连续字节流)]格式，此时连续排列的每12Bits代表一个象素。
     在16位彩色模式下为0表示R G B颜色分量所占用的位数都为5Bits，为1表示R G B颜色分量所占用的位数分别为5Bits,6Bits,5Bits。
     在18位彩色模式下为0表示"6Bits in Low Byte"，为1表示"6Bits in High Byte"。
     在24位彩色和32位彩色模式下is565无效。
   rgb:
     描述R G B颜色分量的排列顺序，rgb中每2Bits表示一种颜色分量，[00]表示空白，[01]表示Red，[10]表示Green，[11]表示Blue。
----------------------------------------------------------------------------*/
typedef struct
{  unsigned char  is565;
   unsigned char  rgb;
}TTrueColorInfo;
//---------------------------------------------------------------------------
extern PWND  g_RootWnd;
extern VRAM  *g_Lcdvram;
extern TWndCanvas *g_RootCanvas;
extern void  VRAMDisplay(VRAM *Vram, int x, int y, int w, int h); 
static int  CM_DecodeBMP(TImageHead *imgHead, STREAM *stream);
static int  CM_DecodeLCDIMG(TImageHead *imgHead, const BYTE *pImage);
static int  CM_DecodeRLE4(BYTE *buf, STREAM *stream);
static int  CM_DecodeRLE8(BYTE *buf,STREAM *stream);
static int  CM_ExpandBitmap(TBitmap *bitmap,TImageHead *imgHead);
static int  CM_DecodeJPG(TImageHead *imgHead, STREAM *stream);
static void cmPixelOpt( PIXEL *desPixelPos, int desBitOffset, PIXEL srcValue, DWORD dwRop);
static void cmHorLineOpt(PIXEL *desAddr, int desOffset, PIXEL *srcAddr, int srcOffset, int width, DWORD dwRop);
static void cmHorLineXor(PIXEL *desAddr, int desOffset, PIXEL *srcAddr, int srcOffset, int width);
static void cmHorLineFilterOpt(PIXEL *desAddr, int desOffset, PIXEL *srcAddr, int srcOffset, int width,PIXEL filterPixel,DWORD dwRop);
static void cmHorLineFilterCopy(PIXEL *desAddr, int desOffset, PIXEL *srcAddr, int srcOffset, int width,PIXEL filterPixel);
static void cmHorLineXorFilter(PIXEL *desAddr, int desOffset, PIXEL *srcAddr, int srcOffset, int width,PIXEL filterPixel);
static void CM_ScaleBoxOpt(TWndCanvas *desCanvas, int nXOriginDest,int nYOriginDest,int nWidthDest,int nHeightDest,TWndCanvas *srcCanvas, int nXOriginSrc,int nYOriginSrc,int nWidthSrc,int nHeightSrc,DWORD dwRop);
static void CM_ScaleBoxCopy(TWndCanvas *desCanvas, int nXOriginDest,int nYOriginDest,int nWidthDest,int nHeightDest,TWndCanvas *srcCanvas, int nXOriginSrc,int nYOriginSrc,int nWidthSrc,int nHeightSrc);
static void CM_ScaleBoxFilterCopy(TWndCanvas *desCanvas,int nXOriginDest,int nYOriginDest,int nWidthDest,int nHeightDest,TWndCanvas *srcCanvas, int nXOriginSrc,int nYOriginSrc,int nWidthSrc,int nHeightSrc);
static void CM_ScaleBoxFilterOpt(TWndCanvas *desCanvas,int nXOriginDest,int nYOriginDest,int nWidthDest,int nHeightDest,TWndCanvas *srcCanvas, int nXOriginSrc,int nYOriginSrc,int nWidthSrc,int nHeightSrc,DWORD dwRop);
/////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------
//GetNextLine 获取指定行的下一行的相同列像素的物理地址
#define GetNextLinePos(pixelPos,vram) ( (pixelPos)+( vram )->UnitPerLine )
//---------------------------------------------------------------------------
//GetNextPixelPosition 获取指定像素的下一个像素的物理地址
#define GetNextPixelPosition(NextPixelPos, NewBitOffset, PixelPos, BitOffset )\
{ if( (BitOffset)<PIXEL_UNIT-BITS_PER_PIXEL   ) \
  { *(NextPixelPos) = PixelPos; *(NewBitOffset) = (BitOffset) + BITS_PER_PIXEL;}\
  else { *(NextPixelPos) = (PixelPos) +1; *(NewBitOffset) =0;}\
}
//---------------------------------------------------------------------------
#define GetPrevPixelPosition(NextPixelPos, NewBitOffset, PixelPos, BitOffset )\
{ if( BITS_PER_PIXEL<=(BitOffset) ) \
  { *(NextPixelPos) = PixelPos; *(NewBitOffset) = (BitOffset) - BITS_PER_PIXEL;}\
  else { *(NextPixelPos) = (PixelPos) -1; *(NewBitOffset) = PIXEL_UNIT- BITS_PER_PIXEL;}\
}
//---------------------------------------------------------------------------
#define GetOffsetPixelPosition(NextPixelPos, NewBitOffset, PixelPos, BitOffset,OffsetX)\
{ int temp_new_bitoffset=(BitOffset)+((OffsetX)<<LOG_PIXEL_BITS);\
  if(temp_new_bitoffset>=8)\
  { *(NextPixelPos) = (PixelPos)+(temp_new_bitoffset>>3);\
    temp_new_bitoffset&=0x7;\
  }\
  else\
  { *(NextPixelPos) = PixelPos;\
  }\
  *(NewBitOffset)=temp_new_bitoffset;\
} 

//---------------------------------------------------------------------------
#if (PIXEL_BIG_ENDIAN==true)
   #define MASK_OFFSET_OPT(bitwidth,bitoffset)         ( ((1<<(bitwidth))-1) << ( PIXEL_UNIT - (bitoffset) - (bitwidth) ) )
   #define PIXEL_OFFSET_OPT(value,bitwidth,bitoffset)  value <<= ( PIXEL_UNIT - (bitoffset) - (bitwidth) )
   #define OPT_OFFSET_PIXEL(value,bitwidth,bitoffset)  ( value << ( PIXEL_UNIT - (bitoffset) - (bitwidth) ) )
   #define PATTERN_SHIFT_LEFT(pattern,bitoffset)       ( (pattern)<<(bitoffset) )
   #define PATTERN_SHIFT_RIGHT(pattern,bitoffset)      ( (pattern)>>(bitoffset) )
   #define UNIT_GET_BITS(unitwidth,value,bitwidth,bitoffset)   ( ((value)>>((unitwidth)-(bitwidth)-(bitoffset)))& BITS_MASK(bitwidth) )
#else
   #define MASK_OFFSET_OPT(bitwidth,bitoffset)         ( ((1<<(bitwidth))-1) << (bitoffset) )
   #define PIXEL_OFFSET_OPT(value,bitwidth,bitoffset)  value <<= (bitoffset)
   #define OPT_OFFSET_PIXEL(value,bitwidth,bitoffset)  ( value << (bitoffset) )
   #define PATTERN_SHIFT_LEFT(pattern,bitoffset)       ( (pattern)>>(bitoffset) )
   #define PATTERN_SHIFT_RIGHT(pattern,bitoffset)      ( (pattern)<<(bitoffset) )
   #define UNIT_GET_BITS(unitwidth,value,bitwidth,bitoffset)    ( ((value)>>(bitoffset))&BITS_MASK(bitwidth) )
#endif
//---------------------------------------------------------------------------
#define SET_PIXEL_LOGIC( TargetPos, OriginPixel, NewPixel,dwRop)\
  switch( dwRop )\
  {  case PL_REPLACE: *(TargetPos) = (NewPixel); break;\
     case PL_XOR:     *(TargetPos) = (OriginPixel)^(NewPixel); break;\
     case PL_AND:     *(TargetPos) = (OriginPixel)&(NewPixel); break;\
     case PL_OR:      *(TargetPos) = (OriginPixel)|(NewPixel); break;\
  }
//---------------------------------------------------------------------------
#define PIXEL_SET_LOGIC( destPos, srcPixel,dwRop)\
  switch( dwRop )\
  {  case PL_REPLACE: *(destPos) =  (srcPixel); break;\
     case PL_XOR:     *(destPos) ^= (srcPixel); break;\
     case PL_AND:     *(destPos) &= (srcPixel); break;\
     case PL_OR:      *(destPos) |= (srcPixel); break;\
  }
//---------------------------------------------------------------------------
#define SET_3BYTE_LOGIC( TargetPos,srcByte1,srcByte2,srcByte3,dwRop)  switch( dwRop )\
{  case PL_REPLACE: *(BYTE *)(TargetPos) = (BYTE)(srcByte1);\
                    *((BYTE *)(TargetPos)+1) = (BYTE)(srcByte2);\
 				    *((BYTE *)(TargetPos)+2) = (BYTE)(srcByte3);\
                    break;\
   case PL_XOR:     *(BYTE *)(TargetPos) ^= (BYTE)(srcByte1);\
					*((BYTE *)(TargetPos)+1) ^= (BYTE)(srcByte2);\
					*((BYTE *)(TargetPos)+2) ^= (BYTE)(srcByte3);\
                    break;\
   case PL_AND:     *(BYTE *)(TargetPos)  &= (BYTE)(srcByte1);\
					*((BYTE *)(TargetPos)+1) &= (BYTE)(srcByte2);\
					*((BYTE *)(TargetPos)+2) &= (BYTE)(srcByte3);\
                    break;\
   case PL_OR:      *(BYTE *)(TargetPos) |= (BYTE)(srcByte1);\
					*((BYTE *)(TargetPos)+1) |= (BYTE)(srcByte2);\
					*((BYTE *)(TargetPos)+2) |= (BYTE)(srcByte3);\
                    break;\
}
//---------------------------------------------------------------------------
#if (ON_SIMULATOR)   /* PC Simulator environment */
  #define RefreshSimulatorScreen(x,y,w,h)\
  { extern void InvalidateLcdArea(int,int,int,int);\
    InvalidateLcdArea(x,y,w,h);\
  }
#else
  #define RefreshSimulatorScreen(x,y,w,h)
#endif
//---------------------------------------------------------------------------
#if(LCD_FRAME_BUFFER)
  #define IsFrameBufferCanvas(ACanvas)        ((ACanvas)->Owner==g_RootWnd)
#else
  #define IsFrameBufferCanvas(ACanvas)         false
#endif
//---------------------------------------------------------------------------
#if(LCD_RANGE_CHECK==true)
#define CanvasToLcd(ACanvas,x,y,width,height)\
{  if(IsFrameBufferCanvas(ACanvas))\
   { RefreshSimulatorScreen(x,y,width,height);\
   }\
   else if((width)>0 && (height)>0)\
   { int abs_left,abs_top,abs_right,abs_bottom,abs_width,abs_height;\
	 abs_left=(ACanvas)->Vram->Left+(ACanvas)->VX + (x);\
	 abs_right=abs_left+(width);\
     if(abs_left<0)abs_left=0;\
     if(abs_right>LCD_WIDTH)abs_right=LCD_WIDTH;\
	 abs_width=abs_right-abs_left;\
	 if(abs_width>0)\
	 { abs_top=(ACanvas)->Vram->Top+(ACanvas)->VY + (y);\
 	   abs_bottom=abs_top+(height);\
	   if(abs_top<0)abs_top=0;\
       if(abs_bottom>LCD_HEIGHT)abs_bottom=LCD_HEIGHT;\
	   abs_height=abs_bottom-abs_top;\
       if(abs_height>0)\
	   { VRAMDisplay((ACanvas)->Vram,abs_left-(ACanvas)->Vram->Left,abs_top-(ACanvas)->Vram->Top,abs_width,abs_height);\
         RefreshSimulatorScreen(abs_left,abs_top,abs_width,abs_height);\
	   }\
	 }\
   }\
}
#else
#define CanvasToLcd(ACanvas,x,y,width,height)\
{  if(IsFrameBufferCanvas(ACanvas))\
   { RefreshSimulatorScreen(x,y,width,height);\
   }\
   else\
   { int abs_left,abs_top;\
	 abs_left=(ACanvas)->Vram->Left+(ACanvas)->VX + (x);\
	 abs_top=(ACanvas)->Vram->Top+(ACanvas)->VY + (y);\
     VRAMDisplay((ACanvas)->Vram,abs_left-(ACanvas)->Vram->Left,abs_top-(ACanvas)->Vram->Top,width,height);\
     RefreshSimulatorScreen(abs_left,abs_top,width,height);\
   }\
}
#endif
//---------------------------------------------------------------------------
//绘图边界检查.只要作CANVAS边界溢出检查，至于LCD视域边界检查则放在WriteToLCD中去做
//这时检查的目的是阻止画笔超出VRAM区域
//---------------------------------------------------------------------------
#if (VRAM_RANGE_CHECK==true)
//---------------------------------------------------------------------------
#define CHECK_CANVAS_RECT(ACanvas,x,y,width,height)\
{ if(ACanvas)\
  {  if(x<(ACanvas)->ClipRect.left)\
     { width -= (ACanvas)->ClipRect.left - (x); \
       x=(ACanvas)->ClipRect.left;\
     }\
     if(x+width > (ACanvas)->ClipRect.right)\
     { width = (ACanvas)->ClipRect.right-x;\
     }\
     if(width<=0)return;\
     if(y<(ACanvas)->ClipRect.top)\
     { height -= (ACanvas)->ClipRect.top - (y); \
       y=(ACanvas)->ClipRect.top;\
     }\
     if(y+height > (ACanvas)->ClipRect.bottom)\
     { height=(ACanvas)->ClipRect.bottom-y;\
     }\
     if(height<=0)return;\
   }else return; \
}
//---------------------------------------------------------------------------
#define CHECK_CANVAS_HORLINE(ACanvas,x,y,width)\
    if((ACanvas) && width>0 && y>=(ACanvas)->ClipRect.top && y<(ACanvas)->ClipRect.bottom)\
    { if(x<(ACanvas)->ClipRect.left)\
      { width -= (ACanvas)->ClipRect.left - (x); \
        if(width<=0)return;\
        x=(ACanvas)->ClipRect.left;\
      }\
      if(x+width>(ACanvas)->ClipRect.right)\
      { width=(ACanvas)->ClipRect.right-x;\
        if(width<=0)return;\
      }\
    }else return;
//---------------------------------------------------------------------------
#define CHECK_CANVAS_VERLINE(ACanvas,x,y,height)\
    if((ACanvas) && height>0 && x>=(ACanvas)->ClipRect.left && x<(ACanvas)->ClipRect.right)\
    { if(y<(ACanvas)->ClipRect.top)\
      { height -= (ACanvas)->ClipRect.top - (y); \
        if(height<=0)return;\
        y=(ACanvas)->ClipRect.top;\
      }\
      if(y+height>(ACanvas)->ClipRect.bottom)\
      { height=(ACanvas)->ClipRect.bottom-y;\
        if(height<=0)return;\
      }\
    } else return;
//---------------------------------------------------------------------------
#else
//---------------------------------------------------------------------------
#define CHECK_CANVAS_RECT(ACanvas,x,y,width,height)
#define CHECK_CANVAS_HORLINE(ACanvas,x,y,width)
#define CHECK_CANVAS_VERLINE(ACanvas,x,y,height)
//---------------------------------------------------------------------------
#endif
//---------------------------------------------------------------------------
VRAM *CreateVRAM(int LcdOffsetX,int LcdOffsetY, int width, int height, void *Buffer,BOOL ZeroInitialize)
{ DWORD BytePerLine = ( (width*BITS_PER_PIXEL+31)>>3 )& ~0x3; /*字节总数必须为4的整数位，以与winbmp格式兼容*/
  DWORD UnitPerLine=  BytePerLine / UNIT_BYTE;
  VRAM *vram = (VRAM *)GetMem(sizeof(VRAM));
  if(vram)
  { vram->OwnBuffer=(Buffer==NULL);
    vram->BufferSize = BytePerLine * height;
    if(!Buffer)
	{ Buffer=GetMem(vram->BufferSize);
	  if(!Buffer)
	  { FreeMem(vram);
	    return NULL;
	  } 
	}
    vram->Left = LcdOffsetX;
    vram->Top = LcdOffsetY;
    vram->Width = width;
    vram->Height = height;
    vram->BytePerLine = BytePerLine;
    vram->UnitPerLine = UnitPerLine;
    vram->Buffer=Buffer;

    vram->GroupOperation=0;
    if(ZeroInitialize) memset( vram->Buffer, 0, vram->BufferSize );
  }
  return vram;
}
//---------------------------------------------------------------------------
//FreeVRAM 释放一块指定的vram
void FreeVRAM(VRAM *vram )
{  if(vram)
   { if(vram->OwnBuffer)FreeMem(vram->Buffer);
     FreeMem(vram);
   }
}
/*---------------------------------------------------------------------------
FUNCTION
  ColorMapToPixel
DESCRIPTION
  返回RGB颜色的索引号
  对于不同的LCD控制器，调色板的颜色索引顺序有可能不同，因此必须改写本函数
---------------------------------------------------------------------------*/
int ColorMapToPixel(RGBQUAD *rgbColor) {
 #if (BITS_PER_PIXEL<8) /*灰度屏: 计算亮度*/
   int lightscale =   3 * rgbColor->r  + 6 * rgbColor->g +  rgbColor->b ;
 #endif
 
 #if (BITS_PER_PIXEL==1) 
   return (lightscale>630)?0:1;
 #elif(BITS_PER_PIXEL==2)
   if(lightscale>630) return  (lightscale>1430)?0:1; 
   else  return (lightscale>150)?2:3;
    
 #elif(BITS_PER_PIXEL==4)
   if(lightscale>630)
    { if(lightscale>1430)
      { if(lightscale>1950) return (lightscale>2240)?0:1;
        else return (lightscale>1680)?2:3;
      }
      else
      { if(lightscale>990) return (lightscale>1200)?4:5;
        else return (lightscale>800)?6:7;
      }
    }
   else
    { if(lightscale>150)
      { if(lightscale>350) return (lightscale>480)?8:9;
        else return (lightscale>240)?10:11;
      }
      else
      { if(lightscale>30) return (lightscale>80)?12:13;
        else return (lightscale>0)?14:15;
      }
    }
 #elif(BITS_PER_PIXEL==8)
   int temp,mappixel;
   temp=rgbColor->r >>(8-INDEX_RED_BITWIDTH);
   mappixel =  temp<<(INDEX_GREEN_BITWIDTH+INDEX_BLUE_BITWIDTH);
   temp=rgbColor->g >> (8-INDEX_GREEN_BITWIDTH);
   mappixel |= temp << INDEX_BLUE_BITWIDTH;
   mappixel |=  rgbColor->b >> (8-INDEX_BLUE_BITWIDTH) ;
   return mappixel;
 #elif(BITS_PER_PIXEL==16)
   int mappixel =  ( rgbColor->r << 8) & ~BITS_MASK(INDEX_GREEN_BITWIDTH+INDEX_BLUE_BITWIDTH);
   mappixel |=  ( rgbColor->g << (8-INDEX_RED_BITWIDTH) ) & ~BITS_MASK(INDEX_BLUE_BITWIDTH);
   mappixel |=  rgbColor->b >> (8-INDEX_BLUE_BITWIDTH) ;
   return mappixel;
 #else
   return  *(DWORD *)rgbColor;
 #endif
}

	
void PixelMapToColor(int index,RGBQUAD *rgbColor) { 
  #if(BITS_PER_PIXEL==1)
  { if(index==0)
    { rgbColor->r=0x99;
      rgbColor->g=0xaa;
      rgbColor->b=0x99;
      rgbColor->a=0;
    }
    else
    { rgbColor->r=rgbColor->g=rgbColor->b=rgbColor->a=0;
    }
  }
  #elif(BITS_PER_PIXEL<8)
  { int temp = (1<<BITS_PER_PIXEL) - index;
    if(temp>=2) temp= ( temp * temp * 255 ) >> ( BITS_PER_PIXEL <<1 );
    else temp = 0;
    rgbColor->r=rgbColor->g=rgbColor->b=temp;
    rgbColor->a=0;
  }
  #elif(BITS_PER_PIXEL==8)
  {  int temp;
     temp=index>>(INDEX_GREEN_BITWIDTH+INDEX_BLUE_BITWIDTH);
     rgbColor->r = (BYTE) ( temp<<(8-INDEX_RED_BITWIDTH) );

     temp=index>>INDEX_BLUE_BITWIDTH;
     rgbColor->g = (BYTE) ( temp<<(8-INDEX_GREEN_BITWIDTH) );

     rgbColor->b = (BYTE) ( index<<(8-INDEX_BLUE_BITWIDTH) );
     rgbColor->a = 0;

	 rgbColor->r |= (rgbColor->r>>INDEX_RED_BITWIDTH);
     rgbColor->g |= (rgbColor->g>>INDEX_GREEN_BITWIDTH);
     rgbColor->b |= (rgbColor->b>>INDEX_BLUE_BITWIDTH);

	 rgbColor->r |= (rgbColor->r>>INDEX_RED_BITWIDTH);
     rgbColor->g |= (rgbColor->g>>INDEX_GREEN_BITWIDTH);
     rgbColor->b |= (rgbColor->b>>INDEX_BLUE_BITWIDTH);

  }
  #elif(BITS_PER_PIXEL==16)
  {  int temp;
     temp=index>>(INDEX_GREEN_BITWIDTH+INDEX_BLUE_BITWIDTH);
     rgbColor->r = (BYTE) ( temp<<(8-INDEX_RED_BITWIDTH) );

     temp=index>>INDEX_BLUE_BITWIDTH;
     rgbColor->g = (BYTE) ( temp<<(8-INDEX_GREEN_BITWIDTH) );

     rgbColor->b = (BYTE) ( index<<(8-INDEX_BLUE_BITWIDTH) );
     rgbColor->a = 0;
  }
  #else
  {  *(DWORD *)rgbColor=index; 
  }
  #endif
}
//---------------------------------------------------------------------------
void SetColor(HDC dc, TCOLOR RgbColor)
{   ((TWndCanvas *)dc)->Pen.Foreground = ColorMapToPixel((RGBQUAD *)&RgbColor);
}
//---------------------------------------------------------------------------
void SetBkColor(HDC dc, TCOLOR RgbColor)
{   ((TWndCanvas *)dc)->Pen.Background = ColorMapToPixel((RGBQUAD *)&RgbColor);
}
//---------------------------------------------------------------------------
void SetBkMode(HDC dc, int bkMode)
{  SetFontStyle(dc,FS_OPAQUE,(bkMode==OPAQUE));
}
//-------------------------------------------------------------------------------------
#if(BITS_PER_PIXEL==PIXEL_UNIT) /* 8 16 32 */
  #define Do_VramPixelLocate(PixelPos,BitOffset,SrcVram,X,Y) \
  { *(PIXEL **)(PixelPos)=(PIXEL *)(SrcVram)->Buffer + (Y) * (SrcVram)->UnitPerLine +(X);\
    *(BitOffset) = 0 ;\
  }
#elif(BITS_PER_PIXEL==24) 
  #define Do_VramPixelLocate(PixelPos,BitOffset,SrcVram,X,Y) \
  { *(PIXEL **)(PixelPos)=(PIXEL *)((BYTE *)(SrcVram)->Buffer + (Y) * (SrcVram)->BytePerLine +(X)*3 );\
    *(BitOffset) = 0 ;\
  }
#elif(BITS_PER_PIXEL<8)
  #define Do_VramPixelLocate(PixelPos,BitOffset,SrcVram,X,Y) \
  { DWORD LineOffset = (X) << LOG_PIXEL_BITS;\
    *(PIXEL **)(PixelPos) = (PIXEL *)(SrcVram)->Buffer + (Y) * (SrcVram)->UnitPerLine + ( (X) >> LOG_PIXEL_NUM );\
    *(BitOffset) =  LineOffset & ( PIXEL_UNIT -1 ); \
  }
#endif
//---------------------------------------------------------------------------
//CanvasPixelLocate 获取指定像素的物理地址
void CanvasPixelLocate( PIXEL **PixelPos, int *BitOffset, TWndCanvas *ACanvas, int X, int Y )
{ X+=ACanvas->VX;
  Y+=ACanvas->VY;
  Do_VramPixelLocate(PixelPos,BitOffset,ACanvas->Vram,X,Y);
}
//-------------------------------------------------------------------------------------
void  VramPixelLocate( BYTE **PixelPos, int *BitOffset, VRAM *SrcVram, int X, int Y )
{  Do_VramPixelLocate(PixelPos,BitOffset,SrcVram,X,Y);
}
//-------------------------------------------------------------------------------------
//cmPixelOpt 对指定像素进行逻辑操作（包括按位与、或、异或、复制等）
void cmPixelOpt( PIXEL *desPixelPos, int desBitOffset, PIXEL srcValue, DWORD dwRop)
{  
   #if(BITS_PER_PIXEL<8)
     PIXEL srcMask=(PIXEL)MASK_OFFSET_OPT(BITS_PER_PIXEL,desBitOffset);
     PIXEL_OFFSET_OPT(srcValue,BITS_PER_PIXEL,desBitOffset);
     SET_PIXEL_LOGIC(&srcValue,*desPixelPos,srcValue,dwRop);
     *desPixelPos=(*desPixelPos & ~srcMask)|(srcValue & srcMask);
   #elif(BITS_PER_PIXEL==PIXEL_UNIT)
     SET_PIXEL_LOGIC(desPixelPos,*desPixelPos,srcValue,dwRop);
   #elif(BITS_PER_PIXEL==24)
	 SET_3BYTE_LOGIC( desPixelPos,srcValue,srcValue>>8,srcValue>>16,dwRop);;
   #endif
}

//---------------------------------------------------------------------------
void  CanvasCopy(TWndCanvas *TgtCanvas,int tgtx,int tgty,
                 TWndCanvas *SrcCanvas,int srcx,int srcy,
				 int width,int height)
{ int SrcVramUnitPerLine = SrcCanvas->Vram->UnitPerLine;
  PIXEL  *srcLinePos, *desLinePos;
  int   srcBitOffset, desBitOffset;
  CanvasPixelLocate( &srcLinePos, &srcBitOffset, SrcCanvas, srcx, srcy);
  CanvasPixelLocate( &desLinePos, &desBitOffset, TgtCanvas, tgtx, tgty);

  if( (srcx==0 && tgtx==0 )
     && (width==SrcCanvas->Vram->Width )
	 && (width==TgtCanvas->Vram->Width ))
  {  memcpy(desLinePos,srcLinePos,(DWORD)height * SrcVramUnitPerLine * UNIT_BYTE);
  }
  else
  { int TgtVramUnitPerLine = TgtCanvas->Vram->UnitPerLine;
    int i,j;
    #if(BITS_PER_PIXEL==PIXEL_UNIT) /* 8 16 32 */
    for(j = 0; j < height; j++ )
    { for(i=0;i<width;i++)
      { desLinePos[i]=srcLinePos[i];
      }
      desLinePos += TgtVramUnitPerLine;
      srcLinePos += SrcVramUnitPerLine;
    }
    #elif(BITS_PER_PIXEL==24)
    { BYTE *desPos,*srcPos;
      for(j = 0; j < height; j++ )
      { desPos=(BYTE *)desLinePos;
        srcPos=(BYTE *)srcLinePos;
        for(i=0;i<width;i++)
        { *desPos++ = *srcPos++;
          *desPos++ = *srcPos++;
          *desPos++ = *srcPos++;
        }
        desLinePos+=TgtVramUnitPerLine;
        srcLinePos+=SrcVramUnitPerLine;
      }
    }
    #elif(BITS_PER_PIXEL<8)
    PIXEL bitmask,b_bitmask,bits;
    PIXEL *ScrHead=srcLinePos;
    PIXEL *DesHead=desLinePos;
    int totalBits=width<<LOG_PIXEL_BITS;
    if(desBitOffset>0)
    { if(totalBits<PIXEL_UNIT-desBitOffset)
      { bitmask=MASK_OFFSET_OPT(totalBits,desBitOffset);
        totalBits=0;
      }
      else
      { bitmask=MASK_OFFSET_OPT(PIXEL_UNIT-desBitOffset,desBitOffset);
        totalBits-=(PIXEL_UNIT-desBitOffset);
      }
      for(j = 0; j < height; j++ )
      { if(desBitOffset<=srcBitOffset)
        { bits=*srcLinePos;
          if(desBitOffset<srcBitOffset) bits=PATTERN_SHIFT_LEFT(bits,srcBitOffset-desBitOffset) | PATTERN_SHIFT_RIGHT(*(srcLinePos+1),PIXEL_UNIT+desBitOffset-srcBitOffset);
        }
        else
        { bits=PATTERN_SHIFT_RIGHT(*srcLinePos,desBitOffset-srcBitOffset);
        }
        *desLinePos=(*desLinePos)&(~bitmask) | (bits&bitmask);
        desLinePos+=TgtVramUnitPerLine;
        srcLinePos+=SrcVramUnitPerLine;
      }
      if(desBitOffset<=srcBitOffset)ScrHead++;
      DesHead++;
    }
    srcBitOffset=(desBitOffset>srcBitOffset)?(PIXEL_UNIT+srcBitOffset-desBitOffset):(srcBitOffset-desBitOffset);
    desBitOffset=0;
    if(srcBitOffset)
    { int srcheadbits=PIXEL_UNIT-srcBitOffset;
      width=totalBits>>LOG_UNIT;
      if(width)
      { srcLinePos=ScrHead;
        desLinePos=DesHead;
        for(j = 0; j < height; j++ )
        { for(i = 0; i < width; i++ )
          { desLinePos[i]=PATTERN_SHIFT_LEFT(srcLinePos[i],srcBitOffset) | PATTERN_SHIFT_RIGHT(srcLinePos[i+1],srcheadbits);
          }
          desLinePos+=TgtVramUnitPerLine;
          srcLinePos+=SrcVramUnitPerLine;
        }
        totalBits&=(PIXEL_UNIT-1);
      }
      if(totalBits)
      { bitmask=MASK_OFFSET_OPT(totalBits,0);
        b_bitmask=~bitmask;
        srcLinePos=ScrHead+width;
        desLinePos=DesHead+width;
        for(j = 0; j < height; j++ )
        { bits=PATTERN_SHIFT_LEFT(*srcLinePos,srcBitOffset) | PATTERN_SHIFT_RIGHT(*(srcLinePos+1),srcheadbits);
         *desLinePos=( *desLinePos & b_bitmask ) | ( bits & bitmask );
          desLinePos+=TgtVramUnitPerLine;
          srcLinePos+=SrcVramUnitPerLine;
        }
      }
    }
    else
    { width=totalBits>>LOG_UNIT;
      if(width)
      { srcLinePos=ScrHead;
        desLinePos=DesHead;
        for(j = 0; j < height; j++ )
        { for(i = 0; i < width; i++ )
          { desLinePos[i]=srcLinePos[i];
          }
          desLinePos+=TgtVramUnitPerLine;
          srcLinePos+=SrcVramUnitPerLine;
        }
        totalBits&=(PIXEL_UNIT-1);
      }
      if(totalBits>0)
      { bitmask=MASK_OFFSET_OPT(totalBits,0);
        b_bitmask=~bitmask;
        srcLinePos=ScrHead+width;
        desLinePos=DesHead+width;
        for(j = 0; j < height; j++ )
        { bits=*srcLinePos;
          *desLinePos=( *desLinePos & b_bitmask ) | ( bits & bitmask );
          desLinePos+=TgtVramUnitPerLine;
          srcLinePos+=SrcVramUnitPerLine;
        }
      }
    }
    #endif
  }
}
//---------------------------------------------------------------------------
void  VramToFrameBuffer(VRAM *srcVram, int srcx, int srcy, int width, int height)
{ 
  int SrcVramUnitPerLine = srcVram->UnitPerLine;
  PIXEL  *srcLinePos, *desLinePos;
  int   srcBitOffset, desBitOffset;
  VramPixelLocate( (BYTE **)&srcLinePos, &srcBitOffset, srcVram, srcx, srcy);
  VramPixelLocate( (BYTE **)&desLinePos, &desBitOffset, g_Lcdvram, srcVram->Left+srcx, srcVram->Top+srcy);

  if(srcx==0 && width==LCD_WIDTH && srcVram->Width==LCD_WIDTH)
  {  memcpy(desLinePos,srcLinePos,(DWORD)height * SrcVramUnitPerLine * UNIT_BYTE);
  }
  else
  { int TgtVramUnitPerLine = g_Lcdvram->UnitPerLine;
    int i,j;
    #if(BITS_PER_PIXEL==PIXEL_UNIT) /* 8 16 32 */
	if(width>3)
	{ for(j=0,i=width*sizeof(PIXEL); j < height; j++ )
      { memcpy(desLinePos,srcLinePos,i);
	    desLinePos+=TgtVramUnitPerLine;
        srcLinePos+=SrcVramUnitPerLine;
      }
	}
	else
	{ for(j = 0; j < height; j++ )
      { for(i=0;i<width;i++)
        { desLinePos[i]=srcLinePos[i];
        }
        desLinePos+=TgtVramUnitPerLine;
        srcLinePos+=SrcVramUnitPerLine;
      }
	}
    #elif(BITS_PER_PIXEL==24)
    { BYTE *desPos,*srcPos;
      for(j = 0; j < height; j++ )
      { desPos=(BYTE *)desLinePos;
        srcPos=(BYTE *)srcLinePos;
        for(i=0;i<width;i++)
        { *desPos++ = *srcPos++;
          *desPos++ = *srcPos++;
          *desPos++ = *srcPos++;
        }
        desLinePos+=TgtVramUnitPerLine;
        srcLinePos+=SrcVramUnitPerLine;
      }
    }
    #elif(BITS_PER_PIXEL<8)
    PIXEL bitmask,b_bitmask,bits;
    PIXEL *ScrHead=srcLinePos;
    PIXEL *DesHead=desLinePos;
    int totalBits=width<<LOG_PIXEL_BITS;
    if(desBitOffset>0)
    { if(totalBits<PIXEL_UNIT-desBitOffset)
      { bitmask=MASK_OFFSET_OPT(totalBits,desBitOffset);
        totalBits=0;
      }
      else
      { bitmask=MASK_OFFSET_OPT(PIXEL_UNIT-desBitOffset,desBitOffset);
        totalBits-=(PIXEL_UNIT-desBitOffset);
      }
      for(j = 0; j < height; j++ )
      { if(desBitOffset<=srcBitOffset)
        { bits=*srcLinePos;
          if(desBitOffset<srcBitOffset) bits=PATTERN_SHIFT_LEFT(bits,srcBitOffset-desBitOffset) | PATTERN_SHIFT_RIGHT(*(srcLinePos+1),PIXEL_UNIT+desBitOffset-srcBitOffset);
        }
        else
        { bits=PATTERN_SHIFT_RIGHT(*srcLinePos,desBitOffset-srcBitOffset);
        }
        *desLinePos=(*desLinePos)&(~bitmask) | (bits&bitmask);
        desLinePos+=TgtVramUnitPerLine;
        srcLinePos+=SrcVramUnitPerLine;
      }
      if(desBitOffset<=srcBitOffset)ScrHead++;
      DesHead++;
    }
    srcBitOffset=(desBitOffset>srcBitOffset)?(PIXEL_UNIT+srcBitOffset-desBitOffset):(srcBitOffset-desBitOffset);
    desBitOffset=0;
    if(srcBitOffset)
    { int srcheadbits=PIXEL_UNIT-srcBitOffset;
      width=totalBits>>LOG_UNIT;
      if(width)
      { srcLinePos=ScrHead;
        desLinePos=DesHead;
        for(j = 0; j < height; j++ )
        { for(i = 0; i < width; i++ )
          { desLinePos[i]=PATTERN_SHIFT_LEFT(srcLinePos[i],srcBitOffset) | PATTERN_SHIFT_RIGHT(srcLinePos[i+1],srcheadbits);
          }
          desLinePos+=TgtVramUnitPerLine;
          srcLinePos+=SrcVramUnitPerLine;
        }
        totalBits&=(PIXEL_UNIT-1);
      }
      if(totalBits)
      { bitmask=MASK_OFFSET_OPT(totalBits,0);
        b_bitmask=~bitmask;
        srcLinePos=ScrHead+width;
        desLinePos=DesHead+width;
        for(j = 0; j < height; j++ )
        { bits=PATTERN_SHIFT_LEFT(*srcLinePos,srcBitOffset) | PATTERN_SHIFT_RIGHT(*(srcLinePos+1),srcheadbits);
         *desLinePos=( *desLinePos & b_bitmask ) | ( bits & bitmask );
          desLinePos+=TgtVramUnitPerLine;
          srcLinePos+=SrcVramUnitPerLine;
        }
      }
    }
    else
    { width=totalBits>>LOG_UNIT;
      if(width)
      { srcLinePos=ScrHead;
        desLinePos=DesHead;
        for(j = 0; j < height; j++ )
        { for(i = 0; i < width; i++ )
          { desLinePos[i]=srcLinePos[i];
          }
          desLinePos+=TgtVramUnitPerLine;
          srcLinePos+=SrcVramUnitPerLine;
        }
        totalBits&=(PIXEL_UNIT-1);
      }
      if(totalBits>0)
      { bitmask=MASK_OFFSET_OPT(totalBits,0);
        b_bitmask=~bitmask;
        srcLinePos=ScrHead+width;
        desLinePos=DesHead+width;
        for(j = 0; j < height; j++ )
        { bits=*srcLinePos;
          *desLinePos=( *desLinePos & b_bitmask ) | ( bits & bitmask );
          desLinePos+=TgtVramUnitPerLine;
          srcLinePos+=SrcVramUnitPerLine;
        }
      }
    }
    #endif
  }
}
//---------------------------------------------------------------------------
#if(ON_SIMULATOR && LCD_FRAME_BUFFER)
 #define  IsCanvasNeedExpose(ACanvas)  ( (ACanvas)->Vram->GroupOperation==0 && (ACanvas)->Owner && (ACanvas)->Owner->unmapcount==0 ) || (ACanvas)->Owner==g_RootWnd
#else
 #define  IsCanvasNeedExpose(ACanvas)    (ACanvas)->Vram->GroupOperation==0 && (ACanvas)->Owner && (ACanvas)->Owner->unmapcount==0  
#endif 
//---------------------------------------------------------------------------
/*注：在桌面(RootWindow)上绘图比较特殊，它直接覆盖性地输出到屏幕而不剪切任何窗口*/
#define CM_ExposeCanvas(ACanvas,x,y,width,height)\
{  if( IsCanvasNeedExpose(ACanvas) )\
   {  if( !(ACanvas)->Owner->Family->TopWnd->Prev)\
      { CanvasToLcd(ACanvas,x,y,width,height);\
	  }\
      else\
      { extern BOOL g_SomeWndNeedPaint;\
	    TRECT  expose_rect;\
	    g_SomeWndNeedPaint=true;\
		(ACanvas)->Owner->Family->CanvasChanged=true;\
        expose_rect.left=(ACanvas)->Vram->Left+(ACanvas)->VX +x;\
        expose_rect.top=(ACanvas)->Vram->Top+(ACanvas)->VY +y;\
        expose_rect.right=expose_rect.left+width;\
        expose_rect.bottom=expose_rect.top+height;\
        RegionUnionRect(&(ACanvas)->Vram->ExposeRegion, &expose_rect);\
	  }\
   }\
} 
//---------------------------------------------------------------------------
#define WRITE_PIXEL(ACanvas,x,y) \
   { CanvasPixelLocate( &PixelPos, &BitOffset, (ACanvas), (x), (y) ); \
     cmPixelOpt( PixelPos, BitOffset, (PIXEL)(ACanvas)->Pen.Foreground, (ACanvas)->Pen.Mode&0x03 ); \
  }
//---------------------------------------------------------------------------
#define PUT_PIXEL(ACanvas,x,y) \
   { if((x)>=(ACanvas)->ClipRect.left && (x)<(ACanvas)->ClipRect.right && (y)>=(ACanvas)->ClipRect.top && (y)<(ACanvas)->ClipRect.bottom)\
     { WRITE_PIXEL(ACanvas,x,y);\
       CM_ExposeCanvas(ACanvas,x,y,1,1);\
	 }\
  }
//---------------------------------------------------------------------------
void  DrawPixel(HDC dc,int x,int y)
{ int BitOffset;
  PIXEL *PixelPos;
  PUT_PIXEL((TWndCanvas *)dc,x,y);
}
//---------------------------------------------------------------------------
//用指定的颜色画直线,并与背景色进行逻辑操作（包括按位与、或、异或、复制等）
void DrawHorLine(HDC dc, int x, int y, int width)
{  
   PIXEL FillColorIndex=((TWndCanvas *)dc)->Pen.Foreground;
   int HeadBitOffset;
   int i;
   PIXEL *PixelPos;
   DWORD dwRop=((TWndCanvas *)dc)->Pen.Mode & 0x03;
   CHECK_CANVAS_HORLINE((TWndCanvas *)dc,x,y,width);
   CanvasPixelLocate( &PixelPos, &HeadBitOffset, (TWndCanvas *)dc, x, y );
   #if(BITS_PER_PIXEL == PIXEL_UNIT) /* 8 16 32 */
   { for(i=0;i<width;i++)
	 { SET_PIXEL_LOGIC(PixelPos,*PixelPos,FillColorIndex,dwRop);
       PixelPos++;
	 }
   }
   #elif(BITS_PER_PIXEL == 24)
   { 
     BYTE FillColorByte1=(BYTE)FillColorIndex;
     BYTE FillColorByte2=(BYTE)(FillColorIndex >> 8);
	 BYTE FillColorByte3=(BYTE)(FillColorIndex >> 16);
	 for(i=0;i<width;i++)
	 { SET_3BYTE_LOGIC(PixelPos,FillColorByte1,FillColorByte2,FillColorByte3,dwRop);
       PixelPos=(PIXEL *)((BYTE *)PixelPos+3);
	 }
   }
   #elif(BITS_PER_PIXEL < 8)
   { 
     PIXEL headbitmask,tailbitmask,newpixel;
     int UnitCount,totalBits;
	 if(FillColorIndex!=0)
     { PIXEL Foreground=FillColorIndex;
       for(i=BITS_PER_PIXEL;i<PIXEL_UNIT;i+=BITS_PER_PIXEL)
	   { FillColorIndex=(FillColorIndex<<BITS_PER_PIXEL)|Foreground;
	   }
     }
     totalBits=width<<LOG_PIXEL_BITS;
     if(HeadBitOffset)
	 { if(totalBits<PIXEL_UNIT-HeadBitOffset)
	   { headbitmask=MASK_OFFSET_OPT(totalBits,HeadBitOffset);
         UnitCount=0;
         tailbitmask=0;
	   } 
       else
	   { int otherbits=totalBits-(PIXEL_UNIT-HeadBitOffset);
         headbitmask=MASK_OFFSET_OPT(PIXEL_UNIT-HeadBitOffset,HeadBitOffset);
         UnitCount=otherbits/PIXEL_UNIT;
         tailbitmask=otherbits%PIXEL_UNIT;
         if(tailbitmask)
		 { tailbitmask=MASK_OFFSET_OPT(tailbitmask,0);
		 }
	   }
	 }
     else
	 { UnitCount= totalBits / PIXEL_UNIT;
       tailbitmask=MASK_OFFSET_OPT((totalBits % PIXEL_UNIT),0);
	 }
	 
     if(HeadBitOffset)
	 {  SET_PIXEL_LOGIC(&newpixel,*PixelPos,FillColorIndex,dwRop);
       *PixelPos++=(*PixelPos & ~headbitmask) | (newpixel & headbitmask);;
	 }
	 
     if(UnitCount)
	 { for(i=0;i<UnitCount;i++)
	   { SET_PIXEL_LOGIC(PixelPos,*PixelPos,FillColorIndex,dwRop);
         PixelPos++;
	   }
	 }
	 
     if(tailbitmask)
	 { SET_PIXEL_LOGIC(&newpixel,*PixelPos,FillColorIndex,dwRop);
       *PixelPos=( *PixelPos & ~tailbitmask ) | ( newpixel & tailbitmask );
	 }
   }
   #endif
   CM_ExposeCanvas((TWndCanvas *)dc,x,y,width,1);
}
//---------------------------------------------------------------------------
void DrawVerLine(HDC dc, int x, int y, int height)
{  
   VRAM  *vram=((TWndCanvas *)dc)->Vram;
   int i,BitOffset;
   PIXEL FillColorIndex=((TWndCanvas *)dc)->Pen.Foreground;
   PIXEL *PixelPos;
   DWORD dwRop=((TWndCanvas *)dc)->Pen.Mode&0x03;
   CHECK_CANVAS_VERLINE(((TWndCanvas *)dc),x,y,height);
   CanvasPixelLocate( &PixelPos, &BitOffset, (TWndCanvas *)dc, x, y );
   #if(BITS_PER_PIXEL == PIXEL_UNIT) /* 8 16 32 */
   { for(i=0;i<height;i++)
     { SET_PIXEL_LOGIC(PixelPos,*PixelPos,FillColorIndex,dwRop);
       PixelPos=GetNextLinePos(PixelPos, vram );
     }
   }
   #elif(BITS_PER_PIXEL == 24)
   { BYTE FillColorByte1=(BYTE)FillColorIndex;
     BYTE FillColorByte2=(BYTE)(FillColorIndex>>8);
	 BYTE FillColorByte3=(BYTE)(FillColorIndex>>16);
	 for(i=0;i<height;i++)
     { SET_3BYTE_LOGIC(PixelPos,FillColorByte1,FillColorByte2,FillColorByte3,dwRop);
       PixelPos=GetNextLinePos(PixelPos, vram );
     }
   }
   #elif(BITS_PER_PIXEL < 8)
   { PIXEL newpixel,srcMask,b_srcMask;
	 srcMask=(PIXEL)MASK_OFFSET_OPT(BITS_PER_PIXEL,BitOffset);
     PIXEL_OFFSET_OPT(FillColorIndex,BITS_PER_PIXEL,BitOffset);
     b_srcMask = ~srcMask;
     for(i=0;i<height;i++)
	 {  SET_PIXEL_LOGIC(&newpixel,*PixelPos,FillColorIndex,dwRop);
        *PixelPos=(*PixelPos & b_srcMask)|(newpixel & srcMask);
        PixelPos=GetNextLinePos(PixelPos, vram );
	 }
   } 
   #endif
   CM_ExposeCanvas((TWndCanvas *)dc,x,y,1,height);
}
//---------------------------------------------------------------------------
void DrawLine(HDC dc, int x1,int y1,int x2,int y2)
{ 
  int dx=x2-x1,dy=y2-y1,stepx,stepy,change;
  PIXEL *PixelPos;
  int BitOffset;
  if(dx==0){DrawVerLine(dc,x1,(y2>y1)?y1:y2,(y2>y1)?y2+1-y1:y1+1-y2);return;}
  else if(dx>0)stepx=1; else {dx=-dx;stepx=-1;}
  if(dy==0){DrawHorLine(dc,(x2>x1)?x1:x2,y1,(x2>x1)?x2+1-x1:x1+1-x2);return;}
  else if (dy>0) stepy=1; else {dy=-dy;stepy=-1;}
  if (dx > dy)
  { change = dx>>1;
    while (x1 != x2)
    { PUT_PIXEL((TWndCanvas *)dc,x1,y1);
      x1 += stepx; change += dy;
      if (change>dx){y1+=stepy;change-=dx;}
    }
  }
  else
  { change = dy>>1;
    while (y1!=y2)
    { PUT_PIXEL((TWndCanvas *)dc,x1,y1);
      y1+=stepy;change+=dx;
      if (change>dy){x1+=stepx; change-=dy;}
    }
  }
  PUT_PIXEL((TWndCanvas *)dc,x2,y2); //the last pixel can not be ignored
}

//---------------------------------------------------------------------------
void FillRect(HDC dc,int left,int top,int width,int height)
{ 
  if(dc)
  { VRAM  *vram=((TWndCanvas *)dc)->Vram;
    PIXEL *PixelPos,*StartPos;
    PIXEL FillColorIndex;
    int HeadBitOffset;
    int i,j;
    DWORD dwRop = ((TWndCanvas *)dc)->Pen.Mode & 0x03;
    CHECK_CANVAS_RECT((TWndCanvas *)dc,left,top,width,height);
    CanvasPixelLocate( &StartPos, &HeadBitOffset, (TWndCanvas *)dc, left, top);
    FillColorIndex = ((TWndCanvas *)dc)->Pen.Foreground;
    
	#if(BITS_PER_PIXEL==PIXEL_UNIT)  /* 8 16 32 */
    { for(j=0;j<height;j++)
      { PixelPos=StartPos;
        StartPos=GetNextLinePos(StartPos, vram );
        for(i=0;i<width;i++)
        { SET_PIXEL_LOGIC(PixelPos,*PixelPos,FillColorIndex,dwRop);
          PixelPos++;
        }
      }
    }
    #elif(BITS_PER_PIXEL==24)
    { BYTE FillColorByte1=(BYTE)FillColorIndex;
      BYTE FillColorByte2=(BYTE)(FillColorIndex>>8);
	  BYTE FillColorByte3=(BYTE)(FillColorIndex>>16);
	  for(j=0;j<height;j++)
      { PixelPos=StartPos;
        StartPos=GetNextLinePos(StartPos, vram );
        for(i=0;i<width;i++)
        { SET_3BYTE_LOGIC(PixelPos,FillColorByte1,FillColorByte2,FillColorByte3,dwRop);
          PixelPos=(PIXEL *)((BYTE *)PixelPos+3);
        }
      }
    }
    #elif(BITS_PER_PIXEL<8)
    { int totalBits,UnitCount;
      PIXEL headbitmask,tailbitmask,b_headbitmask,b_tailbitmask,newpixel;
      if(FillColorIndex!=0)
      { PIXEL Foreground=FillColorIndex;
        for(i=BITS_PER_PIXEL;i<PIXEL_UNIT;i+=BITS_PER_PIXEL)
        { FillColorIndex=(FillColorIndex<<BITS_PER_PIXEL)|Foreground;
        }
      }
      totalBits=width<<LOG_PIXEL_BITS;
      if(HeadBitOffset)
      { if(totalBits<PIXEL_UNIT-HeadBitOffset)
       { headbitmask=MASK_OFFSET_OPT(totalBits,HeadBitOffset);
         UnitCount=0;
         tailbitmask=0;
       }
       else
       { int headbitcount=PIXEL_UNIT-HeadBitOffset;
         int otherbits=totalBits-headbitcount;
         headbitmask=MASK_OFFSET_OPT(headbitcount,HeadBitOffset);
         UnitCount=otherbits/PIXEL_UNIT;
         tailbitmask=otherbits%PIXEL_UNIT;
         if(tailbitmask)
         { tailbitmask=MASK_OFFSET_OPT(tailbitmask,0);
         }
       }
      }
      else
      { UnitCount= totalBits/PIXEL_UNIT;
        tailbitmask=MASK_OFFSET_OPT((totalBits%PIXEL_UNIT),0);
      }
      b_headbitmask=~headbitmask;
      b_tailbitmask=~tailbitmask;
      for(j=0;j<height;j++)
      { PixelPos=StartPos;
        StartPos=GetNextLinePos(StartPos, vram );
        if(HeadBitOffset)
        {  SET_PIXEL_LOGIC(&newpixel,*PixelPos,FillColorIndex,dwRop);
           *PixelPos++=(*PixelPos & b_headbitmask) | (newpixel & headbitmask);;
        }
        if(UnitCount)
        { for(i=0;i<UnitCount;i++)
          { SET_PIXEL_LOGIC(PixelPos,*PixelPos,FillColorIndex,dwRop);
            PixelPos++;
          }
        }
        if(tailbitmask)
        { SET_PIXEL_LOGIC(&newpixel,*PixelPos,FillColorIndex,dwRop);
          *PixelPos=( *PixelPos & b_tailbitmask ) | ( newpixel & tailbitmask );
        }
      }
    }  
    #endif
    CM_ExposeCanvas( (TWndCanvas *)dc,left,top,width,height);
  }
}
//---------------------------------------------------------------------------
void ClearRect(HDC dc,int left,int top,int width,int height)
{ 
  if(dc)
  { VRAM  *vram=((TWndCanvas *)dc)->Vram;
    int i,j;
    PIXEL FillColorIndex;
    PIXEL *PixelPos,*StartPos;
    int HeadBitOffset;
    CHECK_CANVAS_RECT((TWndCanvas *)dc,left,top,width,height);
    CanvasPixelLocate( &StartPos, &HeadBitOffset, (TWndCanvas *)dc,left, top);
    FillColorIndex=((TWndCanvas *)dc)->Pen.Background;
	
    #if(BITS_PER_PIXEL==PIXEL_UNIT)  /* 8 16 32 */
    { for(j=0;j<height;j++)
      { PixelPos=StartPos;
        StartPos=GetNextLinePos(StartPos, vram );
        for(i=0;i<width;i++)
        { *PixelPos++=FillColorIndex;
        }
      }
    }
	#elif(BITS_PER_PIXEL==24)
    { BYTE FillColorByte1=(BYTE)FillColorIndex;
      BYTE FillColorByte2=(BYTE)(FillColorIndex>>8);
	  BYTE FillColorByte3=(BYTE)(FillColorIndex>>16);
	  for(j=0;j<height;j++)
      { PixelPos=StartPos;
        StartPos=GetNextLinePos(StartPos, vram );
        for(i=0;i<width;i++)
        { *(BYTE *)PixelPos=FillColorByte1;
		  PixelPos=(PIXEL *)((BYTE *)PixelPos + 1);
		  *(BYTE *)PixelPos=FillColorByte2;
		  PixelPos=(PIXEL *)((BYTE *)PixelPos + 1);
		  *(BYTE *)PixelPos=FillColorByte3;
		  PixelPos=(PIXEL *)((BYTE *)PixelPos + 1);
        }
      }
    }
    #elif(BITS_PER_PIXEL<8)
    { int totalBits,UnitCount;
      PIXEL headbitmask,tailbitmask,b_headbitmask,b_tailbitmask;
      if(FillColorIndex!=0)
      { PIXEL Foreground=FillColorIndex;
        for(i=BITS_PER_PIXEL;i<PIXEL_UNIT;i+=BITS_PER_PIXEL)
        { FillColorIndex=(FillColorIndex<<BITS_PER_PIXEL)|Foreground;
        }
      }
      totalBits=width<<LOG_PIXEL_BITS;
      if(HeadBitOffset)
      { if(totalBits<PIXEL_UNIT-HeadBitOffset)
       { headbitmask=MASK_OFFSET_OPT(totalBits,HeadBitOffset);
         UnitCount=0;
         tailbitmask=0;
       }
       else
       { int headbitcount=PIXEL_UNIT-HeadBitOffset;
         int otherbits=totalBits-headbitcount;
         headbitmask=MASK_OFFSET_OPT(headbitcount,HeadBitOffset);
         UnitCount=otherbits/PIXEL_UNIT;
         tailbitmask=otherbits%PIXEL_UNIT;
         if(tailbitmask)
         { tailbitmask=MASK_OFFSET_OPT(tailbitmask,0);
         }
       }
      }
      else
      { UnitCount= totalBits/PIXEL_UNIT;
        tailbitmask=MASK_OFFSET_OPT((totalBits%PIXEL_UNIT),0);
      }
      b_headbitmask=~headbitmask;
      b_tailbitmask=~tailbitmask;
      for(j=0;j<height;j++)
      { PixelPos=StartPos;
        StartPos=GetNextLinePos(StartPos, vram );
        if(HeadBitOffset)
        {  *PixelPos++=(*PixelPos & b_headbitmask) | (FillColorIndex & headbitmask);;
        }
        if(UnitCount)
        { for(i=0;i<UnitCount;i++)
          { *PixelPos++=FillColorIndex;
          }
        }
        if(tailbitmask)
        { *PixelPos=( *PixelPos & b_tailbitmask ) | ( FillColorIndex & tailbitmask );
        }
      }
    }  
    #endif
    CM_ExposeCanvas((TWndCanvas *)dc,left,top,width,height);
  }
}
//---------------------------------------------------------------------------
void DrawEllipse(HDC dc, int x, int y, int a, int b)
{
  int x0=0;
  int y0 = b;
  int a0 = a;
  int b0 = b;
  int asqr = a0 * a0;
  int two_asqr = asqr<<1;
  int bsqr = b0 * b0;
  int two_bsqr = bsqr<<1;
  int d, dx, dy;
  int BitOffset;
  PIXEL *PixelPos;
  d = bsqr - asqr * b0 + (asqr >> 2);
  dx = 0;
  dy = (long)two_asqr * b0;
  do
  {	PUT_PIXEL((TWndCanvas *)dc,x+x0, y+y0 );
	PUT_PIXEL((TWndCanvas *)dc, x-x0, y+y0 );
	PUT_PIXEL((TWndCanvas *)dc,x+x0, y-y0 );
	PUT_PIXEL((TWndCanvas *)dc,x-x0, y-y0 );
	if ( d > 0 )  
	{ y0--;
      dy -= two_asqr;
      d  -= dy;
    }
    x0++;
    dx += two_bsqr;
    d  += (bsqr+dx);
  }while ( dx < dy );

  d += (3*(asqr-bsqr)/2 - (dx+dy))/2;
  do
  {	PUT_PIXEL((TWndCanvas *)dc,x+x0, y+y0 );
	PUT_PIXEL((TWndCanvas *)dc,x-x0, y+y0 );
	PUT_PIXEL((TWndCanvas *)dc, x+x0, y-y0 );
	PUT_PIXEL((TWndCanvas *)dc, x-x0, y-y0 );
	if ( d < 0 )
	{ x0++;
      dx += two_bsqr;
      d  += dx;
    }
    y0--;
    dy -= two_asqr;
    d  += (asqr-dy);
  } while (y0>=0);
    
}
//---------------------------------------------------------------------------
void DrawCircle(HDC dc, int xCenter, int yCenter, int radius)
{ 
  int x, y, d;
  int BitOffset;	
  PIXEL *PixelPos;
  if(radius<=0)return;
  x = xCenter;
  y = yCenter + radius;
  d = 3 - 2 * radius;
  do
  {	PUT_PIXEL((TWndCanvas *)dc, x, y );
	PUT_PIXEL((TWndCanvas *)dc,x, yCenter - (y - yCenter) );
	PUT_PIXEL((TWndCanvas *)dc, xCenter - (x - xCenter), y );
	PUT_PIXEL((TWndCanvas *)dc, xCenter - (x - xCenter), yCenter - (y - yCenter) );
	PUT_PIXEL((TWndCanvas *)dc,xCenter + (y - yCenter), yCenter + (x - xCenter ) );
	PUT_PIXEL((TWndCanvas *)dc,xCenter + (y - yCenter), yCenter - (x - xCenter) );
	PUT_PIXEL((TWndCanvas *)dc, xCenter - (y - yCenter), yCenter + (x - xCenter ) );
	PUT_PIXEL((TWndCanvas *)dc, xCenter - (y - yCenter), yCenter - (x - xCenter) );
	if ( d < 0 )
	{ d = d + ((x - xCenter) << 2) + 6;
	}
	else
	{ d = d + (((x - xCenter) - (y - yCenter)) << 2 ) + 10;
	  y--;
	}
	x++;
  } 
  while ( (x - xCenter) < (y - yCenter) );
  PUT_PIXEL((TWndCanvas *)dc,(xCenter + (y - yCenter)), (yCenter + (x - xCenter )) );
  PUT_PIXEL((TWndCanvas *)dc, (xCenter + (y - yCenter)), (yCenter - (x - xCenter)) );
  PUT_PIXEL((TWndCanvas *)dc,(xCenter - (y - yCenter)), (yCenter + (x - xCenter )) );
  PUT_PIXEL((TWndCanvas *)dc,(xCenter - (y - yCenter)), (yCenter - (x - xCenter)) );
}
//---------------------------------------------------------------------------
void DrawSector(HDC dc, int xCenter, int yCenter, int radius,int angle1,int angle2)
{
  int x, y, d,c1,c2,c,step=0;
  int BitOffset,quarter1,quarter2,quarter3,quarter4;	
  PIXEL *PixelPos;
  
  if(radius <= 0)
	  return;
  
  x = xCenter;
  y = yCenter + radius;
  d = 3 - 2 * radius;
  c1=(int)(angle1*radius*3.14159/180);
  c2=(int)(angle2*radius*3.14159/180);
  quarter1=(int)(radius*3.14159/2);
  quarter2=(int)(radius*3.14159);
  quarter3=(int)(radius*3.14159*3/2);
  quarter4=(int)(radius*3.14159*2);
  
  while(1)
  {	c=quarter4-step;
	if(c>=c1 && c<=c2)	PUT_PIXEL((TWndCanvas *)dc,xCenter + (y - yCenter), yCenter + (x - xCenter ) );
	
	if(step>=c1 && step<=c2) PUT_PIXEL((TWndCanvas *)dc,xCenter + (y - yCenter), yCenter - (x - xCenter) );
    
	c=quarter2+step;
	if(c>=c1 && c<=c2) PUT_PIXEL((TWndCanvas *)dc, xCenter - (y - yCenter), yCenter + (x - xCenter ) );
	
	c=quarter2-step;
	if(c>=c1 && c<=c2) PUT_PIXEL((TWndCanvas *)dc, xCenter - (y - yCenter), yCenter - (x - xCenter) );
	  
	if (  x - xCenter  >=  y - yCenter  ) break;

	c=quarter3+step;
	if(c>=c1 && c<=c2) PUT_PIXEL((TWndCanvas *)dc, x, y );
     
	c=quarter1-step;
    if(c>=c1 && c<=c2) PUT_PIXEL((TWndCanvas *)dc,x, yCenter - (y - yCenter) );

	c=quarter3-step;
	if(c>=c1 && c<=c2) PUT_PIXEL((TWndCanvas *)dc, xCenter - (x - xCenter), y );

	c= quarter1+step;
	if(c>=c1 && c<=c2) PUT_PIXEL((TWndCanvas *)dc, xCenter - (x - xCenter), yCenter - (y - yCenter) );
    
	if ( d < 0 )
	{ d = d + ((x - xCenter) << 2) + 6;
	}
	else
	{ d = d + (((x - xCenter) - (y - yCenter)) << 2 ) + 10;
	  y--;
	}
	x++;
	step++;
  } 
  
}
//---------------------------------------------------------------------------
/*the arithmetic of FillSector if not very well,it should to be optimized later*/
void FillSector(HDC dc, int xCenter, int yCenter, int radius,int angle1,int angle2)
{ int x, y, d,c1,c2,c,step=0;
  int quarter1,quarter2,quarter3,quarter4;	

  if(radius<=0)return;
  x = xCenter;
  y = yCenter + radius;
  d = 3 - 2 * radius;
  c1=(int)(angle1*radius*3.14159/180);
  c2=(int)(angle2*radius*3.14159/180);
  quarter1=(int)(radius*3.14159/2);
  quarter2=(int)(radius*3.14159);
  quarter3=(int)(radius*3.14159*3/2);
  quarter4=(int)(radius*3.14159*2);
  while(1)
  {	c=quarter4-step;
	if(c>=c1 && c<=c2)	DrawLine((TWndCanvas *)dc,xCenter + (y - yCenter), yCenter + (x - xCenter ) ,xCenter,yCenter );
	
	if(step>=c1 && step<=c2) DrawLine((TWndCanvas *)dc,xCenter + (y - yCenter), yCenter - (x - xCenter),xCenter,yCenter );
    
	c=quarter2+step;
	if(c>=c1 && c<=c2) DrawLine((TWndCanvas *)dc, xCenter - (y - yCenter), yCenter + (x - xCenter ),xCenter,yCenter );
	
	c=quarter2-step;
	if(c>=c1 && c<=c2) DrawLine((TWndCanvas *)dc, xCenter - (y - yCenter), yCenter - (x - xCenter),xCenter,yCenter);
	  
	if (  x - xCenter  >=  y - yCenter  ) break;

	c=quarter3+step;
	if(c>=c1 && c<=c2) DrawLine((TWndCanvas *)dc, x, y,xCenter,yCenter );
     
	c=quarter1-step;
    if(c>=c1 && c<=c2) DrawLine((TWndCanvas *)dc,x, yCenter - (y - yCenter),xCenter,yCenter );

	c=quarter3-step;
	if(c>=c1 && c<=c2) DrawLine((TWndCanvas *)dc, xCenter - (x - xCenter), y ,xCenter,yCenter);

	c= quarter1+step;
	if(c>=c1 && c<=c2) DrawLine((TWndCanvas *)dc, xCenter - (x - xCenter), yCenter - (y - yCenter) ,xCenter,yCenter);
    
	if ( d < 0 )
	{ d = d + ((x - xCenter) << 2) + 6;
	}
	else
	{ d = d + (((x - xCenter) - (y - yCenter)) << 2 ) + 10;
	  y--;
	}
	x++;
	step++;
  } 
}
//---------------------------------------------------------------------------
void FillEllipse(HDC dc, int x, int y, int a, int b)
{ int x0=0;
  int y0 = b;
  int a0 = a;
  int b0 = b;
  int asqr = a0 * a0;
  int two_asqr = asqr<<1;
  int bsqr = b0 * b0;
  int two_bsqr = bsqr<<1;
  int d, dx, dy;
  d = bsqr - asqr * b0 + (asqr >> 2);
  dx = 0;
  dy = (long)two_asqr * b0;
  do
  { if ( d > 0 )  
	{ DrawHorLine(dc,x-x0,y+y0,(x0<<1)+1);
  	  DrawHorLine(dc,x-x0,y-y0,(x0<<1)+1);	

	  y0--;
      dy -= two_asqr;
      d  -= dy;
    }
    x0++;
    dx += two_bsqr;
    d  += (bsqr+dx);
  }while ( dx < dy );

  d += (3*(asqr-bsqr)/2 - (dx+dy))/2;
  do
  {	DrawHorLine(dc,x-x0,y+y0,(x0<<1)+1);
	DrawHorLine(dc,x-x0,y-y0,(x0<<1)+1);
	if ( d < 0 )
	{ x0++;
      dx += two_bsqr;
      d  += dx;
    }
    y0--;
    dy -= two_asqr;
    d  += (asqr-dy);
  } while (y0>=0);
    
}
//---------------------------------------------------------------------------
void FillCircle(HDC dc, int xCenter, int yCenter, int radius)
{
  int x, y, d,w;
  if(radius<=0)return;
  x = xCenter;
  y = yCenter + radius;
  d = 3 - 2 * radius;
  
  while (x - xCenter  <=  y - yCenter)
  { w=((y - yCenter)<<1)+1;
	DrawHorLine(dc,xCenter - (y - yCenter),yCenter + (x - xCenter ),w) ;
	if(x != xCenter)DrawHorLine(dc,xCenter - (y - yCenter),yCenter - (x - xCenter),w) ;
	if ( d < 0 )
	{ d = d + ((x - xCenter) << 2) + 6;
	}
	else  if(x - xCenter  <  y - yCenter)
	{ w=((x-xCenter)<<1)+1;
	  DrawHorLine(dc,xCenter - (x - xCenter),y,w);
	  DrawHorLine(dc,xCenter - (x - xCenter) ,yCenter - (y - yCenter),w) ;

	  d = d + (((x - xCenter) - (y - yCenter)) << 2 ) + 10;
	  y--;
	} 
	x++;
  }  
}
//---------------------------------------------------------------------------
void DrawPatternHorLine(HDC dc, int x,int y,int width,BYTE pattern,BOOL bigEndian)
{ PIXEL *PixelPos;
  int i,j;
  int   OriginLeft=x;
  DWORD dwRop=((TWndCanvas *)dc)->Pen.Mode & 0x03;
  PIXEL ForeColorIndex=((TWndCanvas *)dc)->Pen.Foreground;
  BYTE  PtnBits[8];
  int   BitOffset;

  CHECK_CANVAS_HORLINE((TWndCanvas *)dc,x,y,width);

  if(bigEndian)
  { for(i=0,j=x-OriginLeft;i<8;i++,j++)
    PtnBits[i]=(pattern<<(j&0x7))&0x80;
  }
  else
  { for(i=0,j=x-OriginLeft;i<8;i++,j++)
    PtnBits[i]=(pattern>>(j&0x7))&0x01;
  }
  CanvasPixelLocate( &PixelPos, &BitOffset, (TWndCanvas *)dc, x, y );

  #if(BITS_PER_PIXEL == PIXEL_UNIT)  /* 8 16 32 */
  { for(i=0;i<width;i++)
    { if(PtnBits[i&0x7])
      { SET_PIXEL_LOGIC( PixelPos, *PixelPos, ForeColorIndex, dwRop );
      }
      PixelPos++;
    }
   }
  #elif(BITS_PER_PIXEL == 24)
  { BYTE ForeColorByte1=(BYTE)ForeColorIndex;
    BYTE ForeColorByte2=(BYTE)(ForeColorIndex>>8);
    BYTE ForeColorByte3=(BYTE)(ForeColorIndex>>16);
	for(i=0;i<width;i++)
    { if(PtnBits[i&0x7])
      { SET_3BYTE_LOGIC( PixelPos, ForeColorByte1,ForeColorByte1,ForeColorByte1,dwRop );
      }
      PixelPos=(PIXEL *)((BYTE *)PixelPos +3);
    }
  }
  #elif(BITS_PER_PIXEL < 8)
  { PIXEL srcMask,srcValue;
    for(i=0;i<width;i++)
    { if(PtnBits[i&0x7])
      { srcValue=ForeColorIndex;
        srcMask=(PIXEL)MASK_OFFSET_OPT(BITS_PER_PIXEL,BitOffset);
        PIXEL_OFFSET_OPT(srcValue,BITS_PER_PIXEL,BitOffset);
        SET_PIXEL_LOGIC(&srcValue,*PixelPos,srcValue,dwRop);
        *PixelPos=(*PixelPos & ~srcMask)|(srcValue & srcMask);
      }
      GetNextPixelPosition( &PixelPos, &BitOffset, PixelPos, BitOffset );
    }
  }
  #endif
  CM_ExposeCanvas((TWndCanvas *)dc,x,y,width,1);
}
//---------------------------------------------------------------------------
void DrawPatternVerLine(HDC dc, int x,int y,int height,BYTE pattern,BOOL bigEndian)
{ 
  VRAM *vram=((TWndCanvas *)dc)->Vram;
  PIXEL *PixelPos;
  int   i,j;
  int   OriginTop=y;
  DWORD dwRop=((TWndCanvas *)dc)->Pen.Mode&0x03;
  PIXEL ForeColorIndex=((TWndCanvas *)dc)->Pen.Foreground;
  BYTE  PtnBits[8];
  int   BitOffset;

  CHECK_CANVAS_VERLINE((TWndCanvas *)dc,x,y,height);

  if(bigEndian)
  { for(i=0,j=y-OriginTop;i<8;i++,j++)
    PtnBits[i]=(pattern << ( j & 0x7 )) & 0x80;
  }
  else
  { for(i=0,j=y-OriginTop;i<8;i++,j++)
    PtnBits[i]=(pattern >> ( j & 0x7 ))&0x01;
  }
  CanvasPixelLocate( &PixelPos, &BitOffset, (TWndCanvas *)dc, x, y );
  
  #if(BITS_PER_PIXEL==PIXEL_UNIT)  /* 8 16 32 */
  { for(i=0;i<height;i++)
    { if(PtnBits[i&0x7])
      {  SET_PIXEL_LOGIC(PixelPos,*PixelPos,ForeColorIndex,dwRop);
      }
      PixelPos=GetNextLinePos(PixelPos, vram );
    }
  }
  #elif(BITS_PER_PIXEL==24)
  { BYTE ForeColorByte1=(BYTE)ForeColorIndex;
    BYTE ForeColorByte2=(BYTE)(ForeColorIndex>>8);
    BYTE ForeColorByte3=(BYTE)(ForeColorIndex>>16);
	for(i=0;i<height;i++)
    { if(PtnBits[i&0x7])
      {  SET_3BYTE_LOGIC(PixelPos,ForeColorByte1,ForeColorByte2,ForeColorByte3,dwRop);
      }
      PixelPos=GetNextLinePos(PixelPos, vram );
    }
  }
  #elif(BITS_PER_PIXEL<8)
  { PIXEL  srcMask=(PIXEL) MASK_OFFSET_OPT(BITS_PER_PIXEL,BitOffset);
    PIXEL  srcValue,b_srcMask=~srcMask;
    PIXEL_OFFSET_OPT(ForeColorIndex,BITS_PER_PIXEL,BitOffset);
    ForeColorIndex=ForeColorIndex& srcMask;
    if(dwRop==PL_REPLACE)
    { for(i=0;i<height;i++)
      { if(PtnBits[i&0x7])
        { *PixelPos=(*PixelPos & b_srcMask) | ForeColorIndex;
        }
        PixelPos=GetNextLinePos(PixelPos, vram );
     }
    }
    else
    { for(i=0;i<height;i++)
      { if(PtnBits[i&0x7])
        { SET_PIXEL_LOGIC(&srcValue,*PixelPos,ForeColorIndex,dwRop);
          *PixelPos=(*PixelPos & b_srcMask)|(srcValue & srcMask);
        }
        PixelPos=GetNextLinePos(PixelPos, vram );
      }
    }
  }
  #endif
  CM_ExposeCanvas((TWndCanvas *)dc,x,y,1,height);
}

//---------------------------------------------------------------------------
void DrawFontMatrix(HDC dc,int xPos,int yPos,int width,int height,BYTE *ptnArray)
{
  PIXEL ForeColorIndex,BackColorIndex,*PixelPos,*LineHeadPos;
  int  OriginPosX = xPos,OriginPosY=yPos;
  int  i,j,k,dwRop,BitOffset,LineByteOffset,LineHeadBitOffset,BytesPerLine,OpaqueMode;  
  BYTE pattern;
  
  #if(BITS_PER_PIXEL == 24)
  BYTE ForeColorByte1,ForeColorByte2,ForeColorByte3;
  BYTE BackColorByte1,BackColorByte2,BackColorByte3;
  #endif
  
  BytesPerLine = (width + 7) >> 3;
  /*---overrange check------------------------------------*/
  CHECK_CANVAS_RECT((TWndCanvas *)dc,xPos,yPos,width,height);

  /*---variable initialize------------------------------------*/
  dwRop=((TWndCanvas *)dc)->Pen.Mode & STYLEMASK_PENLOGIC;
  OpaqueMode=((TWndCanvas *)dc)->Pen.Mode&FS_OPAQUE;
  ForeColorIndex=((TWndCanvas *)dc)->Pen.Foreground;
  BackColorIndex=((TWndCanvas *)dc)->Pen.Background;
  
  #if(BITS_PER_PIXEL == 24)
  ForeColorByte1=(BYTE)ForeColorIndex;
  ForeColorByte2=(BYTE)(ForeColorIndex>>8);
  ForeColorByte3=(BYTE)(ForeColorIndex>>16);
  if(OpaqueMode)
  { BackColorByte1=(BYTE)BackColorIndex;
    BackColorByte2=(BYTE)(BackColorIndex>>8);
    BackColorByte3=(BYTE)(BackColorIndex>>16);
  }
  #endif

 
  if(yPos!=OriginPosY)
  { ptnArray+=(yPos-OriginPosY)*BytesPerLine; 
  }
  if(xPos!=OriginPosX)
  { LineHeadBitOffset=xPos-OriginPosX;
    if(LineHeadBitOffset>=8)
    { ptnArray+=(LineHeadBitOffset>>3);
      LineHeadBitOffset&=0x7;
    }
  }
  else
  { LineHeadBitOffset=0;
  }
  
  CanvasPixelLocate( &LineHeadPos, &BitOffset, (TWndCanvas *)dc, xPos,yPos);
  for(j=0;j<height;j++)
  { PixelPos=LineHeadPos;
    LineHeadPos=GetNextLinePos(LineHeadPos,((TWndCanvas *)dc)->Vram); 
    LineByteOffset=0;
    k= LineHeadBitOffset;
    pattern=*ptnArray;
    #if(BITS_PER_PIXEL==PIXEL_UNIT) /* 8 16 32 */
    { for(i=0;i<width;i++)
      { if((pattern<<k)&0x80)
	    { SET_PIXEL_LOGIC( PixelPos, *PixelPos, ForeColorIndex, dwRop );
	    }
        else if(OpaqueMode)
		{ SET_PIXEL_LOGIC( PixelPos, *PixelPos, BackColorIndex, dwRop );
		}
        PixelPos++;
        if(k<7)k++;
        else {k=0;pattern=ptnArray[++LineByteOffset];}
      }
    }
    #elif(BITS_PER_PIXEL==24)
    { for(i=0;i<width;i++)
      { if((pattern<<k)&0x80)
        { SET_3BYTE_LOGIC( PixelPos,ForeColorByte1,ForeColorByte2,ForeColorByte3,dwRop);;
	    }
        else if(OpaqueMode)
        { SET_3BYTE_LOGIC( PixelPos,BackColorByte1,BackColorByte2,BackColorByte3,dwRop);;
	    }
  		PixelPos=(PIXEL *)((BYTE *)PixelPos+3);
        if(k<7)k++;
        else {k=0;pattern=ptnArray[++LineByteOffset];}
      }
    }
    #else
    { PIXEL srcMask,srcValue,PixelBitOffset=BitOffset;
      for(i=0;i<width;i++)
      { if((pattern<<k)&0x80)
        { srcValue=OPT_OFFSET_PIXEL(ForeColorIndex,BITS_PER_PIXEL,PixelBitOffset);
          srcMask=(PIXEL)MASK_OFFSET_OPT(BITS_PER_PIXEL,PixelBitOffset);
          SET_PIXEL_LOGIC(&srcValue,*PixelPos,srcValue,dwRop);
          *PixelPos=(*PixelPos & ~srcMask)|(srcValue & srcMask);
        }
        else if(OpaqueMode)
        { srcValue=OPT_OFFSET_PIXEL(BackColorIndex,BITS_PER_PIXEL,PixelBitOffset);
          srcMask=(PIXEL)MASK_OFFSET_OPT(BITS_PER_PIXEL,PixelBitOffset);
          SET_PIXEL_LOGIC(&srcValue,*PixelPos,srcValue,dwRop);
          *PixelPos=(*PixelPos & ~srcMask)|(srcValue & srcMask);
        }
        GetNextPixelPosition( &PixelPos, &PixelBitOffset, PixelPos, PixelBitOffset );
        if(k<7)k++;
        else {k=0;pattern=ptnArray[++LineByteOffset];}
      }
    }
  #endif
    ptnArray+=BytesPerLine;
  }
  CM_ExposeCanvas((TWndCanvas *)dc,xPos,yPos,width,height);
}
//---------------------------------------------------------------------------
int GetTextSize(HDC dc,LPCSTR text,TSIZE *txtsize)
{
  if(text && *text)
  { BYTE ch=*text;
    TFont *tf;
    int nPlainText,colspace,rowspace;
    int textwidth=0,MaxWidth=0,textheight;
	if(dc){tf=((TWndCanvas *)dc)->Font;colspace=GetColSpace(dc);rowspace=GetRowSpace(dc);nPlainText=!(((TWndCanvas *)dc)->Pen.Mode & FS_PLAIN);}
	else {tf=g_RootWnd->Font;colspace=rowspace=0;nPlainText=true;}
	textheight=tf->height;
    while (ch)
    { if(ch>0xa0)
      { if(*++text)
        { textwidth=textwidth+tf->cWidth+colspace;
        }else break;
      }
	  else
      {  if((ch=='\r' || ch=='\n') && nPlainText)
         { if(textwidth>MaxWidth)
           { MaxWidth=textwidth;
           }
           textwidth=0;
           textheight+=(tf->height+rowspace);
           if(ch=='\r' && *(text+1)=='\n') text++;
         }
         else
         { textwidth=textwidth+tf->eWidth+colspace;
         }
      }
      ch=*++text;
    }
    if(textwidth<MaxWidth) textwidth=MaxWidth;
    if(txtsize)
    { txtsize->x=textwidth;
      txtsize->y=textheight;
    }
    return textwidth;
  }
  else
  { if(txtsize)txtsize->x=txtsize->y=0;
	return 0;
  }
}
//---------------------------------------------------------------------------
BOOL  TextOut(HDC dc, int xPos,int yPos,LPCSTR lptext)
{
  TFont *tf;
  BYTE ch,lowch;
  int textwidth,colspace,rowspace,hzkoffset,PenPosX,PenPosY,MaxPosX,nPlainText;
  if(!lptext || !*lptext) return false;
  else ch=*lptext;
  
  PenPosX=xPos;
  PenPosY=yPos;
  MaxPosX=0;
  colspace=GetColSpace(dc);
  rowspace=GetRowSpace(dc);
  nPlainText=!(((TWndCanvas *)dc)->Pen.Mode & FS_PLAIN);
  
  tf=((TWndCanvas *)dc)->Font;

  GroupOn(dc);
  
  while (ch)
  { 
    if(ch > 0xa0)
    {  lowch=*++lptext;
       if(lowch)
       { hzkoffset = ((ch - 0xA1) * 94 + (lowch - 0xA1))*tf->cBytes;
         DrawFontMatrix(dc,PenPosX,PenPosY,tf->cWidth,tf->height,&tf->cBits[hzkoffset]) ;
         PenPosX=PenPosX+tf->cWidth+colspace;
	   }
	   else break;
    }
    else 
    { if((ch=='\r' || ch=='\n') && nPlainText)
      { if(PenPosX>MaxPosX)
        { MaxPosX=PenPosX;
        }
        PenPosX=xPos;
        PenPosY=PenPosY+tf->height+rowspace;
        if(ch=='\r' && *(lptext+1)=='\n') lptext++;
      }
      else
      { hzkoffset=tf->eBytes*ch;
        DrawFontMatrix(dc,PenPosX,PenPosY,tf->eWidth,tf->height,&tf->eBits[hzkoffset]) ;
        PenPosX=PenPosX+tf->eWidth+colspace;
      }
    }

    ch = *++lptext;
  }

  if(PenPosX>MaxPosX) MaxPosX=PenPosX;
  textwidth = MaxPosX-xPos-colspace;
  GroupOff(dc,xPos,yPos, textwidth,PenPosY+tf->height-yPos);
  return textwidth;
}
//---------------------------------------------------------------------------
void GroupOn(HDC dc)
{ if(dc)((TWndCanvas *)dc)->Vram->GroupOperation++;
}
//---------------------------------------------------------------------------
void GroupOff(HDC dc,int x,int y,int width,int height)
{ 
  if(dc)
  { 
    VRAM *Vram=((TWndCanvas *)dc)->Vram;
    if( Vram->GroupOperation >= 2 )
	{  Vram->GroupOperation--;
	   return;
	}
	else
	{ Vram->GroupOperation=0;
	  CHECK_CANVAS_RECT((TWndCanvas *)dc,x,y,width,height);
	  CM_ExposeCanvas((TWndCanvas *)dc,x,y,width,height);
    }
  }
}
/*---------------------------------------------------------------------------
FUNCTION
    ExposeRootWnd
DESCRIPTION
    重新曝光桌面背景
    本函数有系统隐式调用，禁止用户手动调用
PROCESS
	首先将桌面的曝光区域与桌面上的各主窗口剪切后得到最终曝光区域
    然后在剪切后的新区域内重绘桌面背景
---------------------------------------------------------------------------*/
void ReexposeRootWnd(void)
{
  TREGION *pRgn = &g_RootWnd->Family->Vram->ExposeRegion;
  if(!IsRegionEmpty(pRgn))
  { RegionIntersectRect(pRgn, &g_RootWnd->WndRect); /*与桌面剪切*/
	if(!IsRegionEmpty(pRgn))
	{ PWND WndNode=g_RootWnd->Children;
	  while(WndNode)
	  { if(WndNode->unmapcount==0) 
	    { RegionSubtractRect(pRgn,&WndNode->WndRect);
	    }
        WndNode=WndNode->Next;
	  }
	  if(!IsRegionEmpty(pRgn))
	  { TClipRect *node=pRgn->head;
        while(node)
		{ 
          #if(LCD_FRAME_BUFFER)
		  { /*当g_RootWnd的VRAM复用作LCD的帧缓冲使用时，g_RootWnd窗体的曝光方式就要区别于其它主窗口:
			  即要重新绘制g_RootWnd窗体的图像数据，而不是简单的从窗体的Vram中恢复到屏幕。
			 */
			SendMessage((HWND)g_RootWnd,WM_ERASEBKGND,(WPARAM)g_RootCanvas,(LPARAM)&node->rc);
		  }
          #else
		  {  /*在没有使用LCD帧缓冲时，g_RootWindow有自已独立的VRAM，曝光g_RootWindow时不用重绘，即可直接从其Vram中恢复到屏幕*/
			 CanvasToLcd(g_RootCanvas,node->rc.left,node->rc.top,node->rc.right-node->rc.left,node->rc.bottom-node->rc.top);
          }
		  #endif
          node=node->next;

		}
	    g_RootCanvas->ClipRect=g_RootWnd->WndRect;
        ClearRegion(pRgn);
	  }
	}
  }
}
/*---------------------------------------------------------------------------
FUNCTION
    ReexposeTopWnd
DESCRIPTION
    重新曝光主窗口的VRAM映像
    本函数有系统隐式调用，禁止用户手动调用
PROCESS
	首先将该主窗口的曝光区域与覆盖其上的各主窗口剪切后得到最终曝光区域
    然后在剪切后的新区域内将主窗口的VRAM映像重出输出到桌面
---------------------------------------------------------------------------*/
void ReexposeTopWnd(HWND hWnd)
{
  if(IsWnd(hWnd) && !WNDPTR(hWnd)->unmapcount/*&& !WNDPTR(hWnd)->Family->Vram->GroupOperation*/)
  {	TREGION *pRgn=&WNDPTR(hWnd)->Family->Vram->ExposeRegion;
    if(IsRegionEmpty(pRgn))return;
    RegionIntersectRect(pRgn, &WNDPTR(hWnd)->WndRect);
	
    if(!IsRegionEmpty(pRgn))
	{ PWND sibwp=WNDPTR(hWnd)->Prev;
      while(sibwp)
	  { if(sibwp->unmapcount==0)
        { RegionSubtractRect(pRgn,&sibwp->WndRect);
	    }
        sibwp=sibwp->Prev;
	  }
	  if(!IsRegionEmpty(pRgn))
	  { TClipRect *node=pRgn->head;
		TWndCanvas TopCanvas;
	 	TopCanvas.Owner=hWnd;
        TopCanvas.VX = -WNDPTR(hWnd)->WndRect.left;
        TopCanvas.VY = -WNDPTR(hWnd)->WndRect.top;
        TopCanvas.Vram=WNDPTR(hWnd)->Family->Vram;
        while(node)
		{ CanvasToLcd(&TopCanvas,node->rc.left,node->rc.top,node->rc.right-node->rc.left,node->rc.bottom-node->rc.top);
          node=node->next;
		}
        ClearRegion(pRgn);
	  }
	}

  }
} 
//-------------------------------------------------------------------------------------
void CM_ScaleBoxOpt(TWndCanvas *desCanvas, int nXOriginDest,int nYOriginDest,int nWidthDest,int nHeightDest,
                    TWndCanvas *srcCanvas, int nXOriginSrc,int nYOriginSrc,int nWidthSrc,int nHeightSrc,DWORD dwRop)
{
  int xfactor = ((DWORD)nWidthSrc<<16)/nWidthDest;        /* scaled by 65536 */
  int yfactor = ((DWORD)nHeightSrc<<16)/nHeightDest;         /* scaled by 65536 */
  PIXEL  *srcLinePos, *desLinePos;
  int  srcBitOffset, desBitOffset;
  
  CanvasPixelLocate( &srcLinePos, &srcBitOffset, srcCanvas, nXOriginSrc, nYOriginSrc);
  CanvasPixelLocate( &desLinePos, &desBitOffset, desCanvas, nXOriginDest, nYOriginDest);
 
  #if(BITS_PER_PIXEL==PIXEL_UNIT)  /* 8 16 32 */
  { PIXEL *srcPos=srcLinePos;
    PIXEL *desPos=desLinePos;
    int desUnitPerLine=desCanvas->Vram->UnitPerLine;
    int srcUnitPerLine=srcCanvas->Vram->UnitPerLine;
    int y,sy=0;
    for (y = 0; y < nHeightDest;)
    { int x,sx,syint;
      while(y < nHeightDest)
      { sx=x=0;
        while (x < nWidthDest)
        { PIXEL_SET_LOGIC(desPos + x, *(srcPos + (sx >> 16)), dwRop);
          sx += xfactor;
          x++;
        }
        desPos+=desUnitPerLine;
        y++;
        syint = sy >> 16;
        sy += yfactor;
        if ((sy >> 16) != syint)
          break;
      } 
      srcPos = (PIXEL *)srcLinePos + (sy >> 16) * srcUnitPerLine;
    }
  }
  #elif(BITS_PER_PIXEL==24) 
  { BYTE *srcPos=(BYTE *)srcLinePos;
    BYTE *desPos=(BYTE *)desLinePos;
    int desBytePerLine=desCanvas->Vram->BytePerLine;
    int srcBytePerLine=srcCanvas->Vram->BytePerLine;
    int y, sy=0;
    for (y = 0; y < nHeightDest;)
    { int sx,syint;
      while(y < nHeightDest)
      { BYTE *dp=desPos;
        BYTE *dp_limit=desPos+nWidthDest*3;
        sx=0;
        while (dp < dp_limit)
        { BYTE *sp = srcPos + (sx >> 16)*3;
          SET_3BYTE_LOGIC(dp, *sp, *(sp+1),*(sp+2), dwRop);
          dp+=3;
          sp+=3;
          sx += xfactor;
        }
        desPos+=desBytePerLine;
        y++;
        syint = sy >> 16;
        sy += yfactor;
        if ((sy >> 16) != syint)
          break;
      } 
      srcPos = (BYTE *)srcLinePos + (sy >> 16) * srcBytePerLine;
    }
  }
  #elif(BITS_PER_PIXEL<8)
  { PIXEL *srcPos=srcLinePos;
    PIXEL *desPos=desLinePos;
    int desUnitPerLine=desCanvas->Vram->UnitPerLine;
    int srcUnitPerLine=srcCanvas->Vram->UnitPerLine;
    int y,sy=0;
    for (y = 0; y < nHeightDest;)
    { int x,sx,syint;
      while(y < nHeightDest)
      { PIXEL *sp,*dp=desPos;
        int srcOffset=srcBitOffset,desOffset=desBitOffset;
        sx=x=0;
        while (x < nWidthDest)
        {  PIXEL srcPixel,desMask;
           GetOffsetPixelPosition(&sp, &srcOffset, srcPos, srcBitOffset, (sx >> 16));
           srcPixel=UNIT_GET_BITS(PIXEL_UNIT,*sp,BITS_PER_PIXEL,srcOffset); 
           desMask=(PIXEL)MASK_OFFSET_OPT(BITS_PER_PIXEL,desOffset);
           PIXEL_OFFSET_OPT(srcPixel,BITS_PER_PIXEL,desOffset);
           SET_PIXEL_LOGIC(&srcPixel,*dp,srcPixel,dwRop); //raster option
           *dp=(*dp & ~desMask)|(srcPixel & desMask);
           sx += xfactor;
           x++;
           GetNextPixelPosition(&dp, &desOffset, dp, desOffset);
        }
        desPos+=desUnitPerLine;
        y++;
        syint = sy >> 16;
        sy += yfactor;
        if ((sy >> 16) != syint)
          break;
      } 
      srcPos = (PIXEL *)srcLinePos + (sy >> 16) * srcUnitPerLine;
    }
  }
  #endif
}
//-------------------------------------------------------------------------------------
void CM_ScaleBoxCopy(TWndCanvas *desCanvas, int nXOriginDest,int nYOriginDest,int nWidthDest,int nHeightDest,
                     TWndCanvas *srcCanvas, int nXOriginSrc,int nYOriginSrc,int nWidthSrc,int nHeightSrc)
{ int xfactor = ((DWORD)nWidthSrc<<16)/nWidthDest;        /* scaled by 65536 */
  int yfactor = ((DWORD)nHeightSrc<<16)/nHeightDest;         /* scaled by 65536 */
  PIXEL  *srcLinePos, *desLinePos;
  int  srcBitOffset, desBitOffset;
       
  CanvasPixelLocate( &srcLinePos, &srcBitOffset, srcCanvas, nXOriginSrc, nYOriginSrc);
  CanvasPixelLocate( &desLinePos, &desBitOffset, desCanvas, nXOriginDest, nYOriginDest);
 
  #if(BITS_PER_PIXEL==PIXEL_UNIT)  /* 8 16 32 */
  { PIXEL *srcPos=srcLinePos;
    PIXEL *desPos=desLinePos;
    int desUnitPerLine=desCanvas->Vram->UnitPerLine;
    int srcUnitPerLine=srcCanvas->Vram->UnitPerLine;
    int desAreaLineBytes=nWidthDest*UNIT_BYTE;
    int y, sy=0;
    for (y = 0; y < nHeightDest;)
    { int x=0,sx = 0;
      PIXEL *desPosOld = desPos;
      while (x < nWidthDest)
      { *(desPos + x) =  *(srcPos + (sx >> 16) );
        sx += xfactor;
        x++;
      }
      desPos+=desUnitPerLine;
      y++;
      while (y < nHeightDest) 
      { int syint = sy >> 16;
        sy += yfactor;
        if ((sy >> 16) != syint)
           break;
        /* Copy identical lines. */
        memcpy(desPos, desPosOld, desAreaLineBytes);
     
        desPos += desUnitPerLine;
        y++;
      }
      srcPos = (PIXEL *)srcLinePos + (sy >> 16) * srcUnitPerLine;
    }
  }
  #elif(BITS_PER_PIXEL==24) 
  { BYTE *srcPos=(BYTE *)srcLinePos;
    BYTE *desPos=(BYTE *)desLinePos;
    int desAreaLineBytes=nWidthDest*3;
    int y, sy=0;
    for (y = 0; y < nHeightDest;)
    { int x=0,sx = 0;
      BYTE *desPosOld = desPos;
      while (x < nWidthDest)
      { BYTE *dp=desPos + x*3;
        BYTE *sp=srcPos + (sx >> 16)*3;
        *dp =  *sp;
        *(dp+1)=*(sp+1);
        *(dp+2)=*(sp+2);
        sx += xfactor;
        x++;
      }
      desPos+=desCanvas->Vram->BytePerLine;
      y++;
      while (y < nHeightDest) 
      { int syint = sy >> 16;
        sy += yfactor;
        if ((sy >> 16) != syint)
          break;
        /* Copy identical lines. */
        memcpy(desPos, desPosOld, desAreaLineBytes);
            
        desPos += desCanvas->Vram->BytePerLine;
        y++;
      }
      srcPos = (BYTE *)srcLinePos + (sy >> 16) * srcCanvas->Vram->BytePerLine;
    }
  }
  #elif(BITS_PER_PIXEL<8)
  { PIXEL *srcPos=srcLinePos;
    PIXEL *desPos=desLinePos;
    int desUnitPerLine=desCanvas->Vram->UnitPerLine;
    int srcUnitPerLine=srcCanvas->Vram->UnitPerLine;
    int y,sy=0;
    for (y = 0; y < nHeightDest;)
    { int x,sx,syint;
      while(y < nHeightDest)
      { PIXEL *sp,*dp=desPos;
        int srcOffset=srcBitOffset,desOffset=desBitOffset;
        sx=x=0;
        while (x < nWidthDest)
        {  PIXEL srcPixel,desMask;
           GetOffsetPixelPosition(&sp, &srcOffset, srcPos, srcBitOffset, (sx >> 16));
           srcPixel=UNIT_GET_BITS(PIXEL_UNIT,*sp,BITS_PER_PIXEL,srcOffset); 
           desMask=(PIXEL)MASK_OFFSET_OPT(BITS_PER_PIXEL,desOffset);
           PIXEL_OFFSET_OPT(srcPixel,BITS_PER_PIXEL,desOffset);
           *dp=(*dp & ~desMask)| srcPixel;
           sx += xfactor;
           x++;
           GetNextPixelPosition(&dp, &desOffset, dp, desOffset);
        }
        desPos+=desUnitPerLine;
        y++;
        syint = sy >> 16;
        sy += yfactor;
        if ((sy >> 16) != syint)
          break;
      } 
      srcPos = (PIXEL *)srcLinePos + (sy >> 16) * srcUnitPerLine;
    }
  }
  #endif
}
//-------------------------------------------------------------------------------------
void CM_ScaleBoxFilterOpt(TWndCanvas *desCanvas, int nXOriginDest,int nYOriginDest,int nWidthDest,int nHeightDest,
                          TWndCanvas *srcCanvas, int nXOriginSrc,int nYOriginSrc,int nWidthSrc,int nHeightSrc,DWORD dwRop)
{
  int xfactor = ((DWORD)nWidthSrc<<16)/nWidthDest;        /* scaled by 65536 */
  int yfactor = ((DWORD)nHeightSrc<<16)/nHeightDest;         /* scaled by 65536 */
  PIXEL  *srcLinePos, *desLinePos;
  int  srcBitOffset, desBitOffset;
  PIXEL transparentPixel=srcCanvas->Pen.Background;
  CanvasPixelLocate( &srcLinePos, &srcBitOffset, srcCanvas, nXOriginSrc, nYOriginSrc);
  CanvasPixelLocate( &desLinePos, &desBitOffset, desCanvas, nXOriginDest, nYOriginDest);
 
  #if(BITS_PER_PIXEL == PIXEL_UNIT)  /* 8 16 32 */
  { PIXEL *srcPos=srcLinePos;
    PIXEL *desPos=desLinePos;
    int desUnitPerLine=desCanvas->Vram->UnitPerLine;
    int srcUnitPerLine=srcCanvas->Vram->UnitPerLine;
    int y,sy=0;
    for (y = 0; y < nHeightDest;)
    { int x,sx,syint;
      while(y < nHeightDest)
      { sx=x=0;
        while (x < nWidthDest)
        { PIXEL srcPixel=*(srcPos + (sx >> 16));
          if(srcPixel!=transparentPixel)
            PIXEL_SET_LOGIC(desPos + x, srcPixel, dwRop);
          sx += xfactor;
          x++;
        }
        desPos+=desUnitPerLine;
        y++;
        syint = sy >> 16;
        sy += yfactor;
        if ((sy >> 16) != syint)
          break;
      } 
      srcPos = (PIXEL *)srcLinePos + (sy >> 16) * srcUnitPerLine;
    }
  }
  #elif(BITS_PER_PIXEL == 24) 
  { BYTE *srcPos=(BYTE *)srcLinePos;
    BYTE *desPos=(BYTE *)desLinePos;
    int desBytePerLine=desCanvas->Vram->BytePerLine;
    int srcBytePerLine=srcCanvas->Vram->BytePerLine;
	BYTE filter_1=(BYTE)transparentPixel;
    BYTE filter_2=(BYTE)(transparentPixel>>8);
    BYTE filter_3=(BYTE)(transparentPixel>>16);
    BYTE srcByte_1,srcByte_2,srcByte_3;
    int y, sy=0;
    for (y = 0; y < nHeightDest;)
    { int sx,syint;
      while(y < nHeightDest)
      { BYTE *dp=desPos;
        BYTE *dp_limit=desPos+nWidthDest*3;
        sx=0;
        while (dp < dp_limit)
        { BYTE *sp = srcPos + (sx >> 16)*3;
		  srcByte_1=*sp++;
          srcByte_2=*sp++;
          srcByte_3=*sp++;
		  if(srcByte_1!=filter_1 || srcByte_2!=filter_2 || srcByte_3!=filter_3)
		  { SET_3BYTE_LOGIC(dp, srcByte_1, srcByte_2,srcByte_3, dwRop);
 		  }
          dp+=3;
          sx += xfactor;
        }
        desPos+=desBytePerLine;
        y++;
        syint = sy >> 16;
        sy += yfactor;
        if ((sy >> 16) != syint)
          break;
      } 
      srcPos = (BYTE *)srcLinePos + (sy >> 16) * srcBytePerLine;
    }
  }
  #elif(BITS_PER_PIXEL<8)
  { PIXEL *srcPos=srcLinePos;
    PIXEL *desPos=desLinePos;
    int desUnitPerLine=desCanvas->Vram->UnitPerLine;
    int srcUnitPerLine=srcCanvas->Vram->UnitPerLine;
    int y,sy=0;
    for (y = 0; y < nHeightDest;)
    { int x,sx,syint;
      while(y < nHeightDest)
      { PIXEL *sp,*dp=desPos;
        int srcOffset=srcBitOffset,desOffset=desBitOffset;
        sx=x=0;
        while (x < nWidthDest)
        {  PIXEL srcPixel;
           GetOffsetPixelPosition(&sp, &srcOffset, srcPos, srcBitOffset, (sx >> 16));
           srcPixel=UNIT_GET_BITS(PIXEL_UNIT,*sp,BITS_PER_PIXEL,srcOffset); 
           if( srcPixel != transparentPixel)
		   { PIXEL desMask=(PIXEL)MASK_OFFSET_OPT(BITS_PER_PIXEL,desOffset);
             PIXEL_OFFSET_OPT(srcPixel,BITS_PER_PIXEL,desOffset);
             SET_PIXEL_LOGIC(&srcPixel,*dp,srcPixel,dwRop); //raster option
             *dp=(*dp & ~desMask)|(srcPixel & desMask);
		   }
           sx += xfactor;
           x++;
           GetNextPixelPosition(&dp, &desOffset, dp, desOffset);
        }
        desPos+=desUnitPerLine;
        y++;
        syint = sy >> 16;
        sy += yfactor;
        if ((sy >> 16) != syint)
          break;
      } 
      srcPos = (PIXEL *)srcLinePos + (sy >> 16) * srcUnitPerLine;
    }
  }
  #endif
}
//-------------------------------------------------------------------------------------
void CM_ScaleBoxFilterCopy(TWndCanvas *desCanvas, int nXOriginDest,int nYOriginDest,int nWidthDest,int nHeightDest,
                           TWndCanvas *srcCanvas, int nXOriginSrc,int nYOriginSrc,int nWidthSrc,int nHeightSrc)
{
  int xfactor = ((DWORD)nWidthSrc<<16)/nWidthDest;        /* scaled by 65536 */
  int yfactor = ((DWORD)nHeightSrc<<16)/nHeightDest;         /* scaled by 65536 */
  PIXEL  *srcLinePos, *desLinePos;
  int  srcBitOffset, desBitOffset;
  PIXEL transparentPixel=srcCanvas->Pen.Background;
  CanvasPixelLocate( &srcLinePos, &srcBitOffset, srcCanvas, nXOriginSrc, nYOriginSrc);
  CanvasPixelLocate( &desLinePos, &desBitOffset, desCanvas, nXOriginDest, nYOriginDest);
 
  #if(BITS_PER_PIXEL == PIXEL_UNIT)  /* 8 16 32 */
  { PIXEL *srcPos=srcLinePos;
    PIXEL *desPos=desLinePos;
    int desUnitPerLine=desCanvas->Vram->UnitPerLine;
    int srcUnitPerLine=srcCanvas->Vram->UnitPerLine;
    int y,sy=0;
    for (y = 0; y < nHeightDest;)
    { int x,sx,syint;
      while(y < nHeightDest)
      { sx=x=0;
        while (x < nWidthDest)
        { PIXEL srcPixel=*(srcPos + (sx >> 16));
          if(srcPixel!=transparentPixel)
            *(desPos + x)=srcPixel;
          sx += xfactor;
          x++;
        }
        desPos+=desUnitPerLine;
        y++;
        syint = sy >> 16;
        sy += yfactor;
        if ((sy >> 16) != syint)
          break;
      } 
      srcPos = (PIXEL *)srcLinePos + (sy >> 16) * srcUnitPerLine;
    }
  }
  #elif(BITS_PER_PIXEL==24) 
  { BYTE *srcPos=(BYTE *)srcLinePos;
    BYTE *desPos=(BYTE *)desLinePos;
    int desBytePerLine=desCanvas->Vram->BytePerLine;
    int srcBytePerLine=srcCanvas->Vram->BytePerLine;
	BYTE filter_1=(BYTE)transparentPixel;
    BYTE filter_2=(BYTE)(transparentPixel>>8);
    BYTE filter_3=(BYTE)(transparentPixel>>16);
    BYTE srcByte_1,srcByte_2,srcByte_3;
    int y, sy=0;
    for (y = 0; y < nHeightDest;)
    { int sx,syint;
      while(y < nHeightDest)
      { BYTE *dp=desPos;
        BYTE *dp_limit=desPos+nWidthDest*3;
        sx=0;
        while (dp < dp_limit)
        { BYTE *sp = srcPos + (sx >> 16)*3;
		  srcByte_1=*sp++;
          srcByte_2=*sp++;
          srcByte_3=*sp++;
		  if(srcByte_1!=filter_1 || srcByte_2!=filter_2 || srcByte_3!=filter_3)
		  { *dp++=srcByte_1;
		    *dp++=srcByte_2;
			*dp++=srcByte_3;
 		  }
          sx += xfactor;
        }
        desPos+=desBytePerLine;
        y++;
        syint = sy >> 16;
        sy += yfactor;
        if ((sy >> 16) != syint)
          break;
      } 
      srcPos = (BYTE *)srcLinePos + (sy >> 16) * srcBytePerLine;
    }
  }
  #elif(BITS_PER_PIXEL < 8)
  { PIXEL *srcPos=srcLinePos;
    PIXEL *desPos=desLinePos;
    int desUnitPerLine=desCanvas->Vram->UnitPerLine;
    int srcUnitPerLine=srcCanvas->Vram->UnitPerLine;
    int y,sy=0;
    for (y = 0; y < nHeightDest;)
    { int x,sx,syint;
      while(y < nHeightDest)
      { PIXEL *sp,*dp=desPos;
        int srcOffset=srcBitOffset,desOffset=desBitOffset;
        sx=x=0;
        while (x < nWidthDest)
        {  PIXEL srcPixel;
           GetOffsetPixelPosition(&sp, &srcOffset, srcPos, srcBitOffset, (sx >> 16));
           srcPixel=UNIT_GET_BITS(PIXEL_UNIT,*sp,BITS_PER_PIXEL,srcOffset); 
           if( srcPixel != transparentPixel)
		   { PIXEL desMask=(PIXEL)MASK_OFFSET_OPT(BITS_PER_PIXEL,desOffset);
             PIXEL_OFFSET_OPT(srcPixel,BITS_PER_PIXEL,desOffset);
             *dp=(*dp & ~desMask)| srcPixel;
		   }
           sx += xfactor;
           x++;
           GetNextPixelPosition(&dp, &desOffset, dp, desOffset);
        }
        desPos+=desUnitPerLine;
        y++;
        syint = sy >> 16;
        sy += yfactor;
        if ((sy >> 16) != syint)
          break;
      } 
      srcPos = (PIXEL *)srcLinePos + (sy >> 16) * srcUnitPerLine;
    }
  }
  #endif
}
//-------------------------------------------------------------------------------------
//cmHorLineOpt 对指定的两行进行逻辑操作（包括按位与、或、异或、复制等）
//这时逻辑操作的对象是像素的索引号，而不是像素的RGB值。
void cmHorLineOpt(PIXEL *desAddr, int desOffset, PIXEL *srcAddr, int srcOffset, int width,DWORD dwRop)
{ 
   #if (BITS_PER_PIXEL==PIXEL_UNIT) /* 8 16 32 */
   { PIXEL *desEndPos=desAddr + width;
     while(desAddr<desEndPos)
	 { SET_PIXEL_LOGIC(desAddr,*desAddr,*srcAddr,dwRop);
       desAddr++;
       srcAddr++;
	 }
   }
   #elif(BITS_PER_PIXEL==24)
   { BYTE *desBytePos=(BYTE *)desAddr;
     BYTE *srcBytePos=(BYTE *)srcAddr;
     BYTE *desEndPos=desBytePos + width*3;
     while(desBytePos<desEndPos)
     { SET_3BYTE_LOGIC(desBytePos,*srcBytePos,*(srcBytePos+1),*(srcBytePos+3),dwRop);
       desBytePos+=3;
       srcBytePos+=3;
     }
   }
   #elif(BITS_PER_PIXEL<8)
   int totalBits;
   PIXEL bits,bitmask;
   totalBits = width << LOG_PIXEL_BITS;
   if(desOffset > 0)
   { if(totalBits < PIXEL_UNIT - desOffset)
     { bitmask = MASK_OFFSET_OPT(totalBits,desOffset);
       totalBits = 0;
     }
     else
     { bitmask = MASK_OFFSET_OPT(PIXEL_UNIT-desOffset,desOffset);
       totalBits -= (PIXEL_UNIT-desOffset);
     }
     if(desOffset<=srcOffset)
     { bits=*srcAddr++;
       if(desOffset<srcOffset) bits=PATTERN_SHIFT_LEFT(bits,srcOffset-desOffset) | PATTERN_SHIFT_RIGHT(*srcAddr,PIXEL_UNIT+desOffset-srcOffset);
     }
     else
     { bits=PATTERN_SHIFT_RIGHT(*srcAddr,desOffset-srcOffset);
     }
     SET_PIXEL_LOGIC(&bits,*desAddr,bits,dwRop);
     *desAddr++=(*desAddr)&(~bitmask) | (bits&bitmask);
   }
   srcOffset=(desOffset>srcOffset)?(PIXEL_UNIT+srcOffset-desOffset):(srcOffset-desOffset);
   desOffset=0;
   if(srcOffset>0)
   { int srcheadbits=PIXEL_UNIT-srcOffset;
     while(totalBits>=PIXEL_UNIT)
     { bits=*srcAddr++;
       bits=PATTERN_SHIFT_LEFT(bits,srcOffset) | PATTERN_SHIFT_RIGHT(*srcAddr,srcheadbits);
       SET_PIXEL_LOGIC(desAddr,*desAddr,bits,dwRop);
       desAddr++;
       totalBits-=PIXEL_UNIT;
     }
     if(totalBits>0)
     { bits=*srcAddr++;
       bits=PATTERN_SHIFT_LEFT(bits,srcOffset) | PATTERN_SHIFT_RIGHT(*srcAddr,srcheadbits);
       SET_PIXEL_LOGIC(&bits,*desAddr,bits,dwRop);
       bitmask=MASK_OFFSET_OPT(totalBits,0);
       *desAddr=( *desAddr & ~bitmask ) | ( bits & bitmask );
     }
   }
   else
   { while(totalBits>=PIXEL_UNIT)
     { SET_PIXEL_LOGIC(desAddr,*desAddr,*srcAddr,dwRop);
       desAddr++;
       srcAddr++;
       totalBits-=PIXEL_UNIT;
     }
     if(totalBits>0)
     { SET_PIXEL_LOGIC(&bits,*desAddr,*srcAddr,dwRop);
       bitmask=MASK_OFFSET_OPT(totalBits,0);
       *desAddr=( *desAddr & ~bitmask ) | ( bits & bitmask );
     }
   }
   #endif
}
//---------------------------------------------------------------------------
void cmHorLineXor(PIXEL *desAddr, int desOffset, PIXEL *srcAddr, int srcOffset, int width)
{ 
   #if (BITS_PER_PIXEL==PIXEL_UNIT) /* 8 16 32 */
   { PIXEL *desEndPos=desAddr + width;
     while(desAddr<desEndPos)
	 { *desAddr^=*srcAddr; //raster option
       desAddr++;
       srcAddr++;
	 }
   }
   #elif(BITS_PER_PIXEL == 24)
   { BYTE *desBytePos=(BYTE *)desAddr;
     BYTE *srcBytePos=(BYTE *)srcAddr;
     BYTE *desEndPos=desBytePos + width*3;
     while(desBytePos<desEndPos)
     { *desBytePos^=*srcBytePos; //raster option
       desBytePos++;
       srcBytePos++;
     }
   }
   #elif(BITS_PER_PIXEL < 8)
   int totalBits;
   PIXEL bits,bitmask;
   totalBits=width<<LOG_PIXEL_BITS;
   if(desOffset>0)
   { if(totalBits<PIXEL_UNIT-desOffset)
     { bitmask=MASK_OFFSET_OPT(totalBits,desOffset);
       totalBits=0;
     }
     else
     { bitmask=MASK_OFFSET_OPT(PIXEL_UNIT-desOffset,desOffset);
       totalBits-=(PIXEL_UNIT-desOffset);
     }
     if(desOffset<=srcOffset)
     { bits=*srcAddr++;
       if(desOffset<srcOffset) bits=PATTERN_SHIFT_LEFT(bits,srcOffset-desOffset) | PATTERN_SHIFT_RIGHT(*srcAddr,PIXEL_UNIT+desOffset-srcOffset);
     }
     else
     { bits=PATTERN_SHIFT_RIGHT(*srcAddr,desOffset-srcOffset);
     }
     bits^=*desAddr; //raster option
     *desAddr++=(*desAddr)&(~bitmask) | (bits&bitmask);
   }
   srcOffset=(desOffset>srcOffset)?(PIXEL_UNIT+srcOffset-desOffset):(srcOffset-desOffset);
   desOffset=0;
   if(srcOffset>0)
   { int srcheadbits=PIXEL_UNIT-srcOffset;
     while(totalBits>=PIXEL_UNIT)
     { bits=*srcAddr++;
       bits=PATTERN_SHIFT_LEFT(bits,srcOffset) | PATTERN_SHIFT_RIGHT(*srcAddr,srcheadbits);
       *desAddr^=bits;//raster option
       desAddr++;
       totalBits-=PIXEL_UNIT;
     }
     if(totalBits>0)
     { bits=*srcAddr++;
       bits=PATTERN_SHIFT_LEFT(bits,srcOffset) | PATTERN_SHIFT_RIGHT(*srcAddr,srcheadbits);
       bits^=*desAddr; //raster option
       bitmask=MASK_OFFSET_OPT(totalBits,0);
       *desAddr=( *desAddr & ~bitmask ) | ( bits & bitmask );
     }
   }
   else
   { while(totalBits>=PIXEL_UNIT)
     { *desAddr^=*srcAddr;//raster option
       desAddr++;
       srcAddr++;
       totalBits-=PIXEL_UNIT;
     }
     if(totalBits>0)
     { bits=*desAddr^*srcAddr;//raster option
       bitmask=MASK_OFFSET_OPT(totalBits,0);
       *desAddr=( *desAddr & ~bitmask ) | ( bits & bitmask );
     }
   }
   #endif
}
//---------------------------------------------------------------------------
void cmHorLineFilterOpt(PIXEL *desAddr, int desOffset, 
                        PIXEL *srcAddr, int srcOffset, 
						int width,PIXEL filterPixel,DWORD dwRop)
{ 
   #if (BITS_PER_PIXEL==PIXEL_UNIT) /* 8 16 32 */
   { PIXEL *desEndPos=desAddr + width;
     while(desAddr<desEndPos)
	 { if(*srcAddr!=filterPixel)
       { SET_PIXEL_LOGIC(desAddr,*desAddr,*srcAddr,dwRop);//raster option
       }
       desAddr++;
       srcAddr++;
	 }
   }
   #elif(BITS_PER_PIXEL==24)
   { BYTE filter_1=(BYTE)filterPixel;
     BYTE filter_2=(BYTE)(filterPixel>>8);
     BYTE filter_3=(BYTE)(filterPixel>>16);
     BYTE srcByte_1,srcByte_2,srcByte_3;
     BYTE *desBytePos=(BYTE *)desAddr;
     BYTE *srcBytePos=(BYTE *)srcAddr;
     BYTE *desEndPos=desBytePos + width*3;
     while(desBytePos<desEndPos)
     { srcByte_1=*srcBytePos++;
       srcByte_2=*srcBytePos++;
       srcByte_3=*srcBytePos++;
       if(srcByte_1==filter_1 && srcByte_2==filter_2 && srcByte_3==filter_3)
       { desBytePos+=3;
         continue;
       }
       SET_3BYTE_LOGIC(desBytePos,srcByte_1,srcByte_2,srcByte_3,dwRop);
       desBytePos+=3;
     }
   }
   #elif(BITS_PER_PIXEL<8)
   { PIXEL desMask,srcPixel,srcByte=*srcAddr,desByte=*desAddr;
     while(width-->0)
     { srcPixel=UNIT_GET_BITS(PIXEL_UNIT,srcByte,BITS_PER_PIXEL,srcOffset);
       if( srcPixel != filterPixel)
       { desMask=(PIXEL)MASK_OFFSET_OPT(BITS_PER_PIXEL,desOffset);
         PIXEL_OFFSET_OPT(srcPixel,BITS_PER_PIXEL,desOffset);
         SET_PIXEL_LOGIC(&srcPixel,desByte,srcPixel,dwRop); //raster option
         desByte=(desByte & ~desMask)|(srcPixel & desMask);
       }
       if( srcOffset < PIXEL_UNIT-BITS_PER_PIXEL )
       {  srcOffset += BITS_PER_PIXEL;
       }
       else
       { srcByte=*++srcAddr; 
         srcOffset=0;
       }
       if( desOffset < PIXEL_UNIT-BITS_PER_PIXEL )
       {  desOffset += BITS_PER_PIXEL;
       }
       else
       { *desAddr=desByte;
         desByte=*++desAddr; 
         desOffset=0;
       }
     }
     if(desOffset>0)
     { *desAddr=desByte;
     }
   }
   #endif
}
//---------------------------------------------------------------------------
void cmHorLineFilterCopy(PIXEL *desAddr, int desOffset, PIXEL *srcAddr, int srcOffset, int width,PIXEL filterPixel)
{  
   #if (BITS_PER_PIXEL==PIXEL_UNIT) /* 8 16 32 */
   { PIXEL *desEndPos=desAddr + width;
     while(desAddr<desEndPos)
	 { if(*srcAddr!=filterPixel)
       { *desAddr=*srcAddr; //raster option
       }
       desAddr++;
       srcAddr++;
	 }
   }
   #elif(BITS_PER_PIXEL==24)
   { BYTE filter_1=(BYTE)filterPixel;
     BYTE filter_2=(BYTE)(filterPixel>>8);
     BYTE filter_3=(BYTE)(filterPixel>>16);
     BYTE srcByte_1,srcByte_2,srcByte_3;
     BYTE *desBytePos=(BYTE *)desAddr;
     BYTE *srcBytePos=(BYTE *)srcAddr;
     BYTE *desEndPos=desBytePos + width*3;
     while(desBytePos<desEndPos)
     {  srcByte_1=*srcBytePos++;
        srcByte_2=*srcBytePos++;
        srcByte_3=*srcBytePos++;
        if(srcByte_1==filter_1 && srcByte_2==filter_2 && srcByte_3==filter_3)
        { desBytePos+=3;
          continue;
        }
       *desBytePos++=srcByte_1; //raster option
       *desBytePos++=srcByte_2; //raster option
       *desBytePos++=srcByte_3; //raster option
     }
   }
   #elif(BITS_PER_PIXEL<8)
   { PIXEL desMask,srcPixel,srcByte=*srcAddr,desByte=*desAddr;
     while(width-->0)
     { srcPixel=UNIT_GET_BITS(PIXEL_UNIT,srcByte,BITS_PER_PIXEL,srcOffset);
       if( srcPixel != filterPixel)
       { desMask=(PIXEL)MASK_OFFSET_OPT(BITS_PER_PIXEL,desOffset);
         PIXEL_OFFSET_OPT(srcPixel,BITS_PER_PIXEL,desOffset);
         desByte=(desByte & ~desMask)| srcPixel;
       }
       if( srcOffset < PIXEL_UNIT-BITS_PER_PIXEL )
       {  srcOffset += BITS_PER_PIXEL;
       }
       else
       { srcByte=*++srcAddr; 
         srcOffset=0;
       }
       if( desOffset < PIXEL_UNIT-BITS_PER_PIXEL )
       {  desOffset += BITS_PER_PIXEL;
       }
       else
       { *desAddr=desByte;
         desByte=*++desAddr; 
         desOffset=0;
       }
     }
     if(desOffset>0)
     { *desAddr=desByte;
     }
   }
   #endif
}
//---------------------------------------------------------------------------
void cmHorLineXorFilter(PIXEL *desAddr, int desOffset, PIXEL *srcAddr, int srcOffset, int width,PIXEL filterPixel)
{  
     #if (BITS_PER_PIXEL==PIXEL_UNIT) /* 8 16 32 */
   { PIXEL *desEndPos=desAddr + width;
     while(desAddr<desEndPos)
	 { if(*srcAddr!=filterPixel)
       { *desAddr ^= *srcAddr;//raster option
       }
       desAddr++;
       srcAddr++;
	 }
   }
   #elif(BITS_PER_PIXEL==24)
   { BYTE filter_1=(BYTE)filterPixel;
     BYTE filter_2=(BYTE)(filterPixel>>8);
     BYTE filter_3=(BYTE)(filterPixel>>16);
     BYTE srcByte_1,srcByte_2,srcByte_3;
     BYTE *desBytePos=(BYTE *)desAddr;
     BYTE *srcBytePos=(BYTE *)srcAddr;
     BYTE *desEndPos=desBytePos + width*3;
     while(desBytePos<desEndPos)
     { srcByte_1=*srcBytePos++;
       srcByte_2=*srcBytePos++;
       srcByte_3=*srcBytePos++;
       if(srcByte_1==filter_1 && srcByte_2==filter_2 && srcByte_3==filter_3)
       { desBytePos+=3;
         continue;
       }
       *desBytePos++ ^= srcByte_1;//raster option
       *desBytePos++ ^= srcByte_2;//raster option
       *desBytePos++ ^= srcByte_3;//raster option
     }
   }
   #elif(BITS_PER_PIXEL<8)
   { PIXEL desMask,srcPixel,srcByte=*srcAddr,desByte=*desAddr;
     while(width-->0)
     { srcPixel=UNIT_GET_BITS(PIXEL_UNIT,srcByte,BITS_PER_PIXEL,srcOffset);
       if( srcPixel != filterPixel)
       { desMask=(PIXEL)MASK_OFFSET_OPT(BITS_PER_PIXEL,desOffset);
         PIXEL_OFFSET_OPT(srcPixel,BITS_PER_PIXEL,desOffset);
         srcPixel^=desByte; //raster option
         desByte=(desByte & ~desMask)|(srcPixel & desMask);
       }
       if( srcOffset < PIXEL_UNIT-BITS_PER_PIXEL )
       {  srcOffset += BITS_PER_PIXEL;
       }
       else
       { srcByte=*++srcAddr; 
         srcOffset=0;
       }
       if( desOffset < PIXEL_UNIT-BITS_PER_PIXEL )
       {  desOffset += BITS_PER_PIXEL;
       }
       else
       { *desAddr=desByte;
         desByte=*++desAddr; 
         desOffset=0;
       }
     }
     if(desOffset>0)
     { *desAddr=desByte;
     }
   }
   #endif
}
/***************************************************************************
 *  FUNCTION 
 *    GetPosPixel
 *  DESCRIPTION
 *    返回画布上指定偏移坐标处的像素值
 ***************************************************************************/
int GetPosPixel(HDC dc,int xPos, int yPos)
{
  if(dc)
  { 
    xPos+=((TWndCanvas *)dc)->VX;
    xPos+=((TWndCanvas *)dc)->VY;
	
    if(xPos>=0 && yPos>=0)
    { 
      VRAM *vram=((TWndCanvas *)dc)->Vram;
      if(xPos<vram->Width && yPos<vram->Height)
      {  BYTE  *PixelBytePos=(BYTE *)vram->Buffer + yPos * vram->BytePerLine;
         #if(BITS_PER_PIXEL<8)
         { int  BitOffset=( xPos << LOG_PIXEL_BITS) & ( PIXEL_UNIT -1 ) ;
           PixelBytePos += ( xPos >> LOG_PIXEL_NUM );  
           return (PIXEL)UNIT_GET_BITS(PIXEL_UNIT,*PixelBytePos,BITS_PER_PIXEL,BitOffset);
         }
         #elif(BITS_PER_PIXEL==PIXEL_UNIT) /*8 16 32*/
         { PixelBytePos +=  xPos * PIXEL_BYTE;
           return *(PIXEL *)PixelBytePos;
         }
         #elif(BITS_PER_PIXEL==24)
         {  PIXEL myPixel;
            PixelBytePos +=  xPos *3;
            myPixel=*PixelBytePos++;
            myPixel|=*PixelBytePos++<<8;
            myPixel|=*PixelBytePos<<16;
            return myPixel;
         }
         #endif
      }
    }
  }
  return 0;
}
//---------------------------------------------------------------------------
#define  Check_Canvas_Map_Area(desCanvas,nXDest,nYDest,nWidth,nHeight,srcCanvas,nXSrc,nYSrc)\
{  if(nXSrc<0)  { nWidth+=nXSrc;  nXDest-=nXSrc; nXSrc=0; }\
   if(nYSrc<0)  { nHeight+=nYSrc; nYDest-=nYSrc; nYSrc=0; }\
   if(nXDest<(desCanvas)->ClipRect.left)\
   { nWidth -= (desCanvas)->ClipRect.left - (nXDest); \
     nXSrc += (desCanvas)->ClipRect.left - (nXDest);\
	 nXDest=(desCanvas)->ClipRect.left;\
   }\
   if(nYDest<(desCanvas)->ClipRect.top)\
   { nHeight -= (desCanvas)->ClipRect.top - (nYDest); \
     nYSrc += (desCanvas)->ClipRect.top - (nYDest); \
	 nYDest=(desCanvas)->ClipRect.top;\
   }\
   if(srcCanvas->VX + nXSrc + nWidth > srcCanvas->Vram->Width)\
      nWidth = srcCanvas->Vram->Width - srcCanvas->VX - nXSrc;\
   if(srcCanvas->VY + nYSrc + nHeight > srcCanvas->Vram->Height)\
      nHeight = srcCanvas->Vram->Height - srcCanvas->VY - nYSrc;\
   if(nXDest + nWidth > desCanvas->ClipRect.right)\
      nWidth = desCanvas->ClipRect.right -  nXDest;\
   if(nYDest + nHeight > desCanvas->ClipRect.bottom)\
      nHeight = desCanvas->ClipRect.bottom - nYDest;\
}
#define  Check_Canvas_StretchMap_Area(desCanvas,nXOriginDest,nYOriginDest,nWidthDest,nHeightDest,srcCanvas,nXOriginSrc,nYOriginSrc,nWidthSrc,nHeightSrc)\
{  if(nXOriginSrc<0)  { nWidthSrc+=nXOriginSrc;  nXOriginSrc=0; }\
   if(nYOriginSrc<0)  { nHeightSrc+=nYOriginSrc;  nYOriginSrc=0; }\
   if(nXOriginDest<(desCanvas)->ClipRect.left)\
   { nWidthDest -= (desCanvas)->ClipRect.left - (nXOriginDest); \
	 nXOriginDest=(desCanvas)->ClipRect.left;\
   }\
   if(nYOriginDest<(desCanvas)->ClipRect.top)\
   { nHeightDest -= (desCanvas)->ClipRect.top - (nYOriginDest); \
	 nYOriginDest = (desCanvas)->ClipRect.top;\
   }\
   if(srcCanvas->VX + nXOriginSrc + nWidthSrc > srcCanvas->Vram->Width)\
     nWidthSrc = srcCanvas->Vram->Width - srcCanvas->VX - nXOriginSrc;\
   if(srcCanvas->VY + nYOriginSrc + nHeightSrc > srcCanvas->Vram->Height)\
     nHeightSrc = srcCanvas->Vram->Height - srcCanvas->VY - nYOriginSrc;\
   if(nXOriginDest + nWidthDest > desCanvas->ClipRect.right)\
     nWidthDest = desCanvas->ClipRect.right - nXOriginDest;\
   if(nYOriginDest + nHeightDest > desCanvas->ClipRect.bottom)\
     nHeightDest = desCanvas->ClipRect.bottom - nYOriginDest;\
}
/***************************************************************************
 *  FUNCTION 
 *    ExDrawBitmap
 *  DESCRIPTION
 *     位图数据在画布之间的传输操作
 *  INPUT
 *    hdcDest    handle to destination device context 
 *    nXDest     x-coordinate of destination rectangle's upper-left corner
 *    nYDest     y-coordinate of destination rectangle's upper-left corner
 *    nWidth     width of destination rectangle 
 *    nHeight    height of destination rectangle 
 *    bmpSrc     source bitmap pointer 
 *    nXSrc      x-coordinate of source rectangle's upper-left corner  
 *    nYSrc      y-coordinate of source rectangle's upper-left corner
 *    dwRop      raster operation code
 ***************************************************************************/
void ExDrawBitmap(HDC hdcDest,int nXDest,int nYDest,int nWidth,int nHeight,TBitmap *bmpSrc,int nXSrc,int nYSrc)
{
  if(hdcDest && bmpSrc && bmpSrc->Handle && nWidth>0 && nHeight>0)
  { 
    DWORD dwRop=((TWndCanvas *)hdcDest)->Pen.Mode & 0x03;	
    if(!bmpSrc->Transparent) /*绘制不透明图*/
    { BitBlt(hdcDest,nXDest,nYDest,nWidth,nHeight,bmpSrc->Handle,nXSrc,nYSrc,dwRop);
    }
    else /*绘制透明图*/
    { TWndCanvas *desCanvas=(TWndCanvas *)hdcDest;
      TWndCanvas *srcCanvas=(TWndCanvas *)bmpSrc->Handle; 
      Check_Canvas_Map_Area(desCanvas,nXDest,nYDest,nWidth,nHeight,srcCanvas,nXSrc,nYSrc);
      if(nWidth>0 && nHeight>0 && srcCanvas->Vram)
      {  PIXEL transparentPixel=srcCanvas->Pen.Background;
         PIXEL  *srcLinePos, *desLinePos;
         int  i,srcBitOffset, desBitOffset;
         int  desUnitPerLine=desCanvas->Vram->UnitPerLine;
         int  srcUnitPerLine=srcCanvas->Vram->UnitPerLine;
         CanvasPixelLocate( &srcLinePos, &srcBitOffset, srcCanvas, nXSrc, nYSrc);
         CanvasPixelLocate( &desLinePos, &desBitOffset, desCanvas, nXDest, nYDest);
         if(dwRop==PL_REPLACE)
         { for(i = 0; i < nHeight; i++ )
           { cmHorLineFilterCopy( desLinePos, desBitOffset, srcLinePos, srcBitOffset, nWidth,transparentPixel);
             desLinePos+=desUnitPerLine;
             srcLinePos+=srcUnitPerLine;
           }
         }
         else if(dwRop==PL_XOR)
         { for(i = 0; i < nHeight; i++ )
           { cmHorLineXorFilter( desLinePos, desBitOffset, srcLinePos, srcBitOffset, nWidth,transparentPixel);
             desLinePos+=desUnitPerLine;
             srcLinePos+=srcUnitPerLine;
           }
         }
         else
         { for(i = 0; i < nHeight; i++ )
           { cmHorLineFilterOpt( desLinePos, desBitOffset, srcLinePos, srcBitOffset, nWidth,transparentPixel,dwRop);
             desLinePos+=desUnitPerLine;
             srcLinePos+=srcUnitPerLine;
           }
         }
		 CM_ExposeCanvas(desCanvas,nXDest,nYDest,nWidth,nHeight);
      }
    } 
  }
}
//---------------------------------------------------------------------------
void DrawBitmap(HDC hdcDest,int nXDest,int nYDest,TBitmap *bitmap)
{ if(hdcDest && bitmap)
  { ExDrawBitmap(hdcDest,nXDest,nYDest,bitmap->Width,bitmap->Height,bitmap,0,0);
  }
}
/***************************************************************************
 *  FUNCTION 
 *    BitBlt      ( bit blitter)  bit block image transfer
 *  DESCRIPTION
 *     bit blitter
 *     blitter means -- block image transfer
 *     The BitBlt function performs a bit-block transfer of the color data corresponding to a 
 *     rectangle of pixels from the specified source device context into a destination device context.   
 *     位图数据在画布之间的传输操作
 *  INPUT
 *    hdcDest    handle to destination device context 
 *    nXDest     x-coordinate of destination rectangle's upper-left corner
 *    nYDest     y-coordinate of destination rectangle's upper-left corner
 *    nWidth     width of destination rectangle 
 *    nHeight    height of destination rectangle 
 *    hdcSrc     handle to source device context 
 *    nXSrc      x-coordinate of source rectangle's upper-left corner  
 *    nYSrc      y-coordinate of source rectangle's upper-left corner
 *    dwRop      raster operation code
 ***************************************************************************/
void BitBlt(HDC hdcDest,int nXDest,int nYDest,int nWidth,int nHeight,
        HDC hdcSrc,int nXSrc,int nYSrc,DWORD dwRop)
{ 
  if(hdcDest && hdcSrc && nWidth>0 && nHeight>0)
  { 
    TWndCanvas *desCanvas=(TWndCanvas *)hdcDest;
    TWndCanvas *srcCanvas=(TWndCanvas *)hdcSrc;
	
    if(desCanvas->Vram && srcCanvas->Vram)
    { 
      Check_Canvas_Map_Area(desCanvas,nXDest,nYDest,nWidth,nHeight,srcCanvas,nXSrc,nYSrc);
      
	  if(nWidth>0 && nHeight>0)
      { 
        if(dwRop==PL_REPLACE) 
        { /*调用画布直接复制的优化算法*/ 
          CanvasCopy(desCanvas,nXDest,nYDest,srcCanvas,nXSrc,nYSrc,nWidth,nHeight);
        }
        else
        { 
	      PIXEL  *srcLinePos, *desLinePos;
          int  i,srcBitOffset, desBitOffset;
          int  desUnitPerLine=desCanvas->Vram->UnitPerLine;
          int  srcUnitPerLine=srcCanvas->Vram->UnitPerLine;
          CanvasPixelLocate( &srcLinePos, &srcBitOffset, srcCanvas, nXSrc, nYSrc);
          CanvasPixelLocate( &desLinePos, &desBitOffset, desCanvas, nXDest, nYDest);
          
		  if(dwRop==PL_XOR)
          { for(i = 0; i < nHeight; i++ )
            { cmHorLineXor( desLinePos, desBitOffset, srcLinePos, srcBitOffset, nWidth);
              desLinePos+=desUnitPerLine;
              srcLinePos+=srcUnitPerLine;
            }
          }
          else
          { for(i = 0; i < nHeight; i++ )
            { cmHorLineOpt( desLinePos, desBitOffset, srcLinePos, srcBitOffset, nWidth, dwRop );
              desLinePos+=desUnitPerLine;
              srcLinePos+=srcUnitPerLine;
            }
          }
        }
		CM_ExposeCanvas(desCanvas,nXDest,nYDest,nWidth,nHeight);
      }
    }
  }
}
/***************************************************************************
 *  FUNCTION 
 *    StretchBlt
 *  DESCRIPTION
 *     The StretchBlt function copies a bitmap from a source rectangle into a destination rectangle, 
 *     stretching or compressing the bitmap to fit the dimensions of the destination rectangle, if necessary.
 *     Windows stretches or compresses the bitmap according to the stretching mode currently set in
 *     the destination device context. 
 *  INPUT
 *     hdcDest          handle of destination device context 
 *     nXOriginDest     x-coordinate of upper-left corner of dest. rect. 
 *     nYOriginDest     y-coordinate of upper-left corner of dest. rect. 
 *     nWidthDest       width of destination rectangle 
 *     nHeightDest      height of destination rectangle 
 *     hdcSrc           handle of source device context 
 *     nXOriginSrc      x-coordinate of upper-left corner of source rectangle 
 *     nYOriginSrc      y-coordinate of upper-left corner of source rectangle 
 *     nWidthSrc        width of source rectangle 
 *     nHeightSrc       height of source rectangle 
 *     dwRop            raster operation code 
 ***************************************************************************/
void StretchBlt(HDC hdcDest,int nXOriginDest,int nYOriginDest,int nWidthDest,int nHeightDest,
                HDC hdcSrc,int nXOriginSrc,int nYOriginSrc,int nWidthSrc,int nHeightSrc,DWORD dwRop)
{
  if(hdcDest && hdcSrc && nWidthDest>0 && nHeightDest>0 && nWidthSrc>0 && nHeightSrc>0)
  { 
    TWndCanvas *desCanvas=(TWndCanvas *)hdcDest;
    TWndCanvas *srcCanvas=(TWndCanvas *)hdcSrc;  
    
	if(desCanvas->Vram && srcCanvas->Vram)
    { 
      Check_Canvas_StretchMap_Area(desCanvas,nXOriginDest,nYOriginDest,nWidthDest,nHeightDest,srcCanvas,nXOriginSrc,nYOriginSrc,nWidthSrc,nHeightSrc);
      if(nWidthSrc>0 && nHeightSrc>0 && nWidthDest>0 && nHeightDest>0)
      { if(nWidthSrc==nWidthDest && nHeightSrc==nHeightDest)
	    {  BitBlt(hdcDest,nXOriginDest,nYOriginDest,nWidthDest,nHeightDest,hdcSrc,nXOriginSrc,nYOriginSrc,dwRop);
	    }
        else if(dwRop==PL_REPLACE) 
        {  CM_ScaleBoxCopy(desCanvas, nXOriginDest,nYOriginDest,nWidthDest,nHeightDest,srcCanvas,nXOriginSrc,nYOriginSrc,nWidthSrc,nHeightSrc);
		   CM_ExposeCanvas(desCanvas,nXOriginDest,nYOriginDest,nWidthDest,nHeightDest);
		}
        else
        {  CM_ScaleBoxOpt(desCanvas, nXOriginDest,nYOriginDest,nWidthDest,nHeightDest,srcCanvas,nXOriginSrc,nYOriginSrc,nWidthSrc,nHeightSrc,dwRop);  
		   CM_ExposeCanvas(desCanvas,nXOriginDest,nYOriginDest,nWidthDest,nHeightDest);
		}
      }
    }
  }
}

/***************************************************************************
 *  FUNCTION 
 *    ExStretchDrawBitmap
 *  DESCRIPTION
 *     blitter means -- block image transfer
 *     位图数据在画布之间的传输操作
 *  INPUT
 *    hdcDest    handle to destination device context 
 *    nXDest     x-coordinate of destination rectangle's upper-left corner
 *    nYDest     y-coordinate of destination rectangle's upper-left corner
 *    nWidth     width of destination rectangle 
 *    nHeight    height of destination rectangle 
 *    bmpSrc     source bitmap pointer 
 *    nXSrc      x-coordinate of source rectangle's upper-left corner  
 *    nYSrc      y-coordinate of source rectangle's upper-left corner
 *    dwRop      raster operation code
 ***************************************************************************/
void ExStretchDrawBitmap(HDC hdcDest,int nXOriginDest,int nYOriginDest,int nWidthDest,int nHeightDest,
                         TBitmap *bmpSrc,int nXOriginSrc,int nYOriginSrc,int nWidthSrc,int nHeightSrc)
{ 
  if(hdcDest && bmpSrc && bmpSrc->Handle && nWidthDest>0 && nHeightDest>0 && nWidthSrc>0 && nHeightSrc>0)
  { DWORD dwRop=((TWndCanvas *)hdcDest)->Pen.Mode & 0x03;
    if(!bmpSrc->Transparent) /*绘制不透明图*/
    { StretchBlt(hdcDest,nXOriginDest,nYOriginDest,nWidthDest,nHeightDest,bmpSrc->Handle,nXOriginSrc,nYOriginSrc,nWidthSrc,nHeightSrc,dwRop);
    }
    else /*绘制透明图*/
    { TWndCanvas *desCanvas=(TWndCanvas *)hdcDest;
      TWndCanvas *srcCanvas=(TWndCanvas *)bmpSrc->Handle; 
      Check_Canvas_StretchMap_Area(desCanvas,nXOriginDest,nYOriginDest,nWidthDest,nHeightDest,srcCanvas,nXOriginSrc,nYOriginSrc,nWidthSrc,nHeightSrc);
      if(srcCanvas->Vram  && nWidthDest>0 && nHeightDest>0 && nWidthSrc>0 && nHeightSrc>0)
      {  if(nWidthSrc==nWidthDest && nHeightSrc==nHeightDest)
	     {  ExDrawBitmap(hdcDest,nXOriginDest,nYOriginDest,nWidthDest,nHeightDest,bmpSrc,nXOriginSrc,nYOriginSrc);
	     }
		 else if(dwRop==PL_REPLACE) 
         {  CM_ScaleBoxFilterCopy(desCanvas,nXOriginDest,nYOriginDest,nWidthDest,nHeightDest,srcCanvas,nXOriginSrc,nYOriginSrc,nWidthSrc,nHeightSrc);
		    CM_ExposeCanvas(desCanvas,nXOriginDest,nYOriginDest,nWidthDest,nHeightDest);
		 }
         else
		 {  CM_ScaleBoxFilterOpt(desCanvas,nXOriginDest,nYOriginDest,nWidthDest,nHeightDest,srcCanvas,nXOriginSrc,nYOriginSrc,nWidthSrc,nHeightSrc,dwRop);
		    CM_ExposeCanvas(desCanvas,nXOriginDest,nYOriginDest,nWidthDest,nHeightDest);
		 }
      }
    } 
  }
}
//---------------------------------------------------------------------------
void StretchDrawBitmap(HDC hdcDest,int nXDest,int nYDest,int nWidth,int nHeight,TBitmap *bitmap)
{ if(hdcDest && bitmap)
  { ExStretchDrawBitmap(hdcDest,nXDest,nYDest,nWidth,nHeight,bitmap,0,0,bitmap->Width,bitmap->Height);
  }
}
//---------------------------------------------------------------------------
TBitmap *CreateBitmap(void)
{ int AllocateSize=sizeof(TBitmap)+sizeof(TWndCanvas);
  TBitmap *bitmap=(TBitmap *)GetMem(AllocateSize);
  if(bitmap)
  { memset(bitmap,0,AllocateSize);
    bitmap->Handle=(HDC)((unsigned long)bitmap+sizeof(TBitmap));
  }
  return bitmap;
}
//---------------------------------------------------------------------------
void DestroyBitmap(TBitmap *bitmap)
{ if(bitmap && bitmap->Handle)
  { TWndCanvas *bmCanvas=(TWndCanvas *)bitmap->Handle;
    if(bmCanvas->Vram)
    { FreeVRAM(bmCanvas->Vram);
      bmCanvas->Vram=NULL;
    }
    bitmap->Handle=NULL;
    bitmap->Width=0;
    bitmap->Height=0;
    FreeMem(bitmap);
  }
}
//---------------------------------------------------------------------------
#if (BITS_PER_PIXEL<8)
  #define Expand_Bitmap_Extr_define   int unitbits=0,desByteBits=0;
  #define Expand_Bitmap_Line_Finish()\
     if(desByteBits) \
     { *LinePos=unitbits;\
       unitbits=desByteBits=0;\
     }\
     imgLineHead+=imgPitch;\
     LineHead+=vram->BytePerLine;
#else
  #define Expand_Bitmap_Extr_define   
  #define Expand_Bitmap_Line_Finish()\
     imgLineHead+=imgPitch;\
     LineHead+=vram->BytePerLine;   
#endif
 
#if (BITS_PER_PIXEL==PIXEL_UNIT) /* 8 16 32*/
  #define Expand_Bitmap_Judge() \
          { *(PIXEL *)LinePos=mappixel;\
            LinePos+=UNIT_BYTE;\
          } 
#elif (BITS_PER_PIXEL==24)
  #define Expand_Bitmap_Judge()\
          { *LinePos++=(BYTE)mappixel;\
            *LinePos++=(BYTE)(mappixel>>8);\
            *LinePos++=(BYTE)(mappixel>>16);\
          }
#elif (BITS_PER_PIXEL<8)
  #define Expand_Bitmap_Judge()\
          { unitbits|=OPT_OFFSET_PIXEL(mappixel,BITS_PER_PIXEL,desByteBits);\
            desByteBits+=BITS_PER_PIXEL;\
            if(desByteBits==8)\
            { *LinePos++=unitbits;\
              unitbits=desByteBits=0;\
            }\
          }
#endif
//---------------------------------------------------------------------------
BOOL Expand_Bitmap_Color_Less(VRAM *vram, TImageHead *imgHead)
{ Expand_Bitmap_Extr_define 
  int imgHeight=imgHead->height; 
  int imgWidth=imgHead->width; 
  int imgPitch=imgHead->pitch; 
  PIXEL *imgTransPal=imgHead->transPal; 
  BYTE *imgLineHead=imgHead->bits; 
  BYTE *LineHead=(BYTE *)vram->Buffer; 
  BYTE *LinePos,*imgBits; 
  PIXEL mappixel; 
  DWORD mapcolor;
  int   w;
  int   srcByteBits;  
  int   imgBpp=imgHead->bpp;
  int   imgBppMask=(1<<imgBpp)-1;
  while (--imgHeight >= 0) 
  { imgBits=imgLineHead;
    LinePos=LineHead;
    srcByteBits=0;
    for(w=0;w<imgWidth;w++)
    { if(srcByteBits==0)
      { mapcolor=*imgBits++;
        srcByteBits=8-imgBpp;
      }
      else 
      { srcByteBits-=imgBpp;
      }
      mappixel=imgTransPal[(mapcolor>>srcByteBits)&imgBppMask];
      Expand_Bitmap_Judge();
   }
   Expand_Bitmap_Line_Finish();
  }
  return true;
}
//---------------------------------------------------------------------------
BOOL Expand_Bitmap_Color_More(VRAM *vram, TImageHead *imgHead)
{ 
  Expand_Bitmap_Extr_define 
  int imgHeight=imgHead->height; 
  int imgWidth=imgHead->width; 
  int imgPitch=imgHead->pitch;
  
  PIXEL *imgTransPal=imgHead->transPal; 
  BYTE *imgLineHead=imgHead->bits; 
  BYTE *LineHead=(BYTE *)vram->Buffer; 
  BYTE *LinePos,*imgBits;
  PIXEL mappixel;
  
  int w; 
  int imgBpp=imgHead->bpp;
  DWORD mapcolor;
  RGBQUAD rgbColor;
  *(DWORD *)&rgbColor=0;
  while (--imgHeight >= 0) 
  { imgBits=(BYTE *)imgLineHead;
    LinePos=LineHead;
    for(w=0;w<imgWidth;w++)
    { switch(imgBpp)
      { case 8:  mappixel=imgTransPal[*imgBits++];
                 break;
        case 16: if(imgHead->palsize==(1<<16))
				 { /* RGB 565 */ 
                   #if(INDEX_RED_BITWIDTH==5 && INDEX_GREEN_BITWIDTH==6 && INDEX_BLUE_BITWIDTH==5)
                   mappixel=*(WORD *)imgBits;
                   #else
                   mapcolor=*(WORD *)imgBits;
                   rgbColor.r = (BYTE)( (mapcolor>>8)&0xf8 );
                   rgbColor.g = (BYTE)( (mapcolor>>3)&0xfC );
                   rgbColor.b = (BYTE)(  mapcolor<<3 );
                   mappixel=ColorMapToPixel(&rgbColor);
                   #endif
                   imgBits+=2;
				 }
				 else
				 { /* RGB 555 */ 
                   #if(INDEX_RED_BITWIDTH==5 && INDEX_GREEN_BITWIDTH==5 && INDEX_BLUE_BITWIDTH==5)
                   mappixel=*(WORD *)imgBits;
                   #else
                   mapcolor=*(WORD *)imgBits;
                   rgbColor.r = (BYTE)( (mapcolor>>7)&0xf8 );
                   rgbColor.g = (BYTE)( (mapcolor>>2)&0xf8 );
                   rgbColor.b = (BYTE)(  mapcolor<<3 );
                   mappixel=ColorMapToPixel(&rgbColor);
                   #endif
                   imgBits+=2;
				 }
                 break;
        case 24: rgbColor.b = *imgBits++;
                 rgbColor.g = *imgBits++;
                 rgbColor.r = *imgBits++;
                 #if(BITS_PER_PIXEL==24)
                 mappixel=*(DWORD *)&rgbColor;
                 #else
                 mappixel=ColorMapToPixel(&rgbColor);
                 #endif
                 break;
        case 32: rgbColor.b = *imgBits++;
                 rgbColor.g = *imgBits++;
                 rgbColor.r = *imgBits++;
                 rgbColor.a = *imgBits++;
                 #if(BITS_PER_PIXEL==24)
                 mappixel=*(DWORD *)&rgbColor;
                 #else
                 mappixel=ColorMapToPixel(&rgbColor);
                 #endif
                 break;

      }
      Expand_Bitmap_Judge();
   }
   Expand_Bitmap_Line_Finish();
  }
  return true;
}
//---------------------------------------------------------------------------
static BOOL OptimizeExpandBitmap(VRAM *vram,TImageHead *imgHead)
{ if(imgHead->bpp==BITS_PER_PIXEL)
  { BOOL pal_matched=true;
	if(imgHead->bpp<=8)
    { int i,palsize=imgHead->palsize;
      PIXEL *transPal=imgHead->transPal;
      for(i=0;i<palsize;i++)
      { if(transPal[i]!=(PIXEL)i)
        { pal_matched=false;
          break;
        }
      }
    }
    else if(BITS_PER_PIXEL==16)
	{  int effect_bpp=(imgHead->palsize==(1<<16))?16:15;
       if( effect_bpp != (INDEX_RED_BITWIDTH+INDEX_GREEN_BITWIDTH+INDEX_BLUE_BITWIDTH) ) 
	   { return false;
	   }
	}

    if(pal_matched)
    { if(imgHead->pitch == vram->BytePerLine)
      { DWORD datasize=imgHead->pitch * imgHead->height;
        memcpy(vram->Buffer,imgHead->bits,datasize);
      }
      else
      { int imgHeight=vram->Height;
        int lineBytesCount=min(imgHead->pitch,vram->BytePerLine);
        BYTE *imgBits=imgHead->bits;
        BYTE *vramBits=(BYTE *)vram->Buffer;
        while (--imgHeight >= 0) 
        { memcpy(vramBits,imgBits,lineBytesCount);
          vramBits+=vram->BytePerLine;
          imgBits+=imgHead->pitch;
        }
      }
      return true;
    }
  }
  return false;
}
//---------------------------------------------------------------------------
int CM_ExpandBitmap(TBitmap *bitmap,TImageHead *imgHead)
{ if(bitmap && bitmap->Handle)
  { TWndCanvas *bmCanvas=(TWndCanvas *)bitmap->Handle;
    if(bmCanvas->Vram)
    {  int ret=0;
       if(imgHead->bpp==BITS_PER_PIXEL)
       { ret=OptimizeExpandBitmap(bmCanvas->Vram,imgHead);
       }
       if(!ret)
       { if(imgHead->bpp<8)
         {  ret=Expand_Bitmap_Color_Less(bmCanvas->Vram,imgHead);
         }
         else
         {  ret=Expand_Bitmap_Color_More(bmCanvas->Vram,imgHead);
         }
       }
       if(ret)
       { /*以位图左上角像素值作为背静色*/
         bmCanvas->Pen.Background=GetPosPixel((HDC)bmCanvas,0,0);
         return true;
       }
    }
  }
  return false;
}
//---------------------------------------------------------------------------
static BOOL ResetBitmapSize(TBitmap *bitmap,int nWidth,int nHeight)
{ if(bitmap && bitmap->Handle)
  { TWndCanvas *bmCanvas=(TWndCanvas *)bitmap->Handle;
    if(bmCanvas->Vram)
    { if(bmCanvas->Vram->Width!=nWidth || bmCanvas->Vram->Height!=nHeight)
      { FreeVRAM(bmCanvas->Vram);
        bmCanvas->Vram=NULL;
      } else return true;
    }
	
    bmCanvas->Vram=CreateVRAM(0,0,nWidth,nHeight,NULL,false);
    if(bmCanvas->Vram)
    { bitmap->Width=nWidth;
      bitmap->Height=nHeight;
	  bmCanvas->ClipRect.left=bmCanvas->ClipRect.top=0;
	  bmCanvas->ClipRect.right=nWidth;
      bmCanvas->ClipRect.bottom=nHeight;
      return true;
    }
  }
  return false;
}
//---------------------------------------------------------------------------
void UnloadBitmap(TBitmap *bitmap)
{ if(bitmap && bitmap->Handle)
  { TWndCanvas *bmCanvas=(TWndCanvas *)bitmap->Handle;
    if(bmCanvas->Vram)
    { FreeVRAM(bmCanvas->Vram);
      bmCanvas->Vram=NULL;
    }
    bitmap->Width=0;
    bitmap->Height=0;
  }
}
//---------------------------------------------------------------------------
BOOL LoadBitmapFromFile(TBitmap *bitmap, const char* file_name)
{ TImageHead imgHead;
  STREAM *bmpFileStream;
  
  int ret=0;

  imgHead.bits=NULL;
  imgHead.transPal=NULL;
  imgHead.palsize=0;

  bmpFileStream = OpenFileStream(file_name);
  if(bmpFileStream)
  { BYTE MagicNumber[2];
	if(StreamRead(&MagicNumber,2,bmpFileStream)==2)
	{ if(MagicNumber[0]==0x42 && MagicNumber[1]==0x4d)
	  { ret = CM_DecodeBMP(&imgHead, bmpFileStream);
	  }
	  else if (MagicNumber[0]==0xFF && MagicNumber[1] == 0xD8)
	  { ret = CM_DecodeJPG(&imgHead, bmpFileStream);
	  }
    }
    CloseStream(bmpFileStream);
  }
  if(ret)
  { ret= ResetBitmapSize(bitmap,imgHead.width,imgHead.height);
    ret= ret && CM_ExpandBitmap(bitmap,&imgHead);
  }
  if(imgHead.transPal)FreeMem(imgHead.transPal);
  if(imgHead.bits)FreeMem(imgHead.bits);
  if(!ret)
  { UnloadBitmap(bitmap);
  }
  return ret;
}

//---------------------------------------------------------------------------
BOOL LoadBitmapFromMem(TBitmap *bitmap, const BYTE *imgData)
{ int ret=0;
  TImageHead imgHead;
  imgHead.bits=NULL;
  imgHead.transPal=NULL;
  imgHead.palsize=0;
      
  if(*imgData==0x42 && *(imgData+1)==0x4d) /* bmp */
  { STREAM *bmpMemStream = OpenMemStream(imgData,0x7FFFFFFFL);
    if(bmpMemStream)
    {  StreamSeek(bmpMemStream,2,SEEK_SET);
	   ret = CM_DecodeBMP(&imgHead, bmpMemStream);
    }
    CloseStream(bmpMemStream);
  }
  else if(*imgData==0xFF && *(imgData+1)==0xD8) /* JPG */
  { STREAM *bmpMemStream = OpenMemStream(imgData,0x7FFFFFFFL);
    if(bmpMemStream)
    {  StreamSeek(bmpMemStream,2,SEEK_SET);
	   ret = CM_DecodeJPG(&imgHead, bmpMemStream);
    }
    CloseStream(bmpMemStream);
  }
  else
  {   /* decode image data generated by Image2Lcd tool*/
	  ret = CM_DecodeLCDIMG(&imgHead, imgData); 
  }

  if(ret)
  { ret= ResetBitmapSize(bitmap,imgHead.width,imgHead.height);
    ret= ret && CM_ExpandBitmap(bitmap,&imgHead);
  }
  if(imgHead.transPal)FreeMem(imgHead.transPal);
  if(imgHead.bitsFromHeap && imgHead.bits)FreeMem(imgHead.bits);
  if(!ret)
  { UnloadBitmap(bitmap);
  }
  return ret;
}
//---------------------------------------------------------------------------
int CM_DecodeBMP(TImageHead *imgHead, STREAM *stream)
{ int size,i,PalBlockSize,biCompression;
  BOOL topdown_scanmode;

  BMPFILEHEADER fileheader;
  BMPINFOHEADER infoheader;
  RGBQUAD tempPal[256];

  size=StreamRead(&fileheader.bfSize,12,stream);
  if(size!=12)  return false;

  size=StreamRead(&infoheader,4,stream);
  if(size!=4)  return false;
  
  if(infoheader.BiSize>=sizeof(BMPINFOHEADER))
  { size=StreamRead((BYTE *)&infoheader+4,sizeof(BMPINFOHEADER)-4,stream);
    if(size!=sizeof(BMPINFOHEADER)-4) return false;
   
    i=infoheader.BiSize - sizeof(BMPINFOHEADER);
    if(!i) StreamSeek(stream,i,SEEK_CUR);

    PalBlockSize = (fileheader.bfOffBits - sizeof(BMPINFOHEADER) - 14);
    imgHead->palsize=PalBlockSize>>2;
    imgHead->width=infoheader.BiWidth;
	if(infoheader.BiHeight<0)
	{ topdown_scanmode=true;
	  imgHead->height=-infoheader.BiHeight;
	}
	else
	{ topdown_scanmode=false;
	  imgHead->height=infoheader.BiHeight;
	}
    imgHead->bpp=infoheader.BiBitCount; 
    biCompression=infoheader.BiCompression;

    if(imgHead->palsize>0 && imgHead->palsize<=256)
    { size=StreamRead(tempPal,PalBlockSize,stream);
      if(PalBlockSize!=size)  return false;
      if(imgHead->bpp<=8)
      { imgHead->transPal=(PIXEL *)GetMem(imgHead->palsize * sizeof(PIXEL));
        if(!imgHead->transPal)  return false;
		for(i=0;i<imgHead->palsize;i++)
        { imgHead->transPal[i]=ColorMapToPixel(&tempPal[i]);
        }
      }
    }

  }
  else if(infoheader.BiSize==sizeof(BMPCOREHEADER))
  { BMPCOREHEADER coreheader;
    size=StreamRead((BYTE *)&coreheader+4,sizeof(BMPCOREHEADER)-4,stream);
    if(size!=sizeof(BMPCOREHEADER)-4)  return false;
    imgHead->width=coreheader.bcWidth;

   	if(coreheader.bcHeight<0)
	{ topdown_scanmode=true;
	  imgHead->height=-coreheader.bcHeight;
	}
	else
	{ topdown_scanmode=false;
	  imgHead->height=coreheader.bcHeight;
	}
    imgHead->bpp=coreheader.bcBitCount;
    biCompression=0;
 
    PalBlockSize = (fileheader.bfOffBits - 26);
    imgHead->palsize = PalBlockSize / 3;
    if(imgHead->palsize>0 && imgHead->palsize<=256)
    { BYTE *myPal=(BYTE *)tempPal;
      size=StreamRead(myPal,PalBlockSize,stream);
      if(PalBlockSize!=size)  return false;
	  if(imgHead->bpp<=8)
	  { RGBQUAD rgbColor;
		imgHead->transPal=(PIXEL *)GetMem(imgHead->palsize * sizeof(PIXEL));
        if(!imgHead->transPal)  return false;
		rgbColor.a=0;
        for(i=0;i<imgHead->palsize;i++)
		{ rgbColor.b=*myPal++;
          rgbColor.g=*myPal++; 
          rgbColor.r=*myPal++;
          imgHead->transPal[i]=ColorMapToPixel(&rgbColor);
		}
	  }
	  else if(imgHead->bpp==16)
	  { biCompression=BI_BITFIELDS;
	  }
    }
  }
  else
  {  return false;
  }
  
  if(imgHead->bpp==16)
  { if(biCompression==BI_BITFIELDS && *(DWORD *)&tempPal[0] == 0xF800)
    { imgHead->palsize=1<<16; /*rgb565*/
	} else imgHead->palsize=1<<15;/*rgb555*/
  }
	
  imgHead->pitch = ( (imgHead->width*imgHead->bpp+31)>>3 ) & ~0x3;
  
  if(topdown_scanmode && biCompression!=BI_RLE4 && biCompression!=BI_RLE8  && stream->FromMem)
  { imgHead->bits=(BYTE *)stream->Handle+stream->MemPos;
	imgHead->bitsFromHeap=false;
  }
  else
  { BYTE *imgBits;
    int signed_imgPitch;
	imgHead->bits = (BYTE *)GetMem(imgHead->pitch * imgHead->height);
    if(!imgHead->bits) return false;
    imgHead->bitsFromHeap=true;

	if(topdown_scanmode)
	{ imgBits=imgHead->bits;
      signed_imgPitch=imgHead->pitch;
	}
    else
	{ imgBits=imgHead->bits + (imgHead->height-1)*imgHead->pitch;
      signed_imgPitch=-imgHead->pitch; 
	}
    switch(biCompression)
	{ case BI_RLE8: for(i=0; i< imgHead->height;i++) 
                    { if(!CM_DecodeRLE8(imgBits, stream))  return false;
	                  imgBits+=signed_imgPitch;
				    }
                    break;
                 
      case BI_RLE4: for(i=0; i< imgHead->height;i++) 
					{ if(!CM_DecodeRLE4(imgBits, stream))  return false;
	                  imgBits+=signed_imgPitch;
					}
		            break;
      default: if(topdown_scanmode)
			   { StreamRead(imgBits,imgHead->pitch*imgHead->height,stream);
			   }
               else
			   { for(i=0; i< imgHead->height;i++) 
			     { StreamRead(imgBits,imgHead->pitch,stream);
	               imgBits+=signed_imgPitch;
			     }
			   } 
	  }
  }

  return true;
}
//---------------------------------------------------------------------------
int CM_DecodeRLE8(BYTE *buf,STREAM *stream)
{ int c, n;
  BYTE *	p = buf;
  while(1)
  {	n = StreamGetByte(stream);
    switch(n) 
    { case EOF:  return(0);
	  case 0:    /* 0 = escape*/
             n = StreamGetByte(stream);	
			 switch(n) 
             { case 0: return(1);  /* 0 0 = end of current scan line*/
			   case 1: return(1);  /* 0 1 = end of data*/
			   case 2:  		   /* 0 2 xx yy delta mode - NOT SUPPORTED*/
				       (void)StreamGetByte(stream);
				       (void)StreamGetByte(stream);
				       continue;
			   default:			   /* 0 3..255 xx nn uncompressed data*/
				        for(c=0; c<n; c++)
				        	*p++ = StreamGetByte(stream);
				        if(n & 1)
					      (void)StreamGetByte(stream);
				        continue;
			}
	   default:
			  c = StreamGetByte(stream);
			  while(n--)
				*p++ = c;
			  continue;
	}
  }
}
//---------------------------------------------------------------------------
/*
 * Decode one line of RLE4, return 0 when done with all bitmap data
 */
int CM_DecodeRLE4(BYTE *buf, STREAM *stream)
{ static int	last;
  #define rle_put4(b)\
  { last = (last << 4) | (b);\
    if(++once == 2)\
    { *p++ = last;\
	  once = 0;\
    }\
  }  
  BYTE *p=buf;
  int	once=0,c, n, c1, c2;
  
  while(1) 
  { n = StreamGetByte(stream);
	switch(n) 
    { case EOF: return(0);
	  case 0:					/* 0 = escape*/
		     n =StreamGetByte(stream);
             switch(n) 
             { case 0: 			/* 0 0 = end of current scan line*/
				      if(once) 
                        rle_put4(0);
				      return(1);
			   case 1:				/* 0 1 = end of data*/
				      if(once)
					    rle_put4(0);
				      return(1);
			   case 2:				/* 0 2 xx yy delta mode - NOT SUPPORTED*/
			     	  (void)StreamGetByte(stream);
				      (void)StreamGetByte(stream);
				      continue;
			   default:			/* 0 3..255 xx nn uncompressed data*/
				       c2 = (n+3) & ~3;
				       for(c=0; c<c2; c++)
                       { if((c & 1) == 0)
							c1 = StreamGetByte(stream);
					     if(c < n)
							rle_put4((c1 >> 4) & 0x0f);
					     c1 <<= 4;
                       }
				       continue;
			}
	  default:
			c = StreamGetByte(stream);
			c1 = (c >> 4) & 0x0f;
			c2 = c & 0x0f;
			for(c=0; c<n; c++)
            {  rle_put4((c&1)? c2: c1);
            }
			continue;
		}
	}
}
//---------------------------------------------------------------------------
/* decode image data generated by Image2Lcd tool
*/
int  CM_DecodeLCDIMG(TImageHead *imgHead, const BYTE *pImage)
{
	TLcdImageHead *LcdIMG=(TLcdImageHead *)pImage;
    int LcdScanMode;

    LcdScanMode = LcdIMG->scan;
    if(LcdScanMode & 0x3)
    { DebugAlert(0, "Unknown image data format !\n");//非水平方式扫描的位图！
      return false;
    }

    imgHead->bpp=LcdIMG->bpp;
 
    if(LcdScanMode & 0x10)
    { imgHead->width=(LcdIMG->width[0]<<8)+LcdIMG->width[1];
      imgHead->height=(LcdIMG->height[0]<<8)+LcdIMG->height[1];
    }
    else
    { imgHead->width=LcdIMG->width[0] + (LcdIMG->width[1]<<8);
      imgHead->height=LcdIMG->height[0] + (LcdIMG->height[1]<<8);
    }
    
    if(imgHead->width<=0 || imgHead->height<=0)
    { return false;
    }

    imgHead->pitch = (imgHead->width*imgHead->bpp+7)/8;
    pImage+=sizeof(TLcdImageHead);    
    switch(imgHead->bpp)
    {  case 1:
       case 2:
       case 4: //LCD灰度图象
               { int i,j,palSize= 1<<imgHead->bpp;
                 DWORD rgbColor;
                 PIXEL *imgTransPal=(PIXEL *)GetMem(sizeof(PIXEL)*palSize);
                 if(!imgTransPal)return false;
                 else imgHead->transPal=imgTransPal;
                 imgHead->palsize=palSize;
                 j=imgHead->bpp<<1;
                 for(i=1;i<=palSize;i++)
                 { rgbColor= ( i * i * 255 ) >> j;
                   rgbColor=rgbColor|(rgbColor<<8)|(rgbColor<<16);
                   imgTransPal[palSize-i]=ColorMapToPixel((RGBQUAD *)&rgbColor);
                 }
               }
               break;
       case 8: /* 256 color */
               { int i,palSize;
                 PIXEL *imgTransPal;
                 RGBQUAD rgbColor;

                 if(LcdScanMode & 0x10)
                 {  palSize=*pImage++<<8;
                    palSize=palSize|*pImage++;
                 }
                 else
                 { palSize=*pImage++;
                   palSize=palSize|(*pImage++<<8);
                 }
                 if(palSize>0 && palSize<=256)
                 { imgHead->palsize=palSize;
                   imgTransPal=(PIXEL *)GetMem(sizeof(PIXEL)*palSize);
                   if(!imgTransPal)return false;
                   else imgHead->transPal=imgTransPal;
                 } else return false;
                 rgbColor.a=0;
                 for(i=0;i<palSize;i++)
                 { rgbColor.r = *pImage++;
                   rgbColor.g = *pImage++;
                   rgbColor.b = *pImage++;
                   imgTransPal[i]=ColorMapToPixel(&rgbColor);
                 }
               }
             break;
       case 16: { int is565=*pImage;
                  imgHead->palsize=(is565)?(1<<16):(1<<15);
				  pImage+=2; 
                }
                break;
       case 24:
       case 32: pImage+=2;break;
       default: return false; 
    }

    imgHead->bits=(BYTE *)pImage;
    imgHead->bitsFromHeap=false;

    return true;
}



//---------------------------------------------------------------------------
#ifdef JPEG_SUPPORT
//---------------------------------------------------------------------------
#include "jpeglib.h"
#include "jerror.h"

/* Expanded data source object for stdio input */
typedef struct {
  struct jpeg_source_mgr pub;        /* public fields */

  STREAM *stream;                /* source stream */
  BYTE * buffer;                /* start of buffer */
  BOOL start_of_file;        /* have we gotten any data yet? */
} my_source_mgr;

#define INPUT_BUF_SIZE  4096        /* choose an efficiently fread'able size */

typedef my_source_mgr * my_src_ptr;
//---------------------------------------------------------------------------
/*
 * Initialize source --- called by jpeg_read_header
 * before any data is actually read.
 */

static void init_source (j_decompress_ptr cinfo)
{
  my_src_ptr src = (my_src_ptr) cinfo->src;

  /* We reset the empty-input-file flag for each image,
   * but we don't clear the input buffer.
   * This is correct behavior for reading a series of images from one source.
   */
  src->start_of_file = TRUE;
}

/*
 * Fill the input buffer --- called whenever buffer is emptied.
 */

static BOOL fill_input_buffer (j_decompress_ptr cinfo)
{
  my_src_ptr src = (my_src_ptr) cinfo->src;
  size_t nbytes;

  nbytes = StreamRead ( src->buffer,INPUT_BUF_SIZE,(STREAM *)src->stream);

  if (nbytes <= 0)
  {  if (src->start_of_file)        /* Treat empty input file as fatal error */
        ERREXIT(cinfo, JERR_INPUT_EMPTY);
    WARNMS(cinfo, JWRN_JPEG_EOF);
    /* Insert a fake EOI marker */
    src->buffer[0] = (JOCTET) 0xFF;
    src->buffer[1] = (JOCTET) JPEG_EOI;
    nbytes = 2;
  }

  src->pub.next_input_byte = src->buffer;
  src->pub.bytes_in_buffer = nbytes;
  src->start_of_file = FALSE;

  return TRUE;
}

/*
 * Skip data --- used to skip over a potentially large amount of
 * uninteresting data (such as an APPn marker).
 */

static void skip_input_data (j_decompress_ptr cinfo, long num_bytes)
{
  my_src_ptr src = (my_src_ptr) cinfo->src;

  /* Just a dumb implementation for now.  Could use fseek() except
   * it doesn't work on pipes.  Not clear that being smart is worth
   * any trouble anyway --- large skips are infrequent.
   */
  if (num_bytes > 0) {
    while (num_bytes > (long) src->pub.bytes_in_buffer) {
      num_bytes -= (long) src->pub.bytes_in_buffer;
      (void) fill_input_buffer(cinfo);
      /* note we assume that fill_input_buffer will never return FALSE,
       * so suspension need not be handled.
       */
    }
    src->pub.next_input_byte += (size_t) num_bytes;
    src->pub.bytes_in_buffer -= (size_t) num_bytes;
  }
}

/*
 * An additional method that can be provided by data source modules is the
 * resync_to_restart method for error recovery in the presence of RST markers.
 * For the moment, this source module just uses the default resync method
 * provided by the JPEG library.  That method assumes that no backtracking
 * is possible.
 */


/*
 * Terminate source --- called by jpeg_finish_decompress
 * after all data has been read.  Often a no-op.
 *
 * NB: *not* called by jpeg_abort or jpeg_destroy; surrounding
 * application must deal with any cleanup that should happen even
 * for error exit.
 */

static void term_source (j_decompress_ptr cinfo)
{
  /* no work necessary here */
}

/*
 * Prepare for input from a stdio stream.
 * The caller must have already opened the stream, and is responsible
 * for closing it after finishing decompression.
 */

//---------------------------------------------------------------------------
void my_jpeg_data_src (j_decompress_ptr cinfo, STREAM *stream)
{
  my_src_ptr src;

  /* The source object and input buffer are made permanent so that a series
   * of JPEG images can be read from the same file by calling jpeg_stdio_src
   * only before the first one.  (If we discarded the buffer at the end of
   * one image, we'd likely lose the start of the next one.)
   * This makes it unsafe to use this manager and a different source
   * manager serially with the same JPEG object.  Caveat programmer.
   */
  if (cinfo->src == NULL)         /* first time for this JPEG object? */
  {  cinfo->src = (struct jpeg_source_mgr *)
      (*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_PERMANENT,
                                  sizeof(my_source_mgr));
    src = (my_src_ptr) cinfo->src;
    src->buffer = (JOCTET *)
      (*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_PERMANENT,
                                  INPUT_BUF_SIZE * sizeof(JOCTET));
  }

  src = (my_src_ptr) cinfo->src;
  src->pub.init_source = init_source;
  src->pub.fill_input_buffer = fill_input_buffer;
  src->pub.skip_input_data = skip_input_data;
  src->pub.resync_to_restart = jpeg_resync_to_restart; /* use default method */
  src->pub.term_source = term_source;
  src->stream=stream;
  src->pub.bytes_in_buffer = 0; /* forces fill_input_buffer on first read */
  src->pub.next_input_byte = NULL; /* until buffer loaded */

}

int  CM_DecodeJPG(TImageHead *imgHead, STREAM *stream)
{   int ret = 0;    /* image load error*/
    unsigned char magic[5];

    /* This struct contains the JPEG decompression parameters
     * and pointers to working space 
     * (which is allocated as needed by the JPEG library).
     */

    struct jpeg_decompress_struct cinfo;
    struct jpeg_error_mgr jerr;

    StreamRead (magic, 4, stream);
    StreamRead (magic, 4, stream);
    magic [4] = '\0';
 
    if (strncmp((char *)magic, "JFIF", 4) != 0 && strncmp((char *)magic, "Exif", 4) != 0)
        return 0;        /* not JPEG image*/
   
    StreamSeek(stream, -10, SEEK_CUR);
  

    /* Step 1: allocate and initialize JPEG decompression object */

    /* We set up the normal JPEG error routines. */
    cinfo.err = jpeg_std_error (&jerr);

    /* Now we can initialize the JPEG decompression object. */
    jpeg_create_decompress (&cinfo);

    /* Step 2: specify data source */
     my_jpeg_data_src (&cinfo, stream);

    /* Step 3: read file parameters with jpeg_read_header() */
    jpeg_read_header (&cinfo, TRUE);

     /* Step 4: set parameters for decompression */
    cinfo.out_color_space = JCS_RGB;
    cinfo.quantize_colors = FALSE;


    #if (BITS_PER_PIXEL <= 8) 
	{  RGBQUAD rgbColor;
       int i;
      
	   cinfo.quantize_colors = TRUE;
    
       /* Get system palette */
       cinfo.actual_number_of_colors = 0x01 << BITS_PER_PIXEL;
    
	   /* Allocate jpeg colormap space */
        cinfo.colormap = (*cinfo.mem->alloc_sarray)((j_common_ptr) &cinfo, JPOOL_IMAGE,
                       (JDIMENSION)cinfo.actual_number_of_colors, (JDIMENSION)3);
        
		imgHead->transPal=(PIXEL *)GetMem(cinfo.actual_number_of_colors*sizeof(PIXEL));
		imgHead->palsize=cinfo.actual_number_of_colors;
        
		/* Set colormap from system palette */
        for(i = 0; i < cinfo.actual_number_of_colors; ++i)
        {  PixelMapToColor(i,&rgbColor);
           cinfo.colormap[0][i] = rgbColor.b;
           cinfo.colormap[1][i] = rgbColor.g;
           cinfo.colormap[2][i] = rgbColor.r;
		   imgHead->transPal[i]=i;
        }
    }
	#else
	   imgHead->transPal=NULL;
	   imgHead->palsize=0;
    #endif

    jpeg_calc_output_dimensions (&cinfo);

    ret = 0;//ERR_BMP_MEM;

    imgHead->bpp = cinfo.output_components <<3;
    
    imgHead->width = cinfo.output_width;
    imgHead->height = cinfo.output_height;

    imgHead->pitch = ( (imgHead->width*imgHead->bpp+31)>>3 ) & ~0x3;
    
    imgHead->bits = (BYTE *)GetMem(imgHead->pitch * imgHead->height);
    imgHead->bitsFromHeap=true;

    if (!imgHead->bits) goto err;

    /* Step 5: Start decompressor */
    jpeg_start_decompress (&cinfo);

    /* Step 6: while (scan lines remain to be read) */
    while (cinfo.output_scanline < cinfo.output_height)
	{   JSAMPROW rowptr[1];
        BYTE* bits;
        bits = imgHead->bits + cinfo.output_scanline * imgHead->pitch;
        rowptr[0] = (JSAMPROW)(bits);
        jpeg_read_scanlines (&cinfo, rowptr, 1);
    }
    ret = TRUE;

err:
    /* Step 7: Finish decompression */
    jpeg_finish_decompress (&cinfo);

    /* Step 8: Release JPEG decompression object */
    jpeg_destroy_decompress (&cinfo);

    /* May want to check to see whether any corrupt-data
     * warnings occurred (test whether jerr.pub.num_warnings is nonzero).
     */

    return ret;
}
//---------------------------------------------------------------------------
#else
  int  CM_DecodeJPG(TImageHead *imgHead, STREAM *stream)
  { return 0;
  }
//---------------------------------------------------------------------------
#endif 