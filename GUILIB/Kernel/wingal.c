/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:			wingal.c
*  PROGRAMMER:			ming.c
*  Date of Creation:		2006/08/8
*
*  DESCRIPTION: 			
*						
*  NOTE:						 		
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*  void  DirectToLCD(VRAM *,int,int,int,int);  //Direct output to LCD,without LCD frame buffer.
*  void  GUI_TaskSuspendControl(BOOL suspend); 
* -----------------------------------------------------------------------------
*
*  MODIFICATION HISTORY
*     LastModify  2006/12/01
******************************************************************************/
#include "mingui.h"
/*****************************************************************************/


//---------------------------------------------------------------------------
//将Vram中指定的区域(起点从标[left,top])刷新到LCD
//其中left,top是相对Vram的坐标，相对屏幕的坐标是(Vram->Left+left,Vram->Top+top)
//---------------------------------------------------------------------------
#if (BITS_PER_PIXEL==1)
//---------------------------------------------------------------------------
extern   void  LcdDataWrite(BYTE bits);
extern   void  LcdPageSet(int page,int column);
//---------------------------------------------------------------------------
#define  LcdPageBitsBigEndia        false
static   void VramPageToLCD(BYTE *,int,int,int,int,int,int,int);
static   BYTE LcdTransArray[(LCD_HEIGHT+7)>>3][LCD_WIDTH];
extern   void  VramPixelLocate(BYTE **PixelPos, int *BitOffset, VRAM *SrcVram, int X, int Y );
//---------------------------------------------------------------------------
void  DirectToLCD(VRAM *Vram, int left, int top, int width, int height)
{ int VramUnitPerLine,col,x2,y2,page,lastpage,pre_lastpage,linebitoffset,topbitpffset;
  unsigned char *linepos;
  VramPixelLocate( &linepos, &linebitoffset, Vram, left, top);
  VramUnitPerLine = Vram->UnitPerLine;
  left+=Vram->Left;
  top+=Vram->Top;
  y2=top+height;
  page=top>>3;
  lastpage=(y2-1)>>3;
  
  topbitpffset=top&0x7;
  if(topbitpffset)
  { int topbitpffset2=(page==lastpage)?topbitpffset+height:8;
    VramPageToLCD(linepos,linebitoffset,VramUnitPerLine,page,left,width,topbitpffset,topbitpffset2);
    if(page==lastpage) goto FRAME_BUFFER_TO_LCD;
	page++;
	linepos+=(topbitpffset2-topbitpffset)*VramUnitPerLine;
  }

  pre_lastpage=(y2>>3)-1;
  for(;page<=pre_lastpage;page++)
  { VramPageToLCD(linepos,linebitoffset,VramUnitPerLine,page,left,width,0,8);
    linepos+=(VramUnitPerLine<<3);
  }

  if(page==lastpage)
  { int botbitpffset=y2&0x7;
	VramPageToLCD(linepos,linebitoffset,VramUnitPerLine,page,left,width,0,botbitpffset);
  }
 
  FRAME_BUFFER_TO_LCD:
  x2=left+width;
  for(page=top>>3;page<=lastpage;page++)
  { LcdPageSet(page,left);
    for(col=left;col<x2;col++)
    { LcdDataWrite(LcdTransArray[page][col]);
    }
  }
}
//---------------------------------------------------------------------------
#if (LcdPageBitsBigEndia==true)
   #define LCD_PAGE_BITS_OFFSET(bitwidth,bitoffset)   ( ((1<<(bitwidth))-1) << ( PIXEL_UNIT - (bitoffset) - (bitwidth) ) )
#else
   #define LCD_PAGE_BITS_OFFSET(bitwidth,bitoffset)   ( ((1<<(bitwidth))-1) << (bitoffset) )
#endif
//---------------------------------------------------------------------------
#if (PIXEL_BIG_ENDIAN==true)
   #define UNIT_GET_BITS(unitwidth,value,bitwidth,bitoffset)   ( ((value)>>((unitwidth)-(bitwidth)-(bitoffset)))& BITS_MASK(bitwidth) )
#else
   #define UNIT_GET_BITS(unitwidth,value,bitwidth,bitoffset)    ( ((value)>>(bitoffset))&BITS_MASK(bitwidth) )
#endif
//---------------------------------------------------------------------------
void VramPageToLCD(BYTE *pixelpos,int bitoffset,int VramUnitPerLine,int page,int col,int width,int yoff1,int yoff2)
{ int i,j,pre_right,rightbitoffset;  
  unsigned char *p,bitmask,b_bitmask,bits,PixelBlock[8];
 
  if(yoff2-yoff1<8)
  {  bitmask=LCD_PAGE_BITS_OFFSET(yoff2-yoff1,yoff1);
     b_bitmask=~bitmask;
  }
  else
  {  bitmask=0;
  }

  if(bitoffset>0)
  { BOOL only_one_unit=(bitoffset+width<8);
	int headbitoffset2=(only_one_unit)?bitoffset+width:8;
	p=pixelpos++;
    for(i=yoff1;i<yoff2;i++)
	{ PixelBlock[i]=*p;
      p+=VramUnitPerLine;
	}
    for(i=bitoffset;i<headbitoffset2;i++)
	{ bits=0;
	  for(j=yoff1;j<yoff2;j++)
	  { if(UNIT_GET_BITS(PIXEL_UNIT,PixelBlock[j],BITS_PER_PIXEL,i))
	    { bits|= LCD_PAGE_BITS_OFFSET(BITS_PER_PIXEL,j) ;
	    }
	  }
	  if(bitmask)bits=(bits&bitmask)|( LcdTransArray[page][col]&b_bitmask);
      LcdTransArray[page][col++]=bits; 
	}
	if(only_one_unit)return;
	else width-=(8-bitoffset);
  }
  
  pre_right=col+ ( width&~0x7);
  while(col<pre_right)
  {  p=pixelpos++;
	 for(i=yoff1;i<yoff2;i++)
     { PixelBlock[i]=*p;
	   p+=VramUnitPerLine;
     }
     for(i=0;i<8;i++)
	 { bits=0;
	   for(j=yoff1;j<yoff2;j++)
	   { if(UNIT_GET_BITS(PIXEL_UNIT,PixelBlock[j],BITS_PER_PIXEL,i))
	     { bits|= LCD_PAGE_BITS_OFFSET(BITS_PER_PIXEL,j) ;
	     }
	   }
	  if(bitmask)bits=(bits&bitmask)|(LcdTransArray[page][col]&b_bitmask);
      LcdTransArray[page][col++]=bits; 
	 }
  }
 
  rightbitoffset=width&0x7; 
  if(rightbitoffset)
  {  p=pixelpos++;
	 for(i=yoff1;i<yoff2;i++)
     { PixelBlock[i]=*p;
	   p+=VramUnitPerLine;
     }
     for(i=0;i<rightbitoffset;i++)
	 { bits=0;
	   for(j=yoff1;j<yoff2;j++)
	   { if(UNIT_GET_BITS(PIXEL_UNIT,PixelBlock[j],BITS_PER_PIXEL,i))
	     { bits|= LCD_PAGE_BITS_OFFSET(BITS_PER_PIXEL,j) ;
	     }
	   }
	   if(bitmask)bits=(bits&bitmask)|(LcdTransArray[page][col]&b_bitmask);
       LcdTransArray[page][col++]=bits; 
	 }
  }
  
}
//---------------------------------------------------------------------------
#elif (BITS_PER_PIXEL>1)
//---------------------------------------------------------------------------
void  DirectToLCD(VRAM *Vram, int x, int y, int width, int height)
{  printf("You have not defined LCD_FRAME_BUFFER address,and not implemented DirectToLCD function!\n");
}
//---------------------------------------------------------------------------
#endif












//---------------------------------------------------------------------------
#if(BASE_OS_TYPE==2)        /*NUCLEUS OS操作系统下*/
#include "nucleus.h"
//---------------------------------------------------------------------------
NU_SEMAPHORE  *MsgTskSemaphore=NULL;
void GUI_TaskSuspendControl(BOOL SuspendOrResume)
{ if(!MsgTskSemaphore)
  { MsgTskSemaphore=(NU_SEMAPHORE  *)GetMem(sizeof(NU_SEMAPHORE));   /*定义NUCLEUS下信号量*/
    if(!MsgTskSemaphore)return;
	else memset(MsgTskSemaphore,0,sizeof(NU_SEMAPHORE));
    NU_Create_Semaphore(MsgTskSemaphore,"MsgTskSemaphore", 0, NU_FIFO);
  }
  if(SuspendOrResume)
  { NU_Obtain_Semaphore(MsgTskSemaphore, NU_SUSPEND);
  }
  else
  { NU_Release_Semaphore(MsgTskSemaphore);
  }
}
//---------------------------------------------------------------------------
#elif(BASE_OS_TYPE>2)
//---------------------------------------------------------------------------
void GUI_TaskSuspendControl(BOOL suspend)
{ printf("You have not defined GUI_TaskSuspendControl() !\n");

}
//---------------------------------------------------------------------------
#endif
//---------------------------------------------------------------------------








//---------------------------------------------------------------------------
//  File system interface
//---------------------------------------------------------------------------
/*Open an existing file for read.*/
HANDLE File_OpenRead(const char *filename)
{ FILE *f=fopen(filename,"r+b"); 
  return (HANDLE)f;
}


/* Close file. */
void File_CloseRead(HANDLE hFile)
{ fclose((FILE *)hFile);
}


/* Read a byte from file stream. */
unsigned char File_ReadByte(HANDLE hFile)
{ return fgetc((FILE *)hFile); 
}


/* Read array of bytes from file stream and return actually size of read. */
int File_Read(HANDLE hFile,void *buffer, int bytes)
{ return fread(buffer,1,bytes,(FILE *)hFile);
}


/* locate file read position. */
void File_Seek(HANDLE hFile,int offset, int whence)
{ fseek( (FILE *)hFile,offset, whence);
}
//---------------------------------------------------------------------------