/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      winmsg.c
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
*      To My JunHang
*  MODIFICATION HISTORY
*     LastModify  2006/10/30
******************************************************************************/
#include "mingui.h"
//---------------------------------------------------------------------------
typedef struct
{ char  *startaddress,*endaddress;
  char  *readpos,*writepos;
  int    messagesize;
}TMsgQueue;
//---------------------------------------------------------------------------
extern BOOL g_SomeWndNeedPaint;
extern BOOL PeekSimuMessage(void);
extern BOOL CheckPaintMessage(PMSG);
extern void GUI_TaskSuspendControl(BOOL);
TMsgQueue  g_SysMsgQueue;
/*---------------------------------------------------------------------------
FUNCTION
   CreateMsgQueue
DESCRIPTION
  创建消息队列
  每个消自己大小:  sizeof(TMSG)
  队列中消息总数:  MSG_QUEUE_CAPACITY
---------------------------------------------------------------------------*/
void WINAPI InitMsgQueue(int size)
{   int MsgQueueBufSize=sizeof(TMSG)*size;
    g_SysMsgQueue.messagesize=sizeof(TMSG);
    g_SysMsgQueue.startaddress=(char *)GetMem(MsgQueueBufSize);
    g_SysMsgQueue.endaddress=g_SysMsgQueue.startaddress+MsgQueueBufSize;
    g_SysMsgQueue.readpos=g_SysMsgQueue.startaddress;
    g_SysMsgQueue.writepos=g_SysMsgQueue.startaddress;
}
/*---------------------------------------------------------------------------
FUNCTION
   DestroyMsgQueue
DESCRIPTION
  销毁消息队列
---------------------------------------------------------------------------*/
void  WINAPI FreeMsgQueue(void)
{ if(g_SysMsgQueue.startaddress)
  { FreeMem(g_SysMsgQueue.startaddress);
    memset(&g_SysMsgQueue,0,sizeof(TMsgQueue));
  }
} 
/*---------------------------------------------------------------------------
FUNCTION
   GetMessage
DESCRIPTION
   从当前线程的GUI消息队列中读取一条消息，并将该消息从队列中删除。
   如果消息队列为空，则当前线程被阻塞/挂起，直至读取到消息后返回
OUTPUTS
  读取成功则返回true
---------------------------------------------------------------------------*/
BOOL WINAPI GetMessage(PMSG Msg)
{  while(!PeekMessage(Msg,true))
   { /*GUI task hung up*/
     #if(BASE_OS_TYPE>0) 
     GUI_TaskSuspendControl(true);
	 #endif 
   }
   return (Msg->Message!=WM_QUIT);
}
/*---------------------------------------------------------------------------
FUNCTION
   PostMessage
DESCRIPTION
   将指定消息发送到窗口的消息队列，然后不管发送成功与否，直接返回。
   消息队列溢出时，溢出的消息被丢弃。PostMessage不阻塞，直接返回。
OUTPUTS
  发送成功则返回true,否则返回false
---------------------------------------------------------------------------*/
BOOL WINAPI PostMessage(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam)
{ if(IsWnd(hWnd))
  {  switch(Message) 
     { case WM_PAINT: if(!WndGetAttr(hWnd,WS_NEEDPAINT))
					  { g_SomeWndNeedPaint=true;
                        WndAddAttr(hWnd,WS_NEEDPAINT);
                        WNDPTR(hWnd)->Family->CanvasChanged=true;
                      }
                      return true;		
       
       case WM_EXPOSE: if(!WNDPTR(hWnd)->Family->CanvasChanged)
					   { WNDPTR(hWnd)->Family->CanvasChanged=true;
                         g_SomeWndNeedPaint=true;
					   }
                       return true;

	   default:  
		   {  char *oldwritepos=g_SysMsgQueue.writepos;
		      char *newwritepos=oldwritepos+g_SysMsgQueue.messagesize;
              if(newwritepos>=g_SysMsgQueue.endaddress)
			  {  newwritepos=g_SysMsgQueue.startaddress;
			  } 
              if(newwritepos!=g_SysMsgQueue.readpos)
			  { extern TWndDateTime  g_SysTime;
		        TMSG *msg=(TMSG *)g_SysMsgQueue.writepos;
                msg->Handle=hWnd;
                msg->Message=Message;
                msg->WParam=WParam;
                msg->LParam=LParam;
		        msg->Time=(DWORD)g_SysTime;
				g_SysMsgQueue.writepos=newwritepos;

                #if(BASE_OS_TYPE>0) 
                if(g_SysMsgQueue.readpos==oldwritepos)
				{ GUI_TaskSuspendControl(false);/*GUI task resume*/
				}
                #endif 
				
                return true;
			  }
		   }
	 }
  }
  return false;
}

/*---------------------------------------------------------------------------
FUNCTION
   PeekMessage
DESCRIPTION
   从目标应用程序的消息队列中读取一条消息,如果消息队列为空，直接返回.
   布尔型参数Remove的值，决定消息读取后是否从窗口消息队列中删除。
OUTPUTS
  成功读取到一条消息则返回true,否则返回false
---------------------------------------------------------------------------*/
BOOL WINAPI PeekMessage(PMSG Msg,BOOL Remove)
{  /* On PC-Simulator*/
   #if(ON_SIMULATOR) 
   PeekSimuMessage();
   #endif

   if(g_SysMsgQueue.readpos!=g_SysMsgQueue.writepos)
   { memcpy(Msg,g_SysMsgQueue.readpos,g_SysMsgQueue.messagesize);
     if(Remove) 
     { g_SysMsgQueue.readpos+=g_SysMsgQueue.messagesize;
       if(g_SysMsgQueue.readpos>=g_SysMsgQueue.endaddress)
         g_SysMsgQueue.readpos=g_SysMsgQueue.startaddress;
     }
     return true;
   }

   if(g_SomeWndNeedPaint)
   { return CheckPaintMessage(Msg);
   }

   return false;  
}
/*---------------------------------------------------------------------------
FUNCTION
   RemoveMessage
DESCRIPTION
    Remove all messages from msg queue for the target window
---------------------------------------------------------------------------*/
void WINAPI RemoveMessage(HWND hWnd)
{  PMSG  readpos=(PMSG)g_SysMsgQueue.readpos;
   while(readpos!=(PMSG)g_SysMsgQueue.writepos)
   {  if(readpos->Handle==hWnd)
      {  if(readpos==(PMSG)g_SysMsgQueue.readpos)
         { g_SysMsgQueue.readpos+=g_SysMsgQueue.messagesize;
         }else readpos->Handle=NULL;
      }
      readpos++;
	  if((char *)readpos>=g_SysMsgQueue.endaddress)
	  { readpos=(PMSG)g_SysMsgQueue.startaddress;
	  }
    }
}


/*---------------------------------------------------------------------------
FUNCTION
  ErrorMessage
DESCRIPTION
  Show error information
---------------------------------------------------------------------------*/
void ErrorMessage(int errCode, LPCSTR errMsg)
{  
 #if(ON_SIMULATOR)
 { extern void  SimuMsgbox(LPCSTR,LPCSTR, ...);
   SimuMsgbox("--debug info--",errMsg);
 }
 #else
 { extern void DesktopTextout(LPCSTR);
   DesktopTextout(errMsg);
 }
 #endif

 if(errCode==-1)
 { /*If the error is fatal, this function cannot return,
     you may view the call stack to find the error source. */
   while(1);
 }
}
/*---------------------------------------------------------------------------
END --- Thank you!                                                     ming.c
---------------------------------------------------------------------------*/




