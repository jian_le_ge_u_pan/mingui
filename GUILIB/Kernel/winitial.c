/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      winitial.c
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
*      To My JunHang
*  MODIFICATION HISTORY
*     LastModify  2006/10/30
******************************************************************************/
#include "mingui.h"
//---------------------------------------------------------------------------
//16 key NUMPAD Keyboard chars map
static const char *NUMPAD_MapChars[16]=
{/*VK_NUMPAD0  */    "0 "
,/*VK_NUMPAD1  */    "1@./:;~_*#$%^&\\|'\",`"
,/*VK_NUMPAD2  */    "2abcABC"
,/*VK_NUMPAD3  */    "3defDEF"
,/*VK_NUMPAD4  */    "4ghiGHI"
,/*VK_NUMPAD5  */    "5jklJKL"
,/*VK_NUMPAD6  */    "6mnoMNO"
,/*VK_NUMPAD7  */    "7pqrsPQRS"
,/*VK_NUMPAD8  */    "8tuvTUV"
,/*VK_NUMPAD9  */    "9wxyzWXYZ"
,/*VK_MULTIPLY */    "*+-=?!<>(){}[]"
,/*VK_ADD      */    "#"
,/*VK_SEPARATOR*/    ","
,/*VK_SUBTRACT */    "-"
,/*VK_DECIMAL  */    "."
,/*VK_DIVIDE   */    "/"
};
//---------------------------------------------------------------------------
static  BOOL GUI_Running=false;
extern  TWndCanvas *DC_FreeList;
extern  void  *GUI_LcdFrameBuffer;
extern  TBLOCKHEAP g_ClipRegionHeap;
extern  TREGION WndClipRegion;
extern char *g_SysDefaultFont;
extern int  g_SysClockBaseCycle,g_SysBitsPerPixel,g_SysLcdWidth,g_SysLcdHeight;
//---------------------------------------------------------------------------
extern void  SimulatorInitialize(void *,void *,void *,int,int,int,int,int,int,int,int);
extern void *SimulatorAllocateLcdFrameBuffer(void);
extern void  SimulatorDestroyLcdFrameBuffer(void);
extern void *Simulator_GetMem(unsigned long bSize);
extern void  GUI_TimerPulse(void);
extern void  DispatchUserMsg(DWORD msg,DWORD wParam,DWORD lParam);
extern void  GUI_RegisterControls(void);
extern void  GUI_RegisterFonts(void);
extern BOOL  CreateRootWindow(void);
extern void  DestroyRootWindow(void);
extern void  InitMsgQueue(int size);
extern void  FreeMsgQueue(void);
/*---------------------------------------------------------------------------
FUNCTION
  GUI_Initialize
DESCRIPTION
  GUI系统初始化，由GUI隐式隐式调用，不允许用户手动调用
---------------------------------------------------------------------------*/
BOOL GUI_Initialize(void)
{ if(!GUI_Running)
  { GUI_Running=true;
    
    #if(ON_SIMULATOR)
    InitMem((DWORD)Simulator_GetMem(HeapTotalSize),HeapTotalSize); 
	SimulatorInitialize(GUI_TimerPulse,DispatchUserMsg,PixelMapToColor,GUI_CLK_CYCLE,LCD_WIDTH,LCD_HEIGHT,BITS_PER_PIXEL,PIXEL_BIG_ENDIAN,INDEX_RED_BITWIDTH,INDEX_GREEN_BITWIDTH,INDEX_BLUE_BITWIDTH);
	#else
	InitMem(HeapStartAddress,HeapTotalSize);
    #endif
  
	g_SysClockBaseCycle=GUI_CLK_CYCLE;
	g_SysBitsPerPixel=BITS_PER_PIXEL;
	g_SysLcdWidth=LCD_WIDTH;
	g_SysLcdHeight=LCD_HEIGHT;
	g_SysDefaultFont=SYS_DEFAULT_FONT;

    #if(DC_PRELOAD_COUNT>0)
    { DC_FreeList=(TWndCanvas *)GetMem(DC_PRELOAD_COUNT * sizeof(TWndCanvas));
      if(DC_FreeList)
      { int i=0;
        TWndCanvas *node=DC_FreeList;
        while(i<DC_PRELOAD_COUNT)
        { node->Next = node+1;
          node->Prev = node-1;
          node++;
          i++;
        }
        DC_FreeList->Prev=DC_FreeList+DC_PRELOAD_COUNT-1;
        DC_FreeList->Prev->Next=DC_FreeList;
      }
    }
    #endif
	InitBlockDataHeap(&g_ClipRegionHeap, sizeof (TClipRect), CLIPRGN_HEAP_SIZE);

	InitRegion(&WndClipRegion, &g_ClipRegionHeap);

	InitMsgQueue(MSG_QUEUE_CAPACITY);
    
	NUMPAD_CharsMap(NUMPAD_MapChars);

	#if(LCD_FRAME_BUFFER)
      #if(ON_SIMULATOR)
      GUI_LcdFrameBuffer=SimulatorAllocateLcdFrameBuffer();
      #endif
      if(!GUI_LcdFrameBuffer)GUI_LcdFrameBuffer=(void *)LCD_FRAME_BUFFER;
    #else
	  GUI_LcdFrameBuffer=NULL;
    #endif

    GUI_RegisterFonts();
	if(!CreateRootWindow()) return false;
    GUI_RegisterControls();
	
  }
  return true;
}
/*---------------------------------------------------------------------------
FUNCTION
  GUI_Destroy
DESCRIPTION
  销毁GUI系统，释放占用资源，包括：
    (1)销毁调色板
    (2)销毁LCD驱动所占用的资源
  该函数在销毁GUI系统最后一个窗体时被隐式调用，不允许用户手动调用
---------------------------------------------------------------------------*/
void GUI_Destroy(void)
{ if(GUI_Running)
  { FreeBlockDataHeap(&g_ClipRegionHeap);
	FreeMsgQueue();
	DestroyRootWindow();
	#if(ON_SIMULATOR) 
     SimulatorDestroyLcdFrameBuffer();
    #endif 
	GUI_LcdFrameBuffer=NULL;
    GUI_Running=false;/*GUI服务结束标*/
  }
}

/*---------------------------------------------------------------------------
END --- Thank you!                                                     ming.c
---------------------------------------------------------------------------*/




