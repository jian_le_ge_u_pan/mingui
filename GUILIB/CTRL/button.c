/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      Button.c
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
* 
*  MODIFICATION HISTORY
*     LastModify  2006/10/30
******************************************************************************/
#include "mingui.h"
//---------------------------------------------------------------------------
static void Button_Repaint(HWND hWnd)
{ char *BtnCaption;
  HDC dc=BeginPaint(hWnd);
  int clientWidth=crWidth(hWnd);
  int clientHeight=crHeight(hWnd);
  
  /*��λͼ*/
  if(WNDPTR(hWnd)->Logo)
  { int x = (clientWidth - WNDPTR(hWnd)->Logo->Width)/2;
    int y = (clientHeight - WNDPTR(hWnd)->Logo->Height)/2;
    DrawBitmap(dc,x,y,WNDPTR(hWnd)->Logo);
  }

  /*������*/
  BtnCaption=GetWindowText(hWnd);
  if(BtnCaption && *BtnCaption)
  {	SetPenLogic(dc,PL_REPLACE);
    if(WndGetAttr(hWnd,WS_DISABLED))
    { SetColor(dc,CL_WHITE);
      DrawText(dc,1,1,clientWidth,clientHeight,BtnCaption,alCenter|alMiddle);
      SetColor(dc,CL_DARKGRAY);
      DrawText(dc,0,0,clientWidth,clientHeight,BtnCaption,alCenter|alMiddle);
    }
    else
    { DrawText(dc,0,0,clientWidth,clientHeight,BtnCaption,alCenter|alMiddle);
    }
  }

  if(WndGetAttr(hWnd,WS_FOCUS))
  { if(WndGetAttr(hWnd,BS_MENU))
    { /*������״̬��ť������ʾ*/
      SetPenLogic(dc,PL_XOR);
      SetColorIndex(dc,-1);
      FillRect(dc,0,0,clientWidth,clientHeight);
    }
    else if(BtnCaption && *BtnCaption)
    { /*������״̬��ť�����߿�*/ 
      SetPenLogic(dc,PL_XOR);
	  SetColorIndex(dc,-1);
      DrawDashedRect(dc,2,2,clientWidth-4,clientHeight-4);
    }
  }

  EndPaint(hWnd);
}
//---------------------------------------------------------------------------
static HRESULT CALLBACK DefButtonProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam)
{ switch(message)
  { case WM_LBUTTONDOWN:
			WndAddAttr(hWnd,BS_PUSHED);
			InvalidateNCArea(hWnd);
			CMD_NotifyParent(hWnd,BN_PUSHED);
         break;/*DefWindowProc will deal with focus change events*/

    case WM_LBUTTONUP:
			WndSubAttr(hWnd,BS_PUSHED);
			InvalidateNCArea(hWnd);
			CMD_NotifyParent(hWnd,BN_UNPUSHED);
         break;/*DefWindowProc will dispatch BN_CLICKED events*/

    case WM_KEYUP:
		   if(wParam==VK_RETURN && WndGetAttr(hWnd,WS_FOCUS)) 
		   { CMD_NotifyParent(hWnd,CM_CLICKED);
		   }
		 return 0;

	case WM_PAINT:
  	       Button_Repaint(hWnd);
         return 0;

    case WM_KILLFOCUS:
	        InvalidateWindow(hWnd,true); /*repaint client and border*/
            CMD_NotifyParent(hWnd,CM_KILLFOCUS);
	     return 0;

    case WM_SETFOCUS:
            InvalidateWindow(hWnd,true); /*repaint client and border*/
            CMD_NotifyParent(hWnd,CM_SETFOCUS);
         return 0;

    case WM_ENABLE:
            Invalidate(hWnd);
         return 0;

    case WM_SETTEXT:
         if(SaveWindowText(hWnd,(char *)lParam,0))
         { Invalidate(hWnd);
		 }
         return 0;

    case WM_SETLOGO:
            WNDPTR(hWnd)->Logo=(TBitmap *)lParam;
            Invalidate(hWnd);
         return 0;

    case WM_NCCALCSIZE:
	     if(WndGetAttr(hWnd,WS_BORDER) && lParam)
		 { InflateRect((TRECT *)lParam, -2, -2);
		 }
		 return 0;

    case WM_NCPAINT:
          if(WndGetAttr(hWnd,WS_BORDER))
		  { HDC dc=GetWindowDC(hWnd);
			DrawFrame(dc,0,0,absWidth(hWnd),absHeight(hWnd),WndGetAttr(hWnd,WS_FOCUS),WndGetAttr(hWnd,BS_PUSHED));
		    ReleaseDC(dc);
		  }
		 return 0;
  }
 return DefWindowProc(hWnd,message,wParam,lParam);
}

//---------------------------------------------------------------------------
void CM_RegisterButton(void)
{  TWNDCLASS wc;
   memset(&wc,0,sizeof(wc));
   wc.clForeground=CL_BTNTEXT;
   wc.clBackground=CL_BTNFACE;
   wc.lpfnWndProc=DefButtonProc;
   wc.lpszClassName="button";
   RegisterClass(&wc);
}
/*---------------------------------------------------------------------------
END --- Thank you!                                                  ming.c
---------------------------------------------------------------------------*/



