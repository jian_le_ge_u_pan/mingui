/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      GroupBox.c
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
* 
*  MODIFICATION HISTORY
*     LastModify  2008/06/05
******************************************************************************/
#include "mingui.h"
//---------------------------------------------------------------------------
static void GroupBox_NCRepaint(HWND hWnd)
{ char *strCaption;
  HDC dc=GetWindowDC(hWnd);
  int font_hlf_height=(WNDPTR(hWnd)->Font->height>>1);
  Draw3dFrame(dc,0,font_hlf_height,absWidth(hWnd),absHeight(hWnd)-font_hlf_height,true);
  //DrawRect(dc,0,0,absWidth(hWnd),absHeight(hWnd));
  //DrawRect(dc,1,1,absWidth(hWnd)-2,absHeight(hWnd)-2);
  strCaption=GetWindowText(hWnd);
  if(strCaption && *strCaption)
  { ResetForeground(dc);
    SetBkMode(dc,OPAQUE);
    TextOut(dc,WNDPTR(hWnd)->Font->eWidth,0,strCaption);
  }
  ReleaseDC(hWnd);
}
//---------------------------------------------------------------------------
static HRESULT CALLBACK DefGroupBoxProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam)
{ switch(message)
  { case WM_NCCALCSIZE:
	     if(WndGetAttr(hWnd,WS_BORDER) && lParam)
		 { InflateRect((TRECT *)lParam, -2, -2);
		   ((TRECT *)lParam)->top+=(WNDPTR(hWnd)->Font->height-2);
		 }
		 return 0;

    case WM_NCPAINT:
          GroupBox_NCRepaint(hWnd);
		 return 0;
 
    case WM_SETTEXT:
          if(SaveWindowText(hWnd,(char *)lParam,0))
          {  if(WndGetAttr(hWnd,WS_BORDER)) InvalidateNCArea(hWnd);
		  }
         return 0;

 }
 return DefWindowProc(hWnd,message,wParam,lParam);
}
//---------------------------------------------------------------------------
void CM_RegisterGroupBox(void)
{  TWNDCLASS wc;
   memset(&wc,0,sizeof(wc));
   wc.clForeground=CL_BTNTEXT;
   wc.clBackground=CL_BTNFACE;
   wc.lpfnWndProc=DefGroupBoxProc;
   wc.lpszClassName="groupBox";
   RegisterClass(&wc);
}
/*---------------------------------------------------------------------------
END --- Thank you!                                                  ming.c
---------------------------------------------------------------------------*/



