/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      Memo.c
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*     Multi-line Editor
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
* 
*  MODIFICATION HISTORY
*     LastModify  2008/05/20
******************************************************************************/
#include "mingui.h"
//---------------------------------------------------------------------------
#define MARGIN_EDIT_LEFT        3
#define MARGIN_EDIT_TOP         3
#define MARGIN_EDIT_RIGHT       2
#define MARGIN_EDIT_BOTTOM      3
#define CARET_HEIGHT_OVERLAP    1
#define MEDIT_COL_SPACE         1
#define MEDIT_ROW_SPACE         2
#define MEDIT_APPEND_MODE       0x12345
//---------------------------------------------------------------------------
#define LEN_MEMO_BUFFER         512
//---------------------------------------------------------------------------
typedef struct tag_multiedit 
{   TRECT   textArea;
	int     editPos;        /* current edit position */
    int     startPos;       /* start display position */
    int     pageTopLine;    /* 编缉框中可见的第一行对应于整个文全的行号*/
	int     linePerPage;    
    int     caretx,carety,caretheight;
    int     colspace,rowspace; 
	int     dataEnd;        /* data end position */
	int     bufferLen;      
    char   *buffer;		   /* buffer */
}TMultiEdit;

 
#define GetLineIndexByCarret(hWnd,pMemoData)      ( (pMemoData->carety<=pMemoData->textArea.top)?pMemoData->pageTopLine:pMemoData->pageTopLine + (pMemoData->carety-pMemoData->rowspace-pMemoData->textArea.top)/GetSysCharHeight(hWnd) )

   /* 1st:gb:a1-f7,big5:a1-f9  2nd:gb:a1-fe,big5:40-7e,a1-fe */
#define EdtIsACCharBeforePosition(pstr,pos)  ( (pos)>1 && *((BYTE *)(pstr)+pos-1)>0xA0 && *((BYTE *)(pstr)+pos-2)>0xA0  )
 
#define EdtIsACCharAtPosition(pstr,pos)  (  *((BYTE *)(pstr)+pos)>0xA0 && *((BYTE *)(pstr)+pos+1)>0xA0  )

//---------------------------------------------------------------------------
// box、startpos的坐标都是相对dc的坐标。  
#define TB_ERASEBKGND    0x01  /*bit0: 是不擦除背景*/
#define TB_DRAWTEXT      0x02  /* bit1:是否写字*/
#define TB_MEASURE       0x04  /*bit2:是否返回画笔结束位置到startpos*/

static void TextBox(HDC dc,TRECT *box,TPOINT *startpos,LPCSTR lptext,int mode)
{ BYTE ch;
  TFont *tf;
  int disptext,hanzi_mached,linecount,hzkoffset,EndPosX,EndPosY,PenPosX,PenPosY,colspace,rowspace;
  if(!lptext || !*lptext) return;
  
  disptext=mode&TB_DRAWTEXT;
  tf=((TWndCanvas *)dc)->Font;

  hanzi_mached=false;
  colspace=GetColSpace(dc);
  rowspace=GetRowSpace(dc);
  
  PenPosX=startpos->x;
  PenPosY=startpos->y;

  if(disptext) GroupOn(dc);
  if(mode&TB_ERASEBKGND) 
  {   TPOINT endpos=*startpos;
  	  TextBox(dc,box,&endpos,lptext,TB_MEASURE);
      linecount=(endpos.y-startpos->y+rowspace)/(tf->height+rowspace);
	  if(linecount==1)
	  { EraseBackground(dc,startpos->x,startpos->y,endpos.x-startpos->x,tf->height);
	  }
	  else if(linecount>=2)
	  {	EraseBackground(dc,startpos->x,startpos->y,box->right-startpos->x,tf->height);
	    if(linecount>2)
		{ EraseBackground(dc,box->left,startpos->y+tf->height,box->right-box->left,endpos.y-startpos->y-tf->height-tf->height);
		}
	    EraseBackground(dc,box->left,endpos.y-tf->height,endpos.x-box->left,tf->height);
	  }
  }
   
  while (1)
  { ch=*lptext++;
    if(ch>0xa0)
    {  if(hanzi_mached)
       { if(PenPosX+tf->cWidth>box->right)
	     { PenPosX=box->left;
		   PenPosY=PenPosY+tf->height+rowspace;
		   if(PenPosY+tf->height>box->bottom) break;
	     }
		 if(disptext)
	     { hzkoffset = ( (*(BYTE *)(lptext-2) - 0xA1) * 94 + (ch - 0xA1) ) * tf->cBytes;
           DrawFontMatrix(dc,PenPosX,PenPosY,tf->cWidth,tf->height,&tf->cBits[hzkoffset]) ;
	     }
	     PenPosX=PenPosX+tf->cWidth+colspace;
	   }
	   hanzi_mached=!hanzi_mached;
    }
	else
	{ if(ch==0)break;

	  hanzi_mached=false;
	  if(ch=='\r' || ch=='\n')
      { PenPosX=box->left;
	    PenPosY=PenPosY+tf->height+rowspace;
        if(PenPosY+tf->height<=box->bottom)
        { if(ch=='\r' && *(lptext+1)=='\n') lptext++;
		}else break;
      }
      else
      { if(PenPosX+tf->eWidth>box->right)
	    { PenPosX=box->left;
		  PenPosY=PenPosY+tf->height+rowspace;
		  if(PenPosY+tf->height>box->bottom)break;
	    }
		if(disptext)
	    { hzkoffset=tf->eBytes*ch;
          DrawFontMatrix(dc,PenPosX,PenPosY,tf->eWidth,tf->height,&tf->eBits[hzkoffset]) ;
	    }
		PenPosX=PenPosX+tf->eWidth+colspace;
      }
    }
  }

  if(PenPosX>box->left)
  { EndPosX=PenPosX;
    EndPosY=PenPosY+tf->height;
  }
  else
  { EndPosX=box->right;
    EndPosY=PenPosY-rowspace;
  }

  if(disptext)
  { linecount=(EndPosY-startpos->y+rowspace)/(tf->height+rowspace);
	if(linecount==1)
	{ GroupOff(dc,startpos->x,startpos->y,EndPosX-startpos->x,tf->height);
	}
	else if(linecount>=2)
    { GroupOff(dc,startpos->x,startpos->y,box->right-startpos->x,tf->height);
	  if(linecount>2)
	  { GroupOn(dc);
		GroupOff(dc,box->left,startpos->y+tf->height,box->right-box->left,EndPosY-startpos->y-tf->height-tf->height);
	  }
	  GroupOn(dc);
	  GroupOff(dc,box->left,EndPosY-tf->height,EndPosX-box->left,tf->height);
	}
  }

  if(mode&TB_MEASURE)
  { startpos->x=EndPosX;
    startpos->y=EndPosY;
  }
}
//-----------------------------------------------------------------------------------------------
static void TextBoxGroupOff(HDC dc,TRECT *box,TPOINT *startpos,TPOINT *endpos1,TPOINT *endpos2)
{ TPOINT *endpos=(endpos2->y>endpos1->y || (endpos2->y==endpos1->y && endpos2->x>endpos1->x))?endpos2:endpos1;
  int fontheight=((TWndCanvas *)dc)->Font->height;
  int rowspace=GetRowSpace(dc);
  int linecount=(endpos->y - startpos->y + rowspace)/(fontheight+rowspace);
  if(linecount==1)
  { GroupOff(dc,startpos->x,startpos->y,endpos->x-startpos->x,fontheight);
  }
  else if(linecount>=2)
  { GroupOff(dc,startpos->x,startpos->y,box->right-startpos->x,fontheight);
	if(linecount>2)
	{ GroupOn(dc);
	  GroupOff(dc,box->left,startpos->y+fontheight,box->right-box->left,endpos->y-startpos->y-fontheight-fontheight);
	}
	GroupOn(dc);
	GroupOff(dc,box->left,endpos->y-fontheight,endpos->x-box->left,fontheight);
  }
}

//-----------------------------------------------------------------------------------------------
/* 输入鼠标位置curpos,根据框内文本,重新定格curpos,同时取得所在行信息，并返回curpos指向的文本位置.
*/
static int BoxLocateCursor(TMultiEdit *pEdit,int x,int y,int *cx,int *cy,int *linepos,int *linelen)
{ BYTE ch;
  TFont *tf;
  bool hanzi_mached=false;
  LPCSTR pline,pstr,lptext=pEdit->buffer+pEdit->startPos;
  int PenPosX,PenPosY,linesize=0,nx=pEdit->textArea.left,ny=pEdit->textArea.top;
   
  tf=WNDPTR( ( (BYTE *)pEdit-(int)WndClsBuf(0) ) )->Font;

  pline=pstr=lptext;
  
  if(lptext && *lptext)
  {  y=y-((y-pEdit->textArea.top)%(tf->height+pEdit->rowspace));
	 PenPosX=pEdit->textArea.left; 
     PenPosY=pEdit->textArea.top; 

  while (1)
  { ch=*lptext++;
  	if(ch>0xa0)
	{  if(hanzi_mached)
       { if(PenPosX+tf->cWidth>pEdit->textArea.right)
	     { PenPosX=pEdit->textArea.left;
		   PenPosY=PenPosY+tf->height+pEdit->rowspace;
		   if(PenPosY+tf->height<=pEdit->textArea.bottom)
		   { if(PenPosY>y)break;
		     else {pstr=pline=lptext-2;linesize=0;nx=pEdit->textArea.left;ny=PenPosY;}
		   }
		   else break;
	     }
         linesize+=2;
     	 PenPosX=PenPosX+tf->cWidth+pEdit->colspace;
		 if(PenPosX-tf->eWidth-pEdit->colspace<=x)
		 {  pstr=lptext;
			nx=PenPosX;
		 }
	   }
	   hanzi_mached=!hanzi_mached;
    }
    else
    { if(ch==0)break;
	  hanzi_mached=false;
	  if(ch=='\r' || ch=='\n')
      { PenPosX=pEdit->textArea.left;
	    PenPosY=PenPosY+tf->height+pEdit->rowspace;
        if(PenPosY+tf->height<=pEdit->textArea.bottom)
        {  if(ch=='\r' && *(lptext+1)=='\n') lptext++;
           if(PenPosY>y)break;
		   else {pstr=pline=lptext;linesize=0;nx=pEdit->textArea.left;ny=PenPosY;}
		}else break;
      }
      else
      { if(PenPosX+tf->eWidth>pEdit->textArea.right)
	    { PenPosX=pEdit->textArea.left;
		  PenPosY=PenPosY+tf->height+pEdit->rowspace;
		  if(PenPosY+tf->height<=pEdit->textArea.bottom)
		  { if(PenPosY>y)break;
		    else {pstr=pline=lptext-1;linesize=0;nx=pEdit->textArea.left;ny=PenPosY;}
		  }
		  else break;
	    }
	 
	    PenPosX=PenPosX+tf->eWidth+pEdit->colspace;
		linesize++;
		if(PenPosX-(tf->eWidth>>1)-pEdit->colspace<=x)
		{  pstr=lptext;
		   nx=PenPosX;
		}
      }
    }
 
     }

   }
   if(linepos) *linepos=(int)(pline-pEdit->buffer);
   if(linelen) *linelen=linesize;
   if(cx)*cx=nx;
   if(cy)*cy=ny;
   return (int)(pstr-pEdit->buffer);
}

//----------------------------------------------------------------------------------------------- 
static int LineLocateCursor(TMultiEdit *pEdit,int linepos,int *cx)
{ int PenPosX=pEdit->textArea.left;
  char *lptext=&pEdit->buffer[linepos];
  char ch=*lptext;
  if(ch)
  { bool hanzi_mached=false;
    int origin_x=min(*cx,pEdit->textArea.right);
    TFont *tf=WNDPTR( ( (BYTE *)pEdit-(int)WndClsBuf(0) ) )->Font;
    while (ch!='\0' && ch!='\r' && ch!='\n')
    { if((BYTE)ch>0xa0)
	  { if(!hanzi_mached)
        { if(PenPosX+tf->cWidth>origin_x)break;
		  PenPosX=PenPosX+tf->cWidth+pEdit->colspace;
	    }
	    hanzi_mached=!hanzi_mached;
      }
      else
      { hanzi_mached=false;
        if(PenPosX+tf->eWidth>origin_x)break;
		PenPosX=PenPosX+tf->eWidth+pEdit->colspace;
      }
	  ch=*(++lptext);
    }
  }
  *cx=PenPosX;
  return (int)(lptext-pEdit->buffer);
}
//-----------------------------------------------------------------------------------------------
/*根据行号取得文本行首批针，行首偏移，行长度*/
static LPCSTR MemoGetLineInfo(TMultiEdit * pEdit,int linenum,int *linepos,int *linelen,int *linewidth) 
{ BYTE ch;
  TFont *tf;
  bool hanzi_mached=false;
  int PenPosX,PenPosY,lineindex=0,linesize=0;
  LPCSTR pline=pEdit->buffer,lptext=pEdit->buffer;

  if(!lptext || linenum<0)return NULL;

  tf=WNDPTR( ( (BYTE *)pEdit-(int)WndClsBuf(0) ) )->Font;

  PenPosX=pEdit->textArea.left; 
  PenPosY=pEdit->textArea.top; 
  while (1)
  { ch=*lptext++;
  	if(ch>0xa0)
	{  if(hanzi_mached)
       { if(PenPosX+tf->cWidth>pEdit->textArea.right)
	     { if(++lineindex>linenum)break;  
		   else{pline=lptext-2;linesize=0;}
		   PenPosX=pEdit->textArea.left;
		   PenPosY=PenPosY+tf->height+pEdit->rowspace;
	     }
         linesize+=2;
  	     PenPosX=PenPosX+tf->cWidth+pEdit->colspace;
	   }
	   hanzi_mached=!hanzi_mached;
    }
	else
    { if(ch==0)break;
	  hanzi_mached=false;
	  if(ch=='\r' || ch=='\n')
      { if(++lineindex>linenum)break; 
        else{pline=lptext;linesize=0;}
		PenPosX=pEdit->textArea.left;
	    PenPosY=PenPosY+tf->height+pEdit->rowspace;
        if(ch=='\r' && *(lptext+1)=='\n') lptext++;
      }
      else
      { if(PenPosX+tf->eWidth>pEdit->textArea.right)
	    { if(++lineindex>linenum)break; 
		  else{pline=lptext-1;linesize=0;}
		  PenPosX=pEdit->textArea.left;
		  PenPosY=PenPosY+tf->height+pEdit->rowspace;
	    }
	 
	    PenPosX=PenPosX+tf->eWidth+pEdit->colspace;
		linesize++;
      }
    }
 

  }
 
  if(linepos) *linepos=(int)(pline-pEdit->buffer);
  if(linelen) *linelen=linesize;
  if(linewidth) *linewidth=PenPosX-pEdit->textArea.left;
  return (lineindex>=linenum)?pline:NULL;
}

//-----------------------------------------------------------------------------------------------
static HRESULT Memo_InputText(HWND hWnd,char *strText,BOOL bAppend)
{ if(bAppend)
  { if(strText)
	{ TMultiEdit *pMemoData = (TMultiEdit *)WndClsBuf(hWnd);
	  int appendLen,totalLine,linewidth,lastlinepos;
	  appendLen=min( (int)strlen(strText), pMemoData->bufferLen - pMemoData->dataEnd - 1);
	  if(appendLen>0)
	  { memcpy (&pMemoData->buffer[pMemoData->dataEnd],strText, appendLen);
        pMemoData->dataEnd+=appendLen;
        pMemoData->buffer[pMemoData->dataEnd]='\0';
        pMemoData->editPos=pMemoData->dataEnd;	

	    totalLine=Memo_GetLineCount(hWnd);
	    MemoGetLineInfo(pMemoData,totalLine-1,&lastlinepos,NULL,&linewidth);
		
		pMemoData->caretx=pMemoData->textArea.left+linewidth;

        if(totalLine-pMemoData->pageTopLine>pMemoData->linePerPage)
	    { pMemoData->pageTopLine=totalLine-1;
	      pMemoData->startPos=lastlinepos;
		  pMemoData->carety=pMemoData->textArea.top;
	    }
		else
		{ pMemoData->carety=pMemoData->textArea.top + (GetSysCharHeight(hWnd)+pMemoData->rowspace)*(totalLine-pMemoData->pageTopLine-1);
		}
		 
        Invalidate(hWnd);  

	    if(WndGetAttr(hWnd,WS_VSCROLL|WS_BORDER)==(WS_VSCROLL|WS_BORDER))
        { ScrollBar_Synchronize2(hWnd,pMemoData->pageTopLine,totalLine);
        }

 	    if (!WndGetAttr(hWnd,ES_READONLY))
	    {  SetCaretPos(hWnd,pMemoData->caretx-1,pMemoData->carety-CARET_HEIGHT_OVERLAP);
	    }
      }
	} 
	else /*if(!strText)*/
	{ return -1;
	}
  }
  else
  { TMultiEdit *pMemoData = (TMultiEdit *)WndClsBuf(hWnd);
    int len;
    if(strText)
	{ if(strcmp(strText,pMemoData->buffer)==0) return -1;
	  len = min ((int)strlen(strText), pMemoData->bufferLen-1);
	  memcpy (pMemoData->buffer, strText, len);
	}
	else
	{ if(pMemoData->buffer[0]=='\0')return -1;
	  len=0;
	}
    pMemoData->dataEnd = len;
    pMemoData->buffer[len]='\0';
    pMemoData->editPos        = 0;
    pMemoData->caretx=pMemoData->textArea.left;
    pMemoData->carety=pMemoData->textArea.top;
    SetCaretPos(hWnd, pMemoData->caretx-1,pMemoData->carety-CARET_HEIGHT_OVERLAP); 
    pMemoData->startPos       = 0;
    pMemoData->pageTopLine    = 0;

    Invalidate(hWnd);
                
    if(WndGetAttr(hWnd,WS_VSCROLL|WS_BORDER)==(WS_VSCROLL|WS_BORDER))
    { ScrollBar_Synchronize2(hWnd,0,Memo_GetLineCount(hWnd));
    }
  }

  CMD_NotifyParent(hWnd,EN_CHANGED);
  return 0;
}
//-----------------------------------------------------------------------------------------------
static void Memo_OnCreate(HWND hWnd)
{  TMultiEdit *pMemoData=(TMultiEdit *)WndClsBuf(hWnd);
  
   pMemoData->editPos        = 0;
   pMemoData->startPos       = 0;
   pMemoData->pageTopLine    =0;

   pMemoData->colspace         = MEDIT_COL_SPACE;
   pMemoData->rowspace         = MEDIT_ROW_SPACE;

   pMemoData->textArea.left=MARGIN_EDIT_LEFT;
   pMemoData->textArea.top=MARGIN_EDIT_TOP;
   pMemoData->textArea.right=crWidth(hWnd)-MARGIN_EDIT_RIGHT;
   pMemoData->textArea.bottom=crHeight(hWnd)-MARGIN_EDIT_BOTTOM;

   pMemoData->caretx=pMemoData->textArea.left;
   pMemoData->carety=pMemoData->textArea.top;

   pMemoData->caretheight = GetSysCharHeight(hWnd)+CARET_HEIGHT_OVERLAP+CARET_HEIGHT_OVERLAP;  
   
   pMemoData->bufferLen          = LEN_MEMO_BUFFER;
       
   pMemoData->buffer = WndGetText(hWnd);
   pMemoData->dataEnd = strlen (pMemoData->buffer);

   pMemoData->linePerPage=(pMemoData->textArea.bottom-pMemoData->textArea.top+pMemoData->rowspace)/(GetSysCharHeight(hWnd)+pMemoData->rowspace);
   ScrollBar_Initialize(hWnd,pMemoData->pageTopLine,Memo_GetLineCount(hWnd),pMemoData->linePerPage,1);
}
//-----------------------------------------------------------------------------------------------
static void Memo_Repaint(HWND hWnd)
{   HDC dc=BeginPaint(hWnd);
	TMultiEdit *pMemoData=(TMultiEdit *)WndClsBuf(hWnd);
    char *text=pMemoData->buffer + pMemoData->startPos;
    if(*text!='\0')  
    { if(WndGetAttr(hWnd,WS_DISABLED)) SetColor(dc, CL_DARKGRAY);
      else ResetForeground(dc); 
      SetColSpace(dc,pMemoData->colspace);
      SetRowSpace(dc,pMemoData->rowspace);
      TextBox(dc,&pMemoData->textArea,(TPOINT *)&pMemoData->textArea,text,TB_DRAWTEXT);
    } 
	EndPaint(hWnd);
}
//-----------------------------------------------------------------------------------------------
static void Memo_GetChar(HWND hWnd,BYTE LowByte,BYTE HighByte,int KeyDecodeDepth)
{   TPOINT   textendpoint;
	HDC  dc;
    int  i, chars, inserting,bscroll=false,newline=false,newcaretx,newcarety;
	BOOL Enter_SoftToHard=false,OverwriteVisible;
    TMultiEdit *pMemoData= (TMultiEdit *)WndClsBuf(hWnd);

	HideCaret(hWnd);
    newcaretx=pMemoData->caretx;
	newcarety=pMemoData->carety;

    if(KeyDecodeDepth>1 && !EdtIsACCharBeforePosition (pMemoData->buffer, pMemoData->editPos))
	{ /*这里改变插入位置到前一个字符*/
	  if(pMemoData->caretx<=pMemoData->textArea.left)return;
	  pMemoData->caretx -= (GetSysCharWidth(hWnd)+pMemoData->colspace);
	  pMemoData->editPos--;
	}

	if (HighByte) 
	{ chars = 2;
    }
    else 
	{ if(LowByte<32 && LowByte!=0x0A) return;
	  else if(LowByte==0x0A)
	  { if(pMemoData->caretx==pMemoData->textArea.left && pMemoData->editPos>=1)
	    { BYTE PrevLineBottom=pMemoData->buffer[pMemoData->editPos-1];
	      if(PrevLineBottom!=0x0D && PrevLineBottom!=0x0A)Enter_SoftToHard=true;
	    }
	  }
      chars = 1;
    }
   
 
   if ((WndGetAttr(hWnd,ES_REPLACE)&& LowByte!=0x0A)|| KeyDecodeDepth>1)
   {  if (pMemoData->dataEnd == pMemoData->editPos)
      {  inserting = chars;
	  }
      else if (EdtIsACCharAtPosition(pMemoData->buffer, pMemoData->editPos))
	  {  inserting = (chars==2)? 0 : -1;
      }
      else
	  { inserting = (chars==2)? 1 : 0;
      }
   }
   else
   {  inserting = chars;
   }


   // check space,don't forget to keep space for null terminator.
   if ( pMemoData->dataEnd + inserting  >= pMemoData->bufferLen) 
   {  CMD_NotifyParent(hWnd,EN_MAXTEXT);
      return;
   }
 


    if(KeyDecodeDepth<=1) /*非键盘译码状态，输入字符后要改变光标位置*/
	{ if(LowByte==0x0A)
	  { if(!Enter_SoftToHard)
	    { newline=true;
	      newcaretx=pMemoData->textArea.left;
	    }
	  }
	  else
	  { int tw=(chars==2)?GetSysCCharWidth(hWnd):GetSysCharWidth(hWnd);
		newcaretx += tw;
	    if (newcaretx > pMemoData->textArea.right)
		{ newcaretx=pMemoData->textArea.left+tw+pMemoData->colspace; 
	      newline=true;
		}
	    else 
		{ newcaretx += pMemoData->colspace;
		}
	  }
      if(newline)
	  { int th=GetSysCharHeight(hWnd);
		if(newcarety+(th<<1)+pMemoData->rowspace>pMemoData->textArea.bottom) 
	    { bscroll=true;
	    }
	    else
		{ newcarety += (th+pMemoData->rowspace);
		}
	  }
	}


   if(!bscroll)
   {  OverwriteVisible=(!inserting)&&(pMemoData->buffer[pMemoData->editPos]!=0x0D && pMemoData->buffer[pMemoData->editPos]!=0x0A);
	  if(!OverwriteVisible && !Enter_SoftToHard)
	  { dc=GetDC(hWnd);
        SetColSpace(dc,pMemoData->colspace);
        SetRowSpace(dc,pMemoData->rowspace);
	    GroupOn(dc);
        textendpoint=*(TPOINT *)&pMemoData->caretx;
	    TextBox(dc,&pMemoData->textArea,&textendpoint,pMemoData->buffer + pMemoData->editPos,TB_ERASEBKGND|TB_MEASURE);
      }
   }

   if (inserting == -1) 
   {  for (i = pMemoData->editPos; i < pMemoData->dataEnd-1; i++)
      {  pMemoData->buffer [i] = pMemoData->buffer [i + 1];
	  }
   }
   else if (inserting > 0) 
   {  for (i = pMemoData->dataEnd + inserting - 1; i > pMemoData->editPos + inserting - 1; i--)
      {  pMemoData->buffer [i] = pMemoData->buffer [i - inserting];
      }
   }
  
   pMemoData->buffer[pMemoData->editPos] = LowByte;
   if(HighByte)pMemoData->buffer [pMemoData->editPos+1] = HighByte;
   
   if(inserting)
   { pMemoData->dataEnd += inserting;
     pMemoData->buffer [pMemoData->dataEnd] = '\0';
   }

   if(!bscroll)
   { if(OverwriteVisible)
     { char dummybottom;
	   int tw=(chars==2)?GetSysCCharWidth(hWnd):GetSysCharWidth(hWnd),tx,ty;
	   dc=GetDC(hWnd);
	   dummybottom=pMemoData->buffer[pMemoData->editPos+chars];
	   pMemoData->buffer[pMemoData->editPos+chars]='\0';
	   GroupOn(dc);
	   
	   if(newline && newcaretx>pMemoData->textArea.left)
	   { tx=pMemoData->textArea.left;
	     ty=newcarety;
	   }
	   else
	   { tx=pMemoData->caretx;
	     ty=pMemoData->carety;
	   }
	   EraseBackground(dc,tx,ty,tw,GetSysCharHeight(hWnd));
	   TextOut(dc,tx,ty,&pMemoData->buffer[pMemoData->editPos]);
       
	   pMemoData->buffer[pMemoData->editPos+chars]=dummybottom;
	   GroupOff(dc,tx,ty,tw,GetSysCharHeight(hWnd));
       ReleaseDC(dc);
     }
     else if(!Enter_SoftToHard)
	 {  TPOINT textendpoint2=*(TPOINT *)&pMemoData->caretx;
	    TextBox(dc,&pMemoData->textArea,&textendpoint2,pMemoData->buffer + pMemoData->editPos,TB_DRAWTEXT|TB_MEASURE);
	    TextBoxGroupOff(dc,&pMemoData->textArea,(TPOINT *)&pMemoData->caretx,&textendpoint,&textendpoint2);
	    ReleaseDC(dc);
	 }
   }

   pMemoData->editPos+=chars; 
   pMemoData->caretx=newcaretx;
   pMemoData->carety=newcarety;

   if(KeyDecodeDepth<=1)/*非键盘译码状态*/
   { if(bscroll)
	 {  pMemoData->pageTopLine++;
		MemoGetLineInfo(pMemoData,pMemoData->pageTopLine,&pMemoData->startPos,NULL,NULL);
		Invalidate(hWnd);
	 }
	 SetCaretPos(hWnd, pMemoData->caretx-1,pMemoData->carety-CARET_HEIGHT_OVERLAP);


	 if(WndGetAttr(hWnd,WS_VSCROLL|WS_BORDER)==(WS_VSCROLL|WS_BORDER))
	 { ScrollBar_Synchronize2(hWnd,pMemoData->pageTopLine,Memo_GetLineCount(hWnd));
	 }

   }

   CMD_NotifyParent(hWnd,EN_CHANGED);
}
//-----------------------------------------------------------------------------------------------
static void Memo_DeleteChars(HWND hWnd,int chars,BOOL bNeedRepaint)
{ TPOINT textendpoint1,textendpoint2; 
  TMultiEdit *pMemoData =(TMultiEdit *)WndClsBuf(hWnd);
  int i;
  HDC dc;
  if (WndGetAttr(hWnd,ES_READONLY) ||  pMemoData->editPos >= pMemoData->dataEnd || chars<=0)
  { return;
  }
  if(bNeedRepaint)
  { HideCaret(hWnd);
    dc=GetDC(hWnd);
	GroupOn(dc);
    SetColSpace(dc,pMemoData->colspace);
    SetRowSpace(dc,pMemoData->rowspace);
	textendpoint1=*(TPOINT *)&pMemoData->caretx;
	TextBox(dc,&pMemoData->textArea,&textendpoint1,pMemoData->buffer + pMemoData->editPos,TB_ERASEBKGND|TB_MEASURE);
  }
  for (i = pMemoData->editPos; i < pMemoData->dataEnd; i++)
  { pMemoData->buffer [i] = pMemoData->buffer [i+chars];
  }
  pMemoData->buffer [pMemoData->dataEnd - chars]='\0';
  pMemoData->dataEnd -= chars;
  if(bNeedRepaint)
  {  textendpoint2=*(TPOINT *)&pMemoData->caretx;
	 TextBox(dc,&pMemoData->textArea,&textendpoint2,pMemoData->buffer + pMemoData->editPos,TB_DRAWTEXT|TB_MEASURE);
	 TextBoxGroupOff(dc,&pMemoData->textArea,(TPOINT *)&pMemoData->caretx,&textendpoint1,&textendpoint2);
	 ReleaseDC(dc);
  }
  CMD_NotifyParent(hWnd,EN_CHANGED);
}
//-----------------------------------------------------------------------------------------------
void Memo_GetKeyDown(HWND hWnd,UINT Key)
{   TMultiEdit *pMemoData =(TMultiEdit *)WndClsBuf(hWnd);
    switch (Key)
    {
	   case VK_RETURN:
               if(WndGetAttr(hWnd,ES_READONLY|ES_WANTRETURN)==ES_WANTRETURN)
			   { Memo_GetChar(hWnd,0x0A,0,0);  /*接受换行符*/
			   }
             return;
 
        case VK_HOME:
			if (!WndGetAttr(hWnd,ES_READONLY))
			if(pMemoData->caretx>pMemoData->textArea.left)
			{ int curLineIndex=GetLineIndexByCarret(hWnd,pMemoData);
			  MemoGetLineInfo(pMemoData,curLineIndex,&pMemoData->editPos,NULL,NULL); 
			  pMemoData->caretx=pMemoData->textArea.left;
			  SetCaretPos(hWnd,pMemoData->caretx-1,pMemoData->carety-CARET_HEIGHT_OVERLAP);
			}
			return;
           
        case VK_END:
			 if (!WndGetAttr(hWnd,ES_READONLY))
             {	int editpos,linewidth,linesize,curLineIndex;
				curLineIndex=GetLineIndexByCarret(hWnd,pMemoData);
				MemoGetLineInfo(pMemoData,curLineIndex,&editpos,&linesize,&linewidth);
				editpos+=linesize;
				if (pMemoData->editPos != editpos)
				{ pMemoData->editPos  = editpos;
   			      pMemoData->caretx=pMemoData->textArea.left+linewidth;
                  SetCaretPos(hWnd,pMemoData->caretx-1,pMemoData->carety-CARET_HEIGHT_OVERLAP);
				}
             }
             return;

        case VK_BACK:
             if (WndGetAttr(hWnd,ES_READONLY) || pMemoData->editPos == 0) return;
			 /*else do case VK_LEFT:*/
        case VK_LEFT:
			 if (!WndGetAttr(hWnd,ES_READONLY))
             { bool bNeedScroll=false;
			   int chars;
  			   if (pMemoData->editPos == 0) return;
			   
               if(pMemoData->caretx>pMemoData->textArea.left)
			   { if(EdtIsACCharBeforePosition(pMemoData->buffer, pMemoData->editPos))
			     { chars=2;
			       pMemoData->caretx-=(GetSysCCharWidth(hWnd)+pMemoData->colspace);
			     }
			     else
				 { chars=1;
				   pMemoData->caretx-=(GetSysCharWidth(hWnd)+pMemoData->colspace);
				 }
				 pMemoData->editPos-=chars;
			   }
			   else
			   { int linehead,linesize,linewidth,curLineIndex;
			     curLineIndex=GetLineIndexByCarret(hWnd,pMemoData);
			     if(pMemoData->carety>pMemoData->textArea.top)
				 { pMemoData->carety-=(GetSysCharHeight(hWnd)+pMemoData->rowspace);
				 }
			     else
				 { bNeedScroll=true;
				 }
				 MemoGetLineInfo(pMemoData,curLineIndex-1,&linehead,&linesize,&linewidth);
                 chars=pMemoData->editPos-(linehead+linesize);
				 pMemoData->editPos  = linehead+linesize;
   			     pMemoData->caretx=pMemoData->textArea.left+linewidth;

			     if(bNeedScroll)
				 { Invalidate(hWnd);
				   pMemoData->pageTopLine--;
				   pMemoData->startPos=linehead;
				 }
			   }
			   if(Key==VK_BACK && chars>0)
			   { Memo_DeleteChars(hWnd,chars,!bNeedScroll);
			     if(WndGetAttr(hWnd,WS_VSCROLL|WS_BORDER)==(WS_VSCROLL|WS_BORDER))
				 { ScrollBar_Synchronize2(hWnd,pMemoData->pageTopLine,Memo_GetLineCount(hWnd));
				 }
			   }
			   else if(bNeedScroll)
			   {  if(WndGetAttr(hWnd,WS_VSCROLL|WS_BORDER)==(WS_VSCROLL|WS_BORDER))
			        ScrollBar_Synchronize(hWnd,pMemoData->pageTopLine);
			   }

			   SetCaretPos(hWnd,pMemoData->caretx-1,pMemoData->carety-CARET_HEIGHT_OVERLAP);
             }
             return;
                
        case VK_RIGHT:
			 if (!WndGetAttr(hWnd,ES_READONLY))
             { BOOL bScroll=false,newline=false;
               if (pMemoData->editPos == pMemoData->dataEnd)return;
               if(pMemoData->buffer[pMemoData->editPos]=='\n' || pMemoData->buffer[pMemoData->editPos]=='\r')
			   { newline=true;
			     if(pMemoData->buffer[pMemoData->editPos+1]=='\n')
				 { if(pMemoData->buffer[pMemoData->editPos]=='\r')pMemoData->editPos++;
				 }
				 pMemoData->editPos++;
			 
			   }
			   else
			   { int newcx;
			     if(EdtIsACCharAtPosition(pMemoData->buffer, pMemoData->editPos))
				 { pMemoData->editPos+=2;
				   newcx= pMemoData->caretx+GetSysCCharWidth(hWnd)+pMemoData->colspace;
				 }
				 else
				 { pMemoData->editPos+=1;
				   newcx= pMemoData->caretx+GetSysCharWidth(hWnd)+pMemoData->colspace;
				 }
				 
				 if(newcx>pMemoData->textArea.right)
				 { newline=true;
				 }
				 else if(pMemoData->editPos != pMemoData->dataEnd && pMemoData->buffer[pMemoData->editPos]!=0x0A)
				 { if(EdtIsACCharAtPosition(pMemoData->buffer,pMemoData->editPos))
				   { if(newcx+GetSysCCharWidth(hWnd)>pMemoData->textArea.right) newline=true;
				   }
				   else
				   { if(newcx+GetSysCharWidth(hWnd)>pMemoData->textArea.right) newline=true;
				   }
				 }
				 if(!newline) pMemoData->caretx=newcx;
			   }
			   if(newline)
			   { pMemoData->caretx=pMemoData->textArea.left;
                 if(pMemoData->carety+GetSysCharHeight(hWnd)+pMemoData->rowspace+GetSysCharHeight(hWnd)>pMemoData->textArea.bottom)
				 { bScroll=true;
				   pMemoData->pageTopLine++;
				   ScrollBar_Synchronize(hWnd,pMemoData->pageTopLine);
				   MemoGetLineInfo(pMemoData,pMemoData->pageTopLine,&pMemoData->startPos,NULL,NULL);
				 }
				 else
				 { pMemoData->carety += (GetSysCharHeight(hWnd)+pMemoData->rowspace);
				 }
			   }
			   SetCaretPos(hWnd,pMemoData->caretx-1,pMemoData->carety-CARET_HEIGHT_OVERLAP);
               if (bScroll) Invalidate(hWnd);
             }
             return;
                
		case VK_UP:
			 if (!WndGetAttr(hWnd,ES_READONLY))
			 {  int bScroll=false,curLineIndex,linehead;
			    curLineIndex=GetLineIndexByCarret(hWnd,pMemoData);
                if(pMemoData->carety > pMemoData->textArea.top)
				{ pMemoData->carety-= (GetSysCharHeight(hWnd)+pMemoData->rowspace);
				}
				else
				{ if(curLineIndex>0)bScroll=true;
				  else return;
				}
 				MemoGetLineInfo(pMemoData,curLineIndex-1,&linehead,NULL,NULL);
                if(bScroll)
				{ Invalidate(hWnd);
				  pMemoData->startPos=linehead;
				  pMemoData->pageTopLine--;
				  ScrollBar_Synchronize(hWnd,pMemoData->pageTopLine);
				}

				pMemoData->editPos=LineLocateCursor(pMemoData,linehead,&pMemoData->caretx );
								
				SetCaretPos(hWnd,pMemoData->caretx-1,pMemoData->carety-CARET_HEIGHT_OVERLAP);
 			 }
			 else if(pMemoData->pageTopLine>0)
			 {  Invalidate(hWnd);
			    pMemoData->pageTopLine--;
				ScrollBar_Synchronize(hWnd,pMemoData->pageTopLine);
                MemoGetLineInfo(pMemoData,pMemoData->pageTopLine,&pMemoData->startPos,NULL,NULL);
			    pMemoData->editPos=pMemoData->startPos;
				pMemoData->caretx=pMemoData->textArea.left;
                pMemoData->carety=pMemoData->textArea.top;
			 }
			 return;

		case VK_DOWN:
			 if (!WndGetAttr(hWnd,ES_READONLY))
			 { int bScroll=false,curLineIndex,linehead;
			   curLineIndex=GetLineIndexByCarret(hWnd,pMemoData);
			   if(MemoGetLineInfo(pMemoData,curLineIndex+1,&linehead,NULL,NULL))
			   { if(pMemoData->carety+GetSysCharHeight(hWnd)+GetSysCharHeight(hWnd)+pMemoData->rowspace>pMemoData->textArea.bottom)
				 { bScroll=true;
			       Invalidate(hWnd);
				   pMemoData->pageTopLine++;
				   ScrollBar_Synchronize(hWnd,pMemoData->pageTopLine);
				   MemoGetLineInfo(pMemoData,pMemoData->pageTopLine,&pMemoData->startPos,NULL,NULL);
			     }
			     else
				 { pMemoData->carety+=GetSysCharHeight(hWnd)+pMemoData->rowspace;
				 }

				 pMemoData->editPos=LineLocateCursor(pMemoData,linehead,&pMemoData->caretx );
				 
			     SetCaretPos(hWnd,pMemoData->caretx-1,pMemoData->carety-CARET_HEIGHT_OVERLAP);
			   }
			 }
			 else 
			 { int newlinepos;
			   if(MemoGetLineInfo(pMemoData,pMemoData->pageTopLine+1,&newlinepos,NULL,NULL))
			   { Invalidate(hWnd);
			     pMemoData->pageTopLine++;
				 ScrollBar_Synchronize(hWnd,pMemoData->pageTopLine);
                 pMemoData->editPos=pMemoData->startPos=newlinepos;
				 pMemoData->caretx=pMemoData->textArea.left;
                 pMemoData->carety=pMemoData->textArea.top;
			   }
			 }
			 return;

        case VK_INSERT:
               WNDPTR(hWnd)->Style ^= ES_REPLACE;
             return;

        case VK_DELETE:
               if (!WndGetAttr(hWnd,ES_READONLY) && (pMemoData->editPos != pMemoData->dataEnd))
			   { int deleted=(EdtIsACCharAtPosition(pMemoData->buffer,pMemoData->editPos))?2:1;;
			     Memo_DeleteChars(hWnd,deleted,true);
				 if(WndGetAttr(hWnd,WS_VSCROLL|WS_BORDER)==(WS_VSCROLL|WS_BORDER))
				 { ScrollBar_Synchronize2(hWnd,pMemoData->pageTopLine,Memo_GetLineCount(hWnd));
				 }
			   }
              return;
        

   }
   
}


static HRESULT CALLBACK MemoCtrlProc(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam)
{   
	switch (Message)
    {   case WM_CHAR:
             if (!WndGetAttr(hWnd,ES_READONLY))
             {  extern   HWND IME_Window;
			    BYTE wordlo=LOBYTE(WParam),wordhi=HIBYTE(WParam);
                if(wordlo>0xA0 &&  wordhi==0)
                { BYTE savedchar=(BYTE)IME_PopChar();
				  if(savedchar>0xA0)
				  { Memo_GetChar(hWnd,savedchar,wordlo,LParam); 
				  }
				  else
				  { IME_PushChar(wordlo);
				  }
				  return 0;
				}

				if(IME_Window && WNDPTR(IME_Window)->UserData==1)/*中文输入法*/
                {  if(SendMessage((HWND)IME_Window,WM_CHAR,WParam,LParam)==0)return 0;
                }

				Memo_GetChar(hWnd,wordlo,wordhi,LParam); 
             }
             return 0;

        case WM_COMMAND:
              if(LParam==WM_CHAR && !WndGetAttr(hWnd,ES_READONLY))
              { Memo_GetChar(hWnd,LOBYTE(WParam),HIBYTE(WParam),0);
              }
             return 0;

        case WM_KEYDOWN:
               if(WParam!=VK_TAB)
			   { extern   HWND IME_Window;
			     if(IME_Window && WNDPTR(IME_Window)->UserData==1)/*中文输入法*/
                 { if(!IME_isEmpty()) return SendMessage((HWND)IME_Window,WM_KEYDOWN,WParam,LParam);
                 }
  				 Memo_GetKeyDown(hWnd,WParam);
				 return 0;
			   }
			 break;

        case WM_GETTEXTLENGTH:
             return ((TMultiEdit *)WndClsBuf(hWnd))->dataEnd;

        case WM_SETTEXT:
			 return Memo_InputText(hWnd,(char*)LParam,(WParam==MEDIT_APPEND_MODE));

        case WM_LBUTTONDOWN:
            if (!WndGetAttr(hWnd,ES_READONLY))
			{ int xpos=LOWORD(LParam),ypos=HIWORD(LParam);
			  if(PointInRect(xpos, ypos, &WNDPTR(hWnd)->ClientRect))
			  {	TMultiEdit *pMemoData = (TMultiEdit *)WndClsBuf(hWnd);
			    pMemoData->editPos=BoxLocateCursor(pMemoData,xpos-WNDPTR(hWnd)->ClientRect.left,ypos-WNDPTR(hWnd)->ClientRect.top,&pMemoData->caretx,&pMemoData->carety,NULL,NULL);
			    SetCaretPos(hWnd, pMemoData->caretx-1,pMemoData->carety-CARET_HEIGHT_OVERLAP);
			  }
			}
        break;

     
        case EM_SETREADONLY:
			   if (WParam)
			   { WndAddAttr(hWnd,ES_READONLY);
			     DestroyCaret(hWnd);
			   }
			   else
			   { WndSubAttr(hWnd,ES_READONLY);
			     if(WndGetAttr(hWnd,WS_FOCUS))
				 { TMultiEdit *pMemoData = (TMultiEdit *)WndClsBuf(hWnd); 
				   CreateCaret (hWnd,  1 /*+ GetSysCharWidth(hWnd)*/, pMemoData->caretheight);
				   SetCaretPos(hWnd, pMemoData->caretx-1,pMemoData->carety-CARET_HEIGHT_OVERLAP); 
				 } 
			   }
               return 0;
 
  
        case EM_LIMITTEXT:
			 { int newLimit = (int)WParam;
               if (newLimit >= 0)
			   { TMultiEdit *pMemoData = (TMultiEdit *)WndClsBuf(hWnd);
                 char *newbuffer=(char *)RellocMem(pMemoData->buffer,newLimit);
				 if(newbuffer)
				 { pMemoData->buffer=newbuffer;
				   pMemoData->bufferLen = newLimit;
				 }
 			   }
			 }
             return 0;
     

        case WM_VSCROLL:
			   Memo_PageScroll(hWnd,(int)WParam);
			 return 0;
			 
		case WM_CREATE:
		       Memo_OnCreate(hWnd);
			 return 0;

        case WM_FONTCHANGE:
		     { TMultiEdit  *pMemoData=(TMultiEdit *)WndClsBuf(hWnd);
		       pMemoData->caretheight = GetSysCharHeight(hWnd)+CARET_HEIGHT_OVERLAP+CARET_HEIGHT_OVERLAP;  
			   ResetCaret(hWnd,  1 /*+ GetSysCharWidth(hWnd)*/, pMemoData->caretheight);
			 }
		     return 0;

        case WM_SETFOCUS:
			   if (!WndGetAttr(hWnd,ES_READONLY))
               { TMultiEdit  *pMemoData=(TMultiEdit *)WndClsBuf(hWnd);
			     CreateCaret (hWnd,  1 /*+ GetSysCharWidth(hWnd)*/, pMemoData->caretheight);
                 SetCaretPos (hWnd, pMemoData->caretx-1,pMemoData->carety-CARET_HEIGHT_OVERLAP);
               }
               CMD_NotifyParent(hWnd,CM_SETFOCUS); 
             return 0;

        case WM_KILLFOCUS:
                if (!WndGetAttr(hWnd,ES_READONLY))
				{ DestroyCaret(hWnd);
				}
                CMD_NotifyParent(hWnd,CM_KILLFOCUS);
              return 0;

        case WM_ENABLE:
                Invalidate(hWnd);
             return 0; 

        case WM_PAINT:
		       Memo_Repaint(hWnd);
             return 0;
 
    } 

    return DefWindowProc(hWnd, Message, WParam, LParam);
}

//---------------------------------------------------------------------------
void Memo_SetReadOnly(HWND hWnd,BOOL bReadonly)
{ SendMessage(hWnd,EM_SETREADONLY,(WPARAM)bReadonly,0);
}
//---------------------------------------------------------------------------
void Memo_SetMaxLength(HWND hWnd,int maxLength)
{ maxLength++; // keep space for null-terminator of string.
  SendMessage(hWnd,EM_LIMITTEXT,(WPARAM)maxLength,0);
}
//---------------------------------------------------------------------------
void Memo_SetReplaceMode(HWND hWnd,BOOL bSwitch)
{ if(bSwitch) WndAddAttr(hWnd,ES_REPLACE);
  else WndSubAttr(hWnd,ES_REPLACE);
}
//---------------------------------------------------------------------------
int Memo_GetLineCount(HWND hWnd) 
{ BYTE ch;
  bool hanzi_mached=false;
  int linecount=1;
  TMultiEdit *pEdit=(TMultiEdit *)WndClsBuf(hWnd);
  TFont *tf=WNDPTR(hWnd)->Font;
  int PenPosX=pEdit->textArea.left; 
  int PenPosY=pEdit->textArea.top; 
  LPCSTR lptext=pEdit->buffer;
  while (1)
  { ch=*lptext++;
  	if(ch>0xa0)
    {  if(hanzi_mached)
       { if(PenPosX+tf->cWidth>pEdit->textArea.right)
	     { PenPosX=pEdit->textArea.left;
		   PenPosY=PenPosY+tf->height+pEdit->rowspace;
		   linecount++; 
	     }
  	     PenPosX=PenPosX+tf->cWidth+pEdit->colspace;
	   }
	   hanzi_mached=!hanzi_mached;
    }
    else
    { if(ch==0)break;
	  hanzi_mached=false;
	  if(ch=='\r' || ch=='\n')
      { PenPosX=pEdit->textArea.left;
	    PenPosY=PenPosY+tf->height+pEdit->rowspace;
        if(ch=='\r' && *(lptext+1)=='\n') lptext++;
		linecount++; 
      }
      else
      { if(PenPosX+tf->eWidth>pEdit->textArea.right)
	    { PenPosX=pEdit->textArea.left;
		  PenPosY=PenPosY+tf->height+pEdit->rowspace;
		  linecount++; 
	    }
	    PenPosX=PenPosX+tf->eWidth+pEdit->colspace;
      }
    }
  
  }
 
  return linecount;
}
//---------------------------------------------------------------------------
int  Memo_GetLineText(HWND hWnd,int lineIndex,char *linebuf, int bufsize)
{  TMultiEdit *pMemoData=(TMultiEdit *)WndClsBuf(hWnd);
   int linepos,linelen;
   if(linebuf && MemoGetLineInfo(pMemoData,lineIndex,&linepos,&linelen,NULL))
   { if(linelen<bufsize)
     { memcpy(linebuf,&pMemoData->buffer[linepos],linelen);
	   linebuf[linelen]='\0';
	   return linelen;
	 }
	 else
	 { memcpy(linebuf,&pMemoData->buffer[linepos],bufsize);
	   return bufsize;
	 }
   }
   else return 0;
}
//---------------------------------------------------------------------------
void Memo_PageScroll(HWND hWnd,int topLine)
{ TMultiEdit *pMemoData=(TMultiEdit *)WndClsBuf(hWnd);
  int newlinepos;
  if(topLine!=pMemoData->pageTopLine && MemoGetLineInfo(pMemoData,topLine,&newlinepos,NULL,NULL))
  { pMemoData->pageTopLine=topLine;
    pMemoData->editPos=pMemoData->startPos=newlinepos;
	pMemoData->caretx=pMemoData->textArea.left;
    pMemoData->carety=pMemoData->textArea.top;
	SetCaretPos(hWnd, pMemoData->caretx-1,pMemoData->carety-CARET_HEIGHT_OVERLAP);

    Invalidate(hWnd);
	ScrollBar_Synchronize(hWnd,topLine);
  }
}
//---------------------------------------------------------------------------
void Memo_AppendText(HWND hWnd,char *strText)
{ SendMessage(hWnd,WM_SETTEXT,MEDIT_APPEND_MODE,(LPARAM)strText);
}
//---------------------------------------------------------------------------
void Memo_SetText(HWND hWnd,char *strText)
{ SendMessage(hWnd,WM_SETTEXT,0,(LPARAM)strText);
}
//---------------------------------------------------------------------------
void CM_RegisterMultiLineEdit(void)
{  TWNDCLASS wc;
   memset(&wc,0,sizeof(wc));
   wc.dwStyle=WS_BORDER_LOWERED;
   wc.clForeground=CL_WINDOWTEXT;
   wc.clBackground=CL_WHITE;
   wc.cbTextHeap=LEN_MEMO_BUFFER;
   wc.cbWndExtra=sizeof(TMultiEdit);
   wc.lpfnWndProc=MemoCtrlProc;
   wc.lpszClassName="Memo";
   RegisterClass(&wc);
}
//---------------------------------------------------------------------------
