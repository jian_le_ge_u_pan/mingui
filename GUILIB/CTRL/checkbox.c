/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      CheckBox.c
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
* 
*  MODIFICATION HISTORY
*     LastModify  2008/06/05
******************************************************************************/
#include "mingui.h"
//---------------------------------------------------------------------------
#define CHECKBOX_SIZE    16
#define CHECKBOX_MARGIN  3
//---------------------------------------------------------------------------
void CheckBox_SetValue(HWND hWnd,BOOL checked)
{ if(IsWnd(hWnd))
  { if(WNDPTR(hWnd)->ExtraLong[0]!=checked)
    { WNDPTR(hWnd)->ExtraLong[0]=checked;
      Invalidate(hWnd);
    }
  }
}
//---------------------------------------------------------------------------
BOOL CheckBox_GetValue(HWND hWnd)
{ if(IsWnd(hWnd)) return WNDPTR(hWnd)->ExtraLong[0];
  else return false;
}
//---------------------------------------------------------------------------
static void CheckBox_Repaint(HWND hWnd)
{ char *strCaption;
  int box_left,box_top,title_left,captionFontHeidht=WNDPTR(hWnd)->Font->height;
  HDC dc=BeginPaint(hWnd);
     
  if(WndGetAttr(hWnd,CS_LEFTJUSTIFY))
  {  box_left=crWidth(hWnd)-CHECKBOX_SIZE;
   	 title_left=0;
  }
  else
  {  box_left=0;
     title_left=box_left+CHECKBOX_SIZE+CHECKBOX_MARGIN;
  }

  strCaption=GetWindowText(hWnd);
  if(strCaption && *strCaption)
  { int title_top=(crHeight(hWnd)-captionFontHeidht)/2;
	int title_width=TextOut(dc,title_left,title_top,strCaption);
    if(WndGetAttr(hWnd,WS_FOCUS))
    { SetPenLogic(dc,PL_XOR);
      SetColorIndex(dc,-1);
      DrawDashedRect(dc,title_left,title_top,title_width,captionFontHeidht);
	  SetPenLogic(dc,PL_REPLACE);
	  ResetForeground(dc);
	}
  }

  box_top=(crHeight(hWnd)-CHECKBOX_SIZE)/2;

  if(!WndGetAttr(hWnd,WS_TRANSPARENT))
  { if(WndGetAttr(hWnd,WS_DISABLED))
    { SetBkColor(dc,CL_LIGHTGRAY);
	  SetColor(dc,CL_DARKGRAY);
	}
	else
	{ SetBkColor(dc,CL_WHITE);
	}
    ClearRect(dc,box_left,box_top,CHECKBOX_SIZE,CHECKBOX_SIZE);
  }
  else
  {  if(WndGetAttr(hWnd,WS_DISABLED)) SetColor(dc,CL_DARKGRAY);
  }
  Draw3dInset(dc, box_left, box_top,CHECKBOX_SIZE,CHECKBOX_SIZE);
  if(WNDPTR(hWnd)->ExtraLong[0])
  { int px1=box_left+3;
    int py1=box_top+(CHECKBOX_SIZE>>1)-1;
	int px2=box_left+(CHECKBOX_SIZE>>1)-2;
	int py2=box_top+CHECKBOX_SIZE-6;
	int px3=box_left+CHECKBOX_SIZE-4;
    int py3=box_top+4;

	DrawLine(dc,px1,py1,px2,py2);
	DrawLine(dc,px2,py2,px3,py3);
    DrawLine(dc,px1,py1+1,px2,py2+1);
	DrawLine(dc,px2,py2+1,px3,py3+1);
  }

  EndPaint(hWnd);
}
//---------------------------------------------------------------------------
static HRESULT CALLBACK DefCheckBoxProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam)
{ switch(message)
  { case WM_LBUTTONUP:
            if(WndGetAttr(hWnd,WS_FOCUS))
            { if(PointInRect(LOWORD(lParam), HIWORD(lParam), &WNDPTR(hWnd)->WndRect))
              { if(CMD_NotifyParent(hWnd,CM_CLICKED)==-1) return -1;/*filter by parent*/
			    CheckBox_SetValue(hWnd,!WNDPTR(hWnd)->ExtraLong[0]);
			  }
            }
         return 0;

    case WM_KEYUP:
		   if(wParam==VK_RETURN && WndGetAttr(hWnd,WS_FOCUS)) 
		   { if(CMD_NotifyParent(hWnd,BN_CLICKED)==-1) return -1;/*filter by parent*/
		     CheckBox_SetValue(hWnd,!WNDPTR(hWnd)->ExtraLong[0]);
		   }
		 return 0;

	case WM_PAINT:
  	       CheckBox_Repaint(hWnd);
         return 0;

    case WM_KILLFOCUS:
    case WM_SETFOCUS:
            Invalidate(hWnd);
         return 0;

    case WM_ENABLE:
            Invalidate(hWnd);
         return 0;

    case WM_SETTEXT:
            if(SaveWindowText(hWnd,(char *)lParam,0))
            { Invalidate(hWnd);
			}
         return 0;

    case WM_SETLOGO:
            WNDPTR(hWnd)->Logo=(TBitmap *)lParam;
            Invalidate(hWnd);
         return 0;


  }
 return DefWindowProc(hWnd,message,wParam,lParam);
}
//---------------------------------------------------------------------------
void CM_RegisterCheckBox(void)
{  TWNDCLASS wc;
   memset(&wc,0,sizeof(wc));
   wc.clForeground=CL_BTNTEXT;
   wc.clBackground=CL_BTNFACE;
   wc.lpfnWndProc=DefCheckBoxProc;
   wc.lpszClassName="CheckBox";
   RegisterClass(&wc);
}
/*---------------------------------------------------------------------------
END --- Thank you!                                                  ming.c
---------------------------------------------------------------------------*/



