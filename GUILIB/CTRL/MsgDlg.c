/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      msgdlg.c
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*      Input method editor (IME)
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
* 
*  MODIFICATION HISTORY
*     LastModify  2006/12/15
******************************************************************************/
#include "mingui.h"
/********************************************************************************
* FUNCTION NAME : MsgDlg
*
* INPUTS/OUTPUTS:
*   caption :   window title
*   text    :   window text
*   btnArray:   buttons caption,(if more than one,separate it with "|")
*                           
* VALUE RETURNED:
*   button index,(from right to left)
*
* Example:
*   MsgDlg("greet","Are you OK?","yes|no|cancel");
*   if you select button 'yes',the return value is 1, select 'no' return 2, 'cancel' return 3, and so on;
*
* Remark:  this is a small message box, special designed for little size of LCD display.
*          On bigger size of LCD, standard API MessageBox and MessageDlg will be available.
**********************************************************************************/
//---------------------------------------------------------------------------
int MsgDlg(char *text,char *caption,char *btnArray)
{ extern HWND hMessageDlg; 
  extern PWND g_RootWnd;

  #define dialogwidth            119
  #define dialogFontHeight       GetSysCharHeight(g_RootWnd)
  #define dialogheight           (dialogFontHeight*3+16)
  #define dialogtop              ((LCD_HEIGHT-dialogheight-3)/2)
  #define dialogleft             ((LCD_WIDTH-dialogwidth-3)/2)
  #define dlgcaptionbar_height   (dialogFontHeight+4)
  #define dlgbtn_height          (dialogFontHeight+2)
  #define dlgbtn_top             (dlgcaptionbar_height+dialogFontHeight+6)

  TMSG msg;
  int w,x,marginx;
  int quit=0,focus=0,oldfocus=0,btncount=0,size=0; 
  int btnslen=strlen(btnArray);
  char onebtn[16];
  int btnsize[6][2];
  TWinDragger  dragger;
  TRECT dragRect;
  dragRect.left=dragRect.top=0;
  dragRect.right=dialogwidth;
  dragRect.bottom=dlgcaptionbar_height;

  marginx=(btnslen>=18)?2:4;
 
  x=dialogwidth-marginx;
  
  if(hMessageDlg)return -1;
  hMessageDlg=CreateWindow("window",NULL,WS_BORDER|WS_BORDER_RAISED|WS_ALWAYSONTOP,dialogleft,dialogtop,dialogwidth+4,dialogheight+4,NULL,0,NULL);
  Dragger_Init(&dragger,hMessageDlg,&dragRect);
  while(!quit && GetMessage(&msg))
  { if(msg.Handle==hMessageDlg)  
    { switch(msg.Message)
	  { case WM_KEYUP:  switch (msg.WParam)
                        { case VK_RIGHT:
                               if(focus>0)focus--;
                               else if(btncount>0)focus=btncount-1;  
                               break;
                          case VK_LEFT:
                               if(focus<btncount-1)focus++;else focus=0;
                               break;             
	                      case VK_ESCAPE:
	                           focus=0;
	                           quit=1;
                               break;
    	                  case VK_RETURN:
	       	                   quit=1;
	       	                   break;  
                        }
                        if(focus!=oldfocus)
		                { HDC dc=GetDC(hMessageDlg);
						  SetPenLogic(dc,PL_XOR);
						  SetColorIndex(dc,-1);
						  FillRect(dc,btnsize[oldfocus][0]+1,dlgbtn_top+1,btnsize[oldfocus][1]-2,GetSysCharHeight(hMessageDlg));  
                          FillRect(dc,btnsize[focus][0]+1,dlgbtn_top+1,btnsize[focus][1]-2,GetSysCharHeight(hMessageDlg));     
                          oldfocus=focus;
						  ReleaseDC(dc);
		                } 
						DefWindowProc(hMessageDlg,msg.Message,msg.WParam,msg.LParam);
						break;
         case WM_LBUTTONDOWN:
		                Dragger_Process(&dragger,msg.Message,msg.LParam); 
						DefWindowProc(hMessageDlg,msg.Message,msg.WParam,msg.LParam);
						break; 
         case WM_LBUTTONUP: 
		                { int i,mx=LOWORD(msg.LParam),my=HIWORD(msg.LParam);
						  my-=WNDPTR(hMessageDlg)->ClientRect.top;
						  if(my>=dlgbtn_top && my<dlgbtn_top+GetSysCharHeight(hMessageDlg))
						  {  mx-=WNDPTR(hMessageDlg)->ClientRect.left;
						    for(i=0;i<btncount;i++)
						    { if(mx>=btnsize[i][0] && mx<btnsize[i][0]+btnsize[i][1])
							  { focus=i;
							    quit=1;
							  }
						    }
						  }
                        }
                        Dragger_Process(&dragger,msg.Message,msg.LParam);
		                break;
         case WM_MOUSEMOVE: 
                        Dragger_Process(&dragger,msg.Message,msg.LParam);
		                break;		               
         case WM_PAINT:{ HDC dc=BeginPaint(hMessageDlg);
                         FillRect(dc,0,0,dialogwidth,dlgcaptionbar_height);
                         SetPenLogic(dc,PL_XOR);
						 SetColorIndex(dc,-1);
                         TextOut(dc,(int)(dialogwidth-GetTextSize(dc,caption,NULL))/2,2,caption);
                         SetPenLogic(dc,PL_REPLACE);
						 SetColor(dc,CL_BLACK);
                         TextOut(dc,(int)(dialogwidth-GetTextSize(dc,text,NULL))/2,dlgcaptionbar_height+4,text);
                         while(btnslen>0)
                         { btnslen--;
                           if(btnslen==0 || btnArray[btnslen]=='|')
                           { if(btnslen==0)
                             { btnslen--;
                               size++;
                             }
                             if(size>=12)size=12-1;
                             memcpy(onebtn,&btnArray[btnslen+1],size);
                             onebtn[size]='\0';
                             size=0;
                             w=GetTextSize(dc,onebtn,NULL) +2;
                             x=x-w-marginx;
                             TextOut(dc,x+1,dlgbtn_top+1,onebtn);
                             DrawRect(dc,x,dlgbtn_top,w,dlgbtn_height);
                             btnsize[btncount][0]=x;
                             btnsize[btncount][1]=w;
                             btncount++;
                           } else size++;
                         }
						 if(btncount>1)
						 { SetPenLogic(dc,PL_XOR);
						   SetColorIndex(dc,-1);
                           FillRect(dc,btnsize[oldfocus][0]+1,dlgbtn_top+1,btnsize[oldfocus][1]-2,GetSysCharHeight(hMessageDlg));  
						 }
						 EndPaint(hMessageDlg);
						 break;
		               }
           default:   DefWindowProc(hMessageDlg,msg.Message,msg.WParam,msg.LParam);
						 
      }
	}
    else
	{ TranslateMessage(&msg);
      DispatchMessage(&msg);
	}
  }
  
  CloseWindow(hMessageDlg);
  hMessageDlg=0;
  return btncount-focus;
}
//---------------------------------------------------------------------------
void Dragger_Init(TWinDragger *dragger,HWND hWnd,TRECT *barRect)
{ dragger->hWnd=hWnd;
  dragger->DragBar=*barRect;
  dragger->xDragging=dragger->yDragging=0;
}
//---------------------------------------------------------------------------
void Dragger_Process(TWinDragger *dragger,UINT message,LPARAM lParam)
{  switch(message)
   { case WM_LBUTTONDOWN: 
          { dragger->xDragging=LOWORD(lParam)-WNDPTR(dragger->hWnd)->WndRect.left;
            dragger->yDragging=HIWORD(lParam)-WNDPTR(dragger->hWnd)->WndRect.top;
			if(!PointInRect(dragger->xDragging, dragger->yDragging, &dragger->DragBar))
			{ dragger->xDragging=dragger->yDragging=0;
			}
          }
		  break;
     case WM_LBUTTONUP:
	        dragger->xDragging=dragger->yDragging=0;
          break;
     case WM_MOUSEMOVE:
          { if(dragger->xDragging || dragger->yDragging)
	        { int nx=(short)LOWORD(lParam)-dragger->xDragging;
              int ny=(short)HIWORD(lParam)-dragger->yDragging;
	          if(nx!=WNDPTR(dragger->hWnd)->WndRect.left || ny!=WNDPTR(dragger->hWnd)->WndRect.top)
	          {  SetWindowPos(dragger->hWnd,nx-WNDPTR(dragger->hWnd)->Parent->ClientRect.left,ny-WNDPTR(dragger->hWnd)->Parent->ClientRect.top);
	          }
  	        }
          }
		  break;
   }
}
/*---------------------------------------------------------------------------
END --- Thank you!                                                  ming.c
---------------------------------------------------------------------------*/

