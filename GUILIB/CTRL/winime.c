/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      winime.c
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*      Input method editor (IME)
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
* 
*  MODIFICATION HISTORY
*     LastModify  2006/12/15
******************************************************************************/
#include "mingui.h"
//---------------------------------------------------------------------------
HWND IME_Window=NULL;
//---------------------------------------------------------------------------
#define PY_TEXT_SIZE   8
typedef struct
{ HWND hEdit;
  int EditPos; /*指向拼音输入框中最后一个正在编辑的字符*/
  int CountPerPage; /*每页显示的汉字数目*/
  int PageIndex;   /*当前页码*/
  int PageOffset;    /*光标选中的汉字在当前页的索引序号*/
  BOOL InputOrSelect;  /*输入状态还是选择状态*/
  int RangeOffset;
  int RangeCount;
  char *RangeAddress;
  char PY_Text[PY_TEXT_SIZE];
}TImeData;
//---------------------------------------------------------------------------
extern int PY_MapToChinese(char *PinYin, char **Chinese);
//---------------------------------------------------------------------------
void ImeWindowRepaint(HWND hWnd)
{ TImeData *pydata=(TImeData *)WndClsBuf(hWnd);
  HDC dc=BeginPaint(hWnd);
 
  if(pydata->PY_Text[0])
  { SetColor(dc,CL_BTNTEXT);
	SetFontStyle(dc,FS_OPAQUE,true);
	SetColSpace(dc,1);
	TextOut(dc,0,0,pydata->PY_Text);
	if(pydata->RangeCount && pydata->RangeAddress)
	{ int pagestart,count;

      pydata->PageIndex=pydata->RangeOffset/pydata->CountPerPage;
      pydata->PageOffset=pydata->RangeOffset%pydata->CountPerPage;

      pagestart= (pydata->PageIndex * pydata->CountPerPage);
      count= pydata->RangeCount-pagestart;
      if(count>pydata->CountPerPage)count=pydata->CountPerPage;
      count<<=1;
      pagestart<<=1;

      TextOut(dc,0,GetSysCharHeight(hWnd)+1,pydata->RangeAddress+pagestart);
	}
    SetPenLogic(dc,PL_XOR);
	SetColorIndex(dc,-1);
    if(pydata->InputOrSelect)
      FillRect(dc,pydata->EditPos*( GetSysCharWidth(hWnd)+1),0,GetSysCharWidth(hWnd),GetSysCharHeight(hWnd));
    else
      FillRect(dc,pydata->PageOffset*(GetSysCCharWidth(hWnd)+1),GetSysCharHeight(hWnd)+1,GetSysCCharWidth(hWnd),GetSysCharHeight(hWnd));
  }
 
  EndPaint(hWnd);
}
//---------------------------------------------------------------------------
void IME_OutPutChars(HWND hWnd,int pageOffset)
{  HWND ObjWnd=GetFocus();
   if(ObjWnd==hWnd)
   { ObjWnd=((TImeData *)WndClsBuf(hWnd))->hEdit;
   }

   if(IsWnd(ObjWnd) && !WndGetAttr(ObjWnd,WS_DISABLED) )
   { TImeData *pydata=(TImeData *)WndClsBuf(hWnd);
     int WordIndex=pydata->CountPerPage*pydata->PageIndex+pageOffset;
	 if(WordIndex<pydata->RangeCount && WordIndex>=0)
	 { BYTE *phz=(BYTE *)pydata->RangeAddress + (WordIndex<<1);
       SendMessage(ObjWnd,WM_COMMAND,*phz+(*(phz+1)<<8),WM_CHAR);
       pydata->InputOrSelect=true;
       pydata->EditPos=0;
       pydata->PY_Text[0]='\0';
       pydata->RangeOffset=0;
	   pydata->RangeCount=0;
       Invalidate(hWnd);
	 }
   }
}
//---------------------------------------------------------------------------
static HRESULT IME_OnKeyDown(HWND hWnd,WPARAM wParam)
{  TImeData *pydata=(TImeData *)WndClsBuf(hWnd);
   if(pydata->InputOrSelect)
   {  if(wParam==VK_LEFT || wParam==VK_DELETE || wParam==VK_BACK)
      { if(pydata->PY_Text[pydata->EditPos])
        { pydata->PY_Text[pydata->EditPos]=0;
          pydata->RangeCount=PY_MapToChinese(pydata->PY_Text, &pydata->RangeAddress);
          if(pydata->RangeOffset)pydata->RangeOffset=0;
          Invalidate(hWnd);
		  return 1;
        }
        else
		{ if(pydata->EditPos>0) 
          { pydata->EditPos--;
            if(pydata->PY_Text[pydata->EditPos])
            { pydata->PY_Text[pydata->EditPos]=0;
              pydata->RangeCount=PY_MapToChinese(pydata->PY_Text, &pydata->RangeAddress);
              if(pydata->RangeOffset)pydata->RangeOffset=0;
              Invalidate(hWnd);
			  return 1;
            }
          }
		}
      }
      else if(wParam==VK_RIGHT)
	  { if(pydata->PY_Text[pydata->EditPos])
        { pydata->EditPos++;
          Invalidate(hWnd);
		  return 1;
	    }
	  }
      else if(wParam==VK_DOWN || wParam==VK_RETURN)
	  { if(pydata->RangeCount)
        { pydata->InputOrSelect=false;
          Invalidate(hWnd);
		  return 1;
        }
	  }
	  else if(wParam==VK_UP)
	  { return 1;
	  }
   }
   else
   {  if(wParam==VK_RIGHT)
      {  pydata->RangeOffset++;
         if(pydata->RangeOffset>=pydata->RangeCount) pydata->RangeOffset=0;
         Invalidate(hWnd);
		 return 1;
      }
      else if(wParam==VK_LEFT)
      {  pydata->RangeOffset--;
         if(pydata->RangeOffset<0)pydata->RangeOffset=pydata->RangeCount-1;
         Invalidate(hWnd);
		 return 1;
      }
      else if(wParam==VK_UP)
      { pydata->InputOrSelect=true;
        Invalidate(hWnd);
		return 1;
      }
      else if(wParam==VK_RETURN)
      { IME_OutPutChars(hWnd,pydata->PageOffset);
	    return 1;
      }
	  else if(wParam==VK_DOWN)
	  { return 1;
	  }
   }
   return 0;
}
//---------------------------------------------------------------------------
static HRESULT CALLBACK IMEWinProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam)
{ switch(message)
  { 
     case WM_CHAR:
		  { TImeData *pydata=(TImeData *)WndClsBuf(hWnd);
		    if(pydata->InputOrSelect && ( (wParam>='A' && wParam<='Z')||(wParam>='a' && wParam<='z') ) )
            { if(lParam>1)
			  { pydata->PY_Text[pydata->EditPos]=(char)wParam;
 			  }
			  else
			  { if(pydata->PY_Text[pydata->EditPos])
			    { if(pydata->EditPos<PY_TEXT_SIZE-2) pydata->EditPos++;
			    }
				pydata->PY_Text[pydata->EditPos]=(char)wParam;
                pydata->PY_Text[pydata->EditPos+1]='\0';
              } 
			  pydata->RangeCount=PY_MapToChinese(pydata->PY_Text, &pydata->RangeAddress);
		      Invalidate(hWnd);
			  return 0;
            }
          }
	     return -1;
    case WM_KEYDOWN:
		     if(wParam!=VK_TAB) return IME_OnKeyDown(hWnd,wParam);
     	 break;
    case WM_PENDOWN:
		 { TImeData *pydata=(TImeData *)WndClsBuf(hWnd);
		   if(pydata->RangeCount)
		   { int verdiff=HIWORD(lParam)-(WNDPTR(hWnd)->ClientRect.top+GetSysCharHeight(hWnd)+1);
             if(verdiff>=0 && verdiff<GetSysCharHeight(hWnd))
			 {  int PageOffset=(LOWORD(lParam)-WNDPTR(hWnd)->ClientRect.left)/(GetSysCCharWidth(hWnd)+1);
			    IME_OutPutChars(hWnd,PageOffset);
			 }
		   }
		 }
         break;
    case WM_PAINT:
           ImeWindowRepaint(hWnd);
         return 0;        
	case WM_NCPAINT:
		 { HDC dc = GetWindowDC (hWnd);
		   Draw3dOutset(dc, 0, 0,absWidth(hWnd),absHeight(hWnd));
           ReleaseDC (dc);
		 }
         return 0;
    case WM_NCCALCSIZE:
		 if(WndGetAttr(hWnd,WS_BORDER) && lParam)
		 { InflateRect((TRECT *)lParam, -2, -2);
		 }
		 return 0;
   case WM_DESTROY:
		 { WNDPTR(hWnd)->UserData=0;
		 }
		 return 0;
    case WM_CREATE:
		 { TImeData *pydata=(TImeData *)WndClsBuf(hWnd);
	       memset(pydata,0,sizeof(TImeData));
	       pydata->hEdit=(HWND)WNDPTR(hWnd)->ExtraLong[0];
           pydata->InputOrSelect=true;
           pydata->CountPerPage=( WNDPTR(hWnd)->ClientRect.right-WNDPTR(hWnd)->ClientRect.left ) / (GetSysCCharWidth(hWnd)+1);
           WNDPTR(hWnd)->UserData=1;
		 }
	     return 0;
  }
  return DefWindowProc(hWnd,message,wParam,lParam);
}
//---------------------------------------------------------------------------
BOOL CM_RegisterIME(void)
{  TWNDCLASS wc;
   memset(&wc,0,sizeof(wc));
   wc.cbWndExtra=sizeof(TImeData);
   wc.lpfnWndProc=IMEWinProc;
   wc.lpszClassName="IME";
   wc.clForeground=CL_BTNTEXT;
   wc.clBackground=CL_BTNFACE;  
   return RegisterClass(&wc);
}
//---------------------------------------------------------------------------
/* Parent:  parentWindow
   hEdtor:  default input editor window
*/
void IME_Open(HWND hEdtor,HWND parent,int left,int top,int width,int height)
{ if(IME_Window)DestroyWindow(IME_Window);
  IME_Window=CreateWindow("IME","IME",WS_BORDER|WS_ALWAYSONTOP,left,top,width,height,parent,0,hEdtor);
  if(hEdtor)SetFocus(hEdtor);
}
//---------------------------------------------------------------------------
void IME_Close(void)
{ if(IME_Window)
  { DestroyWindow(IME_Window);
    IME_Window=NULL;
  }
}
//---------------------------------------------------------------------------
BOOL IME_isEmpty(void)
{ if(IME_Window)
  { TImeData *pydata=(TImeData *)WndClsBuf(IME_Window);
    if(pydata->PY_Text[0]) return false;
  }
  return true;
}
//---------------------------------------------------------------------------
#define IME_KEEP_CHAR_LIFE    1000   /*ms*/ 
char  IME_SavedChar=0;
DWORD IME_KeepDueTime=0;
extern TWndDateTime  g_SysTime;
//---------------------------------------------------------------------------
void IME_PushChar(char ch)
{  IME_SavedChar=(BYTE)ch;
   IME_KeepDueTime=(DWORD)g_SysTime;
}
//---------------------------------------------------------------------------
char IME_PopChar(void)
{  if((DWORD)g_SysTime<=IME_KeepDueTime)
   {  if(IME_SavedChar!=0)
      { char ret= IME_SavedChar;
        IME_SavedChar=0;
		return ret;
	  }
   }
   else
   { IME_SavedChar=0;
   }
   return 0;
}
/*---------------------------------------------------------------------------
END --- Thank you!                                                  ming.c
---------------------------------------------------------------------------*/



