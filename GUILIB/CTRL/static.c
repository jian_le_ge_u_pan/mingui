/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      Static.c
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
* 
*  MODIFICATION HISTORY
*     LastModify  2006/10/30
******************************************************************************/
#include "mingui.h"
//---------------------------------------------------------------------------
static HRESULT CALLBACK DefStaticProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam)
{ switch(message)
  { case WM_PAINT:
         { HDC dc = BeginPaint(hWnd);
           char *LabelCaption;
		   int clientwidth=crWidth(hWnd);
		   int clientheight=crHeight(hWnd);

           if(WNDPTR(hWnd)->Logo)
           { int x,y;
             if( WNDPTR(hWnd)->Logo->Width < clientwidth )
             { x  =  (clientwidth - WNDPTR(hWnd)->Logo->Width)>>1 ;
             } else x=0;
             
             if( WNDPTR(hWnd)->Logo->Height < clientheight )
             { y = (clientheight - WNDPTR(hWnd)->Logo->Height)>>1 ;
             } else y=0;
             DrawBitmap(dc,x,y,WNDPTR(hWnd)->Logo);
           }

           LabelCaption=GetWindowText(hWnd);
           if(LabelCaption && *LabelCaption)
           { DrawText(dc,0,0,clientwidth,clientheight,LabelCaption,alCenter|alMiddle);
           }
           EndPaint(hWnd);
         } 
         return 0;
    case WM_SETTEXT:
         if(SaveWindowText(hWnd,(char *)lParam,0))
         { Invalidate(hWnd);
		 }
         return 0;
    case WM_SETLOGO:
           WNDPTR(hWnd)->Logo=(TBitmap *)lParam;
           Invalidate(hWnd);
         return 0;
    case WM_ENABLE:
           Invalidate(hWnd);
         return 0;
 }
 return DefWindowProc(hWnd,message,wParam,lParam);
}

//---------------------------------------------------------------------------
void CM_RegisterStatic(void)
{  TWNDCLASS wc;
   memset(&wc,0,sizeof(wc));
   wc.clForeground=CL_BTNTEXT;
   wc.clBackground=CL_BTNFACE;   
   wc.lpfnWndProc=DefStaticProc;
   wc.lpszClassName="static";
   RegisterClass(&wc);
}
/*---------------------------------------------------------------------------
END --- Thank you!                                                  ming.c
---------------------------------------------------------------------------*/



