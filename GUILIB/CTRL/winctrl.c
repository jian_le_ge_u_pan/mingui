/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:			    winctrl.c
*  PROGRAMMER:			    ming.c
*  Date of Creation:		    2006/08/8
*
*  DESCRIPTION: 								
*	  GUI Lib public Interface					
*  NOTE:						 		
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
*
*  MODIFICATION HISTORY
*     LastModify  2008/06/05
******************************************************************************/
//---------------------------------------------------------------------------
#include "WinCtrl.h"
/*------------------------------------------------------------------------------
  GUI register Controls 
-------------------------------------------------------------------------------*/
void GUI_RegisterControls(void)
{ extern void CM_RegisterIME(void);
  extern void CM_RegisterButton(void);
  extern void CM_RegisterCheckBox(void);
  extern void CM_RegisterRadioBox(void);
  extern void CM_RegisterGroupBox(void);
  extern void CM_RegisterStatic(void);
  extern void CM_RegisterMenu(void);
  extern void CM_RegisterListBox(void);
  extern void CM_RegisterDigiEdit(void);
  extern void CM_RegisterSpinEdit(void);
  extern void CM_RegisterSingleLineEdit(void);
  extern void CM_RegisterMultiLineEdit(void);
  extern void CM_RegisterProgressBar(void);
  CM_RegisterIME();
  CM_RegisterButton();
  CM_RegisterCheckBox();
  CM_RegisterRadioBox();
  CM_RegisterGroupBox();
  CM_RegisterStatic();
  CM_RegisterMenu();
  CM_RegisterListBox();
  CM_RegisterSpinEdit();
  CM_RegisterDigiEdit();
  CM_RegisterSingleLineEdit();
  CM_RegisterMultiLineEdit();
  CM_RegisterProgressBar();
}

 

