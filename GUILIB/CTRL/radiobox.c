/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      RadioBox.c
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
* 
*  MODIFICATION HISTORY
*     LastModify  2008/06/05
******************************************************************************/
#include "mingui.h"
//---------------------------------------------------------------------------
#define RADIOBOX_SIZE    12
#define RADIOBOX_MARGIN  3
LPCSTR RADIOBOX_CLASS_NAME="RadioBox";
//---------------------------------------------------------------------------
void RadioBox_SetValue(HWND hWnd,BOOL checked)
{ if(IsWnd(hWnd))
  { if(WNDPTR(hWnd)->ExtraLong[0]!=checked)
    { WNDPTR(hWnd)->ExtraLong[0]=checked;
      Invalidate(hWnd);
    }
    if(checked)
	{ PWND sibwnd=WNDPTR(hWnd)->Parent->Children;
	  while(sibwnd)
	  {  if(sibwnd!=(PWND)hWnd)
	     { if(sibwnd->WinClass->lpszClassName==RADIOBOX_CLASS_NAME)
		   { if(sibwnd->ExtraLong[0])
		     { sibwnd->ExtraLong[0]=0;
	           Invalidate((HWND)sibwnd);
		     }
		   }
	     }
         sibwnd=sibwnd->Next;
	  }
	}
  }
}
//---------------------------------------------------------------------------
BOOL RadioBox_GetValue(HWND hWnd)
{ if(IsWnd(hWnd)) return WNDPTR(hWnd)->ExtraLong[0];
  else return false;
}
//---------------------------------------------------------------------------
static void RadioBox_Repaint(HWND hWnd)
{ char *strCaption;
  HDC dc=BeginPaint(hWnd);
  int cx,cy;
  int title_left;
  if(WndGetAttr(hWnd,CS_LEFTJUSTIFY))
  {	 title_left=0;
	 cx=crWidth(hWnd)-(RADIOBOX_SIZE>>1);
  }
  else
  {  cx=(RADIOBOX_SIZE>>1);
     title_left=RADIOBOX_SIZE+RADIOBOX_MARGIN;
  }
  cy=crHeight(hWnd)>>1;

  strCaption=GetWindowText(hWnd);
  if(strCaption && *strCaption)
  { int title_top=(crHeight(hWnd)-GetSysCharHeight(hWnd))/2;
	int title_width=TextOut(dc,title_left,title_top,strCaption);
    if(WndGetAttr(hWnd,WS_FOCUS))
    { SetPenLogic(dc,PL_XOR);
      SetColorIndex(dc,-1);
      DrawDashedRect(dc,title_left,title_top,title_width,GetSysCharHeight(hWnd));
	  SetPenLogic(dc,PL_REPLACE);
	}
  }
 
  if(!WndGetAttr(hWnd,WS_TRANSPARENT))
  { SetColor(dc,(WndGetAttr(hWnd,WS_DISABLED))?CL_LIGHTGRAY:CL_WHITE);
    FillCircle(dc,cx,cy,RADIOBOX_SIZE>>1);
  }

  SetColor(dc,CL_BLACK);
  DrawCircle(dc,cx,cy,RADIOBOX_SIZE>>1);

  if(WNDPTR(hWnd)->ExtraLong[0])
  { if(WndGetAttr(hWnd,WS_DISABLED)) SetColor(dc,CL_DARKGRAY);
    FillCircle(dc,cx,cy,2);
  }

  EndPaint(hWnd);
}
//---------------------------------------------------------------------------
static HRESULT CALLBACK DefRadioBoxProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam)
{ switch(message)
  { case WM_LBUTTONUP:
            if(WndGetAttr(hWnd,WS_FOCUS))
            { if(PointInRect(LOWORD(lParam), HIWORD(lParam), &WNDPTR(hWnd)->WndRect))
              { if(CMD_NotifyParent(hWnd,CM_CLICKED)==-1) return -1;/*filter by parent*/
		        if(!WNDPTR(hWnd)->ExtraLong[0]) RadioBox_SetValue(hWnd,true);
			  }
            }
         return 0;

    case WM_KEYUP:
		   if(wParam==VK_RETURN && WndGetAttr(hWnd,WS_FOCUS)) 
		   { if(CMD_NotifyParent(hWnd,BN_CLICKED)==-1) return -1;/*filter by parent*/
		     if(!WNDPTR(hWnd)->ExtraLong[0]) RadioBox_SetValue(hWnd,true);
		   }
		 return 0;

	case WM_PAINT:
  	       RadioBox_Repaint(hWnd);
         return 0;

    case WM_ENABLE:
    case WM_KILLFOCUS:
    case WM_SETFOCUS:
            Invalidate(hWnd);
         return 0;

    case WM_SETTEXT:
            if(SaveWindowText(hWnd,(char *)lParam,0))
            { Invalidate(hWnd);
			}
         return 0;

    case WM_SETLOGO:
            WNDPTR(hWnd)->Logo=(TBitmap *)lParam;
            Invalidate(hWnd);
         return 0;
 }
 return DefWindowProc(hWnd,message,wParam,lParam);
}
//---------------------------------------------------------------------------
void CM_RegisterRadioBox(void)
{  TWNDCLASS wc;
   memset(&wc,0,sizeof(wc));
   wc.clForeground=CL_BTNTEXT;
   wc.clBackground=CL_BTNFACE;
   wc.lpfnWndProc=DefRadioBoxProc;
   wc.lpszClassName=RADIOBOX_CLASS_NAME;
   RegisterClass(&wc);
}
/*---------------------------------------------------------------------------
END --- Thank you!                                                  ming.c
---------------------------------------------------------------------------*/



