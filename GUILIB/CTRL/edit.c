/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      Edit.c
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*     Single-Line Editor
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
* 
*  MODIFICATION HISTORY
*     LastModify  2006/10/30
******************************************************************************/
#include "mingui.h"
//---------------------------------------------------------------------------
#define MARGIN_EDIT_LEFT        3
#define MARGIN_EDIT_RIGHT       3
#define SEDIT_COL_SPACE         1 
#define CARET_HEIGHT_OVERLAP    1
#define LEN_SLEDIT_BUFFER       128
 
typedef struct tagSLEDITDATA 
{   int     dataEnd;        /* data end position */
    int     editPos;        /* current edit position */
    int     startPos;       /* start display position */
    int     passwdChar;     /* password character */
    TRECT   textArea;        /* text display area */
    int     colspace;       
 	int     caretheight;
	int     caretx,carety;
    int     bufferLen;      /* hard limit */
    char   *buffer;		/* buffer */
}TSLEdit;

#define EdtIsACCharBeforePosition(pstr,pos)  ( (pos)>1 && *((BYTE *)(pstr)+pos-1)>0xA0 && *((BYTE *)(pstr)+pos-2)>0xA0  )
 
#define EdtIsACCharAtPosition(pstr,pos)  (  *((BYTE *)(pstr)+pos)>0xA0 && *((BYTE *)(pstr)+pos+1)>0xA0  )
 
#define EditGetOutWidth(sEdtInfo)   ( sEdtInfo->textArea.right - sEdtInfo->textArea.left )

 
static int EdtGetStartDispPosAtEnd(const HWND pCtrl, TSLEdit *pSLEditData,int *textwidth)
{ if( pSLEditData->dataEnd>0 )
  { int w,chars,nTextWidth=0;
    int nOutWidth = EditGetOutWidth(pSLEditData);
    int pos=pSLEditData->dataEnd;
    const char* buffer = pSLEditData->buffer;
    while(pos>=0)
	{ if ((BYTE)buffer[pos-1] > 0xA0)
	  { w=WNDPTR(pCtrl)->Font->cWidth+pSLEditData->colspace;
	    chars=2;
	  }
	  else
	  { w=WNDPTR(pCtrl)->Font->eWidth+pSLEditData->colspace;
	    chars=1;
	  }
	  if(nTextWidth+w > nOutWidth) break;
	  else {nTextWidth+=w;pos-=chars;}
	}
	*textwidth=nTextWidth;
	return  (pos>=0)?pos:0;
  }
  else
  { *textwidth=0;
	return 0;
  }
}
 
static int EdtGetDispLen (const HWND pCtrl)
{   int w,chars,nTextWidth=0;
    TSLEdit *pSLEditData = (TSLEdit *)WndClsBuf(pCtrl);
	int nOutWidth = EditGetOutWidth(pSLEditData);
	int pos=pSLEditData->startPos;
    const char* buffer = pSLEditData->buffer;
    while(pos < pSLEditData->dataEnd)
	{  if ((BYTE)buffer[pos] > 0xA0)	/* st:gb:a1-f7,big5:a1-f9 */
	   {  w=WNDPTR(pCtrl)->Font->cWidth;
	      chars=2;
	   }
	   else
	   {  w=WNDPTR(pCtrl)->Font->eWidth;
	      chars=1;
	   }

       if(nTextWidth+w > nOutWidth) break;
	   else {nTextWidth+=(w+pSLEditData->colspace);pos+=chars;}
    }
    return pos-pSLEditData->startPos;
}

static int Edit_LocateCarret(const HWND pCtrl,int *x)
{   int w,chars,pos,x1,x2;
    TSLEdit *pSLEditData = (TSLEdit *)WndClsBuf(pCtrl);
    char* buffer;
	if(*x<=pSLEditData->textArea.left)
	{ *x=pSLEditData->textArea.left;
      return pSLEditData->startPos;
	}
	else 
	{ x2=(*x < pSLEditData->textArea.right)? *x : pSLEditData->textArea.right;
	  x1=pSLEditData->textArea.left;
	  pos=pSLEditData->startPos;
	  buffer = pSLEditData->buffer;
	}
    
    while(pos < pSLEditData->dataEnd)
	{  if ((BYTE)buffer[pos] > 0xA0)	/* st:gb:a1-f7,big5:a1-f9 */
	   {  w=WNDPTR(pCtrl)->Font->cWidth;
	      chars=2;
	   }
	   else
	   {  w=WNDPTR(pCtrl)->Font->eWidth;
	      chars=1;
	   }
       if(x1+w > x2) break;
	   else {x1+=(w+pSLEditData->colspace);pos+=chars;}
	   
    }
	*x=x1;
    return pos;
}


void SLEdit_OnCreate(HWND hWnd)
{  TSLEdit *pSLEditData=(TSLEdit *)WndClsBuf(hWnd);
   
   pSLEditData->editPos        = 0;
   pSLEditData->startPos       = 0;
   pSLEditData->colspace       = SEDIT_COL_SPACE;
   pSLEditData->passwdChar     = '*';
   pSLEditData->textArea.top      = ( crHeight(hWnd) - WNDPTR(hWnd)->Font->height )/2 - CARET_HEIGHT_OVERLAP;
   pSLEditData->textArea.bottom   = crHeight(hWnd) - pSLEditData->textArea.top;
   pSLEditData->textArea.left     =  MARGIN_EDIT_LEFT;
   pSLEditData->textArea.right     = crWidth(hWnd) - MARGIN_EDIT_RIGHT;

   pSLEditData->caretheight = GetSysCharHeight(hWnd)+CARET_HEIGHT_OVERLAP+CARET_HEIGHT_OVERLAP;    
   pSLEditData->carety = pSLEditData->textArea.top;
   pSLEditData->caretx= pSLEditData->textArea.left;
   
   pSLEditData->bufferLen      = LEN_SLEDIT_BUFFER;
       
   pSLEditData->buffer = WndGetText(hWnd);
   pSLEditData->dataEnd = strlen(pSLEditData->buffer);
}

void SLEdit_Repaint(HWND hWnd)
{   int     dispLen;
    TSLEdit *pSLEditData=(TSLEdit *)WndClsBuf(hWnd);
    HDC hdc=BeginPaint(hWnd);
    SetColSpace(hdc,pSLEditData->colspace);
    dispLen = EdtGetDispLen (hWnd);

    if (dispLen > 0) 
	{  char *dispBuffer,botsave;
       if (WndGetAttr(hWnd,ES_PASSWORD))
       { dispBuffer  = (char *)GetMem(dispLen + 1);
		 memset (dispBuffer, '*', dispLen);
	   }
       else
	   { dispBuffer=pSLEditData->buffer + pSLEditData->startPos;
	     botsave=dispBuffer[dispLen];
       }

       dispBuffer [dispLen] = '\0';
        
	   ((TWndCanvas *)hdc)->Pen.Mode |= FS_PLAIN;
       
	   if(WndGetAttr(hWnd,WS_DISABLED)) SetColor (hdc, CL_DARKGRAY);
	   else ResetForeground(hdc);
	    
       
       TextOut(hdc, pSLEditData->textArea.left, pSLEditData->carety+CARET_HEIGHT_OVERLAP, dispBuffer);
	   if (WndGetAttr(hWnd,ES_PASSWORD))
	   { FreeMem(dispBuffer);
	   }
	   else
	   { dispBuffer[dispLen]=botsave;
	   }
    } 

	EndPaint(hWnd);
}



void SLEdit_GetChar(HWND hWnd,BYTE LowByte,BYTE HighByte,int KeyDecodeDepth)
{   TSLEdit *pSLEditData= (TSLEdit *)WndClsBuf(hWnd);
    int  i, chars, inserting,newcaretx,scrollStep=0;

	if (HighByte) 
	{ chars = 2;
    }
    else 
	{ if(LowByte<32) return;
      chars = 1;
    }
    
	HideCaret(hWnd);
    newcaretx=pSLEditData->caretx;
    if(KeyDecodeDepth>1 && !EdtIsACCharBeforePosition (pSLEditData->buffer, pSLEditData->editPos) )
	{ /*这里改变插入位置到前一个字符*/
	  if(pSLEditData->caretx<=pSLEditData->textArea.left)return;
	  pSLEditData->caretx -= (GetSysCharWidth(hWnd)+pSLEditData->colspace);
	  pSLEditData->editPos--;
	}

 
    if (WndGetAttr(hWnd,ES_REPLACE) || KeyDecodeDepth>1)
    {  if (pSLEditData->dataEnd == pSLEditData->editPos)
       {  inserting = chars;
	   }
       else if (EdtIsACCharAtPosition(pSLEditData->buffer, pSLEditData->editPos))
	   {  inserting = (chars==2)? 0 : -1;
       }
       else
	   { inserting = (chars==2)? 1 : 0;
       }
    }
    else
    {  inserting = chars;
    }

    // check space,don't forget to keep space for null-terminator of string.
	if ( pSLEditData->dataEnd + inserting >= pSLEditData->bufferLen ) 
    {  CMD_NotifyParent(hWnd,EN_MAXTEXT);
       return;
    }

    if (inserting == -1) 
    {  for (i = pSLEditData->editPos; i < pSLEditData->dataEnd-1; i++)
	   {  pSLEditData->buffer [i] = pSLEditData->buffer [i + 1];
	   }
    }
    else if (inserting > 0) 
    {  for (i = pSLEditData->dataEnd + inserting - 1; i > pSLEditData->editPos + inserting - 1; i--)
       {  pSLEditData->buffer [i] = pSLEditData->buffer [i - inserting];
       }
    }
  
    pSLEditData->buffer[pSLEditData->editPos] = LowByte;
    if(HighByte)pSLEditData->buffer [pSLEditData->editPos+1] = HighByte;
   
 
    if(inserting)
    { pSLEditData->dataEnd += inserting;
      pSLEditData->buffer [pSLEditData->dataEnd] = '\0';
    }


    if(KeyDecodeDepth<=1) /*非键盘译码状态，输入字符后要改变光标位置*/
    {  int tw=(chars==2)?GetSysCCharWidth(hWnd):GetSysCharWidth(hWnd);
	   newcaretx += (tw+pSLEditData->colspace);
       if (newcaretx  > pSLEditData->textArea.right)
	   {  if (EdtIsACCharAtPosition(pSLEditData->buffer, pSLEditData->startPos))
	      {  scrollStep = 2;
		     newcaretx -=  (GetSysCCharWidth(hWnd)+pSLEditData->colspace);
          }
          else
		  {  if (HighByte) 
	         { if (EdtIsACCharAtPosition(pSLEditData->buffer, pSLEditData->startPos + 1))
               {  scrollStep = 3;
		          newcaretx -=  (GetSysCCharWidth(hWnd)+GetSysCharWidth(hWnd)+(pSLEditData->colspace<<1));
		       }
               else
               {  scrollStep = 2;
			      newcaretx -=  ((GetSysCharWidth(hWnd)+pSLEditData->colspace)<<1);
			   }
             }
             else
			 { scrollStep = 1;
			   newcaretx -=  (GetSysCharWidth(hWnd)+pSLEditData->colspace);
			 }
		  }
                    
          pSLEditData->startPos += scrollStep;
		  InvalidateRect(hWnd, &pSLEditData->textArea);
	   }

	}

    if(!scrollStep)
    { if(inserting)
	  {  TRECT InvRect=pSLEditData->textArea;
	     InvRect.left =  pSLEditData->caretx;
         InvalidateRect(hWnd, &InvRect);
	  }
      else if(!WndGetAttr(hWnd,ES_PASSWORD))
      { char dummybottom;
	    int tw=(chars==2)?GetSysCCharWidth(hWnd):GetSysCharWidth(hWnd);
        HDC dc=GetDC(hWnd);
	    GroupOn(dc);
        dummybottom=pSLEditData->buffer[pSLEditData->editPos+chars];
        pSLEditData->buffer[pSLEditData->editPos+chars]='\0';
	    EraseBackground(dc,pSLEditData->caretx,pSLEditData->carety,tw,pSLEditData->caretheight);
	    TextOut (dc, pSLEditData->caretx,pSLEditData->carety+CARET_HEIGHT_OVERLAP, &pSLEditData->buffer[pSLEditData->editPos]);
	    pSLEditData->buffer[pSLEditData->editPos+chars]=dummybottom;
        GroupOff(dc,pSLEditData->caretx,pSLEditData->carety,tw,pSLEditData->caretheight);
        ReleaseDC(dc);
	  }
   }

 
   pSLEditData->editPos+=chars; 
   pSLEditData->caretx  =  newcaretx; 

   SetCaretPos (hWnd,pSLEditData->caretx-1,pSLEditData->carety);
   CMD_NotifyParent(hWnd,EN_CHANGED);
}

int SLEdit_GetKeyDown(HWND hWnd,UINT Key)
{   TSLEdit *pSLEditData =(TSLEdit *)WndClsBuf(hWnd);
    switch (Key)
    {  case VK_HOME:
              if (pSLEditData->editPos != 0) 
			  { pSLEditData->editPos  = 0;
			    pSLEditData->caretx=pSLEditData->textArea.left;
                SetCaretPos(hWnd, pSLEditData->caretx-1, pSLEditData->carety);
                if (pSLEditData->startPos != 0)
                { InvalidateRect(hWnd,&pSLEditData->textArea);
				}
                pSLEditData->startPos = 0;
			  }
            break;
           
       case VK_END:
              if (pSLEditData->editPos != pSLEditData->dataEnd)
              { int textwidth,newStartPos = EdtGetStartDispPosAtEnd (hWnd, pSLEditData,&textwidth);
                pSLEditData->editPos = pSLEditData->dataEnd;
			    pSLEditData->caretx=pSLEditData->textArea.left+textwidth;
                SetCaretPos(hWnd, pSLEditData->caretx-1, pSLEditData->carety);
                if (pSLEditData->startPos != newStartPos)
                { InvalidateRect(hWnd,&pSLEditData->textArea);
                  pSLEditData->startPos = newStartPos;
				}
			  }
            break;

       case VK_LEFT:
              if (pSLEditData->editPos > 0) 
			  { int chars;
			    if(EdtIsACCharBeforePosition (pSLEditData->buffer, pSLEditData->editPos))
				{ chars=2;
			 	  pSLEditData->caretx -= (GetSysCCharWidth(hWnd)+pSLEditData->colspace);
				}
				else
				{ chars=1;
				  pSLEditData->caretx -= (GetSysCharWidth(hWnd)+pSLEditData->colspace);
				}
				pSLEditData->editPos -= chars;


				if(pSLEditData->caretx<pSLEditData->textArea.left || (pSLEditData->caretx==pSLEditData->textArea.left && pSLEditData->editPos > 0))
				{ if(pSLEditData->editPos>0)
				  {
					if(EdtIsACCharBeforePosition (pSLEditData->buffer, pSLEditData->editPos))
				    { pSLEditData->startPos -= (pSLEditData->caretx==pSLEditData->textArea.left)?2:chars+2;
					  pSLEditData->caretx = pSLEditData->textArea.left + GetSysCCharWidth(hWnd)+pSLEditData->colspace;
				    }
				    else
					{ pSLEditData->startPos -= (pSLEditData->caretx==pSLEditData->textArea.left)?1:chars+1;
					  pSLEditData->caretx = pSLEditData->textArea.left + GetSysCharWidth(hWnd)+pSLEditData->colspace;
					}
				   
				  }
				  else
				  { pSLEditData->startPos = 0;
				    pSLEditData->caretx=pSLEditData->textArea.left;
				  }
				  InvalidateRect(hWnd,&pSLEditData->textArea); /*need scroll*/
                    
				}
                SetCaretPos(hWnd, pSLEditData->caretx-1, pSLEditData->carety);
			  }
			else if(WndGetAttr(hWnd,ES_FULLTABKEY))
			{
			   return 0; /*表示此按键不接受，需要向下传递*/
			}

            break;
                
       case VK_RIGHT:
		      if (pSLEditData->editPos < pSLEditData->dataEnd)
			  { int chars,newcaretx;
				if(EdtIsACCharAtPosition (pSLEditData->buffer, pSLEditData->editPos))
				{ chars=2;
				  newcaretx=pSLEditData->caretx + GetSysCCharWidth(hWnd)+pSLEditData->colspace;
				}
				else
				{ chars=1;
			 	  newcaretx=pSLEditData->caretx + GetSysCharWidth(hWnd)+pSLEditData->colspace;
				}

                if (newcaretx  >= pSLEditData->textArea.right)
				{  int scrollStep=0;
				   if (EdtIsACCharAtPosition(pSLEditData->buffer, pSLEditData->startPos))
				   {  scrollStep = 2;
		              newcaretx -=  (GetSysCCharWidth(hWnd)+pSLEditData->colspace);
				   }
                   else
				   {  if (chars==2) 
				      { if (EdtIsACCharAtPosition(pSLEditData->buffer, pSLEditData->startPos + 1))
					    {  scrollStep = 3;
		                   newcaretx -=  (GetSysCCharWidth(hWnd)+GetSysCharWidth(hWnd)+ (pSLEditData->colspace<<1) );
					    }
                        else
						{  scrollStep = 2;
			               newcaretx -=  ((GetSysCharWidth(hWnd)+pSLEditData->colspace)<<1);
						}
					  }
                      else
					  { scrollStep = 1;
			            newcaretx -=  (GetSysCharWidth(hWnd)+pSLEditData->colspace);
					  }
				   }
				   

                   pSLEditData->startPos += scrollStep;
		           InvalidateRect(hWnd, &pSLEditData->textArea);
				}
                pSLEditData->editPos+=chars;
                pSLEditData->caretx=newcaretx;
                SetCaretPos(hWnd,pSLEditData->caretx-1, pSLEditData->carety);
			} 
			else if(WndGetAttr(hWnd,ES_FULLTABKEY))
			{
			   return 0; /*表示此按键不接受，需要向下传递*/
			}
            break;
                
       case VK_INSERT:
               WNDPTR(hWnd)->Style ^= ES_REPLACE;
            break;
 
       case VK_DELETE:
               if (!WndGetAttr(hWnd,ES_READONLY) && (pSLEditData->editPos != pSLEditData->dataEnd))
			   {  TRECT    InvRect=pSLEditData->textArea;
				  int i,deleted;
				  deleted = (EdtIsACCharAtPosition(pSLEditData->buffer, pSLEditData->editPos))?2:1;
                  for (i = pSLEditData->editPos; i < pSLEditData->dataEnd - deleted; i++)
				  {    pSLEditData->buffer [i] = pSLEditData->buffer [i + deleted];
				  }
                  pSLEditData->buffer[i]='\0';
                  pSLEditData->dataEnd -= deleted;
                  InvalidateRect(hWnd,&InvRect);
			      CMD_NotifyParent(hWnd,EN_CHANGED);
			   }
               break;
 
       case VK_BACK:
              if ( !WndGetAttr(hWnd,ES_READONLY) && (pSLEditData->editPos != 0)) 
			  { SLEdit_GetKeyDown(hWnd,VK_LEFT);
                SLEdit_GetKeyDown(hWnd,VK_DELETE);
			  }
             break;
	

   }
   return 1;
       
}

static HRESULT CALLBACK SLEditCtrlProc(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam)
{   switch (Message)
    {   case WM_CHAR:
             if (!WndGetAttr(hWnd,ES_READONLY))
             {  extern   HWND IME_Window;
			    BYTE wordlo=LOBYTE(WParam),wordhi=HIBYTE(WParam);
                if(wordlo>0xA0 &&  wordhi==0)
                { BYTE savedchar=(BYTE)IME_PopChar();
				  if(savedchar>0xA0)
				  { SLEdit_GetChar(hWnd,savedchar,wordlo,LParam); 
				  }
				  else
				  { IME_PushChar(wordlo);
				  }
				  return 0;
				}

				if(IME_Window && WNDPTR(IME_Window)->UserData==1)/*中文输入法*/
                {  if(SendMessage((HWND)IME_Window,WM_CHAR,WParam,LParam)==0)return 0;
                }

				SLEdit_GetChar(hWnd,wordlo,wordhi,LParam); 
             }
             return 0;
        case WM_COMMAND:
              if(LParam==WM_CHAR && !WndGetAttr(hWnd,ES_READONLY))
              { SLEdit_GetChar(hWnd,LOBYTE(WParam),HIBYTE(WParam),0);
              }
             return 0;
        case WM_KEYDOWN:
			   if(WParam!=VK_TAB)
			   { extern   HWND IME_Window;
			     if(IME_Window && WNDPTR(IME_Window)->UserData==1)/*中文输入法*/
                 { if(!IME_isEmpty()) 
				   { if(SendMessage((HWND)IME_Window,WM_KEYDOWN,WParam,LParam))return 0;
			       }
                 }
			     if(WParam!=VK_UP && WParam!=VK_DOWN)
				 { if(SLEdit_GetKeyDown(hWnd,WParam)) return 0;
				 }
			   }
			 break;

        case WM_GETTEXTLENGTH:
             return ((TSLEdit *)WndClsBuf(hWnd))->dataEnd;

        case WM_SETTEXT:
			 { TSLEdit *pSLEditData= (TSLEdit *)WndClsBuf(hWnd);
               int len;
               if(LParam)
	           { if(strcmp((char*)LParam,pSLEditData->buffer)==0) return -1;
	             len = min ((int)strlen((char*)LParam), pSLEditData->bufferLen-1);
	             memcpy (pSLEditData->buffer, (char*)LParam, len);
   	           }
	           else
	           { if(pSLEditData->buffer[0]=='\0')return -1;
	             len=0;
	           }
               pSLEditData->dataEnd = len;
               pSLEditData->buffer[len]='\0';
               pSLEditData->editPos        = 0;
               pSLEditData->caretx         = pSLEditData->textArea.left;
               pSLEditData->startPos       = 0;
               SetCaretPos(hWnd, pSLEditData->caretx-1,pSLEditData->carety);  
               InvalidateRect(hWnd,&pSLEditData->textArea); 
			 }
             return 0;

        case WM_LBUTTONDOWN:
        {   int cx=LOWORD(LParam)-WNDPTR(hWnd)->ClientRect.left;
			int pos=Edit_LocateCarret(hWnd,&cx);
            TSLEdit *pSLEditData = (TSLEdit *)WndClsBuf(hWnd);
            if(pos!=pSLEditData->editPos)
			{ pSLEditData->editPos=pos;
              pSLEditData->caretx=cx;
			  SetCaretPos(hWnd, pSLEditData->caretx-1, pSLEditData->carety);
			}
        }
        break;
    
 	 
        case EM_SETREADONLY:
		     if( (WndGetAttr(hWnd,ES_READONLY)!=0) != (WParam!=0) )
			 {  if (WParam)
			    { WndAddAttr(hWnd,ES_READONLY);
			      DestroyCaret(hWnd);
			    }
			    else
			    { WndSubAttr(hWnd,ES_READONLY);
			      if(WndGetAttr(hWnd,WS_FOCUS))
				  { TSLEdit *pSLEditData = (TSLEdit *)WndClsBuf(hWnd);
				    CreateCaret (hWnd,  1 , pSLEditData->caretheight);
                   SetCaretPos (hWnd,pSLEditData->caretx-1, pSLEditData->carety);
				  } 
			    }
             }
             return 0;
        case EM_SETPASSWORDCHAR:
             { TSLEdit *pSLEditData = (TSLEdit *)WndClsBuf(hWnd);
               if (pSLEditData->passwdChar != (int)WParam)
			   { if (WndGetAttr(hWnd,ES_PASSWORD))
			     { pSLEditData->passwdChar = (int)WParam;
                   InvalidateRect(hWnd,&pSLEditData->textArea);
                 }
			   }
			 }
             return 0;
    
        case EM_GETPASSWORDCHAR:
			 if(LParam)
			 {  *(char *)LParam = ((TSLEdit *)WndClsBuf(hWnd))->passwdChar;
			 }
             return 0;
   
        case EM_LIMITTEXT:
			 { int newLimit = (int)WParam;
               if (newLimit >= 0)
			   { TSLEdit *pSLEditData = (TSLEdit *)WndClsBuf(hWnd);
			     char *newbuffer=(char *)RellocMem(pSLEditData->buffer,newLimit);
				 if(newbuffer)
				 { pSLEditData->buffer=newbuffer;
				   pSLEditData->bufferLen = newLimit;
                 }
			   }
			 }
             return 0;

		case WM_CREATE:
		       SLEdit_OnCreate(hWnd);
			 return 0;

        case WM_FONTCHANGE:
		     { TSLEdit *pSLEditData=(TSLEdit *)WndClsBuf(hWnd);
			   pSLEditData->caretheight = GetSysCharHeight(hWnd)+CARET_HEIGHT_OVERLAP+CARET_HEIGHT_OVERLAP;    
			   ResetCaret(hWnd, 1 , pSLEditData->caretheight);
			 }
		     return 0;

        case WM_SETFOCUS:
			 { if (!WndGetAttr(hWnd,ES_READONLY))
			   { TSLEdit *pSLEditData=(TSLEdit *)WndClsBuf(hWnd);
                 CreateCaret (hWnd,  1 , pSLEditData->caretheight);
                 SetCaretPos (hWnd,pSLEditData->caretx-1, pSLEditData->carety);
               }
               CMD_NotifyParent(hWnd,CM_SETFOCUS); 
   			 }
             return 0;
         case WM_KILLFOCUS:
                DestroyCaret(hWnd);
                CMD_NotifyParent(hWnd,CM_KILLFOCUS);
              return 0;
        case WM_ENABLE:
                InvalidateRect(hWnd,&((TSLEdit *)WndClsBuf(hWnd))->textArea);
             return 0; 
        case WM_PAINT:
			    SLEdit_Repaint(hWnd);
             return 0;
 
    } 

    return DefWindowProc (hWnd, Message, WParam, LParam);
}
//---------------------------------------------------------------------------
void Edit_SetReadOnly(HWND hWnd,BOOL bReadonly)
{ SendMessage(hWnd,EM_SETREADONLY,(WPARAM)bReadonly,0);
}
//---------------------------------------------------------------------------
void Edit_SetMaxLength(HWND hWnd,int maxLength)
{ maxLength++; // keep space for null-terminator of string.
  SendMessage(hWnd,EM_LIMITTEXT,(WPARAM)maxLength,0);
}
//---------------------------------------------------------------------------
void Edit_SetPasswordChar(HWND hWnd,char pswchar)
{ SendMessage(hWnd,EM_SETPASSWORDCHAR,(WPARAM)pswchar,0);
}
//---------------------------------------------------------------------------
char Edit_GetPasswordChar(HWND hWnd)
{ char psword;
  SendMessage(hWnd,EM_SETPASSWORDCHAR,0,(LPARAM)&psword);
  return psword;
}
//---------------------------------------------------------------------------
void Edit_SetReplaceMode(HWND hWnd,BOOL bSwitch)
{ if(bSwitch) WndAddAttr(hWnd,ES_REPLACE);
  else WndSubAttr(hWnd,ES_REPLACE);
}
//---------------------------------------------------------------------------
void CM_RegisterSingleLineEdit(void)
{  TWNDCLASS wc;
   memset(&wc,0,sizeof(wc));
   wc.dwStyle=WS_BORDER_LOWERED;
   wc.clForeground=CL_WINDOWTEXT;
   wc.clBackground=CL_WHITE;
   wc.cbTextHeap=LEN_SLEDIT_BUFFER;
   wc.cbWndExtra=sizeof(TSLEdit);
   wc.lpfnWndProc=SLEditCtrlProc;
   wc.lpszClassName="Edit";
   RegisterClass(&wc);
}
//---------------------------------------------------------------------------
