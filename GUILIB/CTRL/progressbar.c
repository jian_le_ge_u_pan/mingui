/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      ProgressBar.c
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
* 
* MODIFICATION HISTORY
*     LastModify  2008/06/05
******************************************************************************/
#include "mingui.h"
//---------------------------------------------------------------------------
typedef struct t_ProgressBar
{ int Max,Pos;
}TProgressBar;
//---------------------------------------------------------------------------
void ProgressBar_SetMax(HWND hWnd,int maxValue)
{ TProgressBar *pb = (TProgressBar *)WndClsBuf(hWnd);
  if(pb->Max!=maxValue)
  { pb->Max=maxValue;
    if(pb->Pos>maxValue) pb->Pos=maxValue;
    Invalidate(hWnd);
  }
}
//---------------------------------------------------------------------------
void ProgressBar_SetPos(HWND hWnd,int position)
{ TProgressBar *pb = (TProgressBar *)WndClsBuf(hWnd);
  if(position>pb->Max)position=pb->Max;
  if(position!=pb->Pos)
  { pb->Pos=position;
    Invalidate(hWnd);
  }
}
//---------------------------------------------------------------------------
static void ProgressBar_Repaint(HWND hWnd)
{  HDC dc=BeginPaint(hWnd);
   TProgressBar *pb = (TProgressBar *)WndClsBuf(hWnd);
   int w,scale;
   int clientWidth=crWidth(hWnd),clientHeight=crHeight(hWnd);
  
   if(pb->Pos>0 && pb->Max>0)
   { w=clientWidth*pb->Pos/pb->Max;
     scale=100*pb->Pos/pb->Max;
   }
   else
   { w=scale=0;
   }
  
   if(w>0) FillRect(dc,0,0,w,clientHeight);
  
   if(clientHeight>=GetSysCharHeight(hWnd))
   { int len;
	 char strbuf[8];
     if(scale<10)
	 { strbuf[0]='0'+scale;
	   len=1;
	 }
	 else if(scale<100)
	 { strbuf[0]='0' + scale/10;
	   strbuf[1]='0' + scale%10;
       len=2;
	 }
	 else
	 { strbuf[0]='1';
	   strbuf[1]='0';
	   strbuf[2]='0';
       len=3;
	 }
     strbuf[len]='%';
	 strbuf[len+1]='\0';
     
	 SetPenLogic(dc,PL_XOR);
	 SetColorIndex(dc,-1);
	 DrawText(dc,0,0,clientWidth,clientHeight,strbuf,alCenter|alMiddle);
  }
  
  EndPaint(hWnd);
}
//---------------------------------------------------------------------------
static HRESULT CALLBACK DefProgressBarProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam)
{ switch(message)
  { case WM_PAINT:
		   ProgressBar_Repaint(hWnd);
         return 0;
    case WM_CREATE:
		 { TProgressBar *pb = (TProgressBar *)WndClsBuf(hWnd);
		   pb->Pos=0;
		   pb->Max=100;
		 }
		 return 0;
  }
 return DefWindowProc(hWnd,message,wParam,lParam);
}

//---------------------------------------------------------------------------
void CM_RegisterProgressBar(void)
{  TWNDCLASS wc;
   memset(&wc,0,sizeof(wc));
   wc.clForeground=CL_BLUE;
   wc.clBackground=CL_BTNFACE;   
   wc.lpfnWndProc=DefProgressBarProc;
   wc.lpszClassName="ProgressBar";
   wc.cbWndExtra=sizeof(TProgressBar);
   RegisterClass(&wc);
}
/*---------------------------------------------------------------------------
END --- Thank you!                                                  ming.c
---------------------------------------------------------------------------*/



