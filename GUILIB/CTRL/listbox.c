/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:			ListBox.c
*  PROGRAMMER:			ming.c
*  Date of Creation:		2006/08/8
*
*  DESCRIPTION: 								
*						
*  NOTE:						 		
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
*
*  MODIFICATION HISTORY
*     LastModify  2006/09/20
******************************************************************************/
#include "mingui.h"
//---------------------------------------------------------------------------
#define  Default_LeftMargin             3 
#define  Default_LineMargin             4
#define  MarqueeInterval                1000  
//---------------------------------------------------------------------------
typedef struct _ListBox_Item
{ struct _ListBox_Item *next,*prev;
  int  index;
  char *caption;
}TListBoxItem;
typedef void (*TListItemCallback)(HWND,int);

typedef struct t_ListBox
{ int TotalItemCount,CountPerPage,LineMargin,LeftMargin,ItemHeight;
  TListBoxItem *Head,*PageFirst,*Selected;
  TListItemCallback fuc;
  /*--Marquee-------------------------------*/
  HANDLE MarqueeTimer;
  TListBoxItem *MarqueeItem,*NewMarquee;
  int    MarqueePos;
 }TListBox;
//---------------------------------------------------------------------------
static TListBoxItem *GetItemNodeByIndex(HWND,int);
static void ListBox_MarqueeProc(HWND);
static void RepaintSubItem(HWND hWnd,int index);
static void ListBox_DirectSubFocus(HWND hWnd,BOOL UpOrDown);
static HRESULT CALLBACK DefListBoxProc(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam);
//---------------------------------------------------------------------------
void ListBoxCreate(HWND hWnd)
{ TListBox *m=(TListBox *)WndClsBuf(hWnd);
  
  memset(m,0,sizeof(TListBox));

  m->fuc=(TListItemCallback)WNDPTR(hWnd)->ExtraLong[0];

  m->LeftMargin=Default_LeftMargin;
  m->LineMargin=Default_LineMargin;
  m->ItemHeight=GetSysCharHeight(hWnd)+m->LineMargin;
  m->CountPerPage=(crHeight(hWnd)+(GetSysCharHeight(hWnd)>>1))/m->ItemHeight;

  m->MarqueeTimer=CreateTimer(hWnd,MarqueeInterval,MarqueeInterval,ListBox_MarqueeProc,false);

  ScrollBar_Initialize(hWnd,0,0,m->CountPerPage,m->CountPerPage);
}
//---------------------------------------------------------------------------
void ListBoxDestroy(HWND hWnd)
{ TListBoxItem *node,*delNode,*head;
  TListBox *m=(TListBox *)WndClsBuf(hWnd);
  if(m)
  { DestroyTimer(m->MarqueeTimer);
	head=m->Head;
    if(head)
	{ node=head;
      do
	  { delNode=node;
        node=node->next;
        FreeMem(delNode->caption);
        FreeMem(delNode);
	  }while(node!=head);
	}
  }
}
//---------------------------------------------------------------------------
int GetWindowTextWidth(HDC hWnd,LPCSTR text)
{ if(hWnd && text && *text)
  { BYTE ch=*text;
    int colspace=0,textwidth=0,eWidth=GetSysCharWidth(hWnd),cWidth=GetSysCCharWidth(hWnd);
    while (ch)
    { if(ch>0xa0)
      { if(*++text)
        { textwidth=textwidth+cWidth+colspace;
        }else break;
      }
	  else
      { textwidth=textwidth+eWidth+colspace;
      }
      ch=*++text;
    }
    return textwidth;
  }else return 0;
}

//---------------------------------------------------------------------------
void ListBox_SelectedAssign(HWND hWnd,TListBoxItem *newselected)
{ TListBox *m=(TListBox *)WndClsBuf(hWnd);
  if(m->Selected!=newselected)
  { bool bNeedMarquee=false;
	if(newselected)
	{ int textwidth=GetWindowTextWidth(hWnd,newselected->caption);
      int clipwidth=crWidth(hWnd)-m->LeftMargin;
	  if(textwidth>clipwidth) bNeedMarquee=true;
	}
	if(bNeedMarquee)
	{ if(WndGetAttr(hWnd,WS_FOCUS))EnableTimer(m->MarqueeTimer,true);
	  m->NewMarquee=newselected;
	}
	else
	{ m->NewMarquee=NULL;
	}
	m->Selected=newselected;	
	CMD_NotifyParent(hWnd,CM_CHANGED); 
  }
}
//---------------------------------------------------------------------------
void ListBox_MarqueeProc(HWND hWnd)
{ TListBox *m=(TListBox *)WndClsBuf(hWnd);
  if(m->MarqueeItem==m->NewMarquee)
  { if(m->MarqueeItem)
    { BYTE *byteCaption=(BYTE *)m->MarqueeItem->caption;
      if(byteCaption[m->MarqueePos]>0xA0) m->MarqueePos+=2;
	  else if(byteCaption[m->MarqueePos]>0)m->MarqueePos+=1;
      if(!byteCaption[m->MarqueePos]) m->MarqueePos=0;
	  RepaintSubItem(hWnd,m->MarqueeItem->index);
    }
    else 
	{ EnableTimer(m->MarqueeTimer,false);
	}
  }
  else
  { if(m->MarqueeItem && m->MarqueePos)
    { /*if need reset last marquee then just do it*/
	  m->MarqueePos=0;
	  RepaintSubItem(hWnd,m->MarqueeItem->index);
    }
    m->MarqueePos=0;
    m->MarqueeItem=m->NewMarquee;
  }
}
//---------------------------------------------------------------------------
void ListBoxPageScroll(HWND hWnd,int newtop,int step)
{ TListBox *m=(TListBox *)WndClsBuf(hWnd);
  if(m->PageFirst && newtop!=m->PageFirst->index && m->CountPerPage>1)
  { TListBoxItem *item=m->Head;
    int i;
	for(i=0;i<newtop;i++)item=item->next;
    m->PageFirst=item;
	
	Invalidate(hWnd);
	ScrollBar_Synchronize(hWnd,m->PageFirst->index);
  }
  else
  {  ListBox_DirectSubFocus(hWnd,(BOOL)(step<0));
  }
}
//---------------------------------------------------------------------------
void ListBoxChangeSubFocus(HWND hWnd,int newselect)
{ TListBox *m=(TListBox *)WndClsBuf(hWnd);
  int oldselelect=(m->Selected)?m->Selected->index:-1;
  if(newselect!=oldselelect && m->TotalItemCount>0)
  { int new_pageoffset,old_pageoffset;
	int page_first=m->PageFirst->index;
    int page_bottom=page_first+m->CountPerPage-1;
	if(page_bottom>=m->TotalItemCount)page_bottom=m->TotalItemCount-1;
	new_pageoffset=(newselect>=page_first && newselect<=page_bottom)?newselect-page_first:-1;
	old_pageoffset=(oldselelect>=page_first && oldselelect<=page_bottom)?oldselelect-page_first:-1;

    if(old_pageoffset>=0 || new_pageoffset>=0)
    {   HDC dc=GetDC(hWnd);
        int areaHeight,areaWidth;

        SetPenLogic(dc,PL_XOR);
        SetColorIndex(dc,-1);
        areaWidth=crWidth(hWnd);
        areaHeight=m->ItemHeight;
        
        if( WndGetAttr(hWnd,WS_FOCUS) )
		{
          if(old_pageoffset>=0 && new_pageoffset<0)
		  {  FillRect(dc,0,old_pageoffset*areaHeight,areaWidth,areaHeight);
		  }
          else if(new_pageoffset>=0 && old_pageoffset<0)
		  { FillRect(dc,0,new_pageoffset*areaHeight,areaWidth,areaHeight);
		  }
          else if(new_pageoffset-old_pageoffset==1)
		  { FillRect(dc,0,old_pageoffset*areaHeight,areaWidth,areaHeight<<1);
		  }
          else if(old_pageoffset-new_pageoffset==1)
		  { FillRect(dc,0,new_pageoffset*areaHeight,areaWidth,areaHeight<<1);
		  }
          else
		  { FillRect(dc,0,old_pageoffset*areaHeight,areaWidth,areaHeight);
            FillRect(dc,0,new_pageoffset*areaHeight,areaWidth,areaHeight);
		  }
		}
		else if(m->CountPerPage>1) /*单行框不需要虚线反显*/
		{ if(old_pageoffset>=0)
		  { DrawDashedRect(dc,0,old_pageoffset*areaHeight,areaWidth,areaHeight);
		  }
          if(new_pageoffset>=0)
		  { DrawDashedRect(dc,0,new_pageoffset*areaHeight,areaWidth,areaHeight);
		  }

		}
        ReleaseDC(dc);
    }
	ListBox_SelectedAssign(hWnd,GetItemNodeByIndex(hWnd,newselect));
  }
}
 
//---------------------------------------------------------------------------
void RepaintSubItem(HWND hWnd,int index)
{ TListBox *m=(TListBox *)WndClsBuf(hWnd);
  TListBoxItem *mn=m->PageFirst;
  if(mn && index>=mn->index)
  { int page_bottom=mn->index+m->CountPerPage-1;
	if(page_bottom>=m->TotalItemCount)page_bottom=m->TotalItemCount-1;
    if(index<=page_bottom)
	{ HDC dc;
	  int j,offset=index-mn->index;
	  int ItemWidth=crWidth(hWnd);
      int ItemHeight=m->ItemHeight;
	  for(j=0;j<offset;j++)
	  { mn=mn->next;
	  }
	  dc=GetDC(hWnd);
	  GroupOn(dc);
	  j = offset*ItemHeight;
      EraseBackground(dc, 0,j,ItemWidth,ItemHeight);
	   
	  if(mn->caption && *mn->caption)
	  { char *strCaption=(mn==m->MarqueeItem)?mn->caption+m->MarqueePos:mn->caption;
        SetPenLogic(dc,PL_REPLACE);	   
		ResetForeground(dc);
		TextOut(dc,m->LeftMargin,j+(m->LineMargin>>1),strCaption);
	  }
	  if(mn==m->Selected)
	  { SetPenLogic(dc,PL_XOR);
        SetColorIndex(dc,-1);
		if(WndGetAttr(hWnd,WS_FOCUS))
           FillRect(dc,0,j,ItemWidth,ItemHeight);
		else if(m->CountPerPage>1) /*单行框不需要虚线反显*/
           DrawDashedRect(dc,0,j,ItemWidth,ItemHeight);
	  }
      GroupOff(dc,0,j,ItemWidth,ItemHeight);
      ReleaseDC(dc);
	}
  }	  
 
}
//---------------------------------------------------------------------------
void ListBox_DirectSubFocus(HWND hWnd,int UpOrDown)
{ TListBox *m=(TListBox *)WndClsBuf(hWnd);
  TListBoxItem *ItemSelected=m->Selected;
  BOOL PageScolled=false;
  if(!ItemSelected)
  { if(!m->PageFirst)return;
	 else ItemSelected=m->PageFirst;
  }
  if(UpOrDown)
  {  if(ItemSelected!=m->Head)
     { if(ItemSelected==m->PageFirst)
       { PageScolled=true;
         m->PageFirst=m->PageFirst->prev;
       }
       ItemSelected=ItemSelected->prev;
     }
  }
  else
  {  if(ItemSelected->next!=m->Head)
     { if(ItemSelected->index==m->PageFirst->index+m->CountPerPage-1)
       { PageScolled=true;
         m->PageFirst=m->PageFirst->next;
       }
       ItemSelected=ItemSelected->next;
     }
  }
  
  if(ItemSelected!=m->Selected)
  { if(PageScolled)
    { ListBox_SelectedAssign(hWnd,ItemSelected);
      Invalidate(hWnd);
	  if(WndGetAttr(hWnd,WS_VSCROLL|WS_BORDER)==(WS_VSCROLL|WS_BORDER))
	  {  ScrollBar_Synchronize(hWnd,m->PageFirst->index);
	  }
    }
    else
    { ListBoxChangeSubFocus(hWnd,ItemSelected->index);
    }
  }
}
//---------------------------------------------------------------------------
void ListBoxDealPenEvent(HWND hWnd,DWORD Message,int yPos)
{ TListBox *m=(TListBox *)WndClsBuf(hWnd);
  if(m->TotalItemCount>0 && m->ItemHeight>0)
  {  int oldselect=(m->Selected)?m->Selected->index:-1;
	 int newselect=m->PageFirst->index+ ( yPos - WNDPTR(hWnd)->ClientRect.top ) / m->ItemHeight;
	 if(newselect>=m->TotalItemCount) newselect=-1;

     if(newselect !=oldselect)
	 { ListBoxChangeSubFocus(hWnd,newselect);
	 }
	 if(Message==WM_LBUTTONUP && newselect>=0)
	 { if(m->fuc) m->fuc(hWnd,newselect);
	 }
  }
}
//---------------------------------------------------------------------------
void ListBoxRepaint(HWND hWnd)
{ TListBox *m=(TListBox *)WndClsBuf(hWnd);
  TListBoxItem *mn=m->PageFirst;
  HDC dc=BeginPaint(hWnd);
  int clientwidth=crWidth(hWnd);
 
  if(mn)
  { int offsetY=0;
    int ItemHeight=m->ItemHeight;
	int textLeft=m->LeftMargin;
	int LineMarginTop=m->LineMargin>>1;
	int i,page_bottom=mn->index+m->CountPerPage-1;
	if(page_bottom>=m->TotalItemCount)page_bottom=m->TotalItemCount-1;

	SetFontStyle(dc,FS_PLAIN,true)

	for(i=mn->index;i<=page_bottom;i++)
	{ if(mn->caption!=NULL && *mn->caption)
	  { TextOut(dc,textLeft,LineMarginTop+offsetY,mn->caption);
	  }

      if(mn==m->Selected)
	  { SetPenLogic(dc,PL_XOR);
        SetColorIndex(dc,-1);
        if(WndGetAttr(hWnd,WS_FOCUS))
          FillRect(dc,0,offsetY,clientwidth,ItemHeight);
		else if(m->CountPerPage>1) /*单行框不需要虚线反显*/
		  DrawDashedRect(dc,0,offsetY,clientwidth,ItemHeight);
        SetPenLogic(dc,PL_REPLACE);
        ResetForeground(dc);
	  }

      offsetY+=ItemHeight;
      mn=mn->next;
    }
  }
  
  EndPaint(hWnd);
}
//---------------------------------------------------------------------------
int ListBox_InsertItem(HWND hWnd,int index,char *strItem)
{ TListBoxItem *node;
  TListBox *m=(TListBox *)WndClsBuf(hWnd);
  int i;
  node=(TListBoxItem *)GetMem(sizeof(TListBoxItem));
  if(strItem)
  { node->caption=(char *)GetMem(strlen(strItem)+1);
    strcpy(node->caption,strItem);
  }else node->caption=NULL;

  if(m->TotalItemCount==0)
  { m->Head=m->PageFirst=node->next=node->prev=node;
    node->index=0;
	ListBox_SelectedAssign(hWnd,node);
	index=0;
  }
  else
  { TListBoxItem *insertpos=m->Head;
    
    if(index>=0 && index<m->TotalItemCount)
	{ if(index==0) m->Head=node;
	  else for(i=0;i<index;i++) insertpos=insertpos->next;
	  if(insertpos==m->PageFirst) m->PageFirst=node;
	}else index=m->TotalItemCount; /*return value*/

	node->next=insertpos;
    node->prev=insertpos->prev;
    node->next->prev=node;
    node->prev->next=node;
	node->index=index;
  }
  m->TotalItemCount++;
  for(i=index;i<m->TotalItemCount;i++)
  {  node->index=i;
	 node=node->next;
  }

  Invalidate(hWnd);
  
  if(WndGetAttr(hWnd,WS_VSCROLL|WS_BORDER)==(WS_VSCROLL|WS_BORDER))
  { ScrollBar_Synchronize2(hWnd,m->PageFirst->index,m->TotalItemCount);
  }

 
  return index;
}

//---------------------------------------------------------------------------
void ListBox_ChangeItem(HWND hWnd,int index,char *strItem)
{  TListBoxItem *node=GetItemNodeByIndex(hWnd,index);
   if(node)
   { if(strcmp(node->caption,strItem)!=0)
     { TListBox *m=(TListBox *)WndClsBuf(hWnd);
	   if(node->caption) FreeMem(node->caption);
       if(strItem)
	   { node->caption=(char *)GetMem(strlen(strItem)+1);
         strcpy(node->caption,strItem);
	   }else node->caption=NULL;

	   if(m->MarqueeItem==node)
       { int textwidth=GetWindowTextWidth(hWnd,node->caption);
         int clipwidth=crWidth(hWnd)-m->LeftMargin;
	     if(textwidth<=clipwidth) 
		 { m->MarqueeItem=NULL;
		   if(m->NewMarquee==node)
		   { m->NewMarquee=NULL;
		     EnableTimer(m->MarqueeTimer,false);
		   }
		 }
	   }

	   RepaintSubItem(hWnd,index);
	 }
   }
}
//---------------------------------------------------------------------------
int ListBox_AddItem(HWND hWnd,char *strItem)
{ return ListBox_InsertItem(hWnd,-1,strItem);
}
//---------------------------------------------------------------------------
bool ListBox_DeleteItem(HWND hWnd,int index)
{ TListBox *m=(TListBox *)WndClsBuf(hWnd);
  if(index>=0 && index<m->TotalItemCount)
  {	TListBoxItem *node,*nextnode;
	int i;
	for(i=0,node=m->Head;i<index;i++)
    {  node=node->next;
	}
	
	if(m->TotalItemCount>1)
	{ if(node==m->Selected)
	  {	if(node->next!=m->Head) ListBox_SelectedAssign(hWnd,node->next);
		else ListBox_SelectedAssign(hWnd,node->prev); 
	  }

      if(node==m->Head)m->Head=node->next;
      if(node==m->PageFirst)m->PageFirst=node->next;
	  
	  node->prev->next=node->next;
	  node->next->prev=node->prev;

	  if(m->PageFirst->index+m->CountPerPage>=m->TotalItemCount && m->PageFirst!=m->Head)
	  { m->PageFirst=m->PageFirst->prev; /*a line scoll up*/
	  }
	  
	}
	else
	{ m->Head=m->PageFirst=NULL;
	  ListBox_SelectedAssign(hWnd,NULL);
	}

	if(node==m->NewMarquee)
	{ m->NewMarquee=NULL;
	}
	if(node==m->MarqueeItem)
	{ m->MarqueeItem=NULL;
	}
    
	nextnode=node->next;
	FreeMem(node->caption);
    FreeMem(node);
	m->TotalItemCount--;
	for(i=index;i<m->TotalItemCount;i++)
	{  nextnode->index=i;
	   nextnode=nextnode->next;
	} 
     
    Invalidate(hWnd);

    if(WndGetAttr(hWnd,WS_VSCROLL|WS_BORDER)==(WS_VSCROLL|WS_BORDER))
	{ ScrollBar_Synchronize2(hWnd,(m->PageFirst)?m->PageFirst->index:0,m->TotalItemCount);
	}
    return true;
  }else return false;
}
//---------------------------------------------------------------------------
int  ListBox_SetSelectedIndex(HWND hWnd,int index)
{ TListBox *m=(TListBox *)WndClsBuf(hWnd);
  int oldsel=(m->Selected)?m->Selected->index:-1;
  if(m->PageFirst)
  { if(index<0 || index>=m->TotalItemCount)index=-1;
    if(index==oldsel) return oldsel;
    if(index==-1)
	{ ListBoxChangeSubFocus(hWnd,-1);
	}
	else if(index<m->PageFirst->index)
	{ TListBoxItem *node=m->PageFirst;
	  int offset=node->index - index;
	  while(offset>0)
	  { offset--;
	    node=node->prev;
	  }
	  ListBoxPageScroll(hWnd,node->index,node->index-m->PageFirst->index);
      ListBox_SelectedAssign(hWnd,node);
	}
	else
	{ int page_bottom=m->PageFirst->index+m->CountPerPage-1;
	  if(page_bottom>=m->TotalItemCount)page_bottom=m->TotalItemCount-1;
      if(index<=page_bottom)
	  { ListBoxChangeSubFocus(hWnd,index);
	  }
	  else
	  {  TListBoxItem *node=m->PageFirst;
		 int offset=index - node->index;
		 while(offset>0)
		 { offset--;
	       node=node->next;
		 }
         if(node->index+m->CountPerPage<=m->TotalItemCount)
		 {  ListBoxPageScroll(hWnd,node->index,node->index-m->PageFirst->index);
		 }
		 else
		 { int newtop=m->TotalItemCount-m->CountPerPage;
		   ListBoxPageScroll(hWnd,newtop,newtop-m->PageFirst->index);
		 }
		 ListBox_SelectedAssign(hWnd,node);
	  }
	}
  }
  return oldsel;
}
//---------------------------------------------------------------------------
int ListBox_GetSelectedIndex(HWND hWnd)
{ TListBox *m=(TListBox *)WndClsBuf(hWnd);
  return (m->Selected)?m->Selected->index:-1;
}
//---------------------------------------------------------------------------
int ListBox_GetItemIndex(HWND hWnd,TListBoxItem *item)
{ if(item)
  {  return item->index;
  }
  return -1;
}
//---------------------------------------------------------------------------
char *ListBox_GetSelectedItem(HWND hWnd)
{ TListBox *m=(TListBox *)WndClsBuf(hWnd);
  if(m->Selected) return m->Selected->caption;
  else return NULL;
}
//---------------------------------------------------------------------------
TListBoxItem  *GetItemNodeByIndex(HWND hWnd,int index)
{ if(index>=0)
  { TListBox *m=(TListBox *)WndClsBuf(hWnd);
    TListBoxItem *node;   
    int i;
    for(i=0,node=m->Head;i<m->TotalItemCount;i++,node=node->next)
	{ if(i==index) return node;
	}
  }
  return NULL;
}
//---------------------------------------------------------------------------
char *ListBox_GetItemByIndex(HWND hWnd,int index)
{ TListBoxItem *node=GetItemNodeByIndex(hWnd,index);   
  return (node)?node->caption:NULL;
}
//---------------------------------------------------------------------------
void ListBox_SetMargin(HWND hWnd,int leftMargin,int lineMargin)
{ TListBox *m=(TListBox *)WndClsBuf(hWnd);
  
  if(m->LeftMargin!=leftMargin || m->LineMargin!=lineMargin)
  { 
	if(m->LeftMargin!=leftMargin)
	{ m->LeftMargin=leftMargin;
	}
    if(m->LineMargin!=lineMargin)
	{ int font_height=GetSysCharHeight(hWnd);
	  m->LineMargin=lineMargin;
	  m->ItemHeight=font_height+lineMargin;
	  m->CountPerPage=(crHeight(hWnd)+(font_height>>1))/m->ItemHeight;
	}
    Invalidate(hWnd);
  }
}
//---------------------------------------------------------------------------
HRESULT CALLBACK DefListBoxProc(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam)
{
  switch(Message)
  { case WM_KEYDOWN:
		 if(WParam==VK_UP || WParam==VK_DOWN) 
         { ListBox_DirectSubFocus(hWnd,(BOOL)(WParam==VK_UP));
		   return 0;
		 }
        break;

    case WM_KEYUP:
		 if(WParam==VK_RETURN)
         { TListBox *lstbox=(TListBox *)WndClsBuf(hWnd);
		   if(lstbox->Selected && lstbox->fuc)
		   { lstbox->fuc(hWnd,lstbox->Selected->index);
             return 0; 
           }
		 }
         break;

    case WM_LBUTTONDOWN:
		 { int xPos=LOWORD(LParam);
           int yPos=HIWORD(LParam);
           if(PointInRect(xPos,yPos,&WNDPTR(hWnd)->ClientRect))  
           { ListBoxDealPenEvent(hWnd,Message,yPos); 
		   }
		   break;
		 }

	case WM_VSCROLL:
           ListBoxPageScroll(hWnd,WParam,LParam);
		 return 0;
    
    case WM_LBUTTONUP:
	case WM_MOUSEMOVE:
		 { int xPos=LOWORD(LParam);
           int yPos=HIWORD(LParam);
		   if(PointInRect(xPos,yPos,&WNDPTR(hWnd)->ClientRect))  
		   { ListBoxDealPenEvent(hWnd,Message,yPos);
		     return 0;
		   }
		 }
		 break;

    case WM_KILLFOCUS:
		   RepaintSubItem(hWnd,ListBox_GetSelectedIndex(hWnd));
 		   if(((TListBox *)WndClsBuf(hWnd))->NewMarquee)
    	     EnableTimer(((TListBox *)WndClsBuf(hWnd))->MarqueeTimer,false);
		 return 0;

    case WM_SETFOCUS:
		   RepaintSubItem(hWnd,ListBox_GetSelectedIndex(hWnd));
		   if(((TListBox *)WndClsBuf(hWnd))->NewMarquee)
		     EnableTimer(((TListBox *)WndClsBuf(hWnd))->MarqueeTimer,true);
		 return 0;

	case WM_PAINT:
           ListBoxRepaint(hWnd);
		 return 0;
 
	case WM_CREATE:
            ListBoxCreate(hWnd);
         return 0;

    case WM_DESTROY:
            ListBoxDestroy(hWnd);
	     return 0;
  }
  return DefWindowProc(hWnd,Message,WParam,LParam);
}
//---------------------------------------------------------------------------
void CM_RegisterListBox(void)
{  TWNDCLASS wc;
   memset(&wc,0,sizeof(wc));
   wc.dwStyle=WS_BORDER_LOWERED;
   wc.clForeground=CL_WINDOWTEXT;
   wc.clBackground=CL_WHITE;
   wc.cbWndExtra=sizeof(TListBox);
   wc.lpfnWndProc=DefListBoxProc;
   wc.lpszClassName="ListBox";
   RegisterClass(&wc);
}
//---------------------------------------------------------------------------
void CM_RegisterMenu(void)
{  TWNDCLASS wc;
   memset(&wc,0,sizeof(wc));
   wc.dwStyle=WS_BORDER_RAISED;
   wc.clForeground=CL_WINDOWTEXT;
   wc.clBackground=CL_BTNFACE;
   wc.cbWndExtra=sizeof(TListBox);
   wc.lpfnWndProc=DefListBoxProc;
   wc.lpszClassName="Menu";
   RegisterClass(&wc);
}
//---------------------------------------------------------------------------
