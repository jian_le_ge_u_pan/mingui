/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      winbas.h
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
*
*  MODIFICATION HISTORY
*     LastModify  2006/09/27
******************************************************************************/
//---------------------------------------------------------------------------
#ifndef _WINBAS_H
#define _WINBAS_H
//---------------------------------------------------------------------------
#if (_MSC_VER  || __BORLANDC__)              /*detect operation system type*/
  #define ON_SIMULATOR       true
  typedef __int64            INT64;
  typedef unsigned __int64   UINT64;
#else
  #define ON_SIMULATOR       false
  typedef long long          INT64;
  typedef unsigned long long UINT64;
#endif
//---------------------------------------------------------------------------
typedef int                BOOL;
typedef char               bool;
typedef unsigned int       UINT;
typedef unsigned char      BYTE;
typedef unsigned short     WORD;
typedef unsigned long      DWORD;
typedef unsigned long      WPARAM;
typedef long               LPARAM;
typedef long               HRESULT;
typedef const char        *LPCSTR, *PCSTR;
typedef char              *LPSTR, *PSTR;
typedef UINT64             TWndDateTime;
typedef void              *HANDLE;
typedef HANDLE             HDC;
typedef HANDLE             HWND;
typedef DWORD              TCOLOR;
//---------------------------------------------------------------------------
#define CALLBACK
#define WINAPI
//---------------------------------------------------------------------------
typedef void (*TNotifyEvent)(HWND);
//---------------------------------------------------------------------------
#ifndef NULL
 #define NULL     ((void *)0) 
#endif
//---------------------------------------------------------------------------
#ifndef false
  #define false               0
#endif
//---------------------------------------------------------------------------
#ifndef true
  #define true                1
#endif
//---------------------------------------------------------------------------
#ifndef FALSE
  #define FALSE               0
#endif
//---------------------------------------------------------------------------
#ifndef TRUE
  #define TRUE                1
#endif
//---------------------------------------------------------------------------
#ifndef max
  #define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif
//---------------------------------------------------------------------------
#ifndef min
  #define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif
//---------------------------------------------------------------------------
#ifndef swap
#define swap(a,b)            {a^=b;b^=a;a^=b;}
#endif
//---------------------------------------------------------------------------
#ifndef MAKEWORD
  #define MAKEWORD(a, b)      ((WORD)(((BYTE)(a)) | ((WORD)((BYTE)(b))) << 8))
#endif
//---------------------------------------------------------------------------
#ifndef MAKELONG
  #define MAKELONG(a, b)      ((long)(((WORD)(a)) | ((DWORD)((WORD)(b))) << 16))
#endif
//---------------------------------------------------------------------------
#ifndef LOWORD
  #define LOWORD(L)           ((WORD)(L))
#endif
//---------------------------------------------------------------------------
#ifndef HIWORD
  #define HIWORD(L)           ((WORD)((DWORD)(L)>>16))
#endif
//---------------------------------------------------------------------------
#ifndef LOBYTE
  #define LOBYTE(w)           ((BYTE)(w))
#endif
//---------------------------------------------------------------------------
#ifndef HIBYTE
  #define HIBYTE(w)           ((BYTE)((w)>>8))
#endif
//---------------------------------------------------------------------------
#define BITS_MASK(bitwidth)   ((1L<<(bitwidth))-1)
//---------------------------------------------------------------------------
#define BINODE_REMOVE(node)\
{ (node)->Next->Prev=(node)->Prev;\
  (node)->Prev->Next=(node)->Next;\
}
//---------------------------------------------------------------------------
#define BINODE_PREV_INSERT(node,objlist)\
{ (node)->Next=objlist;\
  (node)->Prev=(objlist)->Prev;\
  (node)->Next->Prev=node;\
  (node)->Prev->Next=node;\
}
//---------------------------------------------------------------------------
#define BINODE_NEXT_INSERT(node,objlist)\
{ (node)->Next=(objlist)->Next;\
  (node)->Prev=objlist;\
  (node)->Next->Prev=node;\
  (node)->Prev->Next=node;\
}
//---------------------------------------------------------------------------
typedef struct     
{ /*a、r、g、b地址由高至低，与像素值中RGB分量的排列顺序一致*/  
  unsigned char b; /*the red component of a RGB color*/
  unsigned char g; /*the red component of a RGB color*/
  unsigned char r; /*the red component of a RGB color*/
  unsigned char a; /*the alpha component of a RGB color*/
}RGBQUAD; 
//---------------------------------------------------------------------------
typedef struct
{ HANDLE Handle;
  long   MemPos;
  long   Length;
  BOOL   FromMem;
}STREAM;
//---------------------------------------------------------------------------
typedef struct tagBLOCKHEAP
{   int          blocksize;
    int          blocknum;
    int          free;
    void*        heap;
}TBLOCKHEAP;
//---------------------------------------------------------------------------
#define FixedWndTextSize    8 /*must bigger than 8*/
typedef union
{  struct
   { BYTE Head;
     BYTE Stop;
     WORD Size;
     char *pText;
   }Info;
   char Text[FixedWndTextSize];
}TVariantText;
//---------------------------------------------------------------------------
typedef struct
{   int qBufferSize;
     unsigned char *qBuffer;
     unsigned char *ReadPos;
     unsigned char *WritePos;
}TQueueBuffer;
//---------------------------------------------------------------------------
TQueueBuffer *QBuffer_Create(int size);
void QBuffer_Destroy(TQueueBuffer *qBuffer);
int QBuffer_Write(TQueueBuffer *qBuffer,unsigned char *data,int len);
int QBuffer_Read(TQueueBuffer *qBuffer,unsigned char *data,int len);
//---------------------------------------------------------------------------
void  InitBlockDataHeap (TBLOCKHEAP *heap, int block_size, int block_count);
void  FreeBlockDataHeap (TBLOCKHEAP *heap);
void *AllocBlock (TBLOCKHEAP *heap);
void  FreeBlock (TBLOCKHEAP *heap, void *block);
//---------------------------------------------------------------------------
void  InitMem(DWORD address,int size);
void *GetMem(long size);
void  FreeMem(void *pMem);
void *RellocMem(void *pMem,long newsize);
void *GetTempMem(long size);
int   CalcuHeapRemains(void);
//---------------------------------------------------------------------------
int  StrToInt(char *data);
void FloatToStr(char *strbuf,int iw,int fw,float fd);
void IntToStr(char *strbuf,int data);
int  strcasecmp(const char *s1, const char *s2);
void InitRandom(unsigned long seed);
void FiniRandom(void);
int  GenRandom(int max);
unsigned long GenRandomWord(void);
//---------------------------------------------------------------------------
typedef struct
{ int x,y;
}TPOINT,TSIZE;
//---------------------------------------------------------------------------
typedef struct
{ int left,top,right,bottom;
}TRECT,*PRECT;
//---------------------------------------------------------------------------
typedef struct _CLIPRECT
{    TRECT rc;
     struct _CLIPRECT* next;
     struct _CLIPRECT* prev;
}TClipRect;
//---------------------------------------------------------------------------
typedef struct
{  int            type;
   TRECT          rcBound;
   TClipRect      *head;
   TClipRect      *tail;
  TBLOCKHEAP     *heap;
}TREGION;
//---------------------------------------------------------------------------
#define PointInRect(x,y,prc)   ( (x)>=(prc)->left && (x)<(prc)->right && (y)>=(prc)->top && (y)<(prc)->bottom )
#define RectInRect(prc,area)   ( (prc)->left>=(area)->left && (prc)->top>=(area)->top && (prc)->right<=(area)->right && (prc)->bottom<=(area)->bottom )
#define IsRectEmpty(prc)       ( !(prc) || (prc)->left>=(prc)->right || (prc)->top>=(prc)->bottom )
#define IsRegionEmpty(pRgn)       ( !(pRgn) || !(pRgn)->head )
#define IsRectOverlap(prc1,prc2)  ( (prc2)->right>(prc1)->left && (prc1)->right>(prc2)->left && (prc2)->bottom>(prc1)->top && (prc1)->bottom>(prc2)->top )

#define SetRectEmpty(prc)      (prc)->left = (prc)->top = (prc)->right = (prc)->bottom = 0
#define OffsetRect(prc,dx,dy)  { (prc)->left += (dx); (prc)->top += (dy); (prc)->right += (dx); (prc)->bottom += (dy); }
#define InflateRect(prc,dx,dy) { (prc)->left -= (dx); (prc)->top -= (dy); (prc)->right += (dx); (prc)->bottom += (dy); }
//---------------------------------------------------------------------------
BOOL RectIntersect(TRECT *tagRect,TRECT *srcRect);
int  RectOffsetShadown(TRECT *tagRects,TRECT *srcRect,int nx,int ny);
//---------------------------------------------------------------------------
void InitRegion(TREGION *pRgn, TBLOCKHEAP *heap);
void ClearRegion(TREGION *pRgn);
void RegionReset(TREGION *pRgn, const TRECT* pRect);
void RegionCopy(TREGION *pDstRgn, const TREGION* pSrcRgn);
void OffsetRegion(TREGION *region, int x, int y);
void GetRegionBound(TREGION *pRgn, TRECT *pRect);
BOOL PointInRegion(TREGION *region, int x, int y);
BOOL RectInRegion(TREGION *region, const TRECT* rect);
void RegionSubtractRect(TREGION *region, const TRECT *rect);
void RegionIntersectRect(TREGION *region, const TRECT* rect);
void RegionIntersect(TREGION *dst, const TREGION *src1, const TREGION *src2);
void RegionSubtract(TREGION *rgnD, const TREGION *rgnM, const TREGION *rgnS);
void RegionUnion(TREGION *dst, const TREGION *src1, const TREGION *src2);
void RegionXor(TREGION *dst, const TREGION *src1, const TREGION *src2);
void RegionUnionRect(TREGION *region, const TRECT *rect);
//---------------------------------------------------------------------------
void DebugAlert(int errCode, LPCSTR lpFormat , ...);
//---------------------------------------------------------------------------
#endif
