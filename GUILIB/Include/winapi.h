/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      winapi.h
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
*
*  MODIFICATION HISTORY
*     LastModify  2006/11/13
******************************************************************************/
#ifndef  _WINAPI_H
#define  _WINAPI_H
//------------------------------------------------------------------------------
/* don't include wingal.h*/
#include "winbas.h"
//---------------------------------------------------------------------------
// GB_COLOR_TABLE
//---------------------------------------------------------------------------
#define   CL_WHITE                0xFFFFFF
#define   CL_LIGHTGRAY            0xC0C0C0
#define   CL_MEDGRAY              0xA4A0A0
#define   CL_DARKGRAY             0x808080
#define   CL_BLACK                0x000000
#define   CL_RED                  0xFF0000
#define   CL_GREEN                0x00FF00
#define   CL_BLUE                 0x0000FF
//---------------------------------------------------------------------------
#define   CL_SCROLLBAR            0XD4D0C8
#define   CL_MENU                 0XFFFFFF
#define   CL_WINDOW               0XFFFFFF
#define   CL_WINDOWFRAME          0X000000
#define   CL_MENUTEXT             0X000000
#define   CL_WINDOWTEXT           0X000000
#define   CL_CAPTIONTEXT          0XFFFFFF
#define   CL_CAPTIONBAR           0x0000FF
#define   CL_HIGHLIGHT            0X316AC5
#define   CL_HIGHLIGHTTEXT        0XFFFFFF
#define   CL_BTNFACE              0XECE9D8
#define   CL_BTNSHADOW            0XACA899
#define   CL_BTNHIGHLIGHT         0XFFFFFF
#define   CL_GRAYTEXT             0XACA899
#define   CL_BTNTEXT              0X000000
#define   CL_3DDKSHADOW           0X716F64
#define   CL_3DLIGHT              0XF1EFE2
#define   CL_MENUHIGHLIGHT        0X316AC5
#define   CL_MENUBAR              0XECE9D8
//------------------------------------------------------------------------------
#ifndef NOSCROLL

/*
 * Scroll Bar Constants
 */
#define SB_HORZ             0
#define SB_VERT             1
#define SB_CTL              2
#define SB_BOTH             3

/*
 * Scroll Bar Commands
 */
#define SB_LINEUP           0
#define SB_LINELEFT         0
#define SB_LINEDOWN         1
#define SB_LINERIGHT        1
#define SB_PAGEUP           2
#define SB_PAGELEFT         2
#define SB_PAGEDOWN         3
#define SB_PAGERIGHT        3
#define SB_THUMBPOSITION    4
#define SB_THUMBTRACK       5
#define SB_TOP              6
#define SB_LEFT             6
#define SB_BOTTOM           7
#define SB_RIGHT            7
#define SB_ENDSCROLL        8

#endif /* !NOSCROLL */

#ifndef NOSHOWWINDOW


/*
 * ShowWindow() Commands
 */
#define SW_HIDE             0
#define SW_SHOWNORMAL       1
#define SW_NORMAL           1
#define SW_SHOWMINIMIZED    2
#define SW_SHOWMAXIMIZED    3
#define SW_MAXIMIZE         3
#define SW_SHOWNOACTIVATE   4
#define SW_SHOW             5
#define SW_MINIMIZE         6
#define SW_SHOWMINNOACTIVE  7
#define SW_SHOWNA           8
#define SW_RESTORE          9
#define SW_SHOWDEFAULT      10
#define SW_FORCEMINIMIZE    11
#define SW_MAX              11

/*
 * Old ShowWindow() Commands
 */
#define HIDE_WINDOW         0
#define SHOW_OPENWINDOW     1
#define SHOW_ICONWINDOW     2
#define SHOW_FULLSCREEN     3
#define SHOW_OPENNOACTIVATE 4

/*
 * Identifiers for the WM_SHOWWINDOW message
 */
#define SW_PARENTCLOSING    1
#define SW_OTHERZOOM        2
#define SW_PARENTOPENING    3
#define SW_OTHERUNZOOM      4


#endif /* !NOSHOWWINDOW */

 
/*
 * WM_KEYUP/DOWN/CHAR HIWORD(lParam) flags
 */
#define KF_EXTENDED       0x0100
#define KF_DLGMODE        0x0800
#define KF_MENUMODE       0x1000
#define KF_ALTDOWN        0x2000
#define KF_REPEAT         0x4000
#define KF_UP             0x8000

#ifndef NOVIRTUALKEYCODES

/*
 * 0x07 : unassigned
 */

#define VK_BACK           0x08
#define VK_TAB            0x09

/*
 * 0x0A - 0x0B : reserved
 */

#define VK_CLEAR          0x0C
#define VK_RETURN         0x0D

#define VK_SHIFT          0x10
#define VK_CONTROL        0x11
#define VK_MENU           0x12
#define VK_PAUSE          0x13
#define VK_CAPITAL        0x14

#define VK_KANA           0x15
#define VK_HANGEUL        0x15  /* old name - should be here for compatibility */
#define VK_HANGUL         0x15
#define VK_JUNJA          0x17
#define VK_FINAL          0x18
#define VK_HANJA          0x19
#define VK_KANJI          0x19

#define VK_ESCAPE         0x1B

#define VK_CONVERT        0x1C
#define VK_NONCONVERT     0x1D
#define VK_ACCEPT         0x1E
#define VK_MODECHANGE     0x1F

#define VK_SPACE          0x20
#define VK_PRIOR          0x21
#define VK_NEXT           0x22
#define VK_END            0x23
#define VK_HOME           0x24
#define VK_LEFT           0x25
#define VK_UP             0x26
#define VK_RIGHT          0x27
#define VK_DOWN           0x28
#define VK_SELECT         0x29
#define VK_PRINT          0x2A
#define VK_EXECUTE        0x2B
#define VK_SNAPSHOT       0x2C
#define VK_INSERT         0x2D
#define VK_DELETE         0x2E
#define VK_HELP           0x2F

/*
 * VK_0 - VK_9 are the same as ASCII '0' - '9' (0x30 - 0x39)
 * 0x40 : unassigned
 * VK_A - VK_Z are the same as ASCII 'A' - 'Z' (0x41 - 0x5A)
 */

#define VK_LWIN           0x5B
#define VK_RWIN           0x5C
#define VK_APPS           0x5D

/*
 * 0x5E : reserved
 */

#define VK_SLEEP          0x5F

#define VK_NUMPAD0        0x60
#define VK_NUMPAD1        0x61
#define VK_NUMPAD2        0x62
#define VK_NUMPAD3        0x63
#define VK_NUMPAD4        0x64
#define VK_NUMPAD5        0x65
#define VK_NUMPAD6        0x66
#define VK_NUMPAD7        0x67
#define VK_NUMPAD8        0x68
#define VK_NUMPAD9        0x69
#define VK_MULTIPLY       0x6A
#define VK_ADD            0x6B
#define VK_SEPARATOR      0x6C
#define VK_SUBTRACT       0x6D
#define VK_DECIMAL        0x6E
#define VK_DIVIDE         0x6F
#define VK_F1             0x70
#define VK_F2             0x71
#define VK_F3             0x72
#define VK_F4             0x73
#define VK_F5             0x74
#define VK_F6             0x75
#define VK_F7             0x76
#define VK_F8             0x77
#define VK_F9             0x78
#define VK_F10            0x79
#define VK_F11            0x7A
#define VK_F12            0x7B
#define VK_F13            0x7C
#define VK_F14            0x7D
#define VK_F15            0x7E
#define VK_F16            0x7F
#define VK_F17            0x80
#define VK_F18            0x81
#define VK_F19            0x82
#define VK_F20            0x83
#define VK_F21            0x84
#define VK_F22            0x85
#define VK_F23            0x86
#define VK_F24            0x87

/*
 * 0x88 - 0x8F : unassigned
 */

#define VK_NUMLOCK        0x90
#define VK_SCROLL         0x91

/*
 * NEC PC-9800 kbd definitions
 */
#define VK_OEM_NEC_EQUAL  0x92   // '=' key on numpad

/*
 * Fujitsu/OASYS kbd definitions
 */
#define VK_OEM_FJ_JISHO   0x92   // 'Dictionary' key
#define VK_OEM_FJ_MASSHOU 0x93   // 'Unregister word' key
#define VK_OEM_FJ_TOUROKU 0x94   // 'Register word' key
#define VK_OEM_FJ_LOYA    0x95   // 'Left OYAYUBI' key
#define VK_OEM_FJ_ROYA    0x96   // 'Right OYAYUBI' key

/*
 * 0x97 - 0x9F : unassigned
 */

/*
 * VK_L* & VK_R* - left and right Alt, Ctrl and Shift virtual keys.
 * Used only as parameters to GetAsyncKeyState() and GetKeyState().
 * No other API or message will distinguish left and right keys in this way.
 */
#define VK_LSHIFT         0xA0
#define VK_RSHIFT         0xA1
#define VK_LCONTROL       0xA2
#define VK_RCONTROL       0xA3
#define VK_LMENU          0xA4
#define VK_RMENU          0xA5


#define VK_RESERVE        0xC0      /*用户自定义键开始*/


#endif /* !NOVIRTUALKEYCODES */

 

#ifndef NOWINMESSAGES


/*
 * Window Messages
 */

#define WM_NULL                         0x0000
#define WM_CREATE                       0x0001
#define WM_DESTROY                      0x0002
#define WM_MOVE                         0x0003
#define WM_SIZE                         0x0005

#define WM_ACTIVATE                     0x0006
/*
 * WM_ACTIVATE state values
 */
#define     WA_INACTIVE     0
#define     WA_ACTIVE       1
#define     WA_CLICKACTIVE  2

#define WM_SETFOCUS                     0x0007
#define WM_KILLFOCUS                    0x0008
#define WM_ENABLE                       0x000A
#define WM_SETREDRAW                    0x000B
#define WM_SETTEXT                      0x000C
#define WM_GETTEXT                      0x000D
#define WM_GETTEXTLENGTH                0x000E
#define WM_PAINT                        0x000F
#define WM_CLOSE                        0x0010

#define WM_SETLOGO                      0x0011

#define WM_QUIT                         0x0012
#define WM_ERASEBKGND                   0x0014
#define WM_SYSCOLORCHANGE               0x0015
#define WM_SHOWWINDOW                   0x0018
#define WM_WININICHANGE                 0x001A

#define WM_DEVMODECHANGE                0x001B
#define WM_ACTIVATEAPP                  0x001C
#define WM_FONTCHANGE                   0x001D
#define WM_TIMECHANGE                   0x001E
#define WM_CANCELMODE                   0x001F
#define WM_SETCURSOR                    0x0020
#define WM_MOUSEACTIVATE                0x0021
#define WM_CHILDACTIVATE                0x0022
#define WM_QUEUESYNC                    0x0023

#define WM_GETMINMAXINFO                0x0024

#define WM_PAINTICON                    0x0026
#define WM_ICONERASEBKGND               0x0027
#define WM_NEXTDLGCTL                   0x0028
#define WM_SPOOLERSTATUS                0x002A
#define WM_DRAWITEM                     0x002B
#define WM_MEASUREITEM                  0x002C
#define WM_DELETEITEM                   0x002D
#define WM_VKEYTOITEM                   0x002E
#define WM_CHARTOITEM                   0x002F
#define WM_SETFONT                      0x0030
#define WM_GETFONT                      0x0031
#define WM_SETHOTKEY                    0x0032
#define WM_GETHOTKEY                    0x0033
#define WM_QUERYDRAGICON                0x0037
#define WM_COMPAREITEM                  0x0039

#define WM_COMPACTING                   0x0041
#define WM_COMMNOTIFY                   0x0044  /* no longer suported */
#define WM_WINDOWPOSCHANGING            0x0046
#define WM_WINDOWPOSCHANGED             0x0047

#define WM_POWER                        0x0048
/*
 * wParam for WM_POWER window message and DRV_POWER driver notification
 */
#define PWR_OK              1
#define PWR_FAIL            (-1)
#define PWR_SUSPENDREQUEST  1
#define PWR_SUSPENDRESUME   2
#define PWR_CRITICALRESUME  3

#define WM_COPYDATA                     0x004A
#define WM_CANCELJOURNAL                0x004B

#define WM_NCCREATE                     0x0081
#define WM_NCDESTROY                    0x0082
#define WM_NCCALCSIZE                   0x0083
#define WM_NCHITTEST                    0x0084
#define WM_NCPAINT                      0x0085
#define WM_NCACTIVATE                   0x0086
#define WM_GETDLGCODE                   0x0087
 
#define WM_NCMOUSEMOVE                  0x00A0
#define WM_NCLBUTTONDOWN                0x00A1
#define WM_NCLBUTTONUP                  0x00A2
#define WM_NCLBUTTONDBLCLK              0x00A3
#define WM_NCRBUTTONDOWN                0x00A4
#define WM_NCRBUTTONUP                  0x00A5
#define WM_NCRBUTTONDBLCLK              0x00A6
#define WM_NCMBUTTONDOWN                0x00A7
#define WM_NCMBUTTONUP                  0x00A8
#define WM_NCMBUTTONDBLCLK              0x00A9

#define WM_KEYFIRST                     0x0100
#define WM_KEYDOWN                      0x0100
#define WM_KEYUP                        0x0101
#define WM_CHAR                         0x0102
#define WM_DEADCHAR                     0x0103
#define WM_SYSKEYDOWN                   0x0104
#define WM_SYSKEYUP                     0x0105
#define WM_SYSCHAR                      0x0106
#define WM_SYSDEADCHAR                  0x0107

#define WM_INITDIALOG                   0x0110
#define WM_COMMAND                      0x0111
#define WM_SYSCOMMAND                   0x0112
#define WM_TIMER                        0x0113
#define WM_HSCROLL                      0x0114
#define WM_VSCROLL                      0x0115
#define WM_INITMENU                     0x0116
#define WM_INITMENUPOPUP                0x0117
#define WM_MENUSELECT                   0x011F
#define WM_MENUCHAR                     0x0120
#define WM_ENTERIDLE                    0x0121 

#define WM_CTLCOLORMSGBOX               0x0132
#define WM_CTLCOLOREDIT                 0x0133
#define WM_CTLCOLORLISTBOX              0x0134
#define WM_CTLCOLORBTN                  0x0135
#define WM_CTLCOLORDLG                  0x0136
#define WM_CTLCOLORSCROLLBAR            0x0137
#define WM_CTLCOLORSTATIC               0x0138

#define WM_MOUSEFIRST                   0x0200
#define WM_MOUSEMOVE                    0x0200
#define WM_LBUTTONDOWN                  0x0201
#define WM_LBUTTONUP                    0x0202
#define WM_LBUTTONDBLCLK                0x0203
#define WM_RBUTTONDOWN                  0x0204
#define WM_RBUTTONUP                    0x0205
#define WM_RBUTTONDBLCLK                0x0206
#define WM_MBUTTONDOWN                  0x0207
#define WM_MBUTTONUP                    0x0208
#define WM_MBUTTONDBLCLK                0x0209
 
#define WM_CUT                          0x0300
#define WM_COPY                         0x0301
#define WM_PASTE                        0x0302
#define WM_CLEAR                        0x0303
#define WM_UNDO                         0x0304
#define WM_RENDERFORMAT                 0x0305
#define WM_RENDERALLFORMATS             0x0306
#define WM_DESTROYCLIPBOARD             0x0307
#define WM_DRAWCLIPBOARD                0x0308
#define WM_PAINTCLIPBOARD               0x0309
#define WM_VSCROLLCLIPBOARD             0x030A
#define WM_SIZECLIPBOARD                0x030B
#define WM_ASKCBFORMATNAME              0x030C
#define WM_CHANGECBCHAIN                0x030D
#define WM_HSCROLLCLIPBOARD             0x030E
#define WM_QUERYNEWPALETTE              0x030F
#define WM_PALETTEISCHANGING            0x0310
#define WM_PALETTECHANGED               0x0311
#define WM_HOTKEY                       0x0312


//////////////////////////////////////////////////////
#define WM_CURSORBLINK                  0x0320
#define WM_EXPOSE                       0x0321
#define WM_NEWCLK                       0x0322
#define WM_PENDOWN                      WM_LBUTTONDOWN
#define WM_PENUP                        WM_LBUTTONUP
#define WM_PENMOVE                      WM_MOUSEMOVE
//////////////////////////////////////////////////////

/*
 * NOTE: All Message Numbers below 0x0400 are RESERVED.
 *
 * Private Window Messages Start Here:
 */
#define WM_USER                         0x0400

 
#endif  /*NOWINMESSAGES*/ 
 

/*
 * Window field offsets for GetWindowLong()
 */
#define GWL_WNDPROC         (-4)
#define GWL_HINSTANCE       (-6)
#define GWL_HWNDPARENT      (-8)
#define GWL_ID              (-12)
#define GWL_STYLE           (-16)
#define GWL_EXSTYLE         (-20)
#define GWL_USERDATA        (-21)
#define GWL_WNDEXTRA        (-22)

/***************************************************************************
 *  window style code
 ***************************************************************************/
#define WS_NEEDPAINT        0x80000000L      /*内部状态*/
#define WS_NCPAINT          0x40000000L      /*内部状态*/
#define WS_FOCUS            0x20000000L      /*内部状态*/
#define WS_CHILD            0x10000000L      /*内部状态*/
#define WS_STYLEMASK        0x0FFFFFFFL      /*样式掩码*/
//---------------------------------------------------------------------------
#define WS_NULL             0x00000000L
#define WS_HIDE             0x08000000L
#define WS_DISABLED         0x04000000L
#define WS_TABSTOP          0x02000000L
#define WS_TRANSPARENT      0x01000000L   /*是否透明*/
#define WS_SYSMENU          0x00800000L
#define WS_DLGFRAME         0x00400000L
#define WS_VSCROLL          0x00200000L
#define WS_HSCROLL          0x00100000L
#define WS_ALWAYSONTOP      0x00080000L
#define WS_ALWAYSONBOTTOM   0x00040000L
#define WS_BORDER           0x00020000L
#define WS_BORDER_RAISED    0x00010000L
#define WS_BORDER_LOWERED   0x00008000L
#define WS_CAPTION         (WS_DLGFRAME|WS_BORDER)
/*---  command message (window/control notification code)  ---*/
#define CM_ENTER            0xFFF0   /*得到回车键*/
#define CM_CLICKED          0xFFF1
#define CM_CHANGED          0xFFF2
#define CM_DISABLE          0xFFF3 
#define CM_SETFOCUS         0xFFF4
#define CM_KILLFOCUS        0xFFF5
#define CM_DBCLICKED        0xFFF6 
/***************************************************************************
 *  MessageBox
 ***************************************************************************/
/*MessageBox style*/
#define MB_OK                       0x00000000L
#define MB_OKCANCEL                 0x00000001L
#define MB_ABORTRETRYIGNORE         0x00000002L
#define MB_YESNOCANCEL              0x00000003L
#define MB_YESNO                    0x00000004L
#define MB_RETRYCANCEL              0x00000005L

#define MB_ICONHAND                 0x00000010L
#define MB_ICONQUESTION             0x00000020L
#define MB_ICONEXCLAMATION          0x00000030L
#define MB_ICONASTERISK             0x00000040L

#define MB_USERICON                 0x00000080L
#define MB_ICONWARNING              MB_ICONEXCLAMATION
#define MB_ICONERROR                MB_ICONHAND

#define MB_ICONINFORMATION          MB_ICONASTERISK
#define MB_ICONSTOP                 MB_ICONHAND

#define MB_DEFBUTTON1               0x00000000L
#define MB_DEFBUTTON2               0x00000100L
#define MB_DEFBUTTON3               0x00000200L

/* MessageBox return value */
#define IDOK                1
#define IDCANCEL            2
#define IDABORT             3
#define IDRETRY             4
#define IDIGNORE            5
#define IDYES               6
#define IDNO                7
#define IDCLOSE             8

/***************************************************************************
 *  画笔像素操作逻辑
 ***************************************************************************/
#define   STYLEMASK_PENLOGIC     0x0003    
#define   PL_REPLACE             0x0000    /*像素覆盖*/
#define   PL_XOR                 0x0001    /*像素异或*/
#define   PL_OR                  0x0002    /*像素或*/
#define   PL_AND                 0x0003    /*像素与*/

#define   SetPenLogic(dc,mode)   dcMode(dc)=(dcMode(dc)&~STYLEMASK_PENLOGIC)|(mode&STYLEMASK_PENLOGIC)
/***************************************************************************
 *  线型式样
 ***************************************************************************/
#define   STYLEMASK_LINE         0x000C
#define   LS_SOLID	             0X0000   /*A solid line*/
#define   LS_DASH	             0X0004   /*A line made up of a series of dashes*/
#define   LS_DOT	             0X0008   /*A line made up of a series of dots*/
#define   LS_DASHDOT	         0X000C   /*A line made up of alternating dashes and dots*/

#define   SetLineStyle(dc,mode)   dcMode(dc)=(dcMode(dc)&~STYLEMASK_LINE)|(mode&STYLEMASK_LINE)
/***************************************************************************
 *  字体式样
 ***************************************************************************/
#define   TRANSPARENT            0
#define   OPAQUE                 FS_OPAQUE 
#define   FS_PLAIN               0x0010    /*不解释特殊符号(换行/制表符)*/
#define   FS_OPAQUE              0x0020    /*文字不透明(自动填充文字背景)*/
#define   SetFontStyle(dc,atr,addorsub)   if(addorsub) dcMode(dc)|=(atr);else dcMode(dc)&=~(atr);
/***************************************************************************
 *  文字间距
 ***************************************************************************/
#define   FS_ROWSPACEMASK         0x0F00
#define   FS_ROWSPACEOFFSET       8
#define   FS_COLSPACEMASK         0xF000
#define   FS_COLSPACEOFFSET       12

#define   GetRowSpace(dc)         ((dcMode(dc)&FS_ROWSPACEMASK)>>FS_ROWSPACEOFFSET)
#define   GetColSpace(dc)         ((dcMode(dc)&FS_COLSPACEMASK)>>FS_COLSPACEOFFSET)
#define   SetColSpace(dc,space)    dcMode(dc) = (dcMode(dc)&~FS_COLSPACEMASK) | (((space)<<FS_COLSPACEOFFSET)&FS_COLSPACEMASK)
#define   SetRowSpace(dc,space)    dcMode(dc) = (dcMode(dc)&~FS_ROWSPACEMASK) | (((space)<<FS_ROWSPACEOFFSET)&FS_ROWSPACEMASK)
//---------------------------------------------------------------------------
enum TAlign{ alLeft=0x01, alCenter=0x02, alRight=0x04, alTop=0x08, alMiddle=0x10, alBottom=0x20 };
//---------------------------------------------------------------------------
typedef struct
{ HDC Handle;
  int Width,Height;
  BOOL Transparent;
}TBitmap;  
//---------------------------------------------------------------------------
typedef struct
{ HWND     Handle;        /* 接收消息的窗体句柄*/
  UINT     Message;       /* message value     */
  WPARAM   WParam;        /* 32bit parameter   */
  LPARAM   LParam;        /* 32bit parameter   */
  DWORD    Time;
}TMSG,*PMSG;
//---------------------------------------------------------------------------
typedef HRESULT (*TWNDPROC)(HWND,UINT,WPARAM,LPARAM);
typedef HRESULT (*TWNDHOOK)(HWND,UINT,WPARAM,LPARAM,BOOL*);
//---------------------------------------------------------------------------
typedef struct
{   TWNDPROC    lpfnWndProc;
    int         cbClsExtra;
    int         cbWndExtra;
    int         cbTextHeap;
	DWORD       dwStyle;
    TCOLOR      clForeground;
    TCOLOR      clBackground;
    HANDLE      hInstance;
    HANDLE      hIcon;
    HANDLE      hCursor;
    LPCSTR      lpszMenuName;
    LPCSTR      lpszClassName;
}TWNDCLASS;
//---------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////
#define   GUI_GetDC                        GetDC 
#define   GUI_GetWindowDC                  GetWindowDC 
#define   GUI_GetDesktopDC                 GetDesktopDC 
#define   GUI_ReleaseDC                    ReleaseDC 
#define   GUI_SetColor                     SetColor
#define   GUI_SetBkColor                   SetBkColor
#define   GUI_SetBkMode                    SetBkMode
#define   GUI_SetFont                      SetFont
#define   GUI_ColorMapToPixel              ColorMapToPixel
#define   GUI_PixelMapToColor              PixelMapToColor
#define   GUI_GetPosPixel                  GetPosPixel 
#define   GUI_SetPenLogic                  SetPenLogic
#define   GUI_SetFontStyle                 SetFontStyle
#define   GUI_SetLineStyle                 SetLineStyle
#define   GUI_SetFillStyle                 SetFillStyle
#define   GUI_GroupOn                      GroupOn
#define   GUI_GroupOff                     GroupOff
#define   GUI_DrawPixel                    DrawPixel
#define   GUI_DrawFontMatrix               DrawFontMatrix
#define   GUI_ClearRect                    ClearRect
#define   GUI_DrawHorLine                  DrawHorLine
#define   GUI_DrawVerLine                  DrawVerLine
#define   GUI_DrawLine                     DrawLine
#define   GUI_DrawRect                     DrawRect
#define   GUI_FillRect                     FillRect
#define   GUI_DrawDashedRect               DrawDashedRect
#define   GUI_DrawEllipse                  DrawEllipse
#define   GUI_FillEllipse                  FillEllipse
#define   GUI_DrawCircle                   DrawCircle
#define   GUI_FillCircle                   FillCircle
#define   GUI_DrawSector                   DrawSector 
#define   GUI_FillSector                   FillSector
#define   GUI_DrawPatternHorLine           DrawPatternHorLine
#define   GUI_DrawPatternVerLine           DrawPatternVerLine
#define   GUI_TextOut                      TextOut
#define   GUI_DrawText                     DrawText
#define   GUI_PrintText                    PrintText
#define   GUI_GetTextSize                  GetTextSize

#define   GUI_DrawFrame                    DrawFrame
#define   GUI_Draw3dFrame                  Draw3dFrame
#define   GUI_Draw3dShadow                 Draw3dShadow
#define   GUI_Draw3dBox                    Draw3dBox
#define   GUI_Draw3dInset                  Draw3dInset
#define   GUI_Draw3dOutset                 Draw3dOutset 

#define   GUI_CreateBitmap                 CreateBitmap
#define   GUI_DestroyBitmap                DestroyBitmap
#define   GUI_LoadBitmapFromFile           LoadBitmapFromFile
#define   GUI_LoadBitmapFromMem            LoadBitmapFromMem 
#define   GUI_UnloadBitmap                 UnloadBitmap

#define   GUI_DrawBitmap                   DrawBitmap
#define   GUI_ExDrawBitmap                 ExDrawBitmap
#define   GUI_StretchDrawBitmap            StretchDrawBitmap
#define   GUI_ExStretchDrawBitmap          ExStretchDrawBitmap
#define   GUI_BitBlt                       BitBlt
#define   GUI_StretchBlt                   StretchBlt

#define   GUI_OpenMemStream                OpenMemStream 
#define   GUI_OpenFileStream               OpenFileStream
#define   GUI_StreamSeek                   StreamSeek
#define   GUI_CloseStream                  CloseStream
#define   GUI_StreamGetByte                StreamGetByte
#define   GUI_StreamRead                   StreamRead

#define   GUI_SendMessage                  SendMessage
#define   GUI_PostMessage                  PostMessage
#define   GUI_PeekMessage                  PeekMessage
#define   GUI_GetMessage                   GetMessage


#define   GUI_TranslateMessage             TranslateMessage
#define   GUI_DispatchMessage              DispatchMessage
#define   GUI_HandleMessage                HandleMessage
#define   GUI_ProcessMessages              ProcessMessages
#define   GUI_PostQuitMessage              PostQuitMessage
#define   GUI_DispatchUserMsg              DispatchUserMsg
#define   GUI_UninstallHook                UninstallHook
#define   GUI_InstallHook                  InstallHook
#define   GUI_DefWindowProc                DefWindowProc
#define   GUI_RegisterClass                RegisterClass
#define   GUI_UnregisterClass              UnregisterClass
#define   GUI_FindClassByName              FindClassByName
#define   GUI_CreateWindow                 CreateWindow
#define   GUI_DestroyWindow                DestroyWindow
#define   GUI_CloseWindow                  CloseWindow
#define   GUI_ShowWindow                   ShowWindow
#define   GUI_EnableWindow                 EnableWindow
#define   GUI_BeginPaint                   BeginPaint
#define   GUI_EndPaint                     EndPaint
#define   GUI_EraseBackground              EraseBackground 
#define   GUI_Invalidate                   Invalidate
#define   GUI_InvalidateWindow             InvalidateWindow
#define   GUI_InvalidateRect               InvalidateRect
#define   GUI_InvalidateNCArea             InvalidateNCArea
#define   GUI_SetForegroundWindow          SetForegroundWindow
#define   GUI_SetBackgroundWindow          SetBackgroundWindow
#define   GUI_GetWindowLong                GetWindowLong
#define   GUI_SetWindowLong                SetWindowLong
#define   GUI_SetFocus                     SetFocus
#define   GUI_GetFocus                     GetFocus
#define   GUI_GetActiveWindow              GetActiveWindow
#define   GUI_IsWindow                     IsWindow
#define   GUI_UpdateWindow                 UpdateWindow
#define   GUI_SetTabOrder                  SetTabOrder
#define   GUI_GetTabOrder                  GetTabOrder
#define   GUI_GetNextWindow                GetNextWindow
#define   GUI_GetPrevWindow                GetPrevWindow
#define   GUI_SetWindowLogo                SetWindowLogo      
#define   GUI_SetWindowText                SetWindowText
#define   GUI_GetWindowText                GetWindowText
#define   GUI_SaveWindowText               SaveWindowText
#define   GUI_SetWindowPos                 SetWindowPos
#define   GUI_GetWindowPos                 GetWindowPos 
#define   GUI_SetWindowTextColor           SetWindowTextColor
#define   GUI_SetWindowBackColor           SetWindowBackColor
#define   GUI_SetWindowFont                SetWindowFont
#define   GUI_RegisterFont                 RegisterFont
#define   GUI_UnregisterFont               UnregisterFont
#define   GUI_GetWeekDay                   GetWeekDay
#define   GUI_GetSysDate                   GetSysDate
#define   GUI_GetSysTime                   GetSysTime
#define   GUI_SetSysDateTime               SetSysDateTime
#define   GUI_CreateTimer                  CreateTimer
#define   GUI_ResetTimer                   ResetTimer
#define   GUI_EnableTimer                  EnableTimer
#define   GUI_IsTimerEnable                IsTimerEnable
#define   GUI_DestroyTimer                 DestroyTimer
#define   GUI_MessageDlg                   MessageDlg
#define   GUI_MessageBox                   MessageBox
#define   GUI_ResetCaret                   ResetCaret
#define   GUI_HideCaret                    HideCaret
#define   GUI_CreateCaret                  CreateCaret
#define   GUI_DestroyCaret                 DestroyCaret
#define   GUI_ShowCaret                    ShowCaret
#define   GUI_SetCaretPos                  SetCaretPos
//---------------------------------------------------------------------------
HDC      GUI_GetDC(HWND hWnd);
HDC      GUI_GetWindowDC(HWND hWnd);
HDC      GUI_GetDesktopDC(void);
void     GUI_ReleaseDC(HDC dc);
void     GUI_GroupOn(HDC dc);
void     GUI_GroupOff(HDC dc,int x,int y,int width,int height);
void     GUI_DrawPixel(HDC dc,int x,int y);
void     GUI_DrawFontMatrix(HDC dc,int xPos,int yPos,int width,int height,BYTE *ptnArray);
void     GUI_ClearRect(HDC dc,int left,int top,int width,int height);
void     GUI_DrawHorLine(HDC dc, int x, int y, int width);
void     GUI_DrawVerLine(HDC dc, int x, int y, int height);
void     GUI_DrawLine(HDC dc, int x1,int y1,int x2,int y2);
void     GUI_DrawRect(HDC dc, int x,int y,int width,int height);
void     GUI_FillRect(HDC dc,int left,int top,int width,int height);
void     GUI_DrawDashedRect(HDC dc, int x,int y,int width,int height);
void     GUI_DrawEllipse(HDC dc, int x,int y,int A,int B);
void     GUI_FillEllipse(HDC dc, int x,int y,int A,int B);
void     GUI_DrawCircle(HDC dc, int x,int y,int radius);
void     GUI_FillCircle(HDC dc, int x,int y,int radius);
void     GUI_DrawSector(HDC dc, int x,int y,int r,int angle1,int angle2);
void     GUI_FillSector(HDC dc, int x,int y,int r,int angle1,int angle2);
void     GUI_DrawPatternHorLine(HDC dc, int x,int y,int width,BYTE pattern,BOOL bigEndian);
void     GUI_DrawPatternVerLine(HDC dc, int x,int y,int height,BYTE pattern,BOOL bigEndian);
BOOL     GUI_TextOut(HDC dc, int xPos,int yPos,LPCSTR lptext);
void     GUI_DrawText(HDC dc,int left,int top,int ClipWidth,int ClipHeight,LPCSTR lptext,DWORD Alignment);
void     GUI_PrintText(HDC dc, int xPos,int yPos,const char *format, ...);
int      GUI_GetTextSize(HDC dc,LPCSTR text,TSIZE *txtsize);
void     GUI_SetColor(HDC dc, TCOLOR rgbColor);
void     GUI_SetBkColor(HDC dc, TCOLOR rgbColor);
void     GUI_SetBkMode(HDC dc, int bkMode);
BOOL     GUI_SetFont(HDC dc,LPCSTR lpFontName);
int      GUI_ColorMapToPixel(RGBQUAD *rgbColor);
void     GUI_PixelMapToColor(int index,RGBQUAD *rgbColor);
int      GUI_GetPosPixel(HDC dc,int xPos, int yPos);
void     GUI_DrawFrame(HDC dc,int left,int top,int width,int height,int focused,BOOL DownOrUp);
void     GUI_Draw3dFrame(HDC dc, int x,int y,int width,int height,BOOL DownOrUp);
void     GUI_Draw3dShadow(HDC hDC,int x,int y,int w,int h,TCOLOR crTop,TCOLOR crBottom);
void     GUI_Draw3dBox(HDC hDC,int x,int y,int w,int h,TCOLOR crTop,TCOLOR crBottom);
void     GUI_Draw3dInset(HDC hDC,int x,int y,int w,int h);
void     GUI_Draw3dOutset(HDC hDC,int x,int y,int w,int h);
//---------------------------------------------------------------------------
TBitmap *GUI_CreateBitmap(void);
BOOL     GUI_LoadBitmapFromFile(TBitmap *bitmap, const char* file_name);
BOOL     GUI_LoadBitmapFromMem(TBitmap *bitmap, const BYTE *imgData);
void     GUI_UnloadBitmap(TBitmap *bitmap);
void     GUI_DestroyBitmap(TBitmap *bitmap);
void     GUI_DrawBitmap(HDC hdcDest,int nXDest,int nYDest,TBitmap *bitmap);
void     GUI_StretchDrawBitmap(HDC hdcDest,int nXOriginDest,int nYOriginDest,int nWidthDest,int nHeightDest,TBitmap *bmpSrc);
void     GUI_ExDrawBitmap(HDC hdcDest,int nXDest,int nYDest,int nWidth,int nHeight,TBitmap *bitmap,int nXSrc,int nYSrc);
void     GUI_ExStretchDrawBitmap(HDC hdcDest,int nXOriginDest,int nYOriginDest,int nWidthDest,int nHeightDest,TBitmap *bmpSrc,int nXOriginSrc,int nYOriginSrc,int nWidthSrc,int nHeightSrc);
void     GUI_BitBlt(HDC hdcDest,int nXDest,int nYDest,int nWidth,int nHeight,HDC hdcSrc,int nXSrc,int nYSrc,DWORD dwRop);
void     GUI_StretchBlt(HDC hdcDest,int nXOriginDest,int nYOriginDest,int nWidthDest,int nHeightDest,HDC hdcSrc,int nXOriginSrc,int nYOriginSrc,int nWidthSrc,int nHeightSrc,DWORD dwRop);
//---------------------------------------------------------------------------
BOOL     GUI_PeekMessage(PMSG Msg,BOOL Remove);
BOOL     GUI_GetMessage(PMSG Msg);
BOOL     GUI_PostMessage(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam);
HRESULT  GUI_SendMessage(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam);
HRESULT  GUI_DefWindowProc(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam);
BOOL     GUI_TranslateMessage(PMSG Msg);
BOOL     GUI_DispatchMessage(PMSG Msg);
BOOL     GUI_HandleMessage(void);
void     GUI_ProcessMessages(void);
void     GUI_PostQuitMessage(int nExitCode);
void     GUI_DispatchUserMsg(DWORD msg,DWORD wParam,DWORD lParam);
void     GUI_UninstallHook(HWND hWnd);
TWNDHOOK GUI_InstallHook(HWND hWnd,UINT hookMsg,TWNDHOOK hookProc);
void     NUMPAD_DecodeShift(BOOL OnOff);
void     NUMPAD_CharsMap(const char **maps);
//---------------------------------------------------------------------------
BOOL     GUI_RegisterClass(const TWNDCLASS *lpWndClass);
BOOL     GUI_UnregisterClass(LPCSTR lpClassName);
TWNDCLASS  *GUI_FindClassByName(LPCSTR lpClassName);
HWND     GUI_CreateWindow(LPCSTR lpClassName,LPCSTR lpWindowName,DWORD dwStyle,int x,int y,int nWidth,int nHeight,HWND parent,UINT id,void *extrData);
void     GUI_DestroyWindow(HWND hWnd);
void     GUI_CloseWindow(HWND hWnd);
BOOL     GUI_ShowWindow(HWND hWnd,BOOL bShow);
BOOL     GUI_EnableWindow(HWND hWnd, BOOL bEnable);
HWND     GUI_SetFocus(HWND hWnd);
HWND     GUI_GetFocus(void);
HWND     GUI_GetActiveWindow(void);
BOOL     GUI_IsWindow(HWND hWnd);
void     GUI_UpdateWindow(HWND hWnd);
void     GUI_SetTabOrder(HWND hWnd,int tabOrder);
int      GUI_GetTabOrder(HWND hWnd);
HWND     GUI_GetNextWindow(HWND hWnd);
HWND     GUI_GetPrevWindow(HWND hWnd);
BOOL     GUI_SetWindowLogo(HWND hWnd,TBitmap *LogoBitmap);
BOOL     GUI_SetWindowText(HWND hWnd,char *lpString);
BOOL     GUI_SaveWindowText(HWND hWnd,LPCSTR lpString,BOOL newheap);
char    *GUI_GetWindowText(HWND hWnd);
void     GUI_SetWindowPos(HWND hWnd, int x,int y);
TPOINT   GUI_GetWindowPos(HWND hWnd);
void     GUI_SetWindowTextColor(HWND hWnd,TCOLOR rgbColor);
void     GUI_SetWindowBackColor(HWND hWnd,TCOLOR rgbColor);
BOOL     GUI_SetWindowFont(HWND hWnd,LPCSTR lpFontName);
BOOL     GUI_RegisterFont(LPCSTR lpClassName,const BYTE *pFontBits);
BOOL     GUI_UnregisterFont(LPCSTR lpFontName);
HDC      GUI_BeginPaint(HWND hWnd);
void     GUI_EndPaint(HWND hWnd);
void     GUI_EraseBackground(HDC hdc,int x,int y,int w,int h);
void     GUI_Invalidate(HWND hWnd);
void     GUI_InvalidateWindow(HWND hWnd,BOOL bPaintNC);
void     GUI_InvalidateRect(HWND hWnd,TRECT *Rect);
void     GUI_InvalidateNCArea(HWND hWnd);
void     GUI_SetForegroundWindow(HWND hWnd);
void     GUI_SetBackgroundWindow(HWND hWnd);
long     GUI_GetWindowLong(HWND hWnd, int nIndex);
long     GUI_SetWindowLong(HWND hWnd, int nIndex, long lNewLong);
//---------------------------------------------------------------------------
STREAM  *GUI_OpenMemStream(const BYTE *pBuf, int bufsize);
STREAM  *GUI_OpenFileStream(const char *file_path);
void     GUI_StreamSeek(STREAM *stream,long offset,int whence);
void     GUI_CloseStream(STREAM *stream);
int      GUI_StreamGetByte(STREAM *stream);
int      GUI_StreamRead(void *ptr, int size, STREAM *stream);
//---------------------------------------------------------------------------
int      GUI_GetWeekDay(int year,int month,int day);
void     GUI_GetSysDate(int *Year, int *Month,int *Day);
void     GUI_GetSysTime(int *Hour, int *Minute,int *Second);
void     GUI_SetSysDateTime(int year,int month,int day,int hour,int minute,int second);
//---------------------------------------------------------------------------
HANDLE   GUI_CreateTimer(HWND hWnd,UINT initial_time,UINT reschedule_time,void(*TimerProc)(HWND),BOOL Enabled);
void     GUI_EnableTimer(HANDLE hTimer,BOOL Enabled);
BOOL     GUI_IsTimerEnable(HANDLE hTimer);
void     GUI_ResetTimer(HANDLE hTimer,HWND hWnd,UINT initial_time,UINT reschedule_time,void(*TimerProc)(HWND),BOOL Enabled);
void     GUI_DestroyTimer(HANDLE hTimer);
//---------------------------------------------------------------------------
int      GUI_MessageDlg(LPCSTR lpText,LPCSTR lpCaption,LPCSTR lpCmdArray);
int      GUI_MessageBox(LPCSTR lpText,LPCSTR lpCaption,UINT flags);
//---------------------------------------------------------------------------
void     GUI_CreateCaret(HWND hWnd, int width, int height);
BOOL     GUI_DestroyCaret(HWND hWnd);
void     GUI_ResetCaret(HWND hWnd,int width, int height);
BOOL     GUI_HideCaret(HWND hWnd);
BOOL     GUI_ShowCaret(HWND hWnd);
BOOL     GUI_SetCaretPos(HWND hWnd,int nX, int nY);
//------------------------------------------------------------------------------

#endif
