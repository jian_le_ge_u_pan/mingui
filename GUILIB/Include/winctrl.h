/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:			    winctrl.h
*  PROGRAMMER:			    ming.c
*  Date of Creation:		2006/08/8
*
*  DESCRIPTION: 								
*	  GUI Control public Interface					
*  NOTE:						 		
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
*
*  MODIFICATION HISTORY
*     LastModify  2008/05/19
******************************************************************************/
//---------------------------------------------------------------------------
#ifndef _WINCTRL_H
#define _WINCTRL_H
//---------------------------------------------------------------------------
#include "MinGUI.h"
/***************************************************************************
 *  Button
 ***************************************************************************/
/* button style */
#define   BS_FLAT                0x0001          /*扁平按钮*/
#define   BS_MENU                0x0002          /*菜单型按扭:取得焦点时反白显示*/
#define   BS_PUSHED              0x0004          /*按钮下凹状态*/
/*  button Notification Codes*/
#define   BN_PUSHED          0x0001
#define   BN_UNPUSHED        0x0002
#define   BN_CLICKED         CM_CLICKED

/***************************************************************************
 *  Edit/Memo
 ***************************************************************************/
/* Edit Control style */
#define ES_PASSWORD         0x0020L
#define ES_REPLACE          0x0400L 
#define ES_READONLY         0x0800L
#define ES_WANTRETURN       0x1000L
#define ES_FULLTABKEY       0x2000L
/* Edit Control Notification Codes */
#define EN_CHANGED           CM_CHANGED
#define EN_ENTER             CM_ENTER   /*得到回车键*/
#define EN_MAXTEXT           0x0501
/* Edit Control message */
#define EM_LIMITTEXT         0x00C5
#define EM_SETPASSWORDCHAR   0x00CC
#define EM_GETPASSWORDCHAR   0x00D2
#define EM_SETREADONLY       0x00CF
//SpinEdit---------------------------------------------------------------------------
void    SpinEdit_Write(HWND hWnd,float value);
float   SpinEdit_Read(HWND hWnd);
//DigiEdit---------------------------------------------------------------------------
void    DigiEdit_Write(HWND hWnd,float value);
float   DigiEdit_Read(HWND hWnd);
//Edit---------------------------------------------------------------------------
void Edit_SetReadOnly(HWND hWnd,BOOL bReadonly);
void Edit_SetMaxLength(HWND hWnd,int maxLength);
void Edit_SetPasswordChar(HWND hWnd,char pswchar);
char Edit_GetPasswordChar(HWND hWnd);
void Edit_SetReplaceMode(HWND hWnd,BOOL bSwitch);
//Memo---------------------------------------------------------------------------
int  Memo_GetLineCount(HWND hWnd);
int  Memo_GetLineText(HWND hWnd,int lineIndex,char *linebuf, int bufsize);
void Memo_SetText(HWND hWnd,char *strText);
void Memo_AppendText(HWND hWnd,char *strText);
void Memo_PageScroll(HWND hWnd,int topLine);
void Memo_SetReadOnly(HWND hWnd,BOOL bReadonly);
void Memo_SetMaxLength(HWND hWnd,int maxLength);
void Memo_SetReplaceMode(HWND hWnd,BOOL bSwitch);
/***************************************************************************
 *  ProgressBar
 ***************************************************************************/
void ProgressBar_SetMax(HWND hWnd,int maxValue);
void ProgressBar_SetPos(HWND hWnd,int position);
/***************************************************************************
 *  CheckBox
 ***************************************************************************/
/* CheckBox Control style */
#define CS_RIGHTJUSTIFY      0x0000
#define CS_LEFTJUSTIFY       0x0001
void CheckBox_SetValue(HWND hWnd,BOOL checked);
BOOL CheckBox_GetValue(HWND hWnd);
/***************************************************************************
 *  RadioBox
 ***************************************************************************/
/* RadioBox Control style */
#define RS_RIGHTJUSTIFY      CS_RIGHTJUSTIFY
#define RS_LEFTJUSTIFY       CS_LEFTJUSTIFY
void RadioBox_SetValue(HWND hWnd,BOOL checked);
BOOL RadioBox_GetValue(HWND hWnd);
/***************************************************************************
 *  ListBox/Menu
 ***************************************************************************/
#define  Menu_AddItem            ListBox_AddItem
#define  Menu_InsertItem         ListBox_InsertItem
#define  Menu_ChangeItem         ListBox_ChangeItem
#define  Menu_DeleteItem         ListBox_DeleteItem
#define  Menu_SetSelectedIndex   ListBox_SetSelectedIndex
#define  Menu_GetSelectedIndex   ListBox_GetSelectedIndex
#define  Menu_GetSelectedItem    ListBox_GetSelectedItem
#define  Menu_GetItemByIndex     ListBox_GetItemByIndex
#define  Menu_SetMargin          ListBox_SetMargin
int      ListBox_AddItem(HWND hWnd,char *strItem);
int      ListBox_InsertItem(HWND hWnd,int index,char *strItem);
void     ListBox_ChangeItem(HWND hWnd,int index,char *strItem);
bool     ListBox_DeleteItem(HWND hWnd,int index);
int      ListBox_SetSelectedIndex(HWND hWnd,int index);
int      ListBox_GetSelectedIndex(HWND hWnd);
char    *ListBox_GetSelectedItem(HWND hWnd);
char    *ListBox_GetItemByIndex(HWND hWnd,int index);
void     ListBox_SetMargin(HWND hWnd,int leftMargin,int lineMargin);
/***************************************************************************
 *  IME
 ***************************************************************************/
void     IME_Open(HWND hEdtor,HWND parent,int left,int top,int width,int height);
void     IME_Close(void);
BOOL     IME_isEmpty(void);
void     IME_PushChar(char ch);
char     IME_PopChar(void);
/***************************************************************************
 *  scrollbar
 ***************************************************************************/
void ScrollBar_Initialize(HWND hWnd,int TopLine,int TotalLine,int LinePerPage,int PageKeepLines);
void ScrollBar_Synchronize(HWND hWnd,int topLine);
void ScrollBar_Synchronize2(HWND hWnd,int topLine,int totalLine);
/***************************************************************************
 *  Others
 ***************************************************************************/
typedef struct{HWND hWnd;TRECT DragBar;int xDragging,yDragging;}TWinDragger; 
//---------------------------------------------------------------------------
void Dragger_Init(TWinDragger *dragger,HWND hWnd,TRECT *barRect);
void Dragger_Process(TWinDragger *dragger,UINT message,LPARAM lParam);
//---------------------------------------------------------------------------
int MsgDlg(char *text,char *caption,char *btnArray);
//---------------------------------------------------------------------------

#endif

