/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:			    mingui.h
*  PROGRAMMER:			    ming.c
*  Date of Creation:		    2006/08/8
*
*  DESCRIPTION: 								
*	  GUI Lib public Interface					
*  NOTE:						 		
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
*
*  MODIFICATION HISTORY
*     LastModify  2006/10/28
******************************************************************************/
#ifndef MINGUI_H
#define MINGUI_H
//---------------------------------------------------------------------------
#include  <string.h>
#include  <stdio.h>
//---------------------------------------------------------------------------
#include  "wingal.h"
#include  "winapi.h"
#include  "wintern.h"
#include  "winctrl.h"
//---------------------------------------------------------------------------
#endif