/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:			wintern.h
*  PROGRAMMER:			ming.c
*  Date of Creation:		2006/08/8
*
*  DESCRIPTION: 								
*			Semaphore			
*  NOTE:						 		
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
*
*  MODIFICATION HISTORY
*     LastModify  2006/09/18
******************************************************************************/
#ifndef _WINTERN_H
#define _WINTERN_H
//---------------------------------------------------------------------------
#include "winapi.h"
//---------------------------------------------------------------------------
#define WNDPTR(hWnd)             ((PWND)(hWnd))
#define WndGetAttr(hWnd,Attr)    (((PWND)(hWnd))->Style &  (Attr))
#define WndAddAttr(hWnd,Attr)    (((PWND)(hWnd))->Style |= (Attr))
#define WndSubAttr(hWnd,Attr)    (((PWND)(hWnd))->Style &=~(Attr))
#define WndExtraData(hWnd)       (((PWND)(hWnd))->ExtraLong[0])
#define WndClsBuf(hWnd)          ((char *)&((PWND)(hWnd))->ExtraLong[1])
#define WndGetText(hWnd)         ( (((PWND)(hWnd))->Caption.Info.Head==0xFF)?((PWND)(hWnd))->Caption.Info.pText:((PWND)(hWnd))->Caption.Text )
//---------------------------------------------------------------------------
#define WndPosX(hWnd)            ( ((PWND)(hWnd))->WndRect.left - ((PWND)(hWnd))->Parent->ClientRect.left )
#define WndPosY(hWnd)            ( ((PWND)(hWnd))->WndRect.top - ((PWND)(hWnd))->Parent->ClientRect.top )
#define crWidth(hWnd)            ( ((PWND)(hWnd))->ClientRect.right - ((PWND)(hWnd))->ClientRect.left )
#define crHeight(hWnd)           ( ((PWND)(hWnd))->ClientRect.bottom - ((PWND)(hWnd))->ClientRect.top )
#define absWidth(hWnd)            ( ((PWND)(hWnd))->WndRect.right - ((PWND)(hWnd))->WndRect.left )
#define absHeight(hWnd)           ( ((PWND)(hWnd))->WndRect.bottom - ((PWND)(hWnd))->WndRect.top )
//---------------------------------------------------------------------------
#define ResetForeground(dc)      ((TWndCanvas *)(dc))->Pen.Foreground= ((TWndCanvas *)(dc))->Owner->Foreground
#define ResetBackground(dc)      ((TWndCanvas *)(dc))->Pen.Background= ((TWndCanvas *)(dc))->Owner->Background
//---------------------------------------------------------------------------
#define GUI_MARK_WND             0x51775277L
//---------------------------------------------------------------------------
#define IsWnd(hWnd)             ( hWnd && ((PWND)(hWnd))->Mark==GUI_MARK_WND )
//---------------------------------------------------------------------------
typedef struct t_winctrl    TWND,*PWND;
//---------------------------------------------------------------------------
typedef struct
{ int	  Left,Top;               //VRAM左上角对应LCD左上角的偏移位置 (unit:pixel)
  int	  Width,Height;           //VRAM 的宽度与高度 (unit:pixel)
  int	  BytePerLine;            //一行VRAM占的字节数
  int     UnitPerLine;            //一行VRAM占的存储单元数
  int     GroupOperation;
  TREGION ExposeRegion;           //相对屏幕的绝对坐标
  void	 *Buffer;
  DWORD   BufferSize;
  BOOL    OwnBuffer;             /*是否内部自动分配的Buffer空间*/
}VRAM,*PVRAM;
//---------------------------------------------------------------------------
typedef struct
{ DWORD  Mode;
  int    Foreground,Background; /* color pixel index */
}TPEN;
//---------------------------------------------------------------------------
typedef struct t_font
{ struct t_font  *Next;
  char fontname[16]; /*字体名称*/
  int  codetype;/*编码方式*/
  int  height;/*字体高度*/
  int  eWidth;/*ASCII字符宽度*/
  int  cWidth;/*汉字宽度*/
  int  eBytes; /*一个ASCII码字模占多少字节*/
  int  cBytes; /*一个汉字字模占多少字节*/
  BYTE *eBits; /*ASCII点阵开始地址*/
  BYTE *cBits; /*汉字点阵开始地址,eBits偏移128个ASCII码点阵*/
}TFont;
//---------------------------------------------------------------------------
typedef struct t_wndcanvas 
{ struct t_wndcanvas *Prev,*Next;
  PWND   Owner;
  VRAM  *Vram;
  int    VX,VY;             /*vram offset*/
  TRECT  ClipRect;  
  TPEN   Pen; 
  TFont *Font;
}TWndCanvas;
//---------------------------------------------------------------------------
#define   dcMode(dc)        ((TWndCanvas *)dc)->Pen.Mode
//---------------------------------------------------------------------------
typedef struct
{ PWND  TopWnd,HisFocus;
  BOOL  CanvasChanged;
  VRAM  *Vram;
}TWndFamily;
//---------------------------------------------------------------------------
 /* window*/
struct t_winctrl
{ struct t_winctrl  *Prev,*Next,*Parent,*Children;
  DWORD         Mark;
  DWORD         ID;                  
  DWORD         Style;               /*窗口样式和状态*/
  TBitmap      *Logo;                /*窗口图标*/
  TVariantText  Caption;             /*窗口标题*/
  TWndCanvas   *Canvas;              /*指向主窗体的画布*/
  TWNDCLASS    *WinClass;		     /* window class*/  
  TWndFamily   *Family;
  TFont        *Font;
  TRECT	        WndRect;	         /* window rect in screen coords*/
  TRECT	        ClientRect;          /* client rect in screen coords*/
  TWNDHOOK      HookProc;
  UINT          HookMessage;
  int           Foreground,Background;  /* color pixel index */
  int           TabOrder;
  int		    unmapcount;	         /* count of reasons not really mapped */
  long          UserData; 
  long          ExtraLong[1];        /* window extra bytes - must be last*/
};
//---------------------------------------------------------------------------
typedef struct t_scrollbar
{ int ScrollbarDragging,flipflop;
  int Left,Top,Right,Bottom;
  int TrackHeight,ScrollTop,ScrollHeight;
  int TotalLine,TopLine,LinePerPage;
  int PageKeepLines; 
}TScrollBar;
//---------------------------------------------------------------------------
#define  CMD_NotifyParent(hWnd,eventcode)    SendMessage((HWND)WNDPTR(hWnd)->Parent, WM_COMMAND, (WPARAM)MAKELONG(WNDPTR(hWnd)->ID,eventcode), (LPARAM)(hWnd))
#define  SetColorIndex(dc,index)             ((TWndCanvas *)(dc))->Pen.Foreground=(index)
#define  GetColorIndex(dc)                   ( ((TWndCanvas *)(dc))->Pen.Foreground )
#define  GetSysCharHeight(hWnd)              ((PWND)(hWnd))->Font->height
#define  GetSysCharWidth(hWnd)               ((PWND)(hWnd))->Font->eWidth
#define  GetSysCCharWidth(hWnd)              ((PWND)(hWnd))->Font->cWidth
//---------------------------------------------------------------------------
/******************************************************************************************/
#endif