/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:			wingal.h
*  PROGRAMMER:			ming.c
*  Date of Creation:	2006/08/08
*
*  DESCRIPTION: 								
*						
*  NOTE:						 		
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
*
*  MODIFICATION HISTORY
*     LastModify  2006/08/08
******************************************************************************/
#ifndef   _WNDGAL_H
#define   _WNDGAL_H
#include  "winbas.h"
//------------------------------------------------------------------------------
#define BASE_OS_TYPE         0           /* 0: Independent of any OS kernal   */
                                         /* 1: Running on PC Simulator        */
                                         /* 2: Running on Nucleus kernal      */
                                         /* 3: Running on AsixOS kernal       */
                                         /* 4: Running on other OS kernal     */
//------------------------------------------------------------------------------
#define GUI_CLK_CYCLE        500         /* GUI的驱动时钟,单位:毫秒           */
/*为防止GUI时钟波动，GUI_CLK_CYCLE请设为1000的某整数等分，如1000,500,200,100 ...等*/                                        
//------------------------------------------------------------------------------
#define	HeapStartAddress     0x31000000           /* GUI系统堆的起始地址 */
#define	HeapTotalSize 	     0xf00000             /* GUI系统堆的大小(要求足够大)  */
//------------------------------------------------------------------------------
#define LCD_FRAME_BUFFER     0x31f00000      /* 屏幕显示缓冲区首址默认值          */
#define LCD_WIDTH            320             /* 屏幕宽度                           */
#define LCD_HEIGHT           240            /* 屏幕高度                           */
#define BITS_PER_PIXEL	     16              /* 单位像素占用的位数1/2/4/8/16/24/32*/
#define PIXEL_BIG_ENDIAN     true            /* 像素存储单元的字位顺序            */
#define VRAM_RANGE_CHECK     true            /* 输出到VRAM时，进行内存边界检查    */
#define LCD_RANGE_CHECK      true            /* 输出到LCD时，进行视域边界检查     */
//------------------------------------------------------------------------------
#define MSG_QUEUE_CAPACITY   50              /* 消息队列容量(要求足够大)*/
//------------------------------------------------------------------------------
#define CLIPRGN_HEAP_SIZE    50              /* 窗口剪切域的预分配矩形数目*/
//------------------------------------------------------------------------------
#define DC_PRELOAD_COUNT     10              /* 预分配DC数目*/
//---------------------------------------------------------------------------
#define SYS_DEFAULT_FONT     "hzk16"         /* 指定系统默认字体（必须是在winfont.c中注册的字体名称）*/
////////////////////////////////////////////////////////////////////////////////////
#if(ON_SIMULATOR && BASE_OS_TYPE!=1)             
   #undef BASE_OS_TYPE
   #define BASE_OS_TYPE  1
#elif(!ON_SIMULATOR && BASE_OS_TYPE==1)
   #undef BASE_OS_TYPE
   #define BASE_OS_TYPE  0
#endif
////////////////////////////////////////////////////////////////////////////////
#if(BITS_PER_PIXEL==1)                  // black and white LCD
////////////////////////////////////////////////////////////////////////////////////
 #define UNIT_PIXEL_MASK        0x01    // (1<<BITS_PER_PIXEL)-1
 #define PIXEL_UNIT             8       // 像素单位的比特数8/16/32  (2的整数次方) ==(BITS_PER_PIXEL+7) / 8 * 8
 #define UNIT_BYTE              1       // PIXEL_UNIT/8
 #define PIXELS_PER_UNIT        8       // PIXEL_UNIT/BITS_PER_PIXEL
 #define LOG_PIXEL_BITS	        0       // log2(BITS_PER_PIXEL)
 #define LOG_UNIT      	        3       // log2(PIXEL_UNIT)
 #define LOG_UNIT_BYTE	        0       // log2(UNIT_BYTE)
 #define LOG_PIXEL_NUM	        3       // log2(PIXELS_PER_UNIT)
 #define INDEX_RED_BITWIDTH     0       //INDEX_RED_BITWIDTH+INDEX_GREEN_BITWIDTH+INDEX_BLUE_BITWIDTH==BITS_PER_PIXEL
 #define INDEX_GREEN_BITWIDTH   0
 #define INDEX_BLUE_BITWIDTH    1
////////////////////////////////////////////////////////////////////////////////////
#elif(BITS_PER_PIXEL==2)                // 4gray
////////////////////////////////////////////////////////////////////////////////////
 #define UNIT_PIXEL_MASK        0x03    // (1<<BITS_PER_PIXEL)-1
 #define PIXEL_UNIT     	    8	    // 像素单位的比特数8/16/32
 #define UNIT_BYTE              1	    // PIXEL_UNIT/8
 #define PIXELS_PER_UNIT	    4	    // PIXEL_UNIT/BITS_PER_PIXEL
 #define LOG_PIXEL_BITS	        1	    // log2(BITS_PER_PIXEL)
 #define LOG_UNIT      	        3	    // log2(PIXEL_UNIT)
 #define LOG_UNIT_BYTE	        0	    // log2(UNIT_BYTE)
 #define LOG_PIXEL_NUM	        2	    // log2(PIXELS_PER_UNIT)
 #define INDEX_RED_BITWIDTH     0       //INDEX_RED_BITWIDTH+INDEX_GREEN_BITWIDTH+INDEX_BLUE_BITWIDTH==BITS_PER_PIXEL
 #define INDEX_GREEN_BITWIDTH   1
 #define INDEX_BLUE_BITWIDTH    1
////////////////////////////////////////////////////////////////////////////////////
#elif(BITS_PER_PIXEL==4)                // 16gray
////////////////////////////////////////////////////////////////////////////////////
 #define UNIT_PIXEL_MASK        0x0F    // (1<<BITS_PER_PIXEL)-1
 #define PIXEL_UNIT     	    8	    // 像素单位的比特数8/16/32
 #define UNIT_BYTE              1	    // PIXEL_UNIT/8
 #define PIXELS_PER_UNIT    	2	    // PIXEL_UNIT/BITS_PER_PIXEL
 #define LOG_PIXEL_BITS	        2	    // log2(BITS_PER_PIXEL)
 #define LOG_UNIT      	        3	    // log2(PIXEL_UNIT)
 #define LOG_UNIT_BYTE	        0	    // log2(UNIT_BYTE)
 #define LOG_PIXEL_NUM	        1	    // log2(PIXELS_PER_UNIT)
 #define INDEX_RED_BITWIDTH     1       //INDEX_RED_BITWIDTH+INDEX_GREEN_BITWIDTH+INDEX_BLUE_BITWIDTH==BITS_PER_PIXEL
 #define INDEX_GREEN_BITWIDTH   2
 #define INDEX_BLUE_BITWIDTH    1
////////////////////////////////////////////////////////////////////////////////////
#elif(BITS_PER_PIXEL==8)                // 256色 332默认调色板
////////////////////////////////////////////////////////////////////////////////////
 #define UNIT_PIXEL_MASK        0xFF    // (1<<BITS_PER_PIXEL)-1
 #define PIXEL_UNIT         	8	    // 像素单位的比特数8/16/32
 #define PIXEL_BYTE             1       // BITS_PER_PIXEL/8
 #define UNIT_BYTE              1	    // PIXEL_UNIT/8
 #define INDEX_RED_BITWIDTH     3       //INDEX_RED_BITWIDTH+INDEX_GREEN_BITWIDTH+INDEX_BLUE_BITWIDTH==BITS_PER_PIXEL
 #define INDEX_GREEN_BITWIDTH   3
 #define INDEX_BLUE_BITWIDTH    2
 ////////////////////////////////////////////////////////////////////////////////////
#elif(BITS_PER_PIXEL==16)               // 16位色   565默认调色板 
////////////////////////////////////////////////////////////////////////////////////
 #define UNIT_PIXEL_MASK       0xFFFF   // (1<<BITS_PER_PIXEL)-1
 #define PIXEL_UNIT	           16       // 像素存储单位的比特数 8/16/32
 #define PIXEL_BYTE             2       // BITS_PER_PIXEL/8
 #define UNIT_BYTE	            2	    // PIXEL_UNIT/8
 #define INDEX_RED_BITWIDTH     5       //INDEX_RED_BITWIDTH+INDEX_GREEN_BITWIDTH+INDEX_BLUE_BITWIDTH==BITS_PER_PIXEL
 #define INDEX_GREEN_BITWIDTH   6
 #define INDEX_BLUE_BITWIDTH    5
////////////////////////////////////////////////////////////////////////////////////
#elif(BITS_PER_PIXEL==24) 
////////////////////////////////////////////////////////////////////////////////////
 #define UNIT_PIXEL_MASK        0x00FFFFFF // (1<<BITS_PER_PIXEL)-1
 #define PIXEL_UNIT	            32         // 像素存储单位的比特数 8/16/32
 #define PIXEL_BYTE             3          // BITS_PER_PIXEL/8
 #define UNIT_BYTE	            4	       // PIXEL_UNIT/8
 #define INDEX_RED_BITWIDTH     8       
 #define INDEX_GREEN_BITWIDTH   8
 #define INDEX_BLUE_BITWIDTH    8
////////////////////////////////////////////////////////////////////////////////////
#elif( BITS_PER_PIXEL==32) 
////////////////////////////////////////////////////////////////////////////////////
 #define UNIT_PIXEL_MASK        0xFFFFFFFF // (1<<BITS_PER_PIXEL)-1
 #define PIXEL_UNIT	            32         // 像素存储单位的比特数 8/16/32
 #define PIXEL_BYTE             4          // BITS_PER_PIXEL/8
 #define UNIT_BYTE	            4	       // PIXEL_UNIT/8
 #define INDEX_RED_BITWIDTH     8       
 #define INDEX_GREEN_BITWIDTH   8
 #define INDEX_BLUE_BITWIDTH    8
////////////////////////////////////////////////////////////////////////////////////
#endif
////////////////////////////////////////////////////////////////////////////////////
#if(LCD_FRAME_BUFFER)
  #define  VRAMDisplay       VramToFrameBuffer
#else
  #define  VRAMDisplay       DirectToLCD
#endif
////////////////////////////////////////////////////////////////////////////////////
extern void  *GUI_LcdFrameBuffer;  /* LCD帧缓冲区首址实际值，在系统初始化时，若不手动赋值，将自动拷贝LCD_FrameBuffer宏定义的值。*/ 
extern void   GUI_TimerPulse(void); /*GUI时钟驱动*/
////////////////////////////////////////////////////////////////////////////////////
#endif
