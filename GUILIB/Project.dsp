# Microsoft Developer Studio Project File - Name="MinGUI" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=MinGUI - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Project.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Project.mak" CFG="MinGUI - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "MinGUI - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "MinGUI - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "MinGUI - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x804 /d "NDEBUG"
# ADD RSC /l 0x804 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "MinGUI - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I "include" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /FR /YX /FD /GZ /Zm500 /c
# ADD BASE RSC /l 0x804 /d "_DEBUG"
# ADD RSC /l 0x804 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "MinGUI - Win32 Release"
# Name "MinGUI - Win32 Debug"
# Begin Group "Demo"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\demo\demo1\demo.c
# End Source File
# Begin Source File

SOURCE=.\demo\demo1\demo_sub0.c
# End Source File
# Begin Source File

SOURCE=.\demo\demo1\demo_sub1.c
# End Source File
# Begin Source File

SOURCE=.\demo\demo1\demo_sub2.c
# End Source File
# Begin Source File

SOURCE=.\demo\demo1\demo_sub3.c
# End Source File
# Begin Source File

SOURCE=.\demo\demo1\graphdata.c
# End Source File
# End Group
# Begin Group "GUI files"

# PROP Default_Filter ""
# Begin Group "CTRL"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\CTRL\button.c
# End Source File
# Begin Source File

SOURCE=.\CTRL\checkbox.c
# End Source File
# Begin Source File

SOURCE=.\CTRL\digiedit.c
# End Source File
# Begin Source File

SOURCE=.\CTRL\edit.c
# End Source File
# Begin Source File

SOURCE=.\CTRL\groupbox.c
# End Source File
# Begin Source File

SOURCE=.\CTRL\listbox.c
# End Source File
# Begin Source File

SOURCE=.\CTRL\memo.c
# End Source File
# Begin Source File

SOURCE=.\CTRL\MsgDlg.c
# End Source File
# Begin Source File

SOURCE=.\CTRL\progressbar.c
# End Source File
# Begin Source File

SOURCE=.\CTRL\radiobox.c
# End Source File
# Begin Source File

SOURCE=.\CTRL\spinedit.c
# End Source File
# Begin Source File

SOURCE=.\CTRL\static.c
# End Source File
# Begin Source File

SOURCE=.\CTRL\winctrl.c
# End Source File
# Begin Source File

SOURCE=.\CTRL\winime.c
# End Source File
# End Group
# Begin Group "Include"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Include\cdjpeg.h
# End Source File
# Begin Source File

SOURCE=.\Include\jconfig.h
# End Source File
# Begin Source File

SOURCE=.\Include\jdct.h
# End Source File
# Begin Source File

SOURCE=.\Include\jdhuff.h
# End Source File
# Begin Source File

SOURCE=.\Include\jerror.h
# End Source File
# Begin Source File

SOURCE=.\Include\jinclude.h
# End Source File
# Begin Source File

SOURCE=.\Include\jmemsys.h
# End Source File
# Begin Source File

SOURCE=.\Include\jmorecfg.h
# End Source File
# Begin Source File

SOURCE=.\Include\jpegint.h
# End Source File
# Begin Source File

SOURCE=.\Include\jpeglib.h
# End Source File
# Begin Source File

SOURCE=.\Include\mingui.h
# End Source File
# Begin Source File

SOURCE=.\Include\winapi.h
# End Source File
# Begin Source File

SOURCE=.\Include\winbas.h
# End Source File
# Begin Source File

SOURCE=.\Include\winctrl.h
# End Source File
# Begin Source File

SOURCE=.\Include\wingal.h
# End Source File
# Begin Source File

SOURCE=.\Include\wintern.h
# End Source File
# End Group
# Begin Group "kernel"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Kernel\wingal.c
# End Source File
# Begin Source File

SOURCE=.\Kernel\wingdi.c
# End Source File
# Begin Source File

SOURCE=.\Kernel\winitial.c
# End Source File
# Begin Source File

SOURCE=.\Kernel\winmsg.c
# End Source File
# Begin Source File

SOURCE=.\Kernel\SimulatorDrv.lib
# End Source File
# End Group
# Begin Group "Fonts"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Fonts\hzk12.c
# End Source File
# Begin Source File

SOURCE=.\Fonts\hzk16.c
# End Source File
# Begin Source File

SOURCE=.\Fonts\winfont.c
# End Source File
# End Group
# End Group
# Begin Group "AppEntry"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\demo\main\demomain.c
# End Source File
# End Group
# End Target
# End Project
