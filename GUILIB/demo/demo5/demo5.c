#include "mingui.h"
/////////////////////////////////////////////////////////
#define  ID_BUTTON0    100
#define  ID_BUTTON1    101
#define  ID_BUTTON2    102
#define  ID_BUTTON3    103
#define  ID_BUTTON4    104

#define  NEW_FONT_NAME   "demofont"
#define  OLD_FONT_NAME   SYS_DEFAULT_FONT
/////////////////////////////////////////////////////////
HWND medit,abutton,btn[4],edt;
HANDLE hSecondTimer=NULL;


PWND myButton;
extern const unsigned char font_new[];

bool readonly=false,enabled=true,openime=false,selectNewFont=true;
/////////////////////////////////////////////////////////
HRESULT CALLBACK MainProcess(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam)
{  switch(Message)
   { 
     case WM_CREATE:
          {  
             
			 btn[0]=CreateWindow("button","切换字体",WS_BORDER|WS_TABSTOP, 16,3,75,25,hWnd,ID_BUTTON0,NULL);
 
             btn[1]=CreateWindow("button","输入法",WS_BORDER|WS_TABSTOP, 40,180,75,25,hWnd,ID_BUTTON1,NULL);
			 btn[2]=CreateWindow("button","灰化",WS_BORDER|WS_TABSTOP, 120,180,75,25,hWnd,ID_BUTTON2,NULL);
             btn[3]=CreateWindow("button","只读",WS_BORDER|WS_TABSTOP, 200,180,75,25,hWnd,ID_BUTTON3,NULL);
			 

			 edt=CreateWindow("Edit","￥%你好吗ello我!",WS_BORDER|WS_TABSTOP|WS_TRANSPARENT, 200,140,100,25,hWnd,0,NULL); 
			 
			 medit=CreateWindow("memo","a主菜单abcdefghijklmn1234567890abcdefghijklmn1234567890abcdef你\r好啊gh悄\n0\n1\n2\n3\n4\n5\n6\n7\n8\n\n9",WS_BORDER|WS_VSCROLL|WS_TABSTOP|ES_WANTRETURN|WS_TRANSPARENT,16,30,280,100,hWnd,0,NULL);
             
             SetWindowFont(medit,NEW_FONT_NAME);
               
          } 
          break;
   case WM_PAINT:
	   /* 
	   { HDC dc=BeginPaint(hWnd);
	    TRECT box={10,50,150,100};
		 TPOINT pos={50,55};
	    
		 SetColor(dc,0xFF6600);
		 FillRect(dc,box.left,box.top,box.right-box.left,box.bottom-box.top);
		 SetColor(dc,0x00);
	     DrawRect(dc,box.left,box.top,box.right-box.left,box.bottom-box.top);
		 TextBox(dc,&box,&pos,"abcdefghijklmn1234567890abcdefghijklmn1234567890abcdef你\n好啊gh悄",0);
	     EndPaint(hWnd);
		 return 0;
	   }*/
        break;

   case WM_DESTROY:
        return 0;

   case WM_TIMER:
         { if((HANDLE)WParam==hSecondTimer)
           { int iHour, iMinute,iSecond;
             HDC dc=GetDC(hWnd);

			 if(selectNewFont) SetFont(dc,OLD_FONT_NAME);
		     else SetFont(dc,NEW_FONT_NAME);

             SetColor(dc,CL_BLACK);
             SetBkColor(dc,CL_GREEN);
	         SetFontStyle(dc,FS_OPAQUE,true);
             GetSysTime(&iHour, &iMinute,&iSecond);
           
	         PrintText(dc,160,8,"%02d:%02d:%02d",iHour,iMinute,iSecond);
		 
             ReleaseDC(dc);
			// Memo_AppendText(medit,"hello");
           }

         }
   
   case WM_COMMAND:
	      if(HIWORD(WParam)==BN_CLICKED)
          {  switch(LOWORD(WParam))
             { case ID_BUTTON0:
					   if(selectNewFont) SetWindowFont(medit,OLD_FONT_NAME);
					   else SetWindowFont(medit,NEW_FONT_NAME);
					   selectNewFont=!selectNewFont;
			        break;
			 
			   case ID_BUTTON1:
			        openime=!openime;
					if(openime)IME_Open(medit,hWnd,20,138,150,38);
					else IME_Close();
			   break;

			   case ID_BUTTON2:
				     enabled=!enabled;
			         EnableWindow(medit,enabled);
				    break;
               case ID_BUTTON3:
				      readonly=!readonly;
				      SendMessage(medit,EM_SETREADONLY,readonly,0);

                    break;
            }
		  }
          break;

   }

    return DefWindowProc(hWnd,Message,WParam,LParam);
}


//////////////////////////////////////////////////////////////////


void DemoMain(void)
{   TMSG msg;
    TWNDCLASS wc;
    HWND winmain;
 
    /* 注册新字体 */
    RegisterFont(NEW_FONT_NAME,font_new);
 
    /*创建主窗口*/
    memset(&wc,0,sizeof(wc));
    wc.clForeground=CL_BTNTEXT;
    wc.clBackground=0xFF6600;      
    wc.lpfnWndProc=MainProcess;
    wc.lpszClassName="mainwnd";
    RegisterClass(&wc);
    winmain=CreateWindow("mainwnd","winmain",WS_CAPTION,0,0,LCD_WIDTH,LCD_HEIGHT,NULL,0,NULL);
      
   
    /* 创建秒时钟*/
	hSecondTimer=CreateTimer(winmain,1000,1000,NULL,true);
   
    /* 打开数字键盘译码，数字键译为26个字符和标点符号*/
    NUMPAD_DecodeShift(true); 




    while(GetMessage(&msg))
    { TranslateMessage(&msg);
      DispatchMessage(&msg);
    }
 
}           

