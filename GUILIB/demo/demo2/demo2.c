#include "mingui.h"
//////////////////////////////////////////////////////////////////

HWND hTimeLabel,win2,winmain,winmain2,hDlg,abutton,spinedit1,SpinEdit1,DigitalEdit1,ime1,edtor,mymenu;

#define  ID_BUTTON0    100
#define  ID_BUTTON_MAIN  200
///////////////////////////////////////////////////
extern unsigned char desktop_array[28355];
extern unsigned char DialogLogoImg[520];
extern unsigned char ppButtonFace1[1049];
extern unsigned char ppButtonFace2[1024];
extern unsigned char DialogLogoImg[520];
extern unsigned char gImage_TimerBkGround[1208];
extern unsigned char Logo_array1[1745];
extern unsigned char Logo_array2[1814];
extern unsigned char Logo_array3[1732];
extern unsigned char Logo_array4[1705];
extern unsigned char Logo_array5[1712];
///////////////////////////////////////////////////
int tmrcount=0;

HWND btn[8];
TBitmap *buttonlogo[8],*timerBkBmp,*ppButtonFaceImage1,*ppButtonFaceImage2,*hDlgLogo,*desktop_bitmap;
BOOL onoff=false;

HRESULT ButtonHook(HWND hWnd,UINT Message,WPARAM wParam,LPARAM lParam,BOOL* bIntercept)
{ if(Message==WM_SETFOCUS)
  { SetWindowLogo(abutton,ppButtonFaceImage2);
  }
  else if(Message==WM_KILLFOCUS)
  {SetWindowLogo(abutton,ppButtonFaceImage1);
  }
  else if(Message==WM_LBUTTONDOWN)
  {/* if(!IsWnd(hDlg))
    { ShowWindow(mymenu,true);
      SetFocus(mymenu);
    }*/
  }
  return -1;
}




HRESULT CALLBACK MainProcess(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam)
{  switch(Message)
   { int i;
   /*  case WM_PAINT:
     {       HDC dc=BeginPaint(hWnd);
             TWndCanvas *ACanvas=(TWndCanvas *)dc;
             TBitmap *bm=CreateBitmap();
             LoadBitmapFromMem(bm,desktop1_array,20506);
             DrawBitmap(dc,0,0,bm);
             //BitBlt(dc,0,0,640,480,bm->Handle,0,0,PL_REPLACE);
             //LoadBitmapFromFile(bm,"C:\\Documents and Settings\\aufame\\桌面\\meditate.JPG");
             //bm->Transparent=true;
             //BitBlt(dc,200,200,640,480,bm->Handle,0,0,PL_XOR);
             // StretchBlt(dc,200,100,300,300,bm->Handle,15,10,bm->Width-30,bm->Height-20,PL_REPLACE);
             //SetPenLogic(dc,PL_AND);
             //StretchDrawBitmap(dc,200,100,300,300,bm);
             //for(i=200;i<300;i++) DrawPixel(dc,i,i);

              DestroyBitmap(bm);
              EndPaint(hWnd);
              return 0;
         }*/
case WM_ERASEBKGND:  /* repaint background*/
		 if(WParam && LParam)
		 { HDC dc=(HDC)WParam;
		   TRECT *prc=(TRECT *)LParam;
		   ExDrawBitmap(dc,prc->left,prc->top,prc->right-prc->left,prc->bottom-prc->top,desktop_bitmap,prc->left,prc->top);
		 }
		 return 0;
    case WM_CREATE:
          {  desktop_bitmap=CreateBitmap();
			 LoadBitmapFromMem(desktop_bitmap,desktop_array);
			 
			 hDlgLogo=CreateBitmap();
             LoadBitmapFromMem(hDlgLogo,DialogLogoImg);

             ppButtonFaceImage1=CreateBitmap();
             LoadBitmapFromMem(ppButtonFaceImage1,ppButtonFace1);
             ppButtonFaceImage2=CreateBitmap();
             LoadBitmapFromMem(ppButtonFaceImage2,ppButtonFace2);
             abutton=CreateWindow("button",NULL,WS_TABSTOP,0,LCD_HEIGHT-23,66,22,hWnd,ID_BUTTON_MAIN,NULL);
             SetWindowLogo(abutton,ppButtonFaceImage1);
             //InstallHook(abutton,0,ButtonHook);

             hTimeLabel=CreateWindow("static",NULL,WS_TRANSPARENT,260,224,60,12,hWnd,0,NULL);
             SetWindowTextColor(hTimeLabel,CL_WHITE);



              for(i=0;i<8;i++)
              { buttonlogo[i]=CreateBitmap();
               //buttonlogo[i]->Transparent=true;
                btn[i]=CreateWindow("button",NULL,WS_BORDER|WS_TABSTOP|WS_TRANSPARENT,2+(i%4)*79,55+(i/4)*61,78,60,hWnd,ID_BUTTON0+i,NULL);
              }
              LoadBitmapFromMem(buttonlogo[0],Logo_array1);
              LoadBitmapFromMem(buttonlogo[1],Logo_array2);
              LoadBitmapFromMem(buttonlogo[2],Logo_array3);
              LoadBitmapFromMem(buttonlogo[3],Logo_array4);
              LoadBitmapFromMem(buttonlogo[4],Logo_array5);


              SetWindowLogo(btn[0],buttonlogo[0]);
              SetWindowLogo(btn[1],buttonlogo[1]);
              SetWindowLogo(btn[2],buttonlogo[2]);
              SetWindowLogo(btn[3],buttonlogo[3]);
              SetWindowLogo(btn[4],buttonlogo[4]);
			 

          }
          break;
   case WM_TIMER:
	  { int iHour, iMinute,iSecond;
            char timershowbuf[12];
            GetSysTime(&iHour, &iMinute,&iSecond);
            sprintf(timershowbuf,"%02d:%02d:%02d",iHour,iMinute,iSecond);
            SetWindowText(hTimeLabel,timershowbuf);
	  }
          break;
    case WM_COMMAND:
         { 
              


		   if(LOWORD(WParam)==ID_BUTTON_MAIN)
		   { switch(HIWORD(WParam))
		     { case BN_PUSHED:
		              SetWindowLogo(abutton,ppButtonFaceImage2);
			        break;
		       case	BN_UNPUSHED:
			          SetWindowLogo(abutton,ppButtonFaceImage1);
					break;
               case BN_CLICKED:
			          if(!IsWnd(hDlg))
					  { ShowWindow(mymenu,true);
                        SetFocus(mymenu);
					  }
		     }
		   }
		   else if(HIWORD(WParam)==BN_CLICKED)
		   { int buttonIndex=LOWORD(WParam)-ID_BUTTON0;
             if(buttonIndex>=0 && buttonIndex<=7)
			 { char strDispBuf[32];
               sprintf(strDispBuf,"Select button index of %d",buttonIndex+1);
               MessageBox(strDispBuf,"你好",MB_YESNOCANCEL|MB_ICONWARNING);
			 }
		   }
	       else if(HIWORD(WParam)==CM_SETFOCUS)
		   {  // if(LOWORD(WParam)==ID_BUTTON0+1) SetWindowLogo((HWND)LParam,buttonlogo[2]);
		   }
           else if(HIWORD(WParam)==CM_KILLFOCUS)
		   {   //if(LOWORD(WParam)==ID_BUTTON0+1) SetWindowLogo((HWND)LParam,buttonlogo[1]);
		   }
         }
         break;      
    case WM_DESTROY:
            DestroyBitmap(timerBkBmp);
            DestroyBitmap(ppButtonFaceImage1);
            DestroyBitmap(ppButtonFaceImage2);
            DestroyBitmap(hDlgLogo);
			DestroyBitmap(desktop_bitmap);
            for(i=0;i<8;i++) DestroyBitmap(buttonlogo[i]);
         break;

   }
 
    return DefWindowProc(hWnd,Message,WParam,LParam);
}




void MemuProcess(int menuItemindex)
{if(menuItemindex!=4)
  {  
	  hDlg=CreateWindow("window","拼音输入法测试",WS_SYSMENU|WS_BORDER|WS_CAPTION|WS_ALWAYSONTOP,20,20,280,150,NULL,0,NULL);

      CreateWindow("static","输入框：",WS_NULL,5,15,70,16,hDlg,0,NULL);
      edtor=CreateWindow("Edit","你好，请用电脑键盘编辑此文字！",WS_BORDER|WS_TABSTOP,70,10,180,25,hDlg,0,NULL);
      CreateWindow("static","请使用键盘输入...",WS_NULL,50,60,180,20,hDlg,0,NULL);

      IME_Open(edtor,hDlg,1,86,274,38);
      SetWindowLogo(hDlg,hDlgLogo);
	  SetFocus(edtor);
  }
  ShowWindow(mymenu,false);

}


void DemoMain(void)
{ 
   TMSG msg;
   TWNDCLASS wc;
   memset(&wc,0,sizeof(wc));
   wc.clForeground=CL_BTNTEXT;
   wc.clBackground=CL_BTNFACE;
   wc.lpfnWndProc=MainProcess;
   wc.lpszClassName="mainwnd";
   RegisterClass(&wc);

   winmain=CreateWindow("mainwnd","winmain",0,0,0,LCD_WIDTH,LCD_HEIGHT,NULL,0,NULL);

    mymenu=CreateWindow("menu","主菜单",WS_BORDER|WS_HIDE|WS_ALWAYSONTOP,2,95,100,120,NULL,0,MemuProcess);
    Menu_AddItem(mymenu,"01.主接线图");
    Menu_AddItem(mymenu,"02.电度量数据");
    Menu_AddItem(mymenu,"03.开关量状态");
    Menu_AddItem(mymenu,"04.实时波形");
    Menu_AddItem(mymenu,"05.exit");

	SetFocus(btn[0]);

  CreateTimer(winmain,1000,1000,NULL,true);
   while(GetMessage(&msg))
   { TranslateMessage(&msg);
     DispatchMessage(&msg);
   }



}           





