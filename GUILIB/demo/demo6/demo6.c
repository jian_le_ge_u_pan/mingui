#include "mingui.h"
 PWND node;
HRESULT CALLBACK HelloworldProcess(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam)
{  switch(Message)
   { case WM_PAINT:
	      { HDC dc=BeginPaint(hWnd);
	        
		   // SetColor(dc,CL_RED);
	        //TextOut(dc,50,50," Hello World！");
		   // FillRect(dc,0,0,crWidth(hWnd),crHeight(hWnd));
	        
			EndPaint(hWnd);
		  } 
          return 0;
     case WM_CREATE:
          {  HWND grp1,grp2,grp3;
		  
		     grp1 =CreateWindow("Groupbox","组合框",WS_BORDER|WS_TABSTOP, 10,10,80,90,hWnd,0,NULL);
		    node=(PWND) CreateWindow("radiobox","单选1",WS_TABSTOP|WS_FOCUS, 5,1,100,20,grp1,0,(void *)1);
			 CreateWindow("radiobox","单选2",WS_TABSTOP, 5,22,100,20,grp1,0,NULL);
			 CreateWindow("radiobox","单选3",WS_TABSTOP, 5,45,100,20,grp1,0,NULL);

			 grp2 =CreateWindow("Groupbox","组合框2",WS_BORDER|WS_TABSTOP, 100,10,80,90,hWnd,0,NULL);
		     CreateWindow("radiobox","选择1",WS_TABSTOP, 1,1,100,20,grp2,0,NULL);
			 CreateWindow("radiobox","选择2",WS_TABSTOP, 1,22,100,20,grp2,0,(void *)1);
			 CreateWindow("radiobox","选择3",WS_TABSTOP, 1,45,100,20,grp2,0,NULL);

			 grp3 =CreateWindow("Groupbox","组合框3",WS_BORDER|WS_TABSTOP, 210,10,80,90,hWnd,0,NULL);
		     CreateWindow("radiobox","选择1",WS_TABSTOP, 1,1,100,20,grp3,0,NULL);
			 CreateWindow("radiobox","选择2",WS_TABSTOP, 1,22,100,20,grp3,0,NULL);
			 CreateWindow("radiobox","选择3",WS_TABSTOP, 1,45,100,20,grp3,0,(void *)1);

			 CreateWindow("checkbox","多选1",WS_TABSTOP, 10,120,80,20,hWnd,0,NULL);
			 CreateWindow("checkbox","多选2",WS_TABSTOP, 90,120,80,20,hWnd,0,NULL);
			 CreateWindow("checkbox","多选3",WS_TABSTOP, 170,120,80,20,hWnd,0,NULL);

			 CreateWindow("button","button",WS_TABSTOP|WS_BORDER, 10,175,75,25,hWnd,0,NULL);
			 CreateWindow("button","button",WS_TABSTOP|WS_BORDER, 100,175,75,25,hWnd,0,NULL);

		  }
	      return 0;  
   }
   return DefWindowProc(hWnd,Message,WParam,LParam);
}
 
void RegisterHelloworld(void)
{   TWNDCLASS wc;

    memset(&wc,0,sizeof(wc));
    wc.clForeground=CL_BTNTEXT;
    wc.clBackground=CL_BTNFACE;      
    wc.lpfnWndProc=HelloworldProcess;
    wc.lpszClassName="Helloworld";
    RegisterClass(&wc);
}           


void do_test(HWND hWnd,int i)
{ char buf[64];
  sprintf(buf,"you select %s",ListBox_GetItemByIndex(hWnd,i));
  MsgDlg("hello",buf," OK ");
}
extern PWND g_RootWnd;
 
HWND new_GetChildTabStop(HWND hWnd)
{   PWND seekNextWnd=NULL,pWnd=WNDPTR(hWnd)->Children;
    int NextTabOrder= ~(1<<(sizeof(int)*8-1)); /*最大的正整数*/
    while(pWnd)
	{ if(WndGetAttr(pWnd,WS_TABSTOP|WS_DISABLED|WS_HIDE)==WS_TABSTOP)
	  { if(pWnd->TabOrder < NextTabOrder)
		{ seekNextWnd=pWnd;
		  NextTabOrder=pWnd->TabOrder;
		}
	  }
	  pWnd=pWnd->Next;
	}

	if(seekNextWnd && seekNextWnd->Children)
	{  HWND childstop=new_GetChildTabStop((HWND)seekNextWnd);
	   if(childstop) return childstop;
	}
	return (HWND)seekNextWnd;
}

HWND CM_GetNewTabStop(HWND hWnd)
{ if(IsWnd(hWnd))
  { PWND seekNextWnd,pWnd;
    int OrginTabOrder,NextTabOrder;

	if(!WNDPTR(hWnd)->Parent || WNDPTR(hWnd)->Parent==g_RootWnd)
	{ return new_GetChildTabStop(hWnd);
	}
    seekNextWnd=NULL;
	OrginTabOrder=WNDPTR(hWnd)->TabOrder;
	NextTabOrder= ~(1<<(sizeof(int)*8-1)); /*最大的正整数*/
	pWnd=WNDPTR(hWnd)->Prev;
    while(pWnd)
	{ if(WndGetAttr(pWnd,WS_TABSTOP|WS_DISABLED|WS_HIDE)==WS_TABSTOP)
	  { if(pWnd->TabOrder >= OrginTabOrder )
	    { if(pWnd->TabOrder < NextTabOrder)
		  { seekNextWnd=pWnd;
		    NextTabOrder=pWnd->TabOrder;
		    if(NextTabOrder<=OrginTabOrder+1) break;
		  }
		}
	  }
	  pWnd=pWnd->Prev;
	}
	if(!pWnd)
	{ pWnd=pWnd=WNDPTR(hWnd)->Next;
	  while(pWnd)
	  { if(WndGetAttr(pWnd,WS_TABSTOP|WS_DISABLED|WS_HIDE)==WS_TABSTOP)
	    { if(pWnd->TabOrder >= OrginTabOrder )
	      { if(pWnd->TabOrder < NextTabOrder)
		    { seekNextWnd=pWnd;
		      NextTabOrder=pWnd->TabOrder;
		      if(NextTabOrder<=OrginTabOrder+1) break;
		    }
		  }
	    }
	    pWnd=pWnd->Next;
	  }
	}

	if(seekNextWnd)
	{ if(seekNextWnd->Children)
	  { HWND childstop=new_GetChildTabStop((HWND)seekNextWnd);
	    if(childstop) return childstop;
	  }
	  return (HWND)seekNextWnd;
	}
	else if(WNDPTR(hWnd)->Parent && WNDPTR(hWnd)->Parent->Parent!=g_RootWnd)
	{ return CM_GetNewTabStop((HWND)WNDPTR(hWnd)->Parent);
	}
	else
	{ return new_GetChildTabStop((HWND)WNDPTR(hWnd)->Parent);
	}
  }
  return NULL;
}

 
void DemoMain(void)
{ TMSG msg;
  HWND hWnd;
  HDC dc=GetDesktopDC();
  TBitmap  *hDlgLogo;

  RegisterHelloworld();

  //ClearRect(dc,0,0,LCD_WIDTH,LCD_HEIGHT);
 
  //FillRect(dc,0,0,LCD_WIDTH,LCD_WIDTH);
  //hDlgLogo=CreateBitmap();
  //LoadBitmapFromMem(hDlgLogo,Logo_array5);
  //LoadBitmapFromFile(hDlgLogo,"E:\\IMGP2895.bmp");
  //DrawBitmap(dc,20,20,hDlgLogo);
  //DrawVerLine(dc,1,1,100);
  //DrawHorLine(dc,0,0,100);
  //TextOut(dc,10,10,"0");
   hWnd=CreateWindow("Helloworld","程序样例2",WS_CAPTION, 0,0,LCD_WIDTH,LCD_HEIGHT,NULL,0,NULL);
  //MessageBox("您好！","caption",MB_YESNO|MB_DEFBUTTON2);
   //MessageDlg("您好！","caption","YES|你好");

   MsgDlg("hello","how are you?","  OK  |CANCEL");

   hWnd=CreateWindow("menu","程序样例1",WS_BORDER|WS_VSCROLL, 0,0,LCD_WIDTH,LCD_HEIGHT,NULL,0,(void *)do_test);
   ListBox_SetMargin(hWnd,5,0);
   Menu_AddItem(hWnd,"1ab");
   Menu_AddItem(hWnd,"2ab");
   Menu_AddItem(hWnd,"3ab");
   Menu_AddItem(hWnd,"4ab");
   Menu_AddItem(hWnd,"5ab");
   Menu_AddItem(hWnd,"6ab");
   Menu_AddItem(hWnd,"7ab");
   Menu_AddItem(hWnd,"8ab");
   Menu_AddItem(hWnd,"9ab");

   ReleaseDC(dc);

   while(GetMessage(&msg))
   { TranslateMessage(&msg);
     DispatchMessage(&msg);
   }
}
