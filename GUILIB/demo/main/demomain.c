/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:      main.c
*  PROGRAMMER:     ming.c
*  Date of Creation:   2006/08/8
*
*  DESCRIPTION:
*
*  NOTE:
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
*      To My JunHang
*  MODIFICATION HISTORY
*     LastModify  2006/10/30
******************************************************************************/
extern void DemoMain(void);
//---------------------------------------------------------------------------
int main(int argc, char* argv[])
{   
     DemoMain();
    
     return 0;
}


