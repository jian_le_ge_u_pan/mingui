 /******************************************************************************
*
* Copyright  2006 National ASIC Center, All right Reserved
*
* FILE NAME:      main.c
* PROGRAMMER:     ming.c
* Date of Creation:   2006/08/8
*
* DESCRIPTION:
*
* NOTE:
*
* FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
*      To My JunHang
* MODIFICATION HISTORY
*     LastModify  2006/10/30
******************************************************************************/
extern void DemoMain(void);
//---------------------------------------------------------------------------
int main(void)
{   
     DemoMain();
    
     return 0;
}


