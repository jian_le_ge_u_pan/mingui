
#include "mingui.h"
//#include "nucleus.h"
//#include "light.h"
//#include "I2C_CARD.h"

																									// 按钮id
#define  ID_BUTTON0    		100
#define  ID_BUTTON_ok		120
#define  ID_BUTTON_cancel	121
#define  ID_BUTTON_MAIN  	200

#define  IDLE_STATUS		0																		// 待机状态，动态显示广告信息
#define  SETUP_STATUS		1																		// 设置状态，进行各项参数的设置
#define	 CARD_STATUS		2																		// 售卡状态，进行电子售卡状态
#define  BACCY_STATUS		3																		// 售烟状态，进行烟草业务
#define	 TOOLS_STATUS		4																		// 工具状态，如计算器、万年历等
#define  OTHER_STATUS		5																		// 备用
#define  MENU_STATUS		6																		// 主菜单状态

char 	paratable[16][17];																// 参数表
//extern int Read_Para(void); 
PWND testWnd;
																									
 
//extern NU_MEMORY_POOL  System_Memory;
//extern INT		StartNet;	
//extern INT		IsConnect;																		// 是否开始接收网络传过来的报文，1为接收报文
//extern INT    		socketd;
int ImeStatus;
HWND hTimeLabel;																					// 时间标签
HANDLE hShowTimer=NULL,hSecondTimer=NULL,hTimeDelay=NULL;															// 计时器
HWND winmain;																						// 主窗口
HWND idle_static[13];																				// 待机状态下的标签
HWND btn[5],btnok,btncancel,btnmain;																// 按钮
HWND hDlgSetup,editsetup;																			// 对话框
HWND set_listbox;																					// 参数设置时，显示参数列表
HWND dynmenu,MyBox;																						// 动态菜单
HWND table_list[40],table_edit[8],table_header[3],table_footer[2],table_title[4];					// 静态或动态表格
HWND MBox,MBoxStatic,TestStatic,TestMemo,MemoOut,StaticPhoto;
HWND hActiveWinow;
TBitmap *buttonlogo[8],*ad_pic[7];																	// 位图

extern BOOL  keypad_shifton;
extern HWND hMessageDlg;
int	 delaytimes=0;																					// 超时
int  showcounter=0;																					// 动态显示图片信息计数
//CHAR ImeStatus=0;																					// 输入法状态，数字、拼音
char *txttxt={"烟信通智烟信通智烟信能交易终"};														// 广告信息，动态更新
char main_status;																					// 记录各个状态
char *headtxt={"2008/08/08 星期日 农历 八月初八"};													// IDLE状态时，头标题

char * default_data;
void BFunction(int index);																			// 5个主要功能按钮的处理函数
void StatusChange(int statusindex);	
//void GUI_CLOCK( UNSIGNED id);
char *tabledata={"1111,11,11,11111,2222,22,22,2222,3333,33,33,3333,4444,44,44,4444,5555,55,55,5555,6666,66,66,6666,7777,77,77,7777,8888,88,88,8888"};
																// 状态转换函数
HRESULT MemoHook(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam,BOOL* bIntercept);
/****************************************************************************************
*	函数名称：	IsCurWnd																*
*	功能说明：	判断给定的句柄是否是当前的活动窗体。									*
*	注意事项：	在Dialog.c文件中，static HWND hMessageDlg=NULL;去掉static				*
*****************************************************************************************/
int IsCurWnd(HWND DoCheckWnd)
{
	HWND	hActiveWinow;
	hActiveWinow=WNDPTR(GetFocus())->Family->TopWnd;
	if(hActiveWinow==DoCheckWnd)
		return 1;																					// 当前窗口就是要检查的窗口
	else
		return 0;																					// 当前窗口不是要检查的窗口
}
/****************************************************************************************
*	函数名称：	GlobalKeyBoardHook														*
*	功能说明：	安装全局的键盘钩子														*
*					发往任何窗体按键消息将首先被钩子处理函数截获.一个窗体只允许安装一个	*
*				消息钩子,如果多次安装则后一个覆盖前一个.								*
*****************************************************************************************/
HRESULT GlobalKeyBoardHook(HWND hWnd,UINT Message,WPARAM wParam,LPARAM lParam,BOOL* bIntercept)
{ if(Message==WM_KEYUP)
  { 
	if(wParam==VK_F3)																				// 判断是否是左面的选择按键
    { 

       	if(IsWnd(hDlgSetup)) 
       	{	
      		printf("DialogOk()");																				// 确认键处理函数
       	}else if(IsWnd(winmain))
       	{ 
   			StatusChange(main_status);																// 空闲状态和主菜单状态切换
       	}
    }
    else if(wParam==VK_F2)
    { 
		if(IsWnd(hDlgSetup))
		{
     		printf("DialogCancel()");																			// 取消键处理函数
     	}
    }
  }
  return -1;
}
/****************************************************************************************
*	函数名称：	MainProcess																*
*	功能说明：	主窗口处理函数 															*
*																						*
*****************************************************************************************/
HRESULT CALLBACK MainProcess(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam)
{  int i,j=0;

	switch(Message)
   {  
	
 
	case WM_CREATE:
          {  
             																						// 主菜单按钮，进入主菜单或者返回待机界面
             btnmain=CreateWindow("button",NULL,WS_BORDER|WS_TABSTOP|BS_MENU,40,218,80,20,hWnd,ID_BUTTON_MAIN,NULL);
             SetWindowText(btnmain,"主菜单");             
             																						// 划上下两个实体蓝色静态框
             idle_static[11]=CreateWindow("static",0,WS_ALWAYSONBOTTOM|WS_BORDER,0,0,320,23,hWnd,0,NULL);
              SetWindowBackColor(idle_static[11],CL_BLUE);
             idle_static[12]=CreateWindow("static",0,WS_ALWAYSONBOTTOM|WS_BORDER,0,217,320,23,hWnd,0,NULL);
              SetWindowBackColor(idle_static[12],CL_BLUE);

																									// 创建时间标签
             hTimeLabel=CreateWindow("static",NULL,WS_ALWAYSONTOP,250,1,70,20,hWnd,0,NULL);	
			 SetWindowBackColor(hTimeLabel,CL_BLUE);												// 设置时间标签的背景色	
			 SetWindowTextColor(hTimeLabel,CL_WHITE);												// 设置文本颜色
			 													
              for(i=0;i<5;i++)																		// 创建5个功能按钮
              { 
              	buttonlogo[i]=CreateBitmap();
               	buttonlogo[i]->Transparent=true;
                btn[i]=CreateWindow("button",NULL,WS_BORDER|WS_TABSTOP|BS_MENU,2+(i%4)*79,55+(i/4)*61,78,60,hWnd,ID_BUTTON0+i,NULL);
                ShowWindow(btn[i],false);
              }
              /*
              LoadBitmapFromMem(buttonlogo[0],Logo_array1,1745);									// 读取5个图片
              LoadBitmapFromMem(buttonlogo[1],Logo_array2,1814);
              LoadBitmapFromMem(buttonlogo[2],Logo_array3,1732);
              LoadBitmapFromMem(buttonlogo[3],Logo_array4,1705);
              LoadBitmapFromMem(buttonlogo[4],Logo_array5,1712);

              SetWindowLogo(btn[0],buttonlogo[0]);													// 设置5个按钮的图片
              SetWindowLogo(btn[1],buttonlogo[1]);
              SetWindowLogo(btn[2],buttonlogo[2]);
              SetWindowLogo(btn[3],buttonlogo[3]);
              SetWindowLogo(btn[4],buttonlogo[4]);
              */
			  for(i=0;i<7;i++)
			  {
			  	ad_pic[i]=CreateBitmap();
               	ad_pic[i]->Transparent=true;
			  }
		/*  
              LoadBitmapFromMem(ad_pic[0],ynj1_array,14228);
              LoadBitmapFromMem(ad_pic[1],ynj2_array,14443);
              LoadBitmapFromMem(ad_pic[2],ynj3_array,15482);
              LoadBitmapFromMem(ad_pic[3],ynj4_array,15013);
              LoadBitmapFromMem(ad_pic[4],ynj5_array,15763);
			  LoadBitmapFromMem(ad_pic[5],ynj6_array,13241);
			  */
			  //LoadBitmapFromMem(ad_pic[6],ynj7_array,14451);
			 
			  idle_static[0]=CreateWindow("static",0,20,0,25,320,20,hWnd,0,NULL);
			  SetWindowText(idle_static[0],headtxt);     
			            
              idle_static[1]=CreateWindow("static",0,0,10,60,120,100,hWnd,0,NULL);
               
			  idle_static[2]=CreateWindow("static","广告信息",WS_BORDER,150,60,165,20,hWnd,0,NULL);
			  SetWindowBackColor(idle_static[2],CL_RED);
			  SetWindowTextColor(idle_static[2],CL_RED);
			  
			  idle_static[10]=idle_static[3]=CreateWindow("static",NULL,WS_BORDER,150,79,165,120,hWnd,0,NULL);
			  testWnd=(PWND)idle_static[10];

			  idle_static[3]=CreateWindow("Memo","王刚、王乐泉、王兆国、王岐山、回良玉、刘淇、刘云山、刘延东、李源潮、汪洋、张高丽、张德江",WS_BORDER,150,79,164,119,hWnd,0,NULL);

			  SetWindowBackColor(idle_static[3],CL_WHITE);
      		  
      		  
      		  idle_static[4]=CreateWindow("static","政策",0,0,170,40,20,hWnd,0,NULL);
              SetWindowTextColor(idle_static[4],CL_RED);
              idle_static[5]=CreateWindow("static","新通知",0,45,170,50,20,hWnd,0,NULL);
              SetWindowTextColor(idle_static[5],CL_RED);
              idle_static[6]=CreateWindow("static","新短信",0,100,170,50,20,hWnd,0,NULL);
              SetWindowTextColor(idle_static[6],CL_RED);
              idle_static[7]=CreateWindow("static","10",0,0,190,40,20,hWnd,0,NULL);
              idle_static[8]=CreateWindow("static","20",0,45,190,50,20,hWnd,0,NULL);
              idle_static[9]=CreateWindow("static","5",0,100,190,50,20,hWnd,0,NULL);
       
			  EnableTimer(hShowTimer,true);
          }
          break;
          
    case WM_TIMER:
	  	{ 	
	  		if((HANDLE)WParam==hSecondTimer)
           { 
				int iHour, iMinute,iSecond;
				char timershowbuf[12];
				GetSysTime(&iHour, &iMinute,&iSecond);
				sprintf(timershowbuf,"%02d:%02d:%02d",iHour,iMinute,iSecond);
				SetWindowText(hTimeLabel,timershowbuf);	
           }
           else if((HANDLE)WParam==hShowTimer)
           {  
			   	//SetWindowLogo(idle_static[1],ad_pic[showcounter++]);
              	if(showcounter>=6)showcounter=0;
				//txttxt="饭后一支烟赛过活神仙";
				//SetWindowText(idle_static[3],txttxt);
           }
	   }
          break;
 
    case WM_COMMAND:
         { if(HIWORD(WParam)==BN_CLICKED)
		   { if(LOWORD(WParam)==ID_BUTTON_MAIN)
		    {
		    	StatusChange(main_status);	
		    }
		   else
		   { 
		   		int buttonIndex=LOWORD(WParam)-ID_BUTTON0;
			 	BFunction(buttonIndex);
		   }
		 }
         }
         break;      

   
    case WM_DESTROY:

         for(i=0;i<5;i++) DestroyBitmap(buttonlogo[i]);
         break;
   }
 
    return DefWindowProc(hWnd,Message,WParam,LParam);
}


HRESULT MemoHook(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam,BOOL* bIntercept)
{  switch(Message)
	{
	
		case WM_KEYDOWN:
			{	
				if(main_status==BACCY_STATUS)
				{
					if(WParam==VK_F17)
					{	
					//	ImeStatus++;
						if(ImeStatus==3) ImeStatus=0;
						
						if(ImeStatus==0)
						{
							keypad_shifton=true;
							IME_Open(MemoOut,hDlgSetup,5,175,310,40);
							//SetWindowText(TestStatic,"中文输入");
							SetWindowText(hDlgSetup,"网络测试                『中文输入』");
						}
						else if(ImeStatus==1)
						{
							keypad_shifton=true;
							IME_Close();
							//SetWindowText(TestStatic,"英文输入");
							SetWindowText(hDlgSetup,"网络测试                『英文输入』");
						}
						else if(ImeStatus==2)
						{
							keypad_shifton=false;
							//SetWindowText(TestStatic,"数字输入");
							SetWindowText(hDlgSetup,"网络测试                『数字输入』");
											
						}
						SetFocus(MemoOut);
					}
				}
			}
	
	
		default:
			break;
	}
return 0;
}
/****************************************************************************************
*	函数名称：	DialogHook																*
*	功能说明：	对话框钩子行数 															*
*				五个功能按钮的处理函数，每当按下其中一个按钮，则创建一个与主窗口不相关的*
*				窗体，然后再次窗体上创建相关控件，如listbox、edit、menu、static等等。	*
*																						*
*****************************************************************************************/
HRESULT MyDialogHook(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam,BOOL *bIntercept)
{//	int i,j;
	
	switch(Message)
	{
		case WM_COMMAND:
			{
				if(LOWORD(WParam)==ID_BUTTON_ok)
				{	
						printf("DialogOk()");																		// 确认键处理函数
				}else if(LOWORD(WParam)==ID_BUTTON_cancel)
				{
					printf("DialogCancel()");																	// 取消键处理函数
				}
			}
			break;
	
	/*	case WM_TIMER:
	  		{ 	
	  			if((HANDLE)WParam==hTimeDelay)
	  			{	
	  				delaytimes++;
	  				if(delaytimes==1)
	  				{
	  					EnableTimer(hTimeDelay,false); 
	  					DestroyWindow(MBox);	
	  				}
	  			}
           	}
           	break; 
	*/	
		default:
			break;
	}
	return 0;
}

/****************************************************************************************
*	函数名称：	DialogOk																*
*	功能说明：	确认键处理函数															*
*																						*
*****************************************************************************************/
int DialogOk(void)
{
	int i;
	
	switch(main_status)
	{
		case CARD_STATUS:
	 		{
	 						
	 		}
	 			break;
	 	case BACCY_STATUS:
	 		{	
	 		
				default_data=GetWindowText(MemoOut);	
		
				
	 		}
	 			break;
	 	case TOOLS_STATUS:
	 		{
	 			
	 		}
	 			break;
	 	case OTHER_STATUS:
	 		{
	 									
	 		}
	 			break; 			
		case SETUP_STATUS:
			{
				if(IsWnd(hDlgSetup))
				{	
					if(IsCurWnd(hMessageDlg))
					{
						SendMessage(hMessageDlg,WM_COMMAND,0,0);
					}else 
					{
						MessageBox("参数保存成功！","设置",MB_OK);
						i=ListBox_GetSelectedIndex(set_listbox);
						memset(paratable[i],0,16);	
						memcpy(paratable[i],GetWindowText(editsetup),16);				//得到交易流水号
					//	i2c_write_C02(0+i*16,paratable[i],16);	
						i=0;
						
						
					}
				}
			}
				break;
		default:
				break;		
	}	
	return 0;
}
/****************************************************************************************
*	函数名称：	DialogCancel															*
*	功能说明：	取消键键处理函数														*
*																						*
*****************************************************************************************/
int DialogCancel(void)
{
	switch(main_status)
	{
		case CARD_STATUS:
	 		{
	 						
	 		}
	 			break;
	 	case BACCY_STATUS:
	 		{
	 		//	NU_Deallocate_Memory(default_data);	
	 		//	StartNet=0;
	 			
	 		}
	 			break;
	 	case TOOLS_STATUS:
	 		{
	 			
	 		}
	 			break;
	 	case OTHER_STATUS:
	 		{
	 									
	 		}
	 			break; 			
		case SETUP_STATUS:
			{
				
			}
				break;
		default:
				break;		
	}	
	if(IsWnd(hDlgSetup))
	{
    	DestroyWindow(hDlgSetup);					
    	main_status=MENU_STATUS;
	}
	return 0;
}

/****************************************************************************************
*	函数名称：	ListboxHook																*
*	功能说明：	列表框钩子函数 															*
*					当焦点在列表框上，截获用户按键（上翻和下翻键），然后获取当前选中的  *
*				列表框的索引值，根据该索引动态的在edit控件上显示当前的参数值。			*
*																						*
*****************************************************************************************/
HRESULT ListboxHook(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam,BOOL* bIntercept)
{
	int i;
	switch(Message)
	{
		case WM_KEYUP:
			{
				if((WParam==VK_UP)||(WParam==VK_DOWN))
				{
					i=ListBox_GetSelectedIndex(hWnd);
					SetWindowText(editsetup,paratable[i]);
				}
			} 
			break;
		case WM_KEYDOWN:
			{
				if(WParam==VK_RETURN)
				SetFocus(editsetup);
			
			}
			break;
		default:
			break;
	}
	return 0;
}

/****************************************************************************************
*	函数名称：	BFunction																*
*	功能说明：	按钮的处理函数 															*
*				五个功能按钮的处理函数，每当按下其中一个按钮，则创建一个与主窗口不相关的*
*				窗体，然后再次窗体上创建相关控件，如listbox、edit、menu、static等等。	*
*																						*
*****************************************************************************************/
HRESULT MyDialogHook(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam,BOOL *bIntercept);
void BFunction(int index)
{
	int i;
//	char strDispBuf[32];
	int	status=0;
//	void	*pointer;
	extern BOOL  keypad_shifton;
	keypad_shifton=false; 

	hDlgSetup=CreateWindow("window","",WS_BORDER|WS_CAPTION|WS_ALWAYSONTOP,00,00,320,240,NULL,0,NULL);
	InstallHook(hDlgSetup,0,MyDialogHook);
	
	btnok=CreateWindow("button",NULL,WS_BORDER|WS_TABSTOP|BS_MENU,40,218,80,20,hDlgSetup,ID_BUTTON_ok,NULL);
	btncancel=CreateWindow("button",NULL,WS_BORDER|WS_TABSTOP|BS_MENU,140,218,80,20,hDlgSetup,ID_BUTTON_cancel,NULL);

	SetWindowText(btnok,"确定");
	SetWindowText(btncancel,"取消");
    
	switch(index)
	{
		case	0:
			{	
				//UINT8 strDispBuf[32]={0xbf,0xa8,0x31,'1',0};

				main_status=BACCY_STATUS;															// 进入售烟状态
			
				SetWindowText(btnok,"发送");
				
				default_data=(char*)GetMem(1024);
				memset(default_data,0,1024);
				
			
				SetWindowText(hDlgSetup,"网络测试");
				
				CreateWindow("staticleft","状态：",20,5,25,50,20,hDlgSetup,0,NULL);
				TestStatic=CreateWindow("staticleft",NULL,20,55,25,260,20,hDlgSetup,0,NULL);
				
				MemoOut=CreateWindow("Memo","",WS_BORDER|WS_TABSTOP,5,50,150,120,hDlgSetup,0,NULL);
				//SetWindowBackColor(MemoOut,CL_WHITE);
				InstallHook(MemoOut,0,MemoHook);
				
				TestMemo=CreateWindow("Memo","",WS_BORDER|WS_TRANSPARENT,165,50,150,120,hDlgSetup,0,NULL);

				StaticPhoto=CreateWindow("static","",0,165,50,150,120,hDlgSetup,0,NULL);

				//SetWindowLogo(StaticPhoto,ad_pic[2]);
					
				ImeStatus=2;
				keypad_shifton=false;
				SetWindowText(hDlgSetup,"网络测试                『数字输入』");

				 
				SetFocus(MemoOut);
				
			//	StartNet=1;																			// 联网络
	 		//	IsConnect=0;
			
				
			}
				break;
				
		case	1:
			{	
				SetWindowText(hDlgSetup,"动态菜单");
				main_status=CARD_STATUS;															// 进入电子售卡状态
				
				dynmenu=CreateWindow("menu","主菜单",WS_BORDER|WS_VSCROLL|WS_TABSTOP,5,25,310,175,hDlgSetup,0,NULL);
				Menu_AddItem(dynmenu,"01.拼音输入法测试");
				Menu_AddItem(dynmenu,"02.拼音输入法测试");
				Menu_AddItem(dynmenu,"03.拼音输入法测试");
				Menu_AddItem(dynmenu,"04.拼音输入法测试");
				Menu_AddItem(dynmenu,"05.拼音输入法测试");
				Menu_AddItem(dynmenu,"06.拼音输入法测试");
				Menu_AddItem(dynmenu,"07.拼音输入法测试");
				Menu_AddItem(dynmenu,"08.拼音输入法测试");
				Menu_AddItem(dynmenu,"09.拼音输入法测试");
				Menu_AddItem(dynmenu,"10.拼音输入法测试");
				Menu_AddItem(dynmenu,"11.拼音输入法测试");
				Menu_AddItem(dynmenu,"12.拼音输入法测试");
				Menu_AddItem(dynmenu,"13.拼音输入法测试");
				Menu_AddItem(dynmenu,"14.拼音输入法测试");
				Menu_AddItem(dynmenu,"15.拼音输入法测试");
				Menu_AddItem(dynmenu,"16.拼音输入法测试");
				Menu_AddItem(dynmenu,"17.拼音输入法测试");
				Menu_AddItem(dynmenu,"18.拼音输入法测试");
				Menu_AddItem(dynmenu,"19.拼音输入法测试");
				Menu_AddItem(dynmenu,"20.拼音输入法测试");
				
		
				SetFocus(dynmenu);
			}
				break;		
		case	2:
			{
				SetWindowText(hDlgSetup,"静态表格");
				main_status=TOOLS_STATUS;															// 进入工具状态
			
				table_header[0]=CreateWindow("staticleft","",20,1,23,140,20,hDlgSetup,0,NULL);
				table_header[1]=CreateWindow("static","",20,140,23,70,20,hDlgSetup,0,NULL);
				table_header[2]=CreateWindow("static","",20,209,23,110,20,hDlgSetup,0,NULL);

				table_title[0]=CreateWindow("static","",WS_BORDER,1,43,140,20,hDlgSetup,0,NULL);
				table_title[1]=CreateWindow("static","",WS_BORDER,140,43,70,20,hDlgSetup,0,NULL);
				table_title[2]=CreateWindow("static","",WS_BORDER,209,43,40,20,hDlgSetup,0,NULL);
				table_title[3]=CreateWindow("static","",WS_BORDER,248,43,70,20,hDlgSetup,0,NULL);

				for( i=0; i<7 ; i++)
				{
					table_list[i]=CreateWindow("staticleft","",WS_BORDER,1,62+i*20-i,140,20,hDlgSetup,0,NULL);
					table_list[i+7]=CreateWindow("staticleft","",WS_BORDER,140,62+i*20-i,70,20,hDlgSetup,0,NULL);
					table_list[i+14]=CreateWindow("staticleft","",WS_BORDER,209,62+i*20-i,40,20,hDlgSetup,0,NULL);
					table_list[i+21]=CreateWindow("staticleft","",WS_BORDER,248,62+i*20-i,70,20,hDlgSetup,0,NULL);
				}

				table_footer[0]=CreateWindow("staticleft","",20,1,198,318,20,hDlgSetup,0,NULL);

				SetWindowText(table_header[0],"定单号:57708795");
				SetWindowText(table_header[1],"单位(元)");
				SetWindowText(table_header[2],"共3页 第1页");

				SetWindowText(table_title[0],"品名");
				SetWindowText(table_title[1]," 批发价");
				SetWindowText(table_title[2],"定量");
				SetWindowText(table_title[3],"总额");

				SetWindowText(table_footer[0],"总金额：100000，总量：160条，状态：有效");
				SetFocus(btncancel);
			}
				break;
			
		case	3:
			{
				main_status=TOOLS_STATUS;															// 进入工具状态
				SetWindowText(hDlgSetup,"可编辑表格");

				table_header[0]=CreateWindow("staticleft","",20,1,23,140,20,hDlgSetup,0,NULL);
				table_header[1]=CreateWindow("static","",20,140,23,70,20,hDlgSetup,0,NULL);
				table_header[2]=CreateWindow("static","",20,209,23,110,20,hDlgSetup,0,NULL);

				table_title[0]=CreateWindow("static","",WS_BORDER,1,43,140,20,hDlgSetup,0,NULL);
				table_title[1]=CreateWindow("static","",WS_BORDER,140,43,70,20,hDlgSetup,0,NULL);
				table_title[2]=CreateWindow("static","",WS_BORDER,209,43,40,20,hDlgSetup,0,NULL);
				table_title[3]=CreateWindow("static","",WS_BORDER,248,43,70,20,hDlgSetup,0,NULL);

				for( i=0; i<7 ; i++)
				{
					table_list[i]=CreateWindow("staticleft","",WS_BORDER,1,62+i*20-i,140,20,hDlgSetup,0,NULL);
					table_list[i+7]=CreateWindow("staticleft","",WS_BORDER,140,62+i*20-i,70,20,hDlgSetup,0,NULL);
					table_list[i+14]=CreateWindow("staticleft","",WS_BORDER,209,62+i*20-i,40,20,hDlgSetup,0,NULL);
					table_list[i+21]=CreateWindow("edit","",WS_BORDER|WS_TABSTOP,248,62+i*20-i,70,20,hDlgSetup,0,NULL);
				}

				table_footer[0]=CreateWindow("staticleft","",20,1,198,318,20,hDlgSetup,0,NULL);

				SetWindowText(table_header[0],"定单号:57708795");
				SetWindowText(table_header[1],"单位(元)");
				SetWindowText(table_header[2],"共3页 第1页");

				SetWindowText(table_title[0],"品名");
				SetWindowText(table_title[1]," 批发价");
				SetWindowText(table_title[2],"定量");
				SetWindowText(table_title[3],"总额");

				SetWindowText(table_footer[0],"总金额：100000，总量：160条，状态：有效");
				SetFocus(table_list[21]);

			}
				break;
		case	4:	//终端设置
			{
			//	Read_Para();																		// 读参数
				SetWindowText(hDlgSetup,"终端设置");
				main_status=SETUP_STATUS;															// 进入终端参数设置状态

				CreateWindow("static","参数列表：",20,5,30,80,20,hDlgSetup,0,NULL);
				//TestStatic=CreateWindow("static","正在保存保存参数......",20,190,80,125,25,hDlgSetup,0,NULL);
				//ShowWindow(TestStatic,false);
				
				set_listbox=CreateWindow("ListBox",NULL,WS_BORDER|WS_TABSTOP|WS_VSCROLL,5,50,180,150,hDlgSetup,0,NULL);
				
				ListBox_AddItem(set_listbox,"00.终端ID");
				ListBox_AddItem(set_listbox,"01.初始密钥");
				ListBox_AddItem(set_listbox,"02.售卡版本信息");
				ListBox_AddItem(set_listbox,"03.液晶对比度");
				ListBox_AddItem(set_listbox,"04.液晶初始界面");
				ListBox_AddItem(set_listbox,"05.安全密码");
				ListBox_AddItem(set_listbox,"06.安全密码超时启动");
				ListBox_AddItem(set_listbox,"07.安全密码选择");
				ListBox_AddItem(set_listbox,"08.按键音");
				ListBox_AddItem(set_listbox,"09.用户按键超时");
				ListBox_AddItem(set_listbox,"10.快捷键");	
				ListBox_AddItem(set_listbox,"11.fsk接收超时参数");	
				ListBox_AddItem(set_listbox,"12.延时参数");	
				ListBox_AddItem(set_listbox,"13.fsk接收超时参数");	
				ListBox_AddItem(set_listbox,"14.FSK信号相关参数");	
				ListBox_AddItem(set_listbox,"15.DTMF信号相关参数");

				InstallHook(set_listbox,0,ListboxHook);
				
				CreateWindow("static","参数值：",20,190,30,64,20,hDlgSetup,0,NULL);
				editsetup=CreateWindow("Edit","",WS_BORDER|WS_TABSTOP,190,50,125,25,hDlgSetup,0,NULL);
				SetWindowText(editsetup,paratable[0]);			

				SetFocus(set_listbox);
			}
				break;
	}
}

/****************************************************************************************
*	函数名称：	DemoMain																*
*	功能说明：	GUI主函数																*
*					创建主窗口和定时器，安装全局键盘钩子函数，循环处理消息				*
*****************************************************************************************/
void DemoMain(void)
{ 
	HDC desktopDC=GetDesktopDC();
	TMSG msg;
	TWNDCLASS wc;
	int	status=0;			
	
	memset(&wc,0,sizeof(wc));
	wc.clForeground=CL_BTNTEXT;
	wc.clBackground=CL_BTNFACE;
	wc.lpfnWndProc=MainProcess;
	wc.lpszClassName="mainwnd";
	RegisterClass(&wc);																				// 注册对话框控件


	SetColor(desktopDC,CL_RED);
    TextOut(desktopDC,50,100,"系统初始化....\n正在解码,请稍候...");


	//InstallGlobalKeyboardHook(GlobalKeyBoardHook);													// 设置钩子函数，发往任何窗体按键消息将首先被钩子处理函数截获.

	winmain=CreateWindow("mainwnd","winmain",0,0,0,LCD_WIDTH,LCD_HEIGHT,NULL,0,NULL);				// 创建主窗口

	hSecondTimer=CreateTimer(winmain,1000,1000,NULL,true);											// 创建定时器
	hShowTimer=CreateTimer(winmain,1000,3000,NULL,true);											// 创建定时器，可不可以公用上面的定时器？
	
	SetFocus(btnmain);																				// 将焦点设置在主按钮上面
  	main_status=IDLE_STATUS;
  	
	while(GetMessage(&msg))
	{ 
		TranslateMessage(&msg);
	    if(msg.Message==WM_KEYUP)GlobalKeyBoardHook(msg.Handle,msg.Message,msg.WParam,msg.LParam,NULL);
		DispatchMessage(&msg);
	}

	while(1);
	printf("应用程序执行完毕！\n");
}           


/****************************************************************************************
*	函数名称：	StatusChange															*
*	功能说明：	几种状态转换（在主菜单和空闲状态进行切换）								*
*****************************************************************************************/

void StatusChange(int statusindex)																	// 状态转换函数
{
	int i;
	switch(statusindex)
	{
		case IDLE_STATUS:
				{		
					EnableTimer(hShowTimer,false);													// 关闭

					for(i=0;i<5;i++)																// 显示5个按钮，并将焦点置于第一个按钮	
					{ 
						ShowWindow(btn[i],true);
					}
	 							
					for(i=1;i<11;i++)																// 显示5个按钮，并将焦点置于第一个按钮	
					{ 
						ShowWindow(idle_static[i],false);
					} 			
	 													
	 				SetFocus(btn[0]);
	 				SetFocus(btnmain);
	 							
	 				EnableTimer(hShowTimer,false);
	 				SetWindowText(btnmain,"待机");  
	 				SetWindowText(idle_static[0],"主菜单");
					main_status=MENU_STATUS;														// 进入主菜单状态			
	 			}
	 			break;
	 					
	 	case MENU_STATUS:	
	 			{ 	
	 				for(i=0;i<5;i++)																// 显示5个按钮，并将焦点置于第一个按钮	
	 				{ 
	 					ShowWindow(btn[i],false);
	 				} 					
	 				SetFocus(btnmain);
	 			
	 				for(i=1;i<11;i++)																// 显示10个静态框	
	 				{ 
	 					ShowWindow(idle_static[i],true);
	 				} 

	 				SetWindowText(btnmain,"主菜单");  
	 				SetWindowText(idle_static[0],headtxt);
					main_status=IDLE_STATUS;														// 返回待机状态			
										
	 				EnableTimer(hShowTimer,true); 
	 			}
	 			break;

		default:
				break;	 				 						
	 	}
}

