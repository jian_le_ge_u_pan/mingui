#include "mingui.h"
/////////////////////////////////////////////////////////
#define  ID_BUTTON1    100
#define  ID_BUTTON2    101
#define  ID_BUTTON3    102
#define  ID_BUTTON4    103
#define  ID_BUTTON5    104
/////////////////////////////////////////////////////////
static HWND demo1,demo2,demo3,about_btn,hAngel,winmain,start_btn,demo_btn1,demo_btn2,demo_btn3;
static HANDLE hSecondTimer=NULL;
static int  showcounter=0;
static TBitmap *bm[5],*hDlgLogo;
static BOOL ShowStarted=false;
static AngleRunning=true;
extern TBitmap *desktop_bitmap;
extern void RegisterDemo1(void);
extern void RegisterDemo2(void);
extern void RegisterDemo3(void);
extern void RegisterAngel(void);
/////////////////////////////////////////////////////////
#define Auto_Demo          false
#define Auto_DELAY         10
////////////////////////////////////////////////////////
int step_1=10;
TRECT limit1={85,LCD_HEIGHT-65,0,0};
void GenNextPos(TPOINT *pos,TPOINT *dest_pos,int min_step)
{ int sign_x,sign_y,offset_x,offset_y;
  offset_x=(dest_pos->x-pos->x);
  offset_y=(dest_pos->y-pos->y);
  if(offset_x>=0)sign_x=1;else{sign_x=-1;offset_x=-offset_x;}
  if(offset_y>=0)sign_y=1;else{sign_y=-1;offset_y=-offset_y;}
  if(offset_x>=offset_y)
  { if(offset_x < min_step) pos->x = dest_pos->x;
    else pos->x += sign_x*min_step;
    if(offset_y <= min_step) pos->y = dest_pos->y;
    else pos->y += sign_y * offset_y * min_step / offset_x;
  }
  else
  { if(offset_y < min_step) pos->y = dest_pos->y;
    else pos->y += min_step * sign_y;
    if(offset_x <= min_step) pos->x = dest_pos->x;
    else pos->x += sign_x * offset_x * min_step / offset_y;
  }
}
/////////////////////////////////////////////////////////
void StepWindow(HWND hWnd,TRECT *pos,int step)
{ TPOINT *curpos=(TPOINT *)pos;
  TPOINT *despos=(TPOINT *)(&pos->right);
  if(curpos->x==despos->x && curpos->y==despos->y)
  { extern int GenRandom(int);
	despos->x=GenRandom(crWidth(WNDPTR(hWnd)->Parent)-absWidth(hWnd));
    despos->y=GenRandom(crHeight(WNDPTR(hWnd)->Parent)-absHeight(hWnd));
  }
  GenNextPos(curpos,despos,step);
  SetWindowPos(hWnd,curpos->x,curpos->y);
}
/////////////////////////////////////////////////////////
HRESULT CALLBACK MainProcess(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam)
{  switch(Message)
   { 
     case WM_CREATE:
          { 
		      extern const unsigned char DialogLogoImg[];
              RegisterDemo1();
              RegisterDemo2();
              RegisterDemo3();
              RegisterAngel();

			  demo1=CreateWindow("Demo1","demo1",WS_CAPTION|WS_HIDE|WS_SYSMENU,15,10,LCD_WIDTH-40,LCD_HEIGHT-65,NULL,0,NULL);
              demo2=CreateWindow("Demo2","demo2",WS_CAPTION|WS_HIDE|WS_SYSMENU,25,20,LCD_WIDTH-40,LCD_HEIGHT-65,NULL,0,NULL);
              demo2=CreateWindow("Demo2","demo2",WS_CAPTION|WS_HIDE|WS_SYSMENU,25,20,LCD_WIDTH-40,LCD_HEIGHT-65,NULL,0,NULL);
              demo3=CreateWindow("Demo3","demo3",WS_HIDE,0,0,LCD_WIDTH,LCD_HEIGHT,NULL,0,NULL);


              start_btn=CreateWindow("button","停止",WS_BORDER|WS_TABSTOP|WS_TRANSPARENT, 10,LCD_HEIGHT-65,70,26,hWnd,ID_BUTTON1,NULL);
              about_btn=CreateWindow("button","关于",WS_BORDER|WS_TABSTOP|WS_TRANSPARENT, 85,LCD_HEIGHT-65,50,26,hWnd,ID_BUTTON2,NULL);
              demo_btn1=CreateWindow("button","演示1",WS_BORDER|WS_TABSTOP, 145,LCD_HEIGHT-65,52,26,hWnd,ID_BUTTON3,NULL);
			  demo_btn2=CreateWindow("button","演示2",WS_BORDER|WS_TABSTOP, 200,LCD_HEIGHT-65,52,26,hWnd,ID_BUTTON4,NULL);
			  demo_btn3=CreateWindow("button","演示3",WS_BORDER|WS_TABSTOP, 255,LCD_HEIGHT-65,52,26,hWnd,ID_BUTTON5,NULL);
			  hAngel=CreateWindow("angel","angel_1",WS_TRANSPARENT,0,0,88,68,hWnd,0,NULL); 
			 // hAngel=CreateWindow("angel","angel_2",WS_TRANSPARENT,20,210,88,68,hWnd,0,NULL);  
			 // hAngel=CreateWindow("angel","angel_3",WS_TRANSPARENT,250,0,88,68,hWnd,0,NULL);  
               
			  SetWindowTextColor(start_btn,CL_GREEN);
			  SetWindowTextColor(about_btn,CL_BLUE);
			  hSecondTimer=CreateTimer(hWnd,1000,GUI_CLK_CYCLE,NULL,false);
	          EnableTimer(hSecondTimer,true);

             
              hDlgLogo=CreateBitmap();
              LoadBitmapFromMem(hDlgLogo,DialogLogoImg);
              SetWindowLogo(hWnd,hDlgLogo);

			  SetWindowFont(hWnd,"hzk12");
          }
          break;
  
   /*
   case WM_ERASEBKGND: 
		 if(WParam)
		 {   HDC dc=(HDC)WParam;
		     DrawBitmap(dc,0,0,desktop_bitmap);
			 ReleaseDC(dc);
		 
		 }
		 return 0;		   
		 */

   case WM_DESTROY:
         {   DestroyBitmap(hDlgLogo);
             break;
         }
 

   case WM_TIMER:
         if((HANDLE)WParam==hSecondTimer)
		 {  StepWindow(about_btn,&limit1,step_1);
		 }
		 break;
   case WM_COMMAND:
	       if(HIWORD(WParam)==BN_CLICKED)
             switch(LOWORD(WParam))
             { case ID_BUTTON1:
			          if(AngleRunning)
					  { EnableWindow(hAngel,false);
					    SetWindowText(start_btn,"开始");
					  }
					  else
					  { EnableWindow(hAngel,true);
					    SetWindowText(start_btn,"停止");
					  }
					  AngleRunning=!AngleRunning;
                    break;
              case ID_BUTTON2:
			           
                       MessageBox("您好！\nGUI测试程序演示中...","caption",MB_YESNOCANCEL|MB_ICONQUESTION|MB_DEFBUTTON2);
                   break;
              case ID_BUTTON3:
	               ShowWindow(demo1,true);
				   SetFocus(demo1);
                   break;
              case ID_BUTTON4:
	               ShowWindow(demo2,true);
				   SetFocus(demo2);
                   break;
              case ID_BUTTON5:
	               ShowWindow(demo3,true);
				   SetFocus(demo3);
                   break;

            }
          break;

   }

    return DefWindowProc(hWnd,Message,WParam,LParam);
}


 
//////////////////////////////////////////////////////////////////
void Wait(long s)
{ int i,j;
  while(s-->0)
  { for(i=0;i<1000;i++)
    for(j=0;j<1000;j++);
  }
}
void simu_key(int keyvalue)
{ DispatchUserMsg(WM_KEYDOWN,keyvalue,0);
  DispatchUserMsg(WM_KEYUP,keyvalue,0);
  SendMessage(hAngel,WM_TIMER,0,0);
  SendMessage(winmain,WM_TIMER,(WPARAM)hSecondTimer,0);
  ProcessMessages(); 
  Wait(Auto_DELAY);
}
void  simu_user(void)
{  int i;
	
   for(i=0;i<100;i++)
   { SendMessage(hAngel,WM_TIMER,0,0);
     SendMessage(winmain,WM_TIMER,(WPARAM)hSecondTimer,0);
     
     ProcessMessages(); 
     Wait(Auto_DELAY);
   }
	

   simu_key(VK_TAB);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   SetFocus(demo_btn1);
   simu_key(VK_RETURN);

   simu_key(VK_TAB);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);

   simu_key(VK_TAB);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);
   simu_key(VK_TAB);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);
   simu_key(VK_TAB);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);
   simu_key(VK_RIGHT);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);
   simu_key(VK_RIGHT);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);
   simu_key(VK_RIGHT);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);

   simu_key(VK_TAB);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);
   simu_key(VK_DOWN);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   CloseWindow(demo1);


   simu_key(VK_TAB);
   SetFocus(demo_btn2);
   simu_key(VK_RETURN);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   CloseWindow(demo2);

   simu_key(VK_TAB);
    SetFocus(demo_btn3);
   simu_key(VK_RETURN);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
   simu_key(VK_TAB);
  simu_key(VK_TAB);
  CloseWindow(demo3);
}


void DemoMain(void)
{  HDC desktopDC;
   TMSG msg;
   TWNDCLASS wc;
   int i;
 
   memset(&wc,0,sizeof(wc));
   wc.clForeground=CL_BTNTEXT;
   wc.clBackground=(TCOLOR)0x0FF3366;
   wc.lpfnWndProc=MainProcess;
   wc.lpszClassName="mainwnd";
   RegisterClass(&wc);
   

   desktopDC=GetDesktopDC();
   
   SetColor(desktopDC,CL_RED);
  
   TextOut(desktopDC,50,100,"系统初始化....\n请稍候...");
   

   ReleaseDC(desktopDC);
   
   for(i=0;i<5;i++)bm[i]=CreateBitmap();

 
   winmain=CreateWindow("mainwnd","demo",WS_CAPTION|WS_TABSTOP|WS_ALWAYSONBOTTOM,0,0,LCD_WIDTH,LCD_HEIGHT,NULL,0,NULL);
   SetFocus(winmain);
   

#if Auto_Demo==true
     while(1)simu_user();
#endif

   while(GetMessage(&msg))
   { TranslateMessage(&msg);
     DispatchMessage(&msg);
   }

   for(i=0;i<3;i++)DestroyBitmap(bm[i]);
   
 }           




