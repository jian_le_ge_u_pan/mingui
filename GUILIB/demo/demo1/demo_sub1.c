#include "mingui.h"
/////////////////////////////////////////////////////////
#define  ID_BUTTON1    100
#define  ID_BUTTON2    101
#define  ID_BUTTON3    102
#define  ID_BUTTON4    103
#define  ID_BUTTON5    104
/////////////////////////////////////////////////////////
static HWND hMain,listbox1,listbox2,abutton,btn[4],sedt,pb,mymenu;
static HANDLE hProgressTimer=NULL;

static TBitmap *bm[3],*hDlgLogo;

extern const unsigned char DialogLogoImg[520];
static void OnMainCreate(HWND);
static int pos=0;
/////////////////////////////////////////////////////////
HRESULT CALLBACK DemoProcess1(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam)
{  switch(Message)
   { int i;
     case WM_CREATE: OnMainCreate(hWnd);
          break;
   case WM_DESTROY:
         {   DestroyBitmap(hDlgLogo);
             for(i=0;i<3;i++)DestroyBitmap(bm[i]);
             break;
         }
   case WM_TIMER:
         { if((HANDLE)WParam==hProgressTimer)
           { int iHour, iMinute,iSecond,iYear,iMonth,iDay;
		     char strTimeBuf[32];
             HDC dc=GetDC(hWnd);
             SetColor(dc,CL_BLACK);
             SetBkColor(dc,CL_BTNFACE);
	         SetFontStyle(dc,FS_OPAQUE,true);
             GetSysTime(&iHour, &iMinute,&iSecond);
             GetSysDate(&iYear, &iMonth,&iDay);
			 sprintf(strTimeBuf,"%02d:%02d:%02d %02d:%02d:%02d",iYear,iMonth,iDay,iHour,iMinute,iSecond);
			 SetWindowText(hWnd,strTimeBuf);
             ReleaseDC(dc);
			 ProgressBar_SetPos(pb,pos++);
			 if(pos>100)pos=0;
           }

         }
		 break;
   case WM_SHOWWINDOW:
	      EnableTimer(hProgressTimer,WParam);
	    return 0;

   case WM_CLOSE:
	      ShowWindow(hWnd,false);
        return 0;/*拦截窗口关闭消息*/

   case WM_COMMAND:
	      if(HIWORD(WParam)==BN_CLICKED)
		  {  switch(LOWORD(WParam))
             { case ID_BUTTON1:
			        
			         ListBox_ChangeItem(listbox1,ListBox_GetSelectedIndex(listbox1),GetWindowText(sedt));
					 break;

			 case ID_BUTTON2:
			         ListBox_InsertItem(listbox1,ListBox_GetSelectedIndex(listbox1),GetWindowText(sedt));
				    break;
              case ID_BUTTON3:
				     if(MessageBox("确定要删除吗？","提示",MB_YESNO|MB_ICONWARNING)==IDYES)
						 ListBox_DeleteItem(listbox1,ListBox_GetSelectedIndex(listbox1));
                   break;
              case ID_BUTTON4:
				     PostMessage(hWnd,WM_CLOSE,0,0);
                   break; 
            }
		  }
          break;

   }

    return DefWindowProc(hWnd,Message,WParam,LParam);
}

 

//////////////////////////////////////////////////////////////////
void OnMainCreate(HWND hWnd)
{   int bottom=crHeight(hWnd);
	listbox1=CreateWindow("ListBox","ListBox",WS_BORDER|WS_TABSTOP|WS_VSCROLL,5,5,100,100,hWnd,0,NULL);
    ListBox_AddItem(listbox1,"主接线图");
    ListBox_AddItem(listbox1,"电度量数据");
    ListBox_AddItem(listbox1,"开关量状态");
    ListBox_AddItem(listbox1,"实时波形");
    ListBox_AddItem(listbox1,"主接线图");
    ListBox_AddItem(listbox1,"电度量数据");
    ListBox_AddItem(listbox1,"开关量状态");
    ListBox_AddItem(listbox1,"实时波形");  

    listbox2=CreateWindow("ListBox","选框",WS_BORDER|WS_VSCROLL|WS_TABSTOP,110,5,78,25,hWnd,0,NULL);
    ListBox_AddItem(listbox2,"01.主接线图");
    ListBox_AddItem(listbox2,"02.电度量数据");
    ListBox_AddItem(listbox2,"03.开关量状态");
    ListBox_AddItem(listbox2,"04.实时波形");
    ListBox_AddItem(listbox2,"05.主接线图");
    ListBox_AddItem(listbox2,"06.电度量数据");
    ListBox_AddItem(listbox2,"07.开关量状态");
    ListBox_AddItem(listbox2,"08.实时波形");
   
    CreateWindow("SpinEdit","spin",WS_BORDER|WS_VSCROLL|WS_TABSTOP,192,5,78,25,hWnd,0,"1.2  20 1.100  1.55 ");

    CreateWindow("digiedit",NULL,WS_BORDER|WS_TABSTOP|WS_VSCROLL, 110,35,78,25,hWnd,0,"2 2  12.34");

    sedt=CreateWindow("Edit","单行编器",WS_BORDER|WS_TABSTOP,192,35,78,25,hWnd,0,NULL);


	CreateWindow("CheckBox","复选框1",WS_TABSTOP, 110,60,80,20,hWnd,0,(void *)1);
	CreateWindow("CheckBox","复选框2",WS_TABSTOP, 192,60,80,20,hWnd,0,NULL);	
    
	CreateWindow("RadioBox","单选框1",WS_TABSTOP, 110,85,80,20,hWnd,0,(void *)1);
	CreateWindow("RadioBox","单选框2",WS_TABSTOP, 192,85,80,20,hWnd,0,NULL);
              
	btn[0]=CreateWindow("button","修改",WS_BORDER|WS_TABSTOP, 5,bottom-42,60,22,hWnd,ID_BUTTON1,NULL);
	btn[1]=CreateWindow("button","添加",WS_BORDER|WS_TABSTOP, 72,bottom-42,60,22,hWnd,ID_BUTTON2,NULL);
    btn[2]=CreateWindow("button","删除",WS_BORDER|WS_TABSTOP, 139,bottom-42,60,22,hWnd,ID_BUTTON3,NULL);
	btn[3]=CreateWindow("button","退出",WS_BORDER|WS_TABSTOP, 206,bottom-42,60,22,hWnd,ID_BUTTON4,NULL);

 
    pb=CreateWindow("ProgressBar",NULL,WS_BORDER,0,bottom-18,crWidth(hWnd),18,hWnd,0,NULL);
         
    hProgressTimer=CreateTimer(hWnd,1000,1000,NULL,false);

    hDlgLogo=CreateBitmap();
    LoadBitmapFromMem(hDlgLogo,DialogLogoImg);
    SetWindowLogo(hWnd,hDlgLogo);
}

void RegisterDemo1(void)
{ TWNDCLASS wc;
  memset(&wc,0,sizeof(wc));
  wc.clForeground=CL_BTNTEXT;
  wc.clBackground=CL_BTNFACE;      
  wc.lpfnWndProc=DemoProcess1;
  wc.lpszClassName="Demo1";
  RegisterClass(&wc);
}
/*
void DemoMain(void)
{  TMSG msg;
   
   
   extern BOOL  keypad_shifton; 
   
   RegisterDemo1();
 


   hMain=CreateWindow("demo1","demo1",WS_CAPTION,0,0,LCD_WIDTH,LCD_HEIGHT,NULL,0,NULL);

  

   hShowTimer=CreateTimer(hMain,100,3000,NULL,false);
   hSecondTimer=CreateTimer(hMain,1000,1000,NULL,true);
   while(GetMessage(&msg))
   { TranslateMessage(&msg);
     DispatchMessage(&msg);
   }

}           
*/
