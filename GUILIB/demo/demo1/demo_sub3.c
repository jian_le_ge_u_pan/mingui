#include "mingui.h"
//////////////////////////////////////////////////////////////////
static HANDLE hTimer=NULL;
static HWND hTimeLabel,winmain,hDlg,abutton,spinedit1,SpinEdit1,DigitalEdit1,ime1,edtor,mymenu;

#define  ID_BUTTON0      100
#define  ID_BUTTON_MAIN  200
#define  ID_BUTTON_CLOSE  201


///////////////////////////////////////////////////
extern unsigned char desktop_array[28355];
extern unsigned char DialogLogoImg[520];
extern unsigned char ppButtonFace1[1049];
extern unsigned char ppButtonFace2[1024];
extern unsigned char DialogLogoImg[520];
extern unsigned char Logo_array1[1745];
extern unsigned char Logo_array2[1814];
extern unsigned char Logo_array3[1732];
extern unsigned char Logo_array4[1705];
extern unsigned char Logo_array5[1712];
///////////////////////////////////////////////////


static HWND btn[8];
static TBitmap *buttonlogo[8],*ppButtonFaceImage1,*ppButtonFaceImage2,*hDlgLogo;
TBitmap *desktop_bitmap;

void MemuProcess(HWND hWnd,int menuItemindex);
 

HRESULT GlobalKeyBoardHook(HWND hWnd,UINT Message,WPARAM wParam,LPARAM lParam,BOOL* bIntercept)
{ if(Message==WM_KEYUP)
  {  if(wParam==VK_F10)
     { if(!IsWnd(hDlg))
       { ShowWindow(mymenu,true);
         SetFocus(mymenu);
       }
     }
     else if(wParam==VK_ESCAPE)
     { if(IsWnd(hDlg))DestroyWindow(hDlg);
	 }
  }
  return -1;
}


HRESULT CALLBACK DemoProcess3(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam)
{  switch(Message)
   { int i;
   
     case WM_ERASEBKGND:  /*重绘背景*/
		 if(WParam)
		 { HDC dc=(HDC)WParam;
		   DrawBitmap(dc,0,0,desktop_bitmap);
		 }
		 return 0;

    case WM_LBUTTONDOWN:
		   ShowWindow(mymenu,false);
		 break;

    case WM_CREATE:
          {  winmain=hWnd;
			 hTimer=CreateTimer(hWnd,1000,1000,NULL,false);
			  
			 desktop_bitmap=CreateBitmap();
			 LoadBitmapFromMem(desktop_bitmap,desktop_array);
			 
			 hDlgLogo=CreateBitmap();
             LoadBitmapFromMem(hDlgLogo,DialogLogoImg);

             ppButtonFaceImage1=CreateBitmap();
             LoadBitmapFromMem(ppButtonFaceImage1,ppButtonFace1);
             ppButtonFaceImage2=CreateBitmap();
             LoadBitmapFromMem(ppButtonFaceImage2,ppButtonFace2);
             abutton=CreateWindow("button",NULL,WS_TABSTOP,0,LCD_HEIGHT-23,66,22,hWnd,ID_BUTTON_MAIN,NULL);
             SetWindowLogo(abutton,ppButtonFaceImage1);


             hTimeLabel=CreateWindow("static",NULL,WS_TRANSPARENT,256,224,65,12,hWnd,0,NULL);
             SetWindowTextColor(hTimeLabel,CL_WHITE);

            
             mymenu=CreateWindow("menu","主菜单",WS_BORDER|WS_HIDE|WS_ALWAYSONTOP,2,110,100,105,NULL,0,(void *)MemuProcess);
             Menu_AddItem(mymenu,"主接线图");
             Menu_AddItem(mymenu,"电度量数据");
             Menu_AddItem(mymenu,"开关量状态量设置");
             Menu_AddItem(mymenu,"实时波形");
             Menu_AddItem(mymenu,"exit");

              for(i=0;i<8;i++)
              { int btn_transparent=(i<=4)?0:WS_TRANSPARENT;
				 buttonlogo[i]=CreateBitmap();
               //buttonlogo[i]->Transparent=true;
                btn[i]=CreateWindow("button",NULL,WS_BORDER|WS_TABSTOP|BS_MENU|btn_transparent,2+(i%4)*79,55+(i/4)*61,78,60,hWnd,ID_BUTTON0+i,NULL);
              }
              LoadBitmapFromMem(buttonlogo[0],Logo_array1);
              LoadBitmapFromMem(buttonlogo[1],Logo_array2);
              LoadBitmapFromMem(buttonlogo[2],Logo_array3);
              LoadBitmapFromMem(buttonlogo[3],Logo_array4);
              LoadBitmapFromMem(buttonlogo[4],Logo_array5);


              SetWindowLogo(btn[0],buttonlogo[0]);
              SetWindowLogo(btn[1],buttonlogo[1]);
              SetWindowLogo(btn[2],buttonlogo[2]);
              SetWindowLogo(btn[3],buttonlogo[3]);
              SetWindowLogo(btn[4],buttonlogo[4]);
			  SetWindowText(btn[7]," 退出\n本演示");
			  //SetWindowTextColor(btn[7],CL_BLUE);
			 

          }
          break;
   case WM_TIMER:
	     { int iHour, iMinute,iSecond;
            char timershowbuf[12];
            GetSysTime(&iHour, &iMinute,&iSecond);
            sprintf(timershowbuf,"%02d:%02d:%02d",iHour,iMinute,iSecond);
            SetWindowText(hTimeLabel,timershowbuf);
		 }
         break;

   case WM_CLOSE:
	      ShowWindow(hWnd,false);
        return 0;/*拦截窗口关闭消息*/

   case WM_SHOWWINDOW:
	      EnableTimer(hTimer,WParam);
	    return 0;

    case WM_COMMAND:
         { 
		   if(LOWORD(WParam)==ID_BUTTON_MAIN)
		   { switch(HIWORD(WParam))
		     { case BN_PUSHED:
		              SetWindowLogo(abutton,ppButtonFaceImage2);
			        break;
		       case	BN_UNPUSHED:
			          SetWindowLogo(abutton,ppButtonFaceImage1);
					break;
               case BN_CLICKED:
			          if(!IsWnd(hDlg))
					  { ShowWindow(mymenu,true);
                        SetFocus(mymenu);
					  }
		     }
		   }
		   else if(HIWORD(WParam)==BN_CLICKED)
		   { int buttonIndex=LOWORD(WParam)-ID_BUTTON0;
             if(buttonIndex>=0 && buttonIndex<7)
			 { char strDispBuf[32];
               sprintf(strDispBuf,"Select button index of %d",buttonIndex+1);
               MessageBox(strDispBuf,"你好",MB_YESNOCANCEL|MB_ICONWARNING);
			 }
			 else if(buttonIndex==7)
			 {  CloseWindow(hWnd);  
			 }
           }
         }
         break;      
    case WM_DESTROY:
        
            DestroyBitmap(ppButtonFaceImage1);
            DestroyBitmap(ppButtonFaceImage2);
            DestroyBitmap(hDlgLogo);
			DestroyBitmap(desktop_bitmap);
			DestroyTimer(hTimer);
            for(i=0;i<8;i++) DestroyBitmap(buttonlogo[i]);

         break;

   }
 
    return DefWindowProc(hWnd,Message,WParam,LParam);
}



static HRESULT  DialogHook1(HWND hWnd,UINT Message,WPARAM wParam,LPARAM lParam,BOOL* bIntercept)
{ if(Message==WM_COMMAND && HIWORD(wParam)==CM_CLICKED && LOWORD(wParam)==ID_BUTTON_CLOSE)
  { CloseWindow(hWnd);
  }
  return 0;
}

void MemuProcess(HWND hWnd,int menuItemindex)
{ if(menuItemindex==4)
  { CloseWindow(winmain); 
  }
  else
  {  hDlg=CreateWindow("window","拼音输入法测试",WS_SYSMENU|WS_BORDER|WS_CAPTION|WS_ALWAYSONTOP,20,20,280,150,NULL,0,NULL);

     CreateWindow("static","输入框：",WS_NULL,5,15,70,16,hDlg,0,NULL);
     edtor=CreateWindow("Edit","你好，请用电脑键盘编辑此文字！",WS_BORDER|WS_TABSTOP,70,10,180,25,hDlg,0,NULL);
     CreateWindow("static","请使用键盘输入...",WS_NULL,50,60,180,20,hDlg,0,NULL);
     CreateWindow("button","关闭",WS_BORDER|WS_TABSTOP,200,35,60,25,hDlg,ID_BUTTON_CLOSE,NULL);
     IME_Open(edtor,hDlg,1,86,274,38);
     SetWindowLogo(hDlg,hDlgLogo);
     SetFocus(edtor);
	 InstallHook(hDlg,WM_COMMAND,DialogHook1);
  }
  ShowWindow(mymenu,false);

}

void RegisterDemo3(void)
{ TWNDCLASS wc;
  memset(&wc,0,sizeof(wc));
  wc.clForeground=CL_BTNTEXT;
  wc.clBackground=CL_BTNFACE;
  wc.lpfnWndProc=DemoProcess3;
  wc.lpszClassName="demo3";
  RegisterClass(&wc);
}
/*
void DemoMain(void)
{ 
   TMSG msg;
   
    RegisterDemo3();

    InstallGlobalKeyboardHook(GlobalKeyBoardHook);

    winmain=CreateWindow("demo3","demo3",0,0,0,LCD_WIDTH,LCD_HEIGHT,NULL,0,NULL);

   

   
   while(GetMessage(&msg))
   { TranslateMessage(&msg);
     DispatchMessage(&msg);
   }

 
}           


*/


