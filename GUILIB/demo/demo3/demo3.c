#include "mingui.h"
/////////////////////////////////////////////////////////
#define  ID_BUTTON1    100
#define  ID_BUTTON2    101
#define  ID_BUTTON3    102
#define  ID_BUTTON4    103
#define  ID_BUTTON5    104
/////////////////////////////////////////////////////////
HWND ImgNav,winmain,winmain2,abutton,btn1,btn2,btn3;
HANDLE hShowTimer=NULL,hSecondTimer=NULL;
int  showcounter=0;
TBitmap *bm[5],*hDlgLogo;
BOOL ShowStarted=false;
PWND wnd1,wnd2;
TREGION *g_pRgn=NULL;

////////////////////////////////////////////////////////
int step_1=15,step_2=3;
TRECT limit1={0,0,0,0};
TRECT limit2={0,0,0,0};
void GenNextPos(TPOINT *pos,TPOINT *dest_pos,int min_step)
{ int sign_x,sign_y,offset_x,offset_y;
  offset_x=(dest_pos->x-pos->x);
  offset_y=(dest_pos->y-pos->y);
  if(offset_x>=0)sign_x=1;else{sign_x=-1;offset_x=-offset_x;}
  if(offset_y>=0)sign_y=1;else{sign_y=-1;offset_y=-offset_y;}
  if(offset_x>=offset_y)
  { if(offset_x < min_step) pos->x = dest_pos->x;
    else pos->x += sign_x*min_step;
    if(offset_y <= min_step) pos->y = dest_pos->y;
    else pos->y += sign_y * offset_y * min_step / offset_x;
  }
  else
  { if(offset_y < min_step) pos->y = dest_pos->y;
    else pos->y += min_step * sign_y;
    if(offset_x <= min_step) pos->x = dest_pos->x;
    else pos->x += sign_x * offset_x * min_step / offset_y;
  }
}
/////////////////////////////////////////////////////////
void StepWindow(HWND hWnd,TRECT *pos,int step)
{ TPOINT *curpos=(TPOINT *)pos;
  TPOINT *despos=(TPOINT *)(&pos->right);
  if(curpos->x==despos->x && curpos->y==despos->y)
  { extern int GenRandom(int);
	despos->x=GenRandom(crWidth(WNDPTR(hWnd)->Parent)-absWidth(hWnd));
    despos->y=GenRandom(crHeight(WNDPTR(hWnd)->Parent)-absHeight(hWnd));
  }
  GenNextPos(curpos,despos,step);
  SetWindowPos(hWnd,curpos->x,curpos->y);
}

/////////////////////////////////////////////////////////
HRESULT CALLBACK MainProcess(HWND hWnd,UINT Message,WPARAM WParam,LPARAM LParam)
{  switch(Message)
   { int i;
     case WM_CREATE:
          {   extern unsigned char imgshow1[10962];
		      extern unsigned char imgshow2[10671];
              extern unsigned char imgshow3[16058];
			  extern unsigned char imgshow4[7552];
			  extern unsigned char imgshow5[18508];
              extern const unsigned char DialogLogoImg[];

              btn1=CreateWindow("button","开始",WS_BORDER|WS_TABSTOP|WS_TRANSPARENT, 50,120,75,25,hWnd,ID_BUTTON1,NULL);
              btn2=CreateWindow("button","关于...",WS_BORDER|WS_TABSTOP, 160,120,75,25,hWnd,ID_BUTTON2,NULL);
              g_pRgn=&WNDPTR(btn2)->Family->Vram->ExposeRegion;
			  SetWindowTextColor(btn1,CL_GREEN);
              ImgNav=CreateWindow("static","图片秀",WS_BORDER|WS_TABSTOP,25,15,LCD_WIDTH-100,LCD_HEIGHT-150,hWnd,0,NULL);
              for(i=0;i<5;i++)bm[i]=CreateBitmap();
              LoadBitmapFromMem(bm[0],imgshow1);
              LoadBitmapFromMem(bm[1],imgshow2);
              LoadBitmapFromMem(bm[2],imgshow3);
			  LoadBitmapFromMem(bm[3],imgshow4);
			  LoadBitmapFromMem(bm[4],imgshow5);

              hDlgLogo=CreateBitmap();
              LoadBitmapFromMem(hDlgLogo,DialogLogoImg);

              SetWindowLogo(hWnd,hDlgLogo);
			  SetForegroundWindow(btn1);
              // LoadBitmapFromFile(bm[0],"E:\\Source\\BCB\\TestGUI\\Demo\\t1.jpg");
              //LoadBitmapFromFile(bm[1],"E:\\Source\\BCB\\TestGUI\\Demo\\t2.jpg");
              //LoadBitmapFromFile(bm[2],"E:\\Source\\BCB\\TestGUI\\Demo\\t3.jpg");
          }
          break;
 case WM_ERASEBKGND:
		 if(WParam && LParam)
		 { HDC dc=(HDC)WParam;
		   TRECT *prc=(TRECT *)LParam;
		   //ClearRect(dc,prc->left,prc->top,prc->right-prc->left,prc->bottom-prc->top);
		   DrawBitmap(dc,0,0,bm[0]);
		 }
         return 0;
		   
   case WM_DESTROY:
         {   DestroyBitmap(hDlgLogo);
             for(i=0;i<5;i++)DestroyBitmap(bm[i]);
             break;
         }
   case WM_TIMER:
         { if((HANDLE)WParam==hSecondTimer)
           { StepWindow(btn1,&limit1,step_1);
		     StepWindow(winmain,&limit2,step_2);
		      
           }
           else if((HANDLE)WParam==hShowTimer)
           {  SetWindowLogo(ImgNav,bm[showcounter++]);
              if(showcounter>=5)showcounter=0;
           }
         }
   case WM_COMMAND:
	       if(HIWORD(WParam)==BN_CLICKED)
             switch(LOWORD(WParam))
             { case ID_BUTTON1:
                       if(ShowStarted)
                       { EnableTimer(hShowTimer,false);
                         SetWindowText((HWND)LParam,"开始");
                         SetWindowText(ImgNav,"图片秀");
                         SetWindowLogo(ImgNav,NULL);
                       }
                       else
                       { EnableTimer(hShowTimer,true);
                         SetWindowText((HWND)LParam,"暂停");
                         SetWindowText(ImgNav,"");
                       }
                       ShowStarted=!ShowStarted;
                    break;
              case ID_BUTTON2:
                       if(WndGetAttr(winmain2,WS_HIDE))
                       { ShowWindow(winmain2, true);
					     ShowWindow(btn1, true);
                          SetFocus(winmain2);
                       }
                       else
                       { ShowWindow(winmain2, false);
					     ShowWindow(btn1, false);
                       } 
                   break;
            }
          break;

   }

    return DefWindowProc(hWnd,Message,WParam,LParam);
}


//////////////////////////////////////////////////////////////////
HRESULT Wnd2Hook(HWND hWnd,UINT Message,WPARAM wParam,LPARAM lParam,BOOL* bIntercept)
{ if(Message==WM_COMMAND && HIWORD(wParam)==BN_CLICKED && LOWORD(wParam)==ID_BUTTON5)
  { MessageBox("您好！\nGUI测试程序演示中...","caption",MB_YESNOCANCEL|MB_ICONQUESTION|MB_DEFBUTTON2);
   *bIntercept=false;   /*消息可以继续向下传递*/
    return -1;
  }
  else if(Message==WM_CLOSE)
  {
   *bIntercept=true;   /*拦截消息，不往下传递*/
    ShowWindow(hWnd,false);
   
 
    return -1;
  }
  else
  {*bIntercept=false;
    return -1;
  }
}

//////////////////////////////////////////////////////////////////
//int OffsetRect(TRECT *tagRect,int nx,int ny
 

void DemoMain(void)
{  TMSG msg;
   TWNDCLASS wc;
   memset(&wc,0,sizeof(wc));
   wc.clForeground=CL_BTNTEXT;
   wc.clBackground=CL_BTNFACE;
   wc.lpfnWndProc=MainProcess;
   wc.lpszClassName="mainwnd";
   RegisterClass(&wc);

  CreateWindow("window","About2",WS_CAPTION|WS_SYSMENU|WS_TABSTOP,-15,60,150,101,NULL,0,NULL);
 
winmain2=CreateWindow("window","About1",WS_CAPTION|WS_SYSMENU|WS_TABSTOP,15,05,150,101,NULL,0,NULL);

   abutton=CreateWindow("button","Hello!",WS_BORDER|WS_TABSTOP,45,30,55,25,winmain2,ID_BUTTON5,NULL);


   InstallHook(winmain2,0,Wnd2Hook);
     //SetWindowTextColor(abutton,CL_RED);
   //SetWindowBackColor(abutton,CL_BLUE);

   winmain=CreateWindow("mainwnd","winmain",WS_CAPTION|WS_TABSTOP,25,25,LCD_WIDTH-50,LCD_HEIGHT-50,NULL,0,NULL);

   wnd2=(PWND)winmain2;
   wnd1=(PWND)winmain;
   hShowTimer=CreateTimer(winmain,1000,1000,NULL,false);
   hSecondTimer=CreateTimer(winmain,1000,1000,NULL,true);


   while(GetMessage(&msg))
   { TranslateMessage(&msg);
     DispatchMessage(&msg);
   }

}           





