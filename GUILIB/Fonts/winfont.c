/******************************************************************************
*
*  Copyright (C)  Chen Ming, All right Reserved
*
*  Current Maintainer: Chen Ming.
*
*  This library is free software; you can redistribute it and/or
*  modify it under the terms of the GNU Library General Public
*  License as published by the Free Software Foundation; either
*  version 2 of the License, or (at your option) any later version.
*
*  This library is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  Library General Public License for more details.
*
*  FILE NAME:			    winfont.c
*  PROGRAMMER:			    ming.c
*  Date of Creation:		2006/08/8
*
*  DESCRIPTION: 								
*	  GUI Lib public Interface					
*  NOTE:						 		
*
*  FUNCTIONS LIST:
* -----------------------------------------------------------------------------
*
* -----------------------------------------------------------------------------
*
*  MODIFICATION HISTORY
*     LastModify  2009/06/04
******************************************************************************/
//---------------------------------------------------------------------------
#include "MinGUI.h"
/*------------------------------------------------------------------------------
  将字体信息注册到GUI
-------------------------------------------------------------------------------*/
void GUI_RegisterFonts(void)
{  extern const unsigned char bitmap_hzk12[];
   extern const unsigned char bitmap_hzk16[];
 
   RegisterFont("hzk12",bitmap_hzk12); /*注册12点阵字体*/

   RegisterFont("hzk16",bitmap_hzk16); /*注册16点阵字体*/
}

 

